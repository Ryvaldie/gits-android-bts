package id.otsuka.gits.sweatgen.mvvm.main.challenge


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentChallangeBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail.ChallengeDetailActivity
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllActivity
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllActivity.Companion.AVAILABLE_CHALLENGE
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllActivity.Companion.PREVIOUS_CHALLENGE
import id.otsuka.gits.sweatgen.util.Other
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.showSnackBar
import kotlinx.android.synthetic.main.fragment_challange.*

class ChallengeFragment : Fragment(), ChallengeUserActionListener {

    lateinit var mViewDataBinding: FragmentChallangeBinding
    lateinit var mViewModel: ChallengeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = FragmentChallangeBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            isRequesting.set(true)
            loadData()
        }

        mViewDataBinding.mListener = this@ChallengeFragment

        setupAdapter()

        return mViewDataBinding.root

    }

    override fun onItemSelected(id: Int) {
        ChallengeDetailActivity.startActivity(id, requireContext())
    }

    override fun onSeeAllClicked(challengeType: Int) {
        if (challengeType == AVAILABLE_CHALLENGE)
            ChallengeSeeAllActivity.startActivity(context!!, AVAILABLE_CHALLENGE)
        else
            ChallengeSeeAllActivity.startActivity(context!!, PREVIOUS_CHALLENGE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.eventMessage.observe(this, Observer {
            when (it) {
                TOKEN_EXPIRE -> {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                }
                Other.NO_INET_CONNECTION -> {
                    (activity as MainActivity).showNoInetConnection()
                }
                else -> {
                    (it ?: "-").showSnackBar(mViewDataBinding.root)
                }
            }
        })

        mViewModel.eventLoading.observe(this, Observer {
            swipe_refresh_challenge.isRefreshing = false
        })

        swipe_refresh_challenge.setOnRefreshListener {
            mViewModel.apply {
                loadData()
            }
        }
    }


    private fun setupAdapter() {
        mViewDataBinding.recyclerChallenge.apply {
            adapter = ChallengeAdapter(requireContext(), ArrayList(), mViewModel, this@ChallengeFragment)
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            (activity as MainActivity).mViewDataBinding?.mViewModel?.loadLayout?.set(false)
        }
    }

    fun obtainViewModel(): ChallengeViewModel = obtainViewModel(ChallengeViewModel::class.java)

    companion object {
        fun newInstance() = ChallengeFragment().apply {

        }

    }

}
