package id.otsuka.gits.sweatgen.mvvm.main.challenge;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.source.GetChallengeDashboardDao
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeType.Companion.HEADER_TYPE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class ChallengeViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val eventMessage = SingleLiveEvent<String?>()
    val eventLoading = SingleLiveEvent<Void>()
    val eventChallengesLoaded = SingleLiveEvent<ArrayList<Challenge>>()
    val highlightChallenges = ObservableArrayList<Challenge>()
    val listChallenges = ObservableArrayList<Challenge>()
    val isRequesting = ObservableBoolean(false)
    val challengeTitle = arrayOf(
            context.getString(R.string.text_mychallenge),
            context.getString(R.string.text_join_challenge),
            context.getString(R.string.text_previous_challenge))

    fun start() {
        val blendedArray = ArrayList<Challenge>()
        blendedArray.apply {
            clear()
            addAll(highlightChallenges)
            addAll(listChallenges)
        }
        eventChallengesLoaded.value = blendedArray
    }

    fun loadData() {

        repository.getChallengeDashboard(repository.getUser()?.id?:0, "", object : GitsDataSource.GetChallengeDashboardCallback {
            override fun onError(errorMessage: String?) {
                isRequesting.set(false)
                eventLoading.call()
                eventMessage.value = errorMessage
            }

            override fun onLoaded(challengeDashboard: GetChallengeDashboardDao) {

                highlightChallenges.apply {
                    clear()
                    addAll(challengeDashboard.highlight ?: listOf())
                }

                listChallenges.apply {
                    clear()
                    if (challengeDashboard.myChallenges != null) {
                        if (challengeDashboard.myChallenges.isNotEmpty()) {
                            challengeDashboard.myChallenges[0].setItemType(HEADER_TYPE)
                            challengeDashboard.myChallenges[0].setTitle(challengeTitle[0])
                            addAll(challengeDashboard.myChallenges)
                        }
                    }

                    if (challengeDashboard.allChallenges != null) {
                        if (challengeDashboard.allChallenges.isNotEmpty()) {
                            challengeDashboard.allChallenges[0].setItemType(HEADER_TYPE)
                            challengeDashboard.allChallenges[0].setTitle(challengeTitle[1])
                            addAll(challengeDashboard.allChallenges)
                        }
                    }

                    if (challengeDashboard.prevChallenges != null) {
                        if (challengeDashboard.prevChallenges.isNotEmpty()) {
                            challengeDashboard.prevChallenges[0].setItemType(HEADER_TYPE)
                            challengeDashboard.prevChallenges[0].setTitle(challengeTitle[2])
                            addAll(challengeDashboard.prevChallenges)
                        }
                    }
                }
                isRequesting.set(false)
                eventLoading.call()
                start()
            }
        })
    }


}