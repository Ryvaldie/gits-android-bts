package id.otsuka.gits.sweatgen.mvvm.main.runtracker

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Looper
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseService
import id.otsuka.gits.sweatgen.util.Other.INTENT_FILTER_GPS
import id.otsuka.gits.sweatgen.util.Other.INTENT_LOCATION_CHANGED
import id.otsuka.gits.sweatgen.util.Other.INTENT_PERMISSION
import id.otsuka.gits.sweatgen.util.Other.INTENT_START_TIMER
import id.otsuka.gits.sweatgen.util.Other.INTENT_TIMER_TICK
import id.otsuka.gits.sweatgen.util.Other.KEY_UPDATE_LOCATION
import id.otsuka.gits.sweatgen.util.Other.NOTIF_CHANNEL
import id.otsuka.gits.sweatgen.util.Other.NOTIF_FOREGROUND_SERVICE_ID
import id.otsuka.gits.sweatgen.util.Other.NOTIF_RUN_TRACKER
import id.otsuka.gits.sweatgen.util.Other.PI_REQUEST_CODE
import java.util.concurrent.TimeUnit


class LocationUpdateService : BaseService(),
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    val TAG = LocationUpdateService::class.java.simpleName
    private lateinit var context: Context
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mCountDownTimer: CountDownTimer? = null
    val numOfMinutes: Long = 48 * 60 * 60 * 1000
    var currentTimeMills: Long = numOfMinutes
    private var runType = 0
    private var challengeString = ""

    val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                INTENT_START_TIMER -> {

                    if (!intent.getBooleanExtra("state", false))
                        if (mCountDownTimer != null) {
                            mCountDownTimer!!.cancel()
                        } else {

                        }
                    else {
                        startCountDownTimer(currentTimeMills)
                    }


                }
            }
        }
    }


    override fun onConnected(p0: Bundle?) {
        updateLocation()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.d(TAG, p0.errorMessage)
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d(TAG, p0.toString())
    }

    override fun onLocationChanged(p0: Location?) {

        val intent = Intent(INTENT_LOCATION_CHANGED)

        intent.putExtra(KEY_UPDATE_LOCATION, p0)

        sendIntentToActivity(intent)
    }

    override fun onStart(intent: Intent?, startId: Int) {
        buildGoogleApiClient()

        runType = intent?.getIntExtra(RunTrackerActivity.RUN_TYPE, 0) ?: 0
        challengeString = intent?.getStringExtra(RunTrackerActivity.CHALLENGE_JSON) ?: ""

        super.onStart(intent, startId)
    }

    override fun onCreate() {
        initContext()

        initLocationManager()

        startForeground(NOTIF_FOREGROUND_SERVICE_ID, getNotification())

        registerMessageReceiver()
    }


    private fun registerMessageReceiver() {
        val intentFilter = IntentFilter()

        intentFilter.addAction(INTENT_START_TIMER)

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, intentFilter)
    }


    private fun initContext() {
        context = applicationContext
    }

    private fun initLocationManager() {
        applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        return START_STICKY
    }

    fun getHighAccuracyRequest(): LocationRequest {
        val mLocationRequest = LocationRequest()

        mLocationRequest.fastestInterval = 0

        mLocationRequest.interval = 0

        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationRequest.smallestDisplacement = 10f

        return mLocationRequest
    }

    private fun updateLocation() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            // Create LocationSettingsRequest object using location request
            val locationSettingRequestBuilder = LocationSettingsRequest.Builder()

            locationSettingRequestBuilder.addLocationRequest(getHighAccuracyRequest())

            locationSettingRequestBuilder.setAlwaysShow(true)

            val locSettingRequest = locationSettingRequestBuilder.build()

            // Check whether location settings are satisfied
            // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
            val settingsClient = LocationServices.getSettingsClient(this)

            val resultPending = settingsClient.checkLocationSettings(locSettingRequest)

            resultPending.addOnCompleteListener {

                try {

                    val response = it.getResult(ApiException::class.java)

                } catch (e: ApiException) {

                    when (e.statusCode) {

                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {

                            try {

                                val resolvable = e as ResolvableApiException

                                val pendingIntent = resolvable.resolution

                                val intent = Intent(INTENT_FILTER_GPS)

                                intent.putExtra("resolution", pendingIntent)

                                sendIntentToActivity(intent)

                            } catch (e: IntentSender.SendIntentException) {

                            } catch (e: ClassCastException) {

                            }
                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            Log.d(TAG, "Settings Not Available")
                        }
                    }
                }
            }

            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(getHighAccuracyRequest(), object : LocationCallback() {
                override fun onLocationResult(locResult: LocationResult?) {
                    onLocationChanged(locResult?.lastLocation)
                }
            }, Looper.myLooper())
        } else {
            val intent = Intent(INTENT_PERMISSION)
            sendIntentToActivity(intent)
        }
    }

    private fun getNotification(): Notification {
        val intent = Intent(context, RunTrackerActivity::class.java)

        intent.putExtra(RunTrackerActivity.RUN_TYPE, runType)

        intent.putExtra(RunTrackerActivity.CHALLENGE_JSON, challengeString)

        val pendingIntent = PendingIntent.getActivity(context, PI_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(NOTIF_CHANNEL, NOTIF_RUN_TRACKER, NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
            val builder = Notification.Builder(applicationContext, NOTIF_CHANNEL)
            builder.setContentIntent(pendingIntent)
            builder.build()
        } else {
            val notification = NotificationCompat.Builder(context)
            notification.setContentTitle(NOTIF_CHANNEL)
                    .setContentText(NOTIF_RUN_TRACKER)
                    .setSmallIcon(R.drawable.ic_launcher_round)
                    .setContentIntent(pendingIntent)
                    .build()
        }
    }


    fun startCountDownTimer(numOfMinutes: Long, interval: Long = 1000) {
        mCountDownTimer = object : CountDownTimer(numOfMinutes, interval) {

            override fun onFinish() {
                Log.i(TAG, "FINISH")
            }

            override fun onTick(millisUntilFinished: Long) {

                val millis: Long = this@LocationUpdateService.numOfMinutes.minus(millisUntilFinished)

                val hms = String.format("%02d:%02d:%02d",
                        TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)))

                currentTimeMills = millisUntilFinished

                val intent = Intent(INTENT_TIMER_TICK)
                intent.putExtra("time", hms)
                intent.putExtra("millis", millis)
                sendIntentToActivity(intent)

            }
        }.start()
    }

    override fun onDestroy() {
        super.onDestroy()
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        stopForeground(true)
//        }
        mCountDownTimer?.cancel()
        currentTimeMills = 0
    }

    private fun sendIntentToActivity(intent: Intent) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    @Synchronized
    protected fun buildGoogleApiClient() {

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient.connect()
    }
}