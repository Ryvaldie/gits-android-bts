package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceresult


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import id.otsuka.gits.sweatgen.databinding.FragmentRaceResultBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceresult.RaceResultViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.withArgs
import kotlinx.android.synthetic.main.fragment_race_result.*


class RaceResultFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentRaceResultBinding
    lateinit var mViewModel: RaceResultViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRaceResultBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            bUrl.set(arguments?.getString(RESULT_URL, ""))
        }

        mViewDataBinding.mListener = object : RaceResultUserActionListener {

        }



        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webview_race_result.webViewClient = object : android.webkit.WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(mViewModel.bUrl.get())
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)

                if (progressBar1 != null)
                    progressBar1.visibility = View.GONE
            }
        }

        toolbar_race_result.setNavigationOnClickListener { activity!!.onBackPressed() }
    }

    fun obtainViewModel(): RaceResultViewModel = obtainViewModel(RaceResultViewModel::class.java)

    companion object {
        val RESULT_URL = "resultUrl"
        fun newInstance(url: String) = RaceResultFragment().withArgs {
            putString(RESULT_URL, url)
        }

    }

}
