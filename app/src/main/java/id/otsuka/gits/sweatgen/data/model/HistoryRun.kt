package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */
data class HistoryRun(val id: Int? = null,
                      val distance: Double? = null,
                      val duration: Long? = null,
                      val calories: Double? = null,
                      val step: Int? = null,
                      val long_lat: String? = null,
                      val date: Long? = null,
                      val timestmaps: String? = null
)