package id.otsuka.gits.sweatgen.mvvm.main.newsfeed

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.HeadlineSectionBinding
import id.otsuka.gits.sweatgen.databinding.ItemHeadlineNewsfeedBinding

class HeadlineAdapter(val context: Context,
                      var data: List<NewsItemModel>,
                      val viewModel: NewsFeedViewModel,
                      val headlineBinding: HeadlineSectionBinding,
                      val listener: NewsFeedUserActionListener

) : PagerAdapter() {

    fun replaceData(data: List<NewsItemModel>) {
        this.data = data
        notifyDataSetChanged()
        viewModel.createSliderIndicator.value = headlineBinding
    }

    override fun getCount(): Int = data.size

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val mViewBinding = ItemHeadlineNewsfeedBinding.inflate(LayoutInflater.from(context), container, false).apply {
            news = data[position]
            mListener = listener
        }

        container.addView(mViewBinding.root)

        return mViewBinding.root
    }
}