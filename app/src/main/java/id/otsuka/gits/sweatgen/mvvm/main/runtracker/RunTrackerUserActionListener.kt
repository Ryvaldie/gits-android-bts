package id.otsuka.gits.sweatgen.mvvm.main.runtracker;


interface RunTrackerUserActionListener {

    // Example
    // fun onClickItem()
    fun onStartPauseBtnClick(state:Boolean)

    fun changeTrackerMode(mode:Boolean)

    fun onFinish()
}