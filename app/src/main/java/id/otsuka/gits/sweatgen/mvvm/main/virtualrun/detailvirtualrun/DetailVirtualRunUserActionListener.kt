package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun;


interface DetailVirtualRunUserActionListener {

    fun onClickTest(text: String)

    fun navItemClick(id: Int)

    fun seeMore()

    fun call()

    fun mail()

    fun web()

    fun toStartRun()

    fun onDialogCancel()

    fun onDialogSubmit()

    fun triggerDialog()

    fun toTokoOtsuka()


}