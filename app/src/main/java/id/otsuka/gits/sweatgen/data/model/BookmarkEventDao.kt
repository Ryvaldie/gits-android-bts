package id.otsuka.gits.sweatgen.data.model

data class BookmarkEventDao(val id: Int = 0,
                            val user_id: Int = 0,
                            val event_id: Int = 0,
                            val isBookmark: Int? = 0
)