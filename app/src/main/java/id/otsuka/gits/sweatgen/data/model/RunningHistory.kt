package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class RunningHistory(val date: String? = null,
                          val distance: String? = null,
                          val duration: String? = null,
                          val calories : String?=null,
                          val step:String?=null
)