package id.otsuka.gits.sweatgen.mvvm.main.runtracker.challengeresult


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.FragmentChallengeResultBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity.Companion.CHALLENGE_JSON
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.withArgs

class ChallengeResultFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentChallengeResultBinding
    lateinit var mViewModel: ChallengeResultViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentChallengeResultBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

            bChallenge.set(Gson().fromJson<Challenge>(activity!!.intent.getStringExtra(CHALLENGE_JSON), Challenge::class.java))

            bCompleted.set(arguments!!.getBoolean(RESULT))

        }

        mViewDataBinding.mListener = object : ChallengeResultUserActionListener {
            override fun backToMenu() {
                activity?.finish()
            }
        }

        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as RunTrackerActivity).stopService()
    }

    fun obtainViewModel(): ChallengeResultViewModel = obtainViewModel(ChallengeResultViewModel::class.java)

    companion object {
        val RESULT = "result"

        fun newInstance(completed: Boolean) = ChallengeResultFragment().withArgs {
            putBoolean(RESULT, completed)
        }

    }

}
