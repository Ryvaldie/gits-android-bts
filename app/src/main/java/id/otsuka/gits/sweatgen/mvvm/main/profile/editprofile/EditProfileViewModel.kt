package id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.filePathToMultipart
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File


class EditProfileViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bUserPhoto = ObservableField<Any>()

    val bDateOfBirth = ObservableField("")
    val bFullName = ObservableField("")
    val bGender = ObservableField(0)
    val bStringGender = ObservableField("")

    val eventLoading = SingleLiveEvent<Boolean>()
    val bMessage = ObservableField("")
    val isSuccess = ObservableBoolean(false)

    fun validateFields(): Boolean = (bUserPhoto.get() != null)

    fun setGender() {
        if (bGender.get() == R.id.radio_male)
            bStringGender.set(context.getString(R.string.text_male))
        else
            bStringGender.set(context.getString(R.string.text_female))
    }

    fun editMyProfile() {

        val name = RequestBody.create(MediaType.parse("text/plain"), bFullName.get() ?: "-")
        val gender = RequestBody.create(MediaType.parse("text/plain"), bStringGender.get() ?: "-")
        val dob = RequestBody.create(MediaType.parse("text/plain"), bDateOfBirth.get() ?: "-")
        val image = (bUserPhoto.get() as File).filePathToMultipart()

        repository.updateUser(userId = (repository.getUser()
                ?: User()).id, name = name, gender = gender, dob = dob, picture = image,
                callback = object : GitsDataSource.EditProfileCallback {
                    override fun onTokenNull() {

                    }

                    override fun onUpdateSuccess(user: User, msg: String?) {
                        isSuccess.set(true)
                        repository.saveUser(user.copy(token = repository.getToken()))
                        eventLoading.value = false
                    }

                    override fun onUpdateFailed(errorMessage: String?) {
                        isSuccess.set(false)
                        bMessage.set(errorMessage)
                        eventLoading.value = false
                    }
                })


    }

}