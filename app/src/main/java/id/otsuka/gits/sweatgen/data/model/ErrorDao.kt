package id.otsuka.gits.sweatgen.data.model

data class ErrorDao(val error: String? = null)