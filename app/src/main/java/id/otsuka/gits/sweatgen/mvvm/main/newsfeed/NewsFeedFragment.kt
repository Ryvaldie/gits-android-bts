package id.otsuka.gits.sweatgen.mvvm.main.newsfeed


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentNewsFeedBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail.NewsDetailActivity
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.SeeAllNewsActivity
import id.otsuka.gits.sweatgen.util.Other.NO_INET_CONNECTION
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.setupViewPagerIndicator
import id.otsuka.gits.sweatgen.util.showSnackBar

class NewsFeedFragment : Fragment(), NewsFeedUserActionListener {

    lateinit var mViewDataBinding: FragmentNewsFeedBinding
    lateinit var mViewModel: NewsFeedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentNewsFeedBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            start()
        }

        mViewDataBinding.mListener = this@NewsFeedFragment


        setupRecycler()
        setupSwipeRefresh()

        return mViewDataBinding.root

    }

    override fun itemOnClick(newsId: Int) {
        NewsDetailActivity.startActivity(newsId, requireContext())
    }

    override fun seeAll() {
        SeeAllNewsActivity.startActivity(context = requireContext())
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            (activity as MainActivity).mViewDataBinding?.mViewModel?.loadLayout?.set(false)
        }
    }

    fun setupSwipeRefresh() {
        mViewDataBinding.swipeRefreshNews.apply {
            setOnRefreshListener {
                isRefreshing = true
                mViewModel.start()
            }
        }
    }

    fun setupRecycler() {
        mViewDataBinding.recyclerNews.apply {
            adapter = NewsAdapter(mViewModel, ArrayList(0), this@NewsFeedFragment, requireContext())
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.showMessage.observe(this, Observer {
            when (it) {
                TOKEN_EXPIRE -> {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                }
                NO_INET_CONNECTION -> {
                    (activity as MainActivity).showNoInetConnection()
                }
                else -> {
                    (it ?: "-").showSnackBar(mViewDataBinding.root)
                }
            }
        })
        mViewModel.createSliderIndicator.observe(this, Observer {
            it?.linDotsNews?.removeAllViews()
            it?.linDotsNews?.setupViewPagerIndicator(mViewModel.bHeadlineList.size)
        })

        mViewModel.stopRefresh.observe(this, Observer {
            mViewDataBinding.swipeRefreshNews.isRefreshing = false
        })
    }

    override fun onResume() {
        super.onResume()
    }

    fun obtainViewModel(): NewsFeedViewModel = obtainViewModel(NewsFeedViewModel::class.java)

    companion object {
        fun newInstance() = NewsFeedFragment().apply {

        }

    }

}
