package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail


class NewsDetailModel(
        var banner: String? = "-",
        var title: String = "-",
        var publishDate: String = "-",
        var category: String = "-",
        var publisher: String = "-",
        var content: String = "-",
        var bioState: Boolean = true,
        var bio: String = "-",
        var categoryId: String? = null
)