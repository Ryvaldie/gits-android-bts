package id.otsuka.gits.sweatgen.mvvm.main.runninghistory

import id.otsuka.gits.sweatgen.data.model.Extras


class RunningHistoryModel(
        val progressIndicator: String,
        val extras: Extras,
        val currentDistance: String,
        val intProgress: Int
)