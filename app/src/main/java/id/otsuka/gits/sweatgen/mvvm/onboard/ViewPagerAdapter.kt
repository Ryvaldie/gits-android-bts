package id.otsuka.gits.sweatgen.mvvm.onboard

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.OnBoardImage
import id.otsuka.gits.sweatgen.databinding.OnboardViewpagerItemBinding

class ViewPagerAdapter(val context: Context,
                       var pagesContent: List<OnBoardImage>
) : PagerAdapter() {

    fun replacePagesContent(pagesContent: List<OnBoardImage>) {
        this.pagesContent = pagesContent
        notifyDataSetChanged()
    }

    override fun getCount(): Int = pagesContent.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val viewDataBinding = OnboardViewpagerItemBinding.inflate(layoutInflater, container, false).apply {
            image = pagesContent[position]
            listener = object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    progbar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    progbar.visibility = View.GONE
                    return false
                }
            }
        }

        container.addView(viewDataBinding.root, 0)

        return viewDataBinding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}