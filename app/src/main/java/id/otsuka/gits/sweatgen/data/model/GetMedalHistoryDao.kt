package id.otsuka.gits.sweatgen.data.model

data class GetMedalHistoryDao(val content: MedalContent,
                              val paginate: Paginate? = null
)