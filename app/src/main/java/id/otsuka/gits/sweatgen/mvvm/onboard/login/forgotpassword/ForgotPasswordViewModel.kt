package id.otsuka.gits.sweatgen.mvvm.onboard.login.forgotpassword;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.ForgotPasswordDao
import id.otsuka.gits.sweatgen.data.model.ForgotPasswordParams
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class ForgotPasswordViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bEmail = ObservableField("")
    val bErrorMsg = ObservableField("")
    val bSuccess = ObservableBoolean(false)

    val eventLoading = SingleLiveEvent<Boolean>()

    fun validateFields(): Boolean = (!bEmail.get().isNullOrEmpty())

    fun forgotPassword(){

        eventLoading.value =true

        val forgotPasswordParams = ForgotPasswordParams(bEmail.get())

        repository.forgotPassword(forgotPasswordParams, object : GitsDataSource.ForgotPasswordCallback {
            override fun onForgotPasswordSuccess(forgotPasswordDao: ForgotPasswordDao, msg: String?) {
                bSuccess.set(true)
                repository.saveForgotPassword(forgotPasswordDao)
                eventLoading.value = false
            }

            override fun onForgotPasswordFailed(errorMessage: String?) {
                bSuccess.set(false)
                bErrorMsg.set(errorMessage)
                eventLoading.value = false
            }
        })

    }

}