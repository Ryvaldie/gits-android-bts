package id.otsuka.gits.sweatgen.mvvm.onboard.login.verificationpassword;


interface VerificationPasswordUserActionListener {

    // Example
    // fun onClickItem()
    fun onResendCode()

    fun setPassword()

    fun backToOnBoard()
}