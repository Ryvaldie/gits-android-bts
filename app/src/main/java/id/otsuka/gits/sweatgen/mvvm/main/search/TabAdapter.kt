package id.otsuka.gits.sweatgen.mvvm.main.search

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import id.otsuka.gits.sweatgen.mvvm.main.search.result.ResultFragment
import id.otsuka.gits.sweatgen.mvvm.main.search.resultevent.ResultEventFragment

class TabAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    val tabs = arrayOf(ResultEventFragment.newInstance(), ResultFragment.newInstance())
    val tabsTitle = arrayOf("Event", "Challenge")

    override fun getCount(): Int = tabs.size

    override fun getItem(position: Int): Fragment {
        return tabs[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return tabsTitle[position]
    }

}