package id.otsuka.gits.sweatgen.data.model

data class ShareLink(val link: String? = null)