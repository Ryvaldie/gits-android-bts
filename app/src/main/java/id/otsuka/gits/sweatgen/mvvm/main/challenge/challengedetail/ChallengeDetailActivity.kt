package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity


class ChallengeDetailActivity : AppCompatActivity(), ChallengeDetailNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenge_detail)
        setupFragment()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.fragments[0]
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }


    fun obtainViewModel(): ChallengeDetailViewModel = obtainViewModel(ChallengeDetailViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        ChallengeDetailFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        val ID_CHALLENGE = "idChallenge"
        fun startActivity(idChallenge: Int, context: Context) {
            context.startActivity(Intent(context, ChallengeDetailActivity::class.java).putExtra(ID_CHALLENGE, idChallenge))
        }
    }
}
