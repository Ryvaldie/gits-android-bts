package id.otsuka.gits.sweatgen.data.model

data class NewsFeed(val id: Int = 0,
                    val title: String? = null,
                    val banner: String? = null,
                    val category_id: String? = null,
                    val slug_title: String? = null,
                    val date_publish: String? = null,
                    val status: Int = 0,
                    val created_by: Publisher? = null,
                    val content: String? = null,
                    val category: String? = null,
                    val isShow: Int? = null,
                    val bio: String? = null

)