package id.otsuka.gits.sweatgen.util.helper

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.content.ContextCompat
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.result.BitmapPhoto
import io.fotoapparat.selector.*
import io.fotoapparat.view.CameraRenderer

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class CustomCameraHelper(val context: Context, val view: CameraRenderer, val activity: Activity,
                         val listener: CaptureCallback
) {

    private lateinit var mCustomCam: Fotoapparat
    val TAG = CustomCameraHelper::class.java.simpleName
    val cameraConfiguration = CameraConfiguration(
            pictureResolution = highestResolution(), // (optional) we want to have the highest possible photo resolution
            previewResolution = highestResolution(), // (optional) we want to have the highest possible preview resolution
            previewFpsRange = highestFps(),          // (optional) we want to have the best frame rate
            focusMode = firstAvailable(              // (optional) use the first focus mode which is supported by device
                    continuousFocusPicture(),
                    autoFocus(),                       // if continuous focus is not available on device, auto focus will be used
                    fixed()                            // if even auto focus is not available - fixed focus mode will be used
            ),
            flashMode = firstAvailable(              // (optional) similar to how it is done for focus mode, this time for flash
                    autoFlash(),
                    torch(),
                    off()
            ),
            antiBandingMode = firstAvailable(       // (optional) similar to how it is done for focus mode & flash, now for anti banding
                    auto(),
                    hz50(),
                    hz60(),
                    none()
            ),
            jpegQuality = manualJpegQuality(100),     // (optional) select a jpeg quality of 90 (out of 0-100) values
            sensorSensitivity = lowestSensorSensitivity(), // (optional) we want to have the lowest sensor sensitivity (ISO)
            frameProcessor = { frame -> }            // (optional) receives each frame from preview stream
    )

    companion object {
        const val REQUEST_STORAGE_AND_CAMERA_CODE = 123
        private val PERMISSIONS_EXTERNAL_STORAGE = arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
        private val PERMISSIONS_CAMERA = arrayOf(Manifest.permission.CAMERA)
        private val PERMISSIONS_CAMERA_AND_STORAGE = arrayOf(Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
    }

    init {
        initiateCustomCam()
        checkCameraAndStoragePermission(activity)
    }

    fun onStartCam() {
        mCustomCam.start()
    }

    fun switchCam(front: Boolean) {
        if (front)
            mCustomCam.switchTo(
                    lensPosition = io.fotoapparat.selector.back(),
                    cameraConfiguration = cameraConfiguration
            ) else
            mCustomCam.switchTo(
                    lensPosition = front(),
                    cameraConfiguration = cameraConfiguration
            )
    }

    fun onStopCam() {
        mCustomCam.stop()
    }

    fun onCapture() {
        listener.onImageRendering()

        val result = mCustomCam.takePicture()

        result.toBitmap()
                .whenAvailable { bitmapPhoto ->
                    listener.onImageAvailable(bitmapPhoto)
                }
    }

    interface CaptureCallback {
        fun onImageRendering()
        fun onImageAvailable(bitmapPhoto: BitmapPhoto?)
        fun onError(errorMessage: String?)
    }

    private fun initiateCustomCam() {
        mCustomCam = Fotoapparat(
                context = context,
                view = view,
                lensPosition = front(),
                cameraConfiguration = cameraConfiguration,
                cameraErrorCallback = { error -> listener.onError(error.message) }
        )
    }

    @SuppressLint("ObsoleteSdkInt")
    fun checkCameraAndStoragePermission(activity: Activity): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                activity.requestPermissions(PERMISSIONS_CAMERA_AND_STORAGE, REQUEST_STORAGE_AND_CAMERA_CODE)
                false
            }
        }
        return true
    }
}