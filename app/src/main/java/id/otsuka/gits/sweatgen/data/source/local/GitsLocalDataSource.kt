package id.otsuka.gits.sweatgen.data.source.local

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.VisibleForTesting
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import id.otsuka.gits.sweatgen.data.model.*
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.util.Preference.FORGOT_KEY
import id.otsuka.gits.sweatgen.util.Preference.KEY_LOGIN
import id.otsuka.gits.sweatgen.util.Preference.SAVED_RUN_LIST
import id.otsuka.gits.sweatgen.util.Preference.USER_KEY
import id.otsuka.gits.sweatgen.util.Preference.VIRTURUN_GUIDE
import id.otsuka.gits.sweatgen.util.createGsonBuilder
import okhttp3.MultipartBody
import okhttp3.RequestBody

class GitsLocalDataSource private constructor(private val context: Context, private val preferences: SharedPreferences) : GitsDataSource {


    override fun getVirtualRunList(userToken: String, userId: Int, callback: GitsDataSource.GetVirtualrunListCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getLeaderboard(userToken: String, challengeId: Int, callback: GitsDataSource.GetLeaderboardCallback) {

    }

    override fun getVirturunById(userToken: String, id: Int, userId: Int, callback: GitsDataSource.GetVirturunByIdCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getHistoryVirtualRun(userToken: String, userId: Int, callback: GitsDataSource.GetVirtualrunListCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getVirturunLeaderboard(userToken: String, virturunId: Int, callback: GitsDataSource.GetLeaderboardCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun postJoinVirturun(userToken: String, virturunId: Int, userId: Int, callback: GitsDataSource.PostJoinChallengeCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun redeemValid(userToken: String, virturunId: Int, userId: Int, accessCode: String, callback: GitsDataSource.GeneralPostCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getVirturunBanner(userToken: String, callback: GitsDataSource.GetVirturunBannerCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllContinuesRun(): ArrayList<SavedVirtualRun> {
        val currentDatas = ArrayList<SavedVirtualRun>()
        val jsonData = preferences.getString(SAVED_RUN_LIST, "")
        if (jsonData.isNotEmpty()) {
            val collectionType = object : TypeToken<ArrayList<SavedVirtualRun>>() {

            }.type
            currentDatas.addAll(context.createGsonBuilder().fromJson(jsonData, collectionType))
        }

        return currentDatas
    }

    override fun getContinuesRun(id: Int): SavedVirtualRun? {

        val currentDatas = getAllContinuesRun()

        currentDatas.mapIndexed { index, savedVirtualRun ->
            return if (savedVirtualRun.id == id)
                savedVirtualRun
            else
                null
        }

        return null
    }

    override fun getVirtualRunGuide(): List<RaceCentralGuide> {
        val currentDatas = ArrayList<RaceCentralGuide>()

        val jsonData = preferences.getString(VIRTURUN_GUIDE, "")

        if (jsonData.isNotEmpty()) {
            try {
                val collectionType = object : TypeToken<ArrayList<RaceCentralGuide>>() {

                }.type
                currentDatas.addAll(context.createGsonBuilder().fromJson(jsonData, collectionType))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return currentDatas
    }

    override fun saveVirtualRunGuide(guide: List<RaceCentralGuide>) {
        val json = Gson().toJson(guide)

        preferences.edit().putString(VIRTURUN_GUIDE, json).apply()
    }

    override fun saveContinuesRun(id: Int, savedVirtualRun: SavedVirtualRun) {
        val currentDatas = getAllContinuesRun()

        val mSavedVirtualRun = getContinuesRun(id)

        if (mSavedVirtualRun != null) {

            currentDatas.forEachIndexed { index, savedVirtualRun ->
                if (savedVirtualRun.id == id)
                    currentDatas.removeAt(index)
            }

        }

        currentDatas.add(savedVirtualRun)

        val json = Gson().toJson(currentDatas)

        preferences.edit().putString(SAVED_RUN_LIST, json).apply()
    }

    override fun getChallengeDashboard(userId: Int, userToken: String, callback: GitsDataSource.GetChallengeDashboardCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getContinuesRunningHistory(userId: Int, challengeId: Int, userToken: String, callback: GitsDataSource.GetContinuesRunningHistoryCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getStickers(userToken: String, callback: GitsDataSource.GetStickersCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllNews(userToken: String, callback: GitsDataSource.GetAllNewsCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun bookmarkEvent(userToken: String, params: BookmarkEventParams, callback: GitsDataSource.BookmarkEventCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAvailableChallenges(userToken: String, userId: Int, callback: GitsDataSource.GetAllChallengeCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getPreviousChallenges(userToken: String, userId: Int, callback: GitsDataSource.GetAllChallengeCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun postJoinChallenge(userToken: String, challengeId: Int, userId: Int, callback: GitsDataSource.PostJoinChallengeCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun login(loginParams: LoginParams, callback: GitsDataSource.LoginCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun register(registerParams: RegisterParams, callback: GitsDataSource.RegisterCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun forgotPassword(forgotPasswordParams: ForgotPasswordParams, callback: GitsDataSource.ForgotPasswordCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setNewPassword(newPasswordParams: NewPasswordParams, callback: GitsDataSource.SetNewPasswordCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun resendCode(tokenForgot: String?, callback: GitsDataSource.ResendCodeCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveUser(user: User) {
        val userString = GsonBuilder().setPrettyPrinting().create().toJson(user)
        preferences.edit().putString(USER_KEY, userString).apply()
    }

    override fun getLatestAppVersionCode(deviceVersionCode: Int, callback: GitsDataSource.GetLatestAppVersionCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllNewsByCategory(userToken: String, categoryId: Int, page: Int, callback: GitsDataSource.GetAllNewsCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getNewsFeeds(userToken: String, callback: GitsDataSource.GetNewsFeedsCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAllNews(userToken: String, page: Int, callback: GitsDataSource.GetAllNewsCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getNewsDetail(userToken: String, newsId: Int, callback: GitsDataSource.GetNewsDetailCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveForgotPassword(forgotPasswordDao: ForgotPasswordDao) {
        val forgotString = GsonBuilder().setPrettyPrinting().create().toJson(forgotPasswordDao)
        preferences.edit().putString(FORGOT_KEY, forgotString).apply()
    }

    override fun updateUser(accessToken: String?, userId: Int, name: RequestBody?, gender: RequestBody?, dob: RequestBody?, picture: MultipartBody.Part, callback: GitsDataSource.EditProfileCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun logout(token: String?, callback: GitsDataSource.LogoutCallback?, isExpired: Boolean) {
        preferences.edit().remove(USER_KEY).apply()
        preferences.edit().remove(KEY_LOGIN).apply()
    }

    override fun addRunHistory(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: GitsDataSource.AddRunHistoryCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRunHistories(userToken: String, userId: Int, page: Int, callback: GitsDataSource.GetRunHistoriesCallback, context: Context) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun resendEmail(token: String, callback: GitsDataSource.ResendEmailCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun userValidation(userId: Int, callback: GitsDataSource.UserValidationCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showUser(token: String?, userId: Int, callback: GitsDataSource.ShowUserCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEventList(token: String?, ownerId: Int, callback: GitsDataSource.GetEventListCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getOnBoardImage(callback: GitsDataSource.GetOnBoardImageCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getEventDetail(token: String?, eventId: Int, userId: Int, callback: GitsDataSource.GetEventDetailCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getImagesGallery(eventId: Int, pid: String, token: String?, callback: GitsDataSource.GetImagesGalleryCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getMedalHistory(userToken: String, userId: Int, page: Int, callback: GitsDataSource.GetMedalCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getChallenges(userToken: String, callback: GitsDataSource.GetChallengesCallback) {

    }

    override fun addRunProgress(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: GitsDataSource.AddRunHistoryCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getChallengeById(userToken: String, idChallenge: Int, userId: Int, callback: GitsDataSource.GetChallengeByIdCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUser(): User? {
        var user: User? = null

        val userString = preferences.getString(USER_KEY, "")

        if (userString != "") {
            user = GsonBuilder().setPrettyPrinting().create().fromJson(userString, object : TypeToken<User>() {}.type)
        }
        return user
    }

    override fun getForgotPasswordDao(): ForgotPasswordDao {
        var forgotPasswordDao = ForgotPasswordDao()

        val forgotString = preferences.getString(FORGOT_KEY, "")

        if (forgotString != "") {
            forgotPasswordDao = GsonBuilder().setPrettyPrinting().create().fromJson(forgotString, object : TypeToken<ForgotPasswordDao>() {}.type)
        }

        return forgotPasswordDao
    }

    override fun getToken(): String? {
        return (getUser() ?: User()).token
    }

    override fun getShareLink(userToken: String, eventId: Int, callback: GitsDataSource.GetShareLinkCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  ========================================================================================================================
     *
     *                                        PUT
     *
     *  ========================================================================================================================
     */
    override fun updatePersonalBio(userId: String, name: String, gender: String, dob: String, callback: GitsDataSource.UpdatePersonalBioCallback) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  ========================================================================================================================
     *
     *                                        ADDITIONAL FUNCTION
     *
     *  ========================================================================================================================
     */

    override fun clearDisposable() {

    }


    companion object {

        private var INSTANCE: GitsLocalDataSource? = null

        @JvmStatic
        fun getInstance(context: Context, preferences: SharedPreferences): GitsLocalDataSource {
            if (INSTANCE == null) {
                synchronized(GitsLocalDataSource::javaClass) {
                    INSTANCE = GitsLocalDataSource(context, preferences)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }
    }
}

