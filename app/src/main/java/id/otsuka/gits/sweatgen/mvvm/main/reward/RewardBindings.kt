package id.otsuka.gits.sweatgen.mvvm.main.reward

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.data.model.ChallengeMedal

/**
 * @author radhikayusuf.
 */

object RewardBindings {
    @BindingAdapter("app:medals")
    @JvmStatic
    fun setupMedals(recyclerView: RecyclerView, medals: List<ChallengeMedal>?) {
        recyclerView.itemAnimator = null
        (recyclerView.adapter as RecyclerAdapter).apply {
            if (medals != null)
                replaceMedals(medals)
        }
    }
}