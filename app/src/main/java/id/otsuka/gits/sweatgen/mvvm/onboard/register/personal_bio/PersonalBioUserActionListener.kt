package id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio;

import android.view.View


interface PersonalBioUserActionListener {

    // Example
    // fun onClickItem()

    fun triggerDatePicker(view: View)

    fun letsGetSweatClick()

}