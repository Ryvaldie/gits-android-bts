package id.otsuka.gits.sweatgen.mvvm.main.event


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentEventBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailActivity
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.NO_INET_CONNECTION
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import kotlinx.android.synthetic.main.fragment_event.*

class EventFragment : Fragment(), RecyclerAdapter.ItemClickListener {

    lateinit var mViewDataBinding: FragmentEventBinding
    lateinit var mViewModel: EventViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentEventBinding.inflate(inflater, container, false)

        mViewModel = obtainViewModel()

        mViewDataBinding.mListener = object : EventUserActionListener {
            override fun toVirtualRun() {
                getString(R.string.text_upcoming).showSnackBar(mViewDataBinding.root)
                // VirtualRunActivity.startActivity(requireContext())
            }
        }

        mViewDataBinding.mViewModel = mViewModel.apply {
            getPocariEventList(1)
            getPocariEventList(0)
            getVirturunBanner()
            obsFragmentManager.set(childFragmentManager)
        }

        setupRecycler()

        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer {
            if (it == true) {
                frame_progbar.visibility = View.VISIBLE
            } else {
                frame_progbar.visibility = View.GONE
                swipe_refresh_event.isRefreshing = false
                if (mViewModel.bMesage.get().isNullOrEmpty()) {
                    // if (mViewModel.obsEventPocari.isNotEmpty())
                    //  mViewDataBinding.linVpIndicator.setupViewPagerIndicator(mViewModel.obsEventPocari.size)
                } else {
                    when (mViewModel.bMesage.get()) {

                        TOKEN_EXPIRE -> {

                            mViewModel.repository.logout(null, null, true)

                            activity!!.redirectToLogin()
                        }
                        NO_INET_CONNECTION -> {
                            (activity as MainActivity).showNoInetConnection()
                        }
                        else -> {
                            mViewModel.bMesage.get()?.showSnackBar(mViewDataBinding.root)
                        }
                    }


                }

            }
        })

        mViewModel.firstPageEvent.observe(this, Observer {
            if (it != null) {
                Handler().postDelayed({
                    if (vp_slider_event != null)
                        try {
                            vp_slider_event.currentItem = it
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                }, 100)
            }
        })

        vp_slider_event.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                swipe_refresh_event?.isEnabled = state == ViewPager.SCROLL_STATE_IDLE
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

            }

        })

        swipe_refresh_event.setOnRefreshListener {
            mViewModel.apply {
                getPocariEventList(1)
                getPocariEventList(0)
                obsFragmentManager.set(childFragmentManager)
            }
        }

    }

    fun setupRecycler() {
        mViewDataBinding.recyclerNews.apply {
            adapter = RecyclerAdapter(ArrayList(0), this@EventFragment)
            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(GridSpacesItemDecoration(2.dpToPx(context), false))

        }
    }

    override fun onItemSelected(eventId: Int) {
        EventDetailActivity.startActivity(context!!, eventId)
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            (activity as MainActivity).mViewDataBinding?.mViewModel?.loadLayout?.set(false)
        }
    }

    fun obtainViewModel(): EventViewModel = obtainViewModel(EventViewModel::class.java)

    companion object {
        fun newInstance() = EventFragment().apply {

        }

    }

}
