package id.otsuka.gits.sweatgen.mvvm.onboard.login.forgotpassword;


interface ForgotPasswordUserActionListener {

    // Example
    // fun onClickItem()
    fun sendPassword()

    fun backToOnBoard()
}