package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */
data class VirturunDetailInformation(val id: Int? = null,
                                     val virtual_run_id: Int? = null,
                                     val rules: List<String>? = null,
                                     val hydration_point: List<String>? = null,
                                     val prize: List<String>? = null
)