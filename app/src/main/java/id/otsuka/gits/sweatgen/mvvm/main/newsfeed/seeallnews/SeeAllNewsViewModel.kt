package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableInt
import id.otsuka.gits.sweatgen.data.model.NewsFeed
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.AllNewsAdapter.Companion.ITEM_PER_PAGE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.agoFormat
import id.otsuka.gits.sweatgen.util.getSpanString
import id.otsuka.gits.sweatgen.util.toFormattedDate


class SeeAllNewsViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    var currentPage = 1

    var isLastPage = ObservableBoolean(false)

    val bAllNews = ObservableArrayList<NewsItemModel>()

    val isRequesting = ObservableBoolean(false)

    val showProgress = SingleLiveEvent<Boolean>()

    val showMessage = SingleLiveEvent<String>()

    val isFirstTime = ObservableBoolean(true)

    val categoryId = ObservableInt(0)

    fun getAllNewsByCategory() {
        isLastPage.set(false)

        repository.getAllNewsByCategory("", categoryId.get(), currentPage, object : GitsDataSource.GetAllNewsCallback {
            override fun onDataNotAvailable() {
                showProgress.value = true
                isLastPage.set(true)
                isRequesting.set(false)
                isFirstTime.set(false)
            }

            override fun onError(errorMessage: String?) {
                isLastPage.set(false)
                isRequesting.set(false)
                isFirstTime.set(false)
                showMessage.value = errorMessage
            }

            override fun onLoaded(news: List<NewsFeed>) {
                if (currentPage == 1)
                    bAllNews.clear()

                bAllNews.addAll(news.map {
                    NewsItemModel(
                            it.id,
                            it.title ?: "-",
                            (it.date_publish
                                    ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").agoFormat(context) + " - " + it.category,
                            (it.content ?: "-").getSpanString(context).toString(),
                            it.banner ?: "")
                })

                isLastPage.set(bAllNews.size < ITEM_PER_PAGE)

                showProgress.value = isLastPage.get()

                isRequesting.set(false)
                isFirstTime.set(false)
            }
        })
    }

    fun getAllNews() {

        isLastPage.set(false)

        repository.getAllNews("", currentPage, object : GitsDataSource.GetAllNewsCallback {
            override fun onDataNotAvailable() {
                showProgress.value = true
                isLastPage.set(true)
                isRequesting.set(false)
                isFirstTime.set(false)
            }

            override fun onError(errorMessage: String?) {
                isLastPage.set(false)
                isRequesting.set(false)
                isFirstTime.set(false)
                showMessage.value = errorMessage
            }

            override fun onLoaded(news: List<NewsFeed>) {
                if (currentPage == 1)
                    bAllNews.clear()

                bAllNews.addAll(news.map {
                    NewsItemModel(
                            it.id,
                            it.title ?: "-",
                            (it.date_publish
                                    ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").agoFormat(context) + " - " + it.category,
                            it.content ?: "-",
                            it.banner ?: "")
                })

                isLastPage.set(bAllNews.size < ITEM_PER_PAGE)

                showProgress.value = isLastPage.get()

                isRequesting.set(false)
                isFirstTime.set(false)
            }
        })
    }
}