package id.otsuka.gits.sweatgen.mvvm.main.profile

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.*
import java.util.*

/**
 * @author radhikayusuf.
 */

object ProfileBindings {

    @BindingAdapter("app:doubleToString")
    @JvmStatic
    fun doubleToString(textView: TextView, value: Double?) {
        textView.apply {
            text = (value ?: 0.0).cutExactly2Num().replace(".", ",").replace(",0", "")

        }
    }

    @BindingAdapter("app:src")
    @JvmStatic
    fun setupIcon(imageView: ImageView, typeRun: String?) {
        imageView.apply {
            if (!typeRun.isNullOrEmpty())
                if (typeRun == "Challenge")
                    setImageResource(R.drawable.ic_challenge_type_run)
                else
                    setImageResource(R.drawable.ic_free_run)
            else
                setImageResource(R.drawable.ic_free_run)
        }
    }

    @BindingAdapter("app:doubleToString2f")
    @JvmStatic
    fun doubleToString2f(textView: TextView, value: Double?) {
        textView.apply {
            text = (value ?: 0.0).cutExactly2Num().replace(".", ",").replace(",00", "")

        }
    }

    @BindingAdapter("app:convertDate")
    @JvmStatic
    fun convertDate(textView: TextView, value: String?) {
        if (value != null) {
            try {
                val date = value.toFormattedDate("yyyy-MM-dd hh:mm:ss", Locale("id", "ID"))
                textView.text = date.toAtMMMMdd()
            } catch (e: Exception) {
                textView.text = "-"
            }

        } else {
            textView.text = "-"
        }
    }

    @BindingAdapter("app:histories")
    @JvmStatic
    fun setHistories(recyclerView: RecyclerView, histories: List<ItemType>) {
        recyclerView.itemAnimator = null
        (recyclerView.adapter as RecyclerAdapter).apply {
            replaceHistories(histories)
        }
    }


    @BindingAdapter("app:tabs")
    @JvmStatic
    fun setupTabs(tabLayout: TabLayout, set: Boolean) {
        tabLayout.apply {
            addTab(newTab().setIcon(ContextCompat.getDrawable(tabLayout.context, R.drawable.ic_history)))
            addTab(newTab().setIcon(ContextCompat.getDrawable(tabLayout.context, R.drawable.ic_love)))
            addTab(newTab().setIcon(ContextCompat.getDrawable(tabLayout.context, R.drawable.ic_ticket)))
            tabGravity = TabLayout.GRAVITY_FILL
        }
    }

    @BindingAdapter("app:profileImageUrl", "app:listener")
    @JvmStatic
    fun loadImage(view: CircleImageView, imageUrl: Any?, listener: RequestListener<Drawable>?) {
        Glide.with(view.context)
                .setDefaultRequestOptions(
                        RequestOptions()
                                .placeholder(R.mipmap.ic_launcher_round)
                                .error(R.mipmap.ic_launcher_round)
                )
                .load(imageUrl)
                .listener(listener)
                .into(view)
    }

    @BindingAdapter("app:age")
    @JvmStatic
    fun setAge(textView: TextView, age: Int) {
        textView.apply {
            text = context.getString(R.string.text_dummy_user_detail).replace("%", age.toString())
        }
    }

    @BindingAdapter("app:setCode")
    @JvmStatic
    fun setCode(textView: TextView, code: String) {
        textView.apply {
            text = context.getString(R.string.text_id_no).replace("%", code)
        }
    }

}