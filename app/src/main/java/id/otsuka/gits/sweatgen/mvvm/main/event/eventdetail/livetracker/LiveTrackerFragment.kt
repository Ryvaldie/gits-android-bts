package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.livetracker


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentLiveTrackerBinding
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.withArgs
import kotlinx.android.synthetic.main.fragment_live_tracker.*

class LiveTrackerFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentLiveTrackerBinding
    lateinit var mViewModel: LiveTrackerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentLiveTrackerBinding.inflate(inflater, container, false).apply {
            if (arguments?.getInt(TOOLBAR_TITLE, 0) == 0) {
                title = getString(R.string.text_live_tracker)
            } else {
                title = getString(arguments?.getInt(TOOLBAR_TITLE, 0)!!)
            }
        }
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            bUrl.set(arguments?.getString(LIVE_TRACKER_URL, ""))
        }

        mViewDataBinding.mListener = object : LiveTrackerUserActionListener {

        }



        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webview_live_tracker.webViewClient = object : android.webkit.WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)

                if (progressBar1 != null)
                    progressBar1.visibility = View.GONE
            }
        }

        toolbar_live_tracker.setNavigationOnClickListener { activity!!.onBackPressed() }
    }

    fun obtainViewModel(): LiveTrackerViewModel = obtainViewModel(LiveTrackerViewModel::class.java)

    companion object {
        val LIVE_TRACKER_URL = "url"
        val TOOLBAR_TITLE = "title"
        fun newInstance(liveTrackerUrl: String, title: Int = 0) = LiveTrackerFragment().withArgs {
            putString(LIVE_TRACKER_URL, liveTrackerUrl)
            putInt(TOOLBAR_TITLE, title)
        }

    }

}
