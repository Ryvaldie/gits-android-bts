package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.text.format.DateUtils
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.Other.JOINED_RUN
import id.otsuka.gits.sweatgen.util.Other.LEADERBOARD_BY_DISTANCE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.cutExactly2Num
import id.otsuka.gits.sweatgen.util.timeFromSecondtoHourMinute
import id.otsuka.gits.sweatgen.util.toFormattedDate
import java.util.*

class ChallengeDetailViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bChallenge = ObservableField<Challenge>()

    val eventLoading = SingleLiveEvent<Boolean>()

    val isRequesting = ObservableBoolean(false)

    val idChallenge = ObservableInt(0)

    val mSnackbarMessage = SingleLiveEvent<String>()

    val isContinues = ObservableBoolean(false)

    val bCurrentDistance = ObservableField("-")

    val bProgressString = ObservableField(context.getString(R.string.text_distance_indicator).replace("*", "-"))

    val bButtonText = ObservableField("")

    val isButtonEnable = ObservableBoolean(true)

    val bProgress = ObservableInt(0)

    val isBtnRequesting = ObservableBoolean(false)

    val eventStartRun = SingleLiveEvent<Void>()

    var isStarted = true

    fun blockAccess() {
        bButtonText.set(context.getString(R.string.text_finished))
        isButtonEnable.set(false)
    }

    fun joinChallenge() {
        isBtnRequesting.set(true)
        repository.postJoinChallenge("", idChallenge.get(), repository.getUser()?.id
                ?: 0, object : GitsDataSource.PostJoinChallengeCallback {
            override fun onFailed(errorMessage: String?) {
                mSnackbarMessage.value = errorMessage
            }

            override fun onFinish() {
                isBtnRequesting.set(false)
            }

            override fun onSuccess() {
                eventStartRun.call()
            }
        })
    }

    fun start() {
        isRequesting.set(true)

        repository.getChallengeById("", idChallenge.get(), repository.getUser()?.id
                ?: 0, object : GitsDataSource.GetChallengeByIdCallback {
            override fun onDataLoaded(challenge: Challenge) {

                bChallenge.set(challenge)

                val date = challenge.from_date_challenge?.toFormattedDate("yyyy-MM-dd", Locale("id", "ID"))

                if (date != null) {
                    isStarted = date.before(Date()) || DateUtils.isToday(date.time)
                }

                isContinues.set((challenge.is_continues != 0))

                if (challenge.run_status != null) {
                    if (challenge.run_status.isEmpty()) {
                        bButtonText.set(context.getString(R.string.text_join_now))
                    } else {
                        if (challenge.run_status == JOINED_RUN)
                            bButtonText.set(context.getString(R.string.text_continue))
                        else
                            blockAccess()
                    }
                }

                setupProgress(challenge)
            }

            override fun onError(errorMessage: String?) {
                mSnackbarMessage.value = errorMessage
            }

            override fun onFinish() {
                isRequesting.set(false)
            }
        })
    }

    fun setupProgress(challenge: Challenge) {
       // if ((challenge.leaderboard ?: "") == LEADERBOARD_BY_DISTANCE) {
            try {
                val kmCurrentDistance = (challenge.distance_sum ?: 0.0).div(1000.0)
                val kmTotalDistance = (challenge.distance_total ?: 0.0).div(1000.0)
                val progress = kmCurrentDistance / kmTotalDistance * 100

                bCurrentDistance.set(kmCurrentDistance.cutExactly2Num())
                bProgressString.set(context.getString(R.string.text_distance_indicator).replace("*", kmTotalDistance.cutExactly2Num()))
                bProgress.set(progress.toInt())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            /*
        } else {
            try {
                val currentDuration = challenge.duration_sum ?: 0
                val totalDuration = challenge.duration
                val progress = currentDuration / totalDuration * 100
                bCurrentDistance.set(currentDuration.timeFromSecondtoHourMinute("%dh %dm"))
                bProgressString.set(context.getString(R.string.text_time_indicator).replace("*", totalDuration.toLong().timeFromSecondtoHourMinute("%dh %dm")))
                bProgress.set(progress.toInt())
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }*/

    }
}