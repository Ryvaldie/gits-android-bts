package id.otsuka.gits.sweatgen.data.model

data class GetNewsFeedDao(val top_news: List<NewsFeed>?,
                          val headline_news: List<NewsFeed>?
)