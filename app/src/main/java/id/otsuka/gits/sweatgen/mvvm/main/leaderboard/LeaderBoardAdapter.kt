package id.otsuka.gits.sweatgen.mvvm.main.leaderboard


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.ItemHeaderLeaderboardBinding
import id.otsuka.gits.sweatgen.databinding.ItemLeaderBoardBinding


class LeaderBoardAdapter(var mData: ArrayList<LeaderBoardModel>,
                         val mViewModel: LeaderBoardViewModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderVH -> holder.bind()
            is LeaderBoardItem -> holder.bind(mData[position.minus(1)])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_header_leaderboard -> {
                HeaderVH(ItemHeaderLeaderboardBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    vm = mViewModel
                })
            }
            else -> {
                LeaderBoardItem(ItemLeaderBoardBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = object : LeaderBoardUserActionListener {

                        override fun onClickItem(data: LeaderBoardModel) {
                            mViewModel.eventClickItem.value = data
                        }

                    }
                })
            }
        }
    }

    override fun getItemCount(): Int = mData.size.plus(1)

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> R.layout.item_header_leaderboard
            else -> R.layout.item_leader_board
        }
    }

    class HeaderVH(val mBinding: ItemHeaderLeaderboardBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind() {

        }
    }

    fun replaceData(data: List<LeaderBoardModel>) {
        mData.apply {
            clear()
            addAll(data)
        }
        notifyItemChanged(1, mData.size)
    }


    class LeaderBoardItem(val mBinding: ItemLeaderBoardBinding) : RecyclerView.ViewHolder(mBinding.root) {

        fun bind(data: LeaderBoardModel) {
            mBinding.mData = data
        }

    }

}