package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class Extras(
        val currentDistance: Double? = null,
        val currentDuration: Int? = null,
        val currentCalories: Double? = null,
        val currentStep: Int? = null,
        val historyRun: List<HistoryRun>?

)