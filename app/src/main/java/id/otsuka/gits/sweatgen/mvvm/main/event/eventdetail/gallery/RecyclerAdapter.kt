package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.ImageGallery
import id.otsuka.gits.sweatgen.databinding.CustomGalleryRviewItemBinding

class RecyclerAdapter(var photos: List<ImageGallery>,
                      val listener: onClickItemListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replacePhotos(photos: List<ImageGallery>) {
        this.photos = photos
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind(photos[position],listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = CustomGalleryRviewItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(view)
    }

    interface onClickItemListener {
        fun onItemSelected(image: ImageGallery)
    }

    class MyViewHolder(val mView: CustomGalleryRviewItemBinding) : RecyclerView.ViewHolder(mView.root) {

        fun bind(obj: ImageGallery, listener: onClickItemListener) {
            mView.image = obj
            mView.mListener = listener
        }
    }

    override fun getItemCount(): Int = photos.size
}