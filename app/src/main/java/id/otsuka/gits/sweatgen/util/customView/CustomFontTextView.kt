package id.otsuka.gits.sweatgen.util.customView

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView
import  id.otsuka.gits.sweatgen.R


class CustomFontTextView : TextView {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)

    }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs,
                    R.styleable.CustomFontTextView)
            val cf = a.getInteger(R.styleable.CustomFontTextView_fontName, 0)
            var fontName = 0
            when (cf) {
                1 -> fontName = R.string.font_avenir_black
                2 -> fontName = R.string.font_avenir_black_oblique
                3 -> fontName = R.string.font_avenir_book
                4 -> fontName = R.string.font_avenir_book_oblique
                5 -> fontName = R.string.font_avenir_heavy
                6 -> fontName = R.string.font_avenir_heavy_oblique
               7 -> fontName = R.string.font_avenir_light
                8 -> fontName = R.string.font_avenir_light_oblique
               9 -> fontName = R.string.font_avenir_medium
                10 -> fontName = R.string.font_avenir_medium_oblique
                11 -> fontName = R.string.font_avenir_oblique
                12 -> fontName = R.string.font_avenir_roman
                else -> fontName = R.string.font_avenir_medium
            }

            val customFont = resources.getString(fontName)
            val tf = Typeface.createFromAsset(context.assets,
                    "font/$customFont.otf")
            setTypeface(tf)

            a.recycle()
        }
    }

}