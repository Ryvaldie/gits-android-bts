package id.otsuka.gits.sweatgen.data.model

data class MedalContent(val current_medal: CurrentMedal? = null,
                        val list_medal: List<ChallengeMedal>? = null
)