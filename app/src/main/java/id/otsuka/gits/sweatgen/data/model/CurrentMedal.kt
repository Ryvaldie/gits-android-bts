package id.otsuka.gits.sweatgen.data.model

data class CurrentMedal(val medal_name: String? = "",
                        val total_point: Int = 0
)