package id.otsuka.gits.sweatgen.mvvm.onboard.register;


interface RegisterUserActionListener {

    // Example
   fun onBtnSettingsClick()

    fun onBtnGoSignInClick()

    fun onBtnRegisterClick()

}