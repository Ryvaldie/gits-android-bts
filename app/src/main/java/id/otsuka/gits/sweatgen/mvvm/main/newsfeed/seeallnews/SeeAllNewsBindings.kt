package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel

/**
 * @author radhikayusuf.
 */

object SeeAllNewsBindings {

    @BindingAdapter("app:newsall")
    @JvmStatic
    fun setupNews(recyclerView: RecyclerView, data: List<NewsItemModel>) {
        (recyclerView.adapter as AllNewsAdapter).apply {
            replaceData(data)
        }
    }
}
