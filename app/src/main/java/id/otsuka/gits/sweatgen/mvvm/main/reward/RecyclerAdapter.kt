package id.otsuka.gits.sweatgen.mvvm.main.reward

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.ChallengeMedal
import id.otsuka.gits.sweatgen.databinding.ItemListLoadingBinding
import id.otsuka.gits.sweatgen.databinding.ItemMedalListBinding

class RecyclerAdapter(var medals: List<ChallengeMedal>,
                      val viewModel: RewardViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var showProgress = true

    companion object {
        const val ITEM_PER_PAGE = 10
        const val TYPE_CONTENT = 1
        const val TYPE_PROGBAR = 2

    }

    fun replaceMedals(medals: List<ChallengeMedal>) {
        if (this.medals.isEmpty()) {
            this.medals = medals
            notifyDataSetChanged()
        } else {
            if (medals.size > this.medals.size) {
                val lastPosition = (ITEM_PER_PAGE.times(viewModel.currentPage)).minus(1)
                this.medals = medals
                notifyItemRangeInserted(lastPosition, this.medals.size.minus(1))
            }
            viewModel.currentPage += 1
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (showProgress) {
            if (medals.size == position) TYPE_PROGBAR else TYPE_CONTENT
        } else {
            TYPE_CONTENT
        }
    }

    fun showProgress(show: Boolean) {
        showProgress = show
//        notifyItemChanged(medals.size)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (medals.isEmpty()) 0 else medals.size + (if (showProgress) 1 else 0)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MedalVH -> {
                holder.bind(medals[position])
            }
            else -> {
                (holder as ProgbarVH).bind(!(viewModel.isLastPage.get()))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var mViewDataBinding: ViewDataBinding = ItemMedalListBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return if (viewType == TYPE_PROGBAR) {
            mViewDataBinding = ItemListLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            ProgbarVH(mViewDataBinding)
        } else {
            MedalVH(mViewDataBinding as ItemMedalListBinding)
        }
    }

    class MedalVH(val mBinding: ItemMedalListBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(obj: ChallengeMedal) {
            mBinding.challMedal = obj
        }
    }

    class ProgbarVH(val mBinding: ItemListLoadingBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(isVisible: Boolean) {
            mBinding.isVisible = isVisible
        }
    }


}