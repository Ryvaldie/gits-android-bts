package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.sharefinalpage


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import id.otsuka.gits.sweatgen.databinding.FragmentShareFinalPageBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.shareToInstagram
import id.otsuka.gits.sweatgen.util.shareToOthers
import java.io.File
import kotlin.math.roundToInt

class ShareFinalPageFragment : Fragment(), ShareFinalPageUserActionListener {

    lateinit var mViewDataBinding: FragmentShareFinalPageBinding
    lateinit var mViewModel: ShareFinalPageViewModel
    lateinit var mParentVm: ShareRunViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentShareFinalPageBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = this@ShareFinalPageFragment

        setupImageView()

        return mViewDataBinding.root

    }

    private fun setupImageView() {

        val displayMetrics = DisplayMetrics()

        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val height = (displayMetrics.heightPixels)

        val width = displayMetrics.widthPixels

        val imgFinalHeight = height.times(0.6).roundToInt()

        val layoutParam = LinearLayout.LayoutParams(width, imgFinalHeight)

        mViewDataBinding.imgvFinalImage.apply {
            layoutParams = layoutParam
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mParentVm = (requireActivity() as ShareRunActivity).obtainViewModel()
        mViewDataBinding.parentVm = mParentVm
    }


    override fun shareToInstagram() {
        requireActivity().shareToInstagram(mParentVm.bEditedImage.get() as File)
    }

    override fun shareToOthers() {
        requireActivity().shareToOthers(mParentVm.bEditedImage.get() as File)
    }

    // TODO import obtainViewModel & add ShareFinalPageViewModel to ViewModelFactory
    fun obtainViewModel(): ShareFinalPageViewModel = obtainViewModel(ShareFinalPageViewModel::class.java)

    companion object {
        fun newInstance() = ShareFinalPageFragment().apply {

        }

    }

}
