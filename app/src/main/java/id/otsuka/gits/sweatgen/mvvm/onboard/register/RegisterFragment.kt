package id.otsuka.gits.sweatgen.mvvm.onboard.register


import android.arch.lifecycle.Observer
import id.otsuka.gits.sweatgen.R
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import id.otsuka.gits.sweatgen.mvvm.onboard.login.LoginFragment
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.databinding.FragmentRegisterBinding
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentAndAddToBackStack
import id.otsuka.gits.sweatgen.util.showSnackBar
import id.otsuka.gits.sweatgen.mvvm.onboard.register.verification.VerificationFragment
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity


class RegisterFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentRegisterBinding
    lateinit var mViewModel: RegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRegisterBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : RegisterUserActionListener {
            override fun onBtnGoSignInClick() {
                (activity as AppCompatActivity).replaceFragmentInActivity(LoginFragment.newInstance(), R.id.frame_main_content)
            }

            override fun onBtnRegisterClick() {
                if (mViewModel.validateFields()) {

                    mViewModel.register()

                } else {
                    getString(R.string.text_please_fill_info).showSnackBar(mViewDataBinding.root)
                }
            }

            override fun onBtnSettingsClick() {
                showToast("Go to settings page")
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer { it ->

            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {
                Handler().post{
                    hideProgressDialog()

                    if (mViewModel.bSuccess.get())
                        (activity as AppCompatActivity).replaceFragmentInActivity(VerificationFragment.newInstance(), R.id.frame_main_content)
                    else
                        if (!mViewModel.bErrorMsgSnack.get().isNullOrEmpty())
                            mViewModel.bErrorMsgSnack.get()?.showSnackBar(mViewDataBinding.root)
                }

            }
        })
    }

    private fun obtainViewModel(): RegisterViewModel = obtainViewModel(RegisterViewModel::class.java)

    companion object {
        fun newInstance() = RegisterFragment().apply {

        }

    }

}
