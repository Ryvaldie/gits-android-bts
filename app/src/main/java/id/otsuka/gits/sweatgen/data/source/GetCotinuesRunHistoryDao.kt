package id.otsuka.gits.sweatgen.data.source

import id.otsuka.gits.sweatgen.data.model.HistoryRun

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class GetCotinuesRunHistoryDao(val header: GetRunningHistoryHeaderContent? = null,
                                    val body: List<HistoryRun>? = null
)