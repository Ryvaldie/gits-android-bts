package id.otsuka.gits.sweatgen.mvvm.onboard.login.forgotpassword


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.databinding.FragmentForgotPasswordBinding
import id.otsuka.gits.sweatgen.mvvm.onboard.login.verificationpassword.VerificationPasswordFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.login.forgotpassword.ForgotPasswordViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentAndAddToBackStack
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import id.otsuka.gits.sweatgen.util.showSnackBar
import kotlinx.android.synthetic.main.fragment_forgot_password.*


class ForgotPasswordFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentForgotPasswordBinding
    lateinit var mViewModel: ForgotPasswordViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentForgotPasswordBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : ForgotPasswordUserActionListener {
            override fun backToOnBoard() {
                activity?.onBackPressed()
            }

            override fun sendPassword() {
                if (mViewModel.validateFields()) {

                    mViewModel.forgotPassword()

                } else {
                    getString(R.string.text_please_fill_info).showSnackBar(mViewDataBinding.root)
                }
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer { it ->

            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {
                Handler().postDelayed({
                    hideProgressDialog()
                    if (mViewModel.bSuccess.get())
//                        getString(R.string.text_check_mail).showSnackBar(mViewDataBinding.root)
                        (activity as AppCompatActivity).replaceFragmentInActivity(VerificationPasswordFragment.newInstance(), R.id.frame_main_content)
                    else
                        mViewModel.bErrorMsg.get()?.showSnackBar(mViewDataBinding.root)
                }, 3000)

            }
        })

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).setSupportActionBar(toolbar_forgot_password)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)


        toolbar_forgot_password.setNavigationOnClickListener { mViewDataBinding.mListener?.backToOnBoard() }
    }

    fun obtainViewModel(): ForgotPasswordViewModel = obtainViewModel(ForgotPasswordViewModel::class.java)

    companion object {
        fun newInstance() = ForgotPasswordFragment().apply {

        }

    }

}
