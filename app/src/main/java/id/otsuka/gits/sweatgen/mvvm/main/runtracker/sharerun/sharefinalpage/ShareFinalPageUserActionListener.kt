package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.sharefinalpage;


interface ShareFinalPageUserActionListener {

    fun shareToInstagram()
    fun shareToOthers()

}