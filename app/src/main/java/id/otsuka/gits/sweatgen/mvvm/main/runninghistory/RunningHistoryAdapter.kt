package id.otsuka.gits.sweatgen.mvvm.main.runninghistory

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.RunningHistory
import id.otsuka.gits.sweatgen.databinding.ItemHeaderRunningHistoryBinding
import id.otsuka.gits.sweatgen.databinding.ItemHistoryRunningBinding

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class RunningHistoryAdapter(val mData: ArrayList<RunningHistory>,
                            val vm: RunningHistoryViewModel,
                            val listener: RunningHistoryUserActionListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceData(newData: List<RunningHistory>) {
        mData.apply {
            clear()
            addAll(newData)
        }
        notifyItemChanged(1, mData.size)
    }

    override fun getItemCount(): Int = mData.size.plus(1)

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> R.layout.item_header_running_history
            else -> R.layout.item_history_running
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderVH -> holder.bind()
            is ItemVH -> holder.bind(mData[position.minus(1)], position.minus(1) == 0, position.minus(1) == mData.lastIndex, position.minus(1))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_header_running_history -> {
                HeaderVH(ItemHeaderRunningHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    vm = this@RunningHistoryAdapter.vm
                })
            }
            else -> {
                ItemVH(ItemHistoryRunningBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = listener
                })
            }
        }
    }

    class ItemVH(val mBinding: ItemHistoryRunningBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(obj: RunningHistory, isFirstm: Boolean, isLastm: Boolean, mIndex: Int) {
            val context = mBinding.root.context

            mBinding.apply {
                date = obj.date
                disTime = context.getString(R.string.text_distime).replace("#", obj.distance
                        ?: "0").replace("*", obj.duration ?: "0")
                isFirst = isFirstm
                isLast = isLastm
                index = mIndex
            }
        }
    }

    class HeaderVH(val mBinding: ItemHeaderRunningHistoryBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind() {

        }
    }
}