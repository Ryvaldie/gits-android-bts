package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.CustomDialogChallengeInfoBinding
import id.otsuka.gits.sweatgen.databinding.FragmentChallengeDetailBinding
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail.ChallengeDetailActivity.Companion.ID_CHALLENGE
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity.Companion.REQ_TO_RUN_TRACKER
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import kotlinx.android.synthetic.main.fragment_challenge_detail.*

class ChallengeDetailFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentChallengeDetailBinding
    lateinit var mViewModel: ChallengeDetailViewModel
    val mDialog by lazy { buildDialog() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = FragmentChallengeDetailBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as ChallengeDetailActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            idChallenge.set(requireActivity().intent?.getIntExtra(ID_CHALLENGE, 0) ?: 0)
            start()
        }

        mViewDataBinding.mListener = object : ChallengeDetailUserActionListener {

            override fun startChallenge(challenge: Challenge?) {
                if (challenge != null) {
                    if ((challenge.run_status ?: "").isEmpty()) {

                        if (!mViewModel.isStarted)
                            mViewModel.mSnackbarMessage.value = getString(R.string.text_not_today)
                        else
                            mViewModel.joinChallenge()

                    } else
                        mViewModel.eventStartRun.call()
                }

            }

            override fun toLeaderboard() {
                LeaderBoardActivity.startActivity(LeaderBoardActivity.CHALLENGE_TYPE, requireContext()
                        , mViewModel.bChallenge.get()?.days_left ?: ""
                        , mViewModel.bCurrentDistance.get() ?: "-",
                        mViewModel.bProgressString.get() ?: "-",
                        mViewModel.idChallenge.get(),
                        mViewModel.bProgress.get(),
                        mViewModel.isContinues.get()
                )
            }

            override fun toRunHistory() {
                RunningHistoryActivity.startActivity(requireContext(),
                        mViewModel.idChallenge.get(),
                        mViewModel.bCurrentDistance.get() ?: "-",
                        mViewModel.bProgressString.get() ?: "-",
                        mViewModel.bProgress.get()
                )
            }

            override fun showInfo() {
                mDialog.show()
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_TO_RUN_TRACKER) {
                data?.getStringExtra("message")!!.showCustomSnackBar(mViewDataBinding.root, color = ContextCompat.getColor(requireContext(), R.color.colorGpsActivated))
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mViewModel.start()
    }


    private fun buildDialog(): AlertDialog {
        val dialog = AlertDialog.Builder(context!!).create()

        val inflater = layoutInflater

        val alertView = CustomDialogChallengeInfoBinding.inflate(inflater).apply {
            desc = mViewModel.bChallenge.get()?.description
            tviewYes.setOnClickListener { dialog.dismiss() }
            tvDescChallenge.movementMethod = ScrollingMovementMethod()
        }

        dialog.setView(alertView.root)

        dialog.setCancelable(false)

        return dialog
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.mSnackbarMessage.observe(this, Observer {
            if (it == TOKEN_EXPIRE) {
                mViewModel.repository.logout(null, null, true)
                activity!!.redirectToLogin()
            } else {
                (it ?: "-").showSnackBar(mViewDataBinding.root)
            }
        })

        mViewModel.eventStartRun.observe(this, Observer {
            if (mViewModel.bChallenge.get() != null) {
                RunTrackerActivity.startActivityForChallenge(context!!, 2, Gson().toJson(mViewModel.bChallenge.get()))
            } else {
                getString(R.string.text_please_refresh).showSnackBar(mViewDataBinding.root)
            }

        })

        mViewModel.eventLoading.observe(this, Observer {
            if (it!!) {
                activity?.showLoading()
            } else {
                activity?.dismissLoading()
                buildDialog()
            }

        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_challenge_details.setNavigationOnClickListener { activity!!.finish() }
    }

    companion object {
        fun newInstance() = ChallengeDetailFragment().apply {

        }

    }

}
