package id.otsuka.gits.sweatgen.mvvm.main.runtracker

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.cutExactly1Num

/**
 * @author radhikayusuf.
 */

object RunTrackerBindings {

    @BindingAdapter("app:srcImg")
    @JvmStatic
    fun setSrc(imgView: ImageView, res: Any) {
        when (res) {
            is Int -> {
                imgView.setImageResource(res)
            }
            is Drawable -> {
                imgView.setImageDrawable(res)
            }
        }

    }

    @BindingAdapter("app:gpsText")
    @JvmStatic
    fun setGpsText(textView: TextView, gpsState: Int) {
        textView.apply {
            when (gpsState) {
                0 -> context.getString(R.string.text_acquire_gps)
                1 -> context.getString(R.string.text_gps_Searching)
                2 -> context.getString(R.string.text_gps_active)
            }
        }

    }

    @BindingAdapter("app:calories")
    @JvmStatic
    fun setCalories(textView: TextView, calories: Float?) {
        if (calories != null) {
            if (calories >= 1000) {

                textView.text = calories.toDouble().div(1000).cutExactly1Num().replace(".", ",").replace(",0", "")

            } else {

                textView.text = calories.toDouble().cutExactly1Num().replace(".", ",").replace(",0", "")


            }

        } else {
            textView.text = "0"
        }

    }

    @BindingAdapter("app:gpsColor")
    @JvmStatic
    fun setGpsColor(frameLayout: FrameLayout, gpsState: Int) {
        frameLayout.apply {
            when (gpsState) {
                0 -> setBackgroundColor(ContextCompat.getColor(context, R.color.colorGpsDeactivated))
                1 -> setBackgroundColor(ContextCompat.getColor(context, R.color.colorBlueRadioCircle))
                2 -> {
                    setBackgroundColor(ContextCompat.getColor(context, R.color.colorGpsActivated))
                    Handler().postDelayed({
                        visibility = View.GONE
                    }, 500)
                }
            }
        }

    }
}