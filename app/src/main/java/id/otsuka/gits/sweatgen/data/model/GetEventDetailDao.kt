package id.otsuka.gits.sweatgen.data.model

data class GetEventDetailDao(val event: EventDetail,
                             val bookmark: BookmarkEventDao? = null
)