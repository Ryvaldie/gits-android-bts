package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.customcam


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentCustomCameraBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageFragment
import id.otsuka.gits.sweatgen.util.fixOrientation
import id.otsuka.gits.sweatgen.util.helper.CustomCameraHelper
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentAndAddToBackStack
import io.fotoapparat.result.BitmapPhoto


class CustomCameraFragment : Fragment(), CustomCameraUserActionListener, CustomCameraHelper.CaptureCallback {

    lateinit var mViewDataBinding: FragmentCustomCameraBinding
    lateinit var mViewModel: CustomCameraViewModel
    val TAG = CustomCameraFragment::class.java.simpleName
    lateinit var mCustomCamHelper: CustomCameraHelper
    lateinit var mActivityVm: ShareRunViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentCustomCameraBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = this@CustomCameraFragment

        initiateCustomCamHelper()

        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(false)
        mActivityVm = (requireActivity() as ShareRunActivity).obtainViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onStart() {
        super.onStart()
        mCustomCamHelper.onStartCam()
    }

    override fun onStop() {
        super.onStop()
        mCustomCamHelper.onStopCam()
    }

    override fun onResume() {
        super.onResume()
        mCustomCamHelper.onStartCam()
    }


    private fun initiateCustomCamHelper() {
        mCustomCamHelper = CustomCameraHelper(requireContext(), mViewDataBinding.camview, requireActivity(), this)
    }

    override fun onError(errorMessage: String?) {
        Log.d(TAG, errorMessage)
    }

    override fun onImageAvailable(bitmapPhoto: BitmapPhoto?) {
        mViewModel.isRendering.set(false)
        if (bitmapPhoto != null) {
            val image: Bitmap = bitmapPhoto.bitmap.fixOrientation(-bitmapPhoto.rotationDegrees.toFloat())
            mActivityVm.bCapturedImage.set(image)
            mCustomCamHelper.onStopCam()
            (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(ShareEditImageFragment.newInstance(), R.id.frame_main_content)
        }
    }

    override fun onImageRendering() {
        mViewModel.isRendering.set(true)
    }


    override fun onCapture() {
        mCustomCamHelper.onCapture()
    }

    override fun onSwitchCam() {
        mCustomCamHelper.switchCam(mViewModel.bCameraFront.get())
        mViewModel.bCameraFront.set(!mViewModel.bCameraFront.get())
    }


    // TODO import obtainViewModel & add CustomCameraViewModel to ViewModelFactory
    fun obtainViewModel(): CustomCameraViewModel = obtainViewModel(CustomCameraViewModel::class.java)

    companion object {
        fun newInstance() = CustomCameraFragment().apply {

        }

    }

}
