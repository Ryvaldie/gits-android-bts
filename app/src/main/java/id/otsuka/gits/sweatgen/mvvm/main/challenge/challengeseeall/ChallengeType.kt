package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall

interface ChallengeType {


    companion object {
        val HEADER_TYPE = 1
        val ITEM_TYPE = 2
    }

    fun getItemType(): Int

    fun setItemType(itemType:Int)

    fun setTitle(title:String)

}