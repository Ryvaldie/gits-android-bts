package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity


class NewsDetailActivity : AppCompatActivity(), NewsDetailNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)
        setupFragment()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        NewsDetailFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        val NEWS_ID = "newsId"

        fun startActivity(newsId: Int, context: Context) {
            context.startActivity(Intent(context, NewsDetailActivity::class.java).putExtra(NEWS_ID, newsId))
        }
    }
}
