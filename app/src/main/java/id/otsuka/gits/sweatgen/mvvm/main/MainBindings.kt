package id.otsuka.gits.sweatgen.mvvm.main

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.data.model.RunningHistory
import id.otsuka.gits.sweatgen.mvvm.main.challenge.ChallengeAdapter
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardAdapter
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardModel
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryAdapter
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll.CameraRollAdapter
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers.StickerAdapter
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunAdapter
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunModel

/**
 * @author radhikayusuf.
 */

object MainBindings {

    @BindingAdapter("app:circleImageUrl")
    @JvmStatic
    fun loadImage(view: CircleImageView, imageUrl: String?) {
        Glide.with(view.context)
                .setDefaultRequestOptions(
                        RequestOptions()
                                .placeholder(R.mipmap.ic_launcher_round)
                                .error(R.mipmap.ic_launcher_round)
                )
                .load(imageUrl)
                .into(view)
    }

    @BindingAdapter("app:listDataLeaderBoard")
    @JvmStatic
    fun setupLeaderboard(recyclerView: RecyclerView, data: List<LeaderBoardModel>) {
        (recyclerView.adapter as LeaderBoardAdapter).replaceData(data)
    }

    @BindingAdapter("app:runs")
    @JvmStatic
    fun setRunHistories(recyclerView: RecyclerView, data: List<RunningHistory>) {
        (recyclerView.adapter as RunningHistoryAdapter).replaceData(data)
    }

    @BindingAdapter("app:challenges")
    @JvmStatic
    fun setupChallenges(recyclerView: RecyclerView, data: List<Challenge>) {
        (recyclerView.adapter as ChallengeAdapter).replaceData(data)
    }

    @BindingAdapter("app:virtualRuns")
    @JvmStatic
    fun loadVirtualRun(recyclerView: RecyclerView, data: List<VirtualRunModel>) {
        (recyclerView.adapter as VirtualRunAdapter).replaceData(data)
    }

    @BindingAdapter("app:imageCache")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?) {

        Glide.with(view.context)
                .load(imageUrl)
                .into(view)
                .apply {
                    RequestOptions().diskCacheStrategy
                }
    }

    @BindingAdapter("app:stickers")
    @JvmStatic
    fun fillSticker(recyclerView: RecyclerView, stickers: List<String>) {
        (recyclerView.adapter as StickerAdapter).fillData(stickers)
    }

    @BindingAdapter("app:imagesGallery")
    @JvmStatic
    fun setImages(recyclerView: RecyclerView, data: List<String>) {
        (recyclerView.adapter as CameraRollAdapter).apply {
            replaceData(data)
        }
    }
}