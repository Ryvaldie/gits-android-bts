package id.otsuka.gits.sweatgen.util.customView

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.widget.Scroller

class ViewpagerCustomDuration(context: Context, attributeSet: AttributeSet) : ViewPager(context, attributeSet) {
    private lateinit var mScroller: FixedSpeedScroller

    init {
        try {
            val viewpager = ViewPager::class.java
            val scroller = viewpager.getDeclaredField("mScroller")
            scroller.isAccessible = true
            mScroller = FixedSpeedScroller(context, DecelerateInterpolator())
            scroller.set(this, mScroller)
        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }
    }

    fun setScrollDuration(duration: Int) {
        mScroller.duration = duration
    }

    private class FixedSpeedScroller(context: Context, interpolator: Interpolator) : Scroller(context) {
        private var mDuration = 500

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int) {
            super.startScroll(startX, startY, dx, dy, mDuration)
        }

        override fun startScroll(startX: Int, startY: Int, dx: Int, dy: Int, duration: Int) {
            super.startScroll(startX, startY, dx, dy, mDuration)
        }

        fun setDuration(duration: Int) {
            this.mDuration = duration
        }

    }
}