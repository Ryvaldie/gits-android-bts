package id.otsuka.gits.sweatgen.mvvm.main.runtracker;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.*
import android.location.Location
import id.otsuka.gits.sweatgen.data.model.AddHistoryRunParams
import id.otsuka.gits.sweatgen.data.model.SavedVirtualRun
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class RunTrackerViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bTime = ObservableField("")

    var afterPause = false

    val bDistance = ObservableField("0")

    val bLastDuration = ObservableLong(0)

    val bDoubleDistance = ObservableField(0f)

    val bSteps = ObservableField("0")

    val bCalories = ObservableField("0")

    val bDoubleCalories = ObservableFloat(0f)

    val obsStartFragmentVisibility = ObservableField(1)

    val loadingEvent = SingleLiveEvent<Boolean>()

    val timeInMillis = ObservableField<Long>(0)

    val obsAddRunHistory = ObservableField<AddHistoryRunParams>()

    val redirectResultPage = SingleLiveEvent<Void>()

    val snackbarMessage = SingleLiveEvent<String?>()

    val obsLocations = ObservableArrayList<Location?>()

    val bCompleted = ObservableBoolean(false)

  //  var savedRun: SavedVirtualRun? = SavedVirtualRun()

    val redirectToChallengeDetail = SingleLiveEvent<Void>()

    /*
    * ada 2 tipe
    * id 1 untuk tipe free run
    * id 2 untuk tipe challenge
    * id 3 untuk tipe virtual run
    * */
    val runningType = ObservableInt(1)

    val obsBtnState = ObservableField(false)
    /*
    * ada 3 kondisi
    * 0 -> gps deactivated
    * 1 -> gps search my current location
    * 2 -> gps activated
    * */
    val gpsState = ObservableField(0)
    /*ada 2 tracker mode,
        kode 0 -> map mode
        kode 1 -> matric mode
        */
    val trackerMode = ObservableField(false)

    val startPauseEvent = SingleLiveEvent<Boolean>()

    fun start() {
        /*
        savedRun = repository.getContinuesRun(1)
        if (savedRun != null) {
            bLastDuration.set(savedRun!!.mDuration)
            bDoubleDistance.set(savedRun?.mDistance?.toFloat())
            bDistance.set((bDoubleDistance.get() ?: 0f).cutExactly2Num())
            bSteps.set(savedRun!!.mStep.toString())

            bCalories.set(savedRun!!.mCalories.cutExactly1Num())
            obsLocations.apply {
                clear()
                addAll(savedRun!!.route)
            }
        }*/
    }

    fun postRunProgress(isFinished: Boolean) {
        if (!isFinished)
            loadingEvent.value = true

        repository.addRunProgress("", obsAddRunHistory.get()!!.copy(user_id = repository.getUser()?.id
                ?: 0), object : GitsDataSource.AddRunHistoryCallback {
            override fun postSuccess() {
                if (!isFinished)
                    redirectToChallengeDetail.call()
                else
                    redirectResultPage.call()
            }

            override fun onFinish() {
                loadingEvent.value = false
            }

            override fun postError(errorMessage: String?) {
                snackbarMessage.value = errorMessage
            }
        })
    }

    fun postRunHistory(isChallengeContinues: Boolean) {

        loadingEvent.value = true

        repository.addRunHistory("", obsAddRunHistory.get()!!.copy(user_id = repository.getUser()?.id
                ?: 0), object : GitsDataSource.AddRunHistoryCallback {
            override fun postSuccess() {
                if (!isChallengeContinues) {
                    redirectResultPage.call()
                    loadingEvent.value = false
                } else
                    postRunProgress(true)
            }

            override fun onFinish() {
            }

            override fun postError(errorMessage: String?) {
                loadingEvent.value = false
                snackbarMessage.value = errorMessage
            }
        })
    }
}