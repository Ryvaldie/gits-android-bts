package id.otsuka.gits.sweatgen.data.model

data class BookmarkEventParams(val user_id: Int = 0,
                               val event_id: Int = 0
)