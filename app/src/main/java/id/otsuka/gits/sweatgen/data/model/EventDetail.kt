package id.otsuka.gits.sweatgen.data.model

import java.io.Serializable

data class EventDetail(
        val id: Int = 0,
        val support_owner: Int = 0,
        val name: String? = null,
        val from_date_event: String? = null,
        val end_date_event: String? = null,
        val category: String? = null,
        val address: String? = null,
        val meta: EventMeta? = null,
        val other: EventDesc? = null,
        val schedule: List<EventRundown>? = null,
        val from_date_race: String? = null,
        val end_date_race: String? = null,
        val link_gallery: String? = null,
        val contact: Contact? = null,
        val location: List<Location>? = null,
        val link_map: String? = null,
        val race_central: List<RaceCentralGuide>? = null,
        val race_guide: List<RaceCentralGuide>? = null,
        val link_track: String? = null,
        val status: Int,
        val result: List<Any>? = null,
        val link_result: String? = null,
        val status_link: String? = null,
        val isBookmark: Int? = 0
) : Serializable