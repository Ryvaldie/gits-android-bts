package id.otsuka.gits.sweatgen.data.model

data class Publisher(val id: Int = 0,
                     val first_name: String? = null,
                     val last_name: String? = null,
                     val email: String? = null,
                     val phone: String? = null
)