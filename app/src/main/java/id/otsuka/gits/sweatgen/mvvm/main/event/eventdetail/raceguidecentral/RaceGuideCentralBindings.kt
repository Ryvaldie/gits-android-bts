package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral;

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.v4.view.ViewPager
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.RaceCentralGuide

/**
 * @author radhikayusuf.
 */

object RaceGuideCentralBindings {

    @BindingAdapter("app:imageUrl", "app:listener", requireAll = false)
    @JvmStatic

    fun loadImage(view: ImageView, imageUrl: Any?, listener: RequestListener<Drawable>?) {

        val finalImage: Any = imageUrl ?: R.drawable.img_slider_event

        Glide.with(view.context)
                .load(finalImage)
                .listener(listener)
                .into(view)
                .apply {
                    RequestOptions().diskCacheStrategy
                }
    }

    @BindingAdapter("app:vpAdapter")
    @JvmStatic
    fun setupViewpagerAdapter(viewPager: ViewPager, pagesContent: List<RaceCentralGuide>) {

        (viewPager.adapter as ViewPagerAdapter).apply {
            replacePagesContent(pagesContent)
        }
    }
}