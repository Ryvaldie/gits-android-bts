package id.otsuka.gits.sweatgen.util

import android.content.Context
import android.preference.PreferenceManager
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.data.source.local.GitsLocalDataSource
import id.otsuka.gits.sweatgen.data.source.remote.GitsRemoteDataSource


/**
 * Created by irfanirawansukirman on 26/01/18.
 */

object Injection {

    fun provideGitsRepository(context: Context) = GitsRepository.getInstance(GitsRemoteDataSource,
            GitsLocalDataSource.getInstance(context, PreferenceManager.getDefaultSharedPreferences(context)))
}