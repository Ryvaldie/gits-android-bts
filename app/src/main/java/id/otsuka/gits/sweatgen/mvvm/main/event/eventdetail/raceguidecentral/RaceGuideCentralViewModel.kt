package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import id.otsuka.gits.sweatgen.data.model.RaceCentralGuide
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class RaceGuideCentralViewModel(context: Application) : AndroidViewModel(context) {
    val obsRaceGuideCentral = ObservableArrayList<RaceCentralGuide>()
    val createIndicator = SingleLiveEvent<Void>()

}