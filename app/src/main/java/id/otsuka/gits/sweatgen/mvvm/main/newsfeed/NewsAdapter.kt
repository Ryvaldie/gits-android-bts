package id.otsuka.gits.sweatgen.mvvm.main.newsfeed

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.HeadlineSectionBinding
import id.otsuka.gits.sweatgen.databinding.ItemNewsfeedHeaderBinding
import id.otsuka.gits.sweatgen.databinding.ItemNewsfeedRegularBinding

class NewsAdapter(val viewmodel: NewsFeedViewModel,
                  var data: List<NewsItemModel>,
                  val listener: NewsFeedUserActionListener,
                  val mContext: Context

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int = data.size.plus(1)

    fun replaceData(data: List<NewsItemModel>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {

            0 -> R.layout.headline_section

            else -> data[position.minus(1)].layoutCode
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderVH -> {
                holder.bind(data[position.minus(1)])
            }
            is RegularVH -> {
                holder.bind(data[position.minus(1)], (position.minus(1) == data.lastIndex))
            }
            is HeadlineVH -> {
                holder.bind(viewmodel.bHeadlineList.isEmpty())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.headline_section -> {
                val binding = HeadlineSectionBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mViewModel = viewmodel
                    mListener = listener
                }

                binding.vpHeadline.apply {

                    adapter = HeadlineAdapter(mContext, ArrayList(0), viewmodel, binding, listener)

                    offscreenPageLimit = 3

                    pageMargin = -(resources.displayMetrics.widthPixels / 15)

                    clipToPadding = false

                    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                        override fun onPageSelected(position: Int) {

                            val linDots = (parent as View).findViewById<LinearLayout>(R.id.lin_dots_news)

                            for (x in 0..viewmodel.bHeadlineList.size.minus(1)) {
                                (linDots.getChildAt(x) as ImageView).apply {
                                    setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_inactive_vp_indicator_race))
                                }
                            }

                            (linDots.getChildAt(position) as ImageView).apply {
                                setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_active_vp_indicator_race))
                            }
                        }

                        override fun onPageScrollStateChanged(state: Int) {

                        }

                        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                        }
                    })
                }
                HeadlineVH(binding)
            }
            R.layout.item_newsfeed_header -> {
                val binding = ItemNewsfeedHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = listener
                }
                HeaderVH(binding)
            }

            else -> {
                val binding = ItemNewsfeedRegularBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = listener
                }
                RegularVH(binding)
            }
        }
    }

    class HeadlineVH(val binding: HeadlineSectionBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(empty: Boolean) {
            binding.hide = empty
        }
    }

    class HeaderVH(val binding: ItemNewsfeedHeaderBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: NewsItemModel) {
            binding.news = obj
        }
    }

    class RegularVH(val binding: ItemNewsfeedRegularBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: NewsItemModel, last: Boolean) {
            binding.news = obj
            binding.last = last

        }
    }
}