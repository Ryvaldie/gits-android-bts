package id.otsuka.gits.sweatgen.data.source.jobs

import android.content.Context
import android.util.Log
import id.otsuka.gits.sweatgen.data.model.*
import id.otsuka.gits.sweatgen.data.source.GetChallengeDashboardDao
import id.otsuka.gits.sweatgen.data.source.GetRunningHistoryHeaderContent
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardModel
import id.otsuka.gits.sweatgen.mvvm.main.profile.ItemType
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunModel
import okhttp3.MultipartBody
import okhttp3.RequestBody

open class GitsRepository(val remoteDataSource: GitsDataSource,
                          val localDataSource: GitsDataSource) : GitsDataSource {


    override fun getHistoryVirtualRun(userToken: String, userId: Int, callback: GitsDataSource.GetVirtualrunListCallback) {
        remoteDataSource.getHistoryVirtualRun("Bearer " + getToken(), userId, object : GitsDataSource.GetVirtualrunListCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(virturuns: List<VirtualRunModel>) {
                callback.onLoaded(virturuns)
            }
        })
    }

    override fun getVirtualRunList(userToken: String, userId: Int, callback: GitsDataSource.GetVirtualrunListCallback) {
        remoteDataSource.getVirtualRunList("Bearer " + getToken(), userId, object : GitsDataSource.GetVirtualrunListCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(virturuns: List<VirtualRunModel>) {
                callback.onLoaded(virturuns)
            }
        })
    }

    override fun saveContinuesRun(id: Int, savedVirtualRun: SavedVirtualRun) {
        localDataSource.saveContinuesRun(id, savedVirtualRun)
    }

    override fun getVirturunLeaderboard(userToken: String, virturunId: Int, callback: GitsDataSource.GetLeaderboardCallback) {
        remoteDataSource.getVirturunLeaderboard("Bearer " + getToken(), virturunId, object : GitsDataSource.GetLeaderboardCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(users: List<LeaderBoardModel>) {
                callback.onLoaded(users)
            }
        })
    }

    override fun getLeaderboard(userToken: String, challengeId: Int, callback: GitsDataSource.GetLeaderboardCallback) {
        remoteDataSource.getLeaderboard("Bearer " + getToken(), challengeId, object : GitsDataSource.GetLeaderboardCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(users: List<LeaderBoardModel>) {
                callback.onLoaded(users)
            }
        })
    }

    override fun getVirtualRunGuide(): List<RaceCentralGuide> {
        return localDataSource.getVirtualRunGuide()
    }

    override fun saveVirtualRunGuide(guide: List<RaceCentralGuide>) {
        localDataSource.saveVirtualRunGuide(guide)
    }

    override fun getVirturunBanner(userToken: String, callback: GitsDataSource.GetVirturunBannerCallback) {
        remoteDataSource.getVirturunBanner("Bearer " + getToken(), object : GitsDataSource.GetVirturunBannerCallback {
            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(data: GetVirturunBannerDao) {
                saveVirtualRunGuide(data.guide_contents)
                callback.onLoaded(data)
            }
        })
    }

    override fun getVirturunById(userToken: String, virturunId: Int, userId: Int, callback: GitsDataSource.GetVirturunByIdCallback) {
        remoteDataSource.getVirturunById("Bearer " + getToken(), virturunId, userId, object : GitsDataSource.GetVirturunByIdCallback {
            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onLoaded(virturun: VirtualRunDetail) {
                callback.onLoaded(virturun)
            }
        })
    }

    override fun getContinuesRunningHistory(userId: Int, challengeId: Int, userToken: String, callback: GitsDataSource.GetContinuesRunningHistoryCallback) {
        remoteDataSource.getContinuesRunningHistory(userId, challengeId, "Bearer " + getToken(), object : GitsDataSource.GetContinuesRunningHistoryCallback {
            override fun onDataNotAvailable(headerData: GetRunningHistoryHeaderContent) {
                callback.onDataNotAvailable(headerData)
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(headerData: GetRunningHistoryHeaderContent, histories: List<HistoryRun>) {
                callback.onLoaded(headerData, histories)
            }
        })
    }

    override fun getAllContinuesRun(): List<SavedVirtualRun> {
        return localDataSource.getAllContinuesRun()
    }

    override fun getContinuesRun(id: Int): SavedVirtualRun? {
        return localDataSource.getContinuesRun(id)
    }

    override fun getStickers(userToken: String, callback: GitsDataSource.GetStickersCallback) {
        remoteDataSource.getStickers("Bearer " + getToken(), object : GitsDataSource.GetStickersCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(stickers: List<String>) {
                callback.onLoaded(stickers)
            }
        })
    }

    override fun getAllNews(userToken: String, callback: GitsDataSource.GetAllNewsCallback) {
        remoteDataSource.getAllNews("Bearer " + getToken(), object : GitsDataSource.GetAllNewsCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(news: List<NewsFeed>) {
                callback.onLoaded(news)
            }
        })
    }

    override fun login(loginParams: LoginParams, callback: GitsDataSource.LoginCallback) {
        remoteDataSource.login(loginParams, object : GitsDataSource.LoginCallback {
            override fun onLoginSuccess(loginDao: LoginDao, msg: String?) {
                callback.onLoginSuccess(loginDao, msg)
            }

            override fun onloginFailed(message: String?) {
                callback.onloginFailed(message)
            }
        })
    }

    override fun bookmarkEvent(userToken: String, params: BookmarkEventParams, callback: GitsDataSource.BookmarkEventCallback) {
        remoteDataSource.bookmarkEvent("Bearer " + getToken(), params, object : GitsDataSource.BookmarkEventCallback {
            override fun onFinish() {
                callback.onFinish()
            }

            override fun postError(errorMessage: String?) {
                callback.postError(errorMessage)
            }

            override fun postSuccess(dao: BookmarkEventDao) {
                callback.postSuccess(dao)
            }
        })
    }

    override fun redeemValid(userToken: String, virturunId: Int, userId: Int, accessCode: String, callback: GitsDataSource.GeneralPostCallback) {
        remoteDataSource.redeemValid("Bearer " + getToken(), virturunId, userId, accessCode, object : GitsDataSource.GeneralPostCallback {
            override fun onFailed(errorMessage: String?) {
                callback.onFailed(errorMessage)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onSuccess(message: String?) {
                callback.onSuccess(message)
            }
        })
    }

    override fun addRunProgress(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: GitsDataSource.AddRunHistoryCallback) {
        remoteDataSource.addRunProgress("Bearer " + getToken(), addHistoryRunParams, object : GitsDataSource.AddRunHistoryCallback {
            override fun postSuccess() {
                callback.postSuccess()
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun postError(errorMessage: String?) {
                callback.postError(errorMessage)
            }
        })
    }

    override fun addRunHistory(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: GitsDataSource.AddRunHistoryCallback) {
        remoteDataSource.addRunHistory("Bearer " + getToken(), addHistoryRunParams, object : GitsDataSource.AddRunHistoryCallback {
            override fun postSuccess() {
                callback.postSuccess()
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun postError(errorMessage: String?) {
                callback.postError(errorMessage)
            }
        })
    }

    override fun register(registerParams: RegisterParams, callback: GitsDataSource.RegisterCallback) {
        remoteDataSource.register(registerParams, object : GitsDataSource.RegisterCallback {
            override fun onRegisterSuccess(registerDao: RegisterDao) {
                callback.onRegisterSuccess(registerDao)
            }

            override fun onRegisterFailed(errorMessage: String?, errorMsg: String?) {
                callback.onRegisterFailed(errorMessage, errorMsg)
            }
        })
    }

    override fun resendCode(tokenForgot: String?, callback: GitsDataSource.ResendCodeCallback) {
        remoteDataSource.resendCode(tokenForgot, object : GitsDataSource.ResendCodeCallback {
            override fun onSendCodeSuccess(msg: String?) {
                callback.onSendCodeSuccess(msg)
            }

            override fun onSendCodeFailed(message: String?) {
                callback.onSendCodeFailed(message)
            }
        })
    }

    override fun forgotPassword(forgotPasswordParams: ForgotPasswordParams, callback: GitsDataSource.ForgotPasswordCallback) {
        remoteDataSource.forgotPassword(forgotPasswordParams, object : GitsDataSource.ForgotPasswordCallback {
            override fun onForgotPasswordSuccess(forgotPasswordDao: ForgotPasswordDao, msg: String?) {
                callback.onForgotPasswordSuccess(forgotPasswordDao, msg)
            }

            override fun onForgotPasswordFailed(errorMessage: String?) {
                callback.onForgotPasswordFailed(errorMessage)
            }
        })
    }

    override fun setNewPassword(newPasswordParams: NewPasswordParams, callback: GitsDataSource.SetNewPasswordCallback) {
        remoteDataSource.setNewPassword(newPasswordParams, object : GitsDataSource.SetNewPasswordCallback {
            override fun onSetNewPasswordSuccess(msg: String?) {
                callback.onSetNewPasswordSuccess(msg)
            }

            override fun onSetNewPasswordFailed(errorMessage: String?) {
                callback.onSetNewPasswordFailed(errorMessage)
            }
        })
    }

    override fun updateUser(accessToken: String?, userId: Int, name: RequestBody?, gender: RequestBody?, dob: RequestBody?, picture: MultipartBody.Part, callback: GitsDataSource.EditProfileCallback) {
        if (getToken() != null)
            remoteDataSource.updateUser("Bearer " + getToken(), userId, name, gender, dob, picture, object : GitsDataSource.EditProfileCallback {
                override fun onUpdateSuccess(user: User, msg: String?) {
                    callback.onUpdateSuccess(user, msg)
                }

                override fun onUpdateFailed(errorMessage: String?) {
                    callback.onUpdateFailed(errorMessage)
                }

                override fun onTokenNull() {

                }

            })
        else
            callback.onTokenNull()
    }

    override fun logout(token: String?, callback: GitsDataSource.LogoutCallback?, isExpired: Boolean) {
        if (getToken() != null)
            if (!isExpired)
                remoteDataSource.logout(getToken(), object : GitsDataSource.LogoutCallback {
                    override fun logoutSuccess() {
                        localDataSource.logout(null, null)
                        callback?.logoutSuccess()
                    }

                    override fun logoutError(errorMessage: String?) {
                        callback?.logoutError(errorMessage)
                    }
                })
            else
                localDataSource.logout(null, null)
    }

    override fun getToken(): String? {
        return localDataSource.getToken()
    }

    override fun saveUser(user: User) {
        localDataSource.saveUser(user)
    }

    override fun saveForgotPassword(forgotPasswordDao: ForgotPasswordDao) {
        localDataSource.saveForgotPassword(forgotPasswordDao)
    }

    override fun postJoinChallenge(userToken: String, challengeId: Int, userId: Int, callback: GitsDataSource.PostJoinChallengeCallback) {
        remoteDataSource.postJoinChallenge("Bearer " + getToken(), challengeId, userId, object : GitsDataSource.PostJoinChallengeCallback {
            override fun onFailed(errorMessage: String?) {
                callback.onFailed(errorMessage)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onSuccess() {
                callback.onSuccess()
            }
        })
    }

    override fun postJoinVirturun(userToken: String, virturunId: Int, userId: Int, callback: GitsDataSource.PostJoinChallengeCallback) {
        remoteDataSource.postJoinVirturun("Bearer " + getToken(), virturunId, userId, object : GitsDataSource.PostJoinChallengeCallback {
            override fun onFailed(errorMessage: String?) {
                callback.onFailed(errorMessage)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onSuccess() {
                callback.onSuccess()
            }
        })
    }

    override fun getLatestAppVersionCode(deviceVersionCode: Int, callback: GitsDataSource.GetLatestAppVersionCallback) {
        remoteDataSource.getLatestAppVersionCode(deviceVersionCode, object : GitsDataSource.GetLatestAppVersionCallback {
            override fun onAppsAlreadyUpdated() {
                callback.onAppsAlreadyUpdated()
            }

            override fun onAppsNeedToUpdate() {
                callback.onAppsNeedToUpdate()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }
        })
    }

    override fun resendEmail(token: String, callback: GitsDataSource.ResendEmailCallback) {
        remoteDataSource.resendEmail(token, object : GitsDataSource.ResendEmailCallback {
            override fun onResendSuccess(msg: String?) {
                callback.onResendSuccess(msg)
            }

            override fun onResendFail(msg: String) {
                callback.onResendFail(msg)
            }
        })
    }

    override fun getPreviousChallenges(userToken: String, userId: Int, callback: GitsDataSource.GetAllChallengeCallback) {
        remoteDataSource.getPreviousChallenges("Bearer " + getToken(), userId, object : GitsDataSource.GetAllChallengeCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(challenges: List<Challenge>) {
                callback.onLoaded(challenges)
            }
        })
    }

    override fun getAvailableChallenges(userToken: String, userId: Int, callback: GitsDataSource.GetAllChallengeCallback) {
        remoteDataSource.getAvailableChallenges("Bearer " + getToken(), userId, object : GitsDataSource.GetAllChallengeCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(challenges: List<Challenge>) {
                callback.onLoaded(challenges)
            }
        })
    }

    override fun getAllNewsByCategory(userToken: String, categoryId: Int, page: Int, callback: GitsDataSource.GetAllNewsCallback) {
        remoteDataSource.getAllNewsByCategory("Bearer " + getToken(), categoryId, page, object : GitsDataSource.GetAllNewsCallback {
            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onLoaded(news: List<NewsFeed>) {
                callback.onLoaded(news)
            }

        })
    }

    override fun getAllNews(userToken: String, page: Int, callback: GitsDataSource.GetAllNewsCallback) {
        remoteDataSource.getAllNews("Bearer " + getToken(), page, object : GitsDataSource.GetAllNewsCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(news: List<NewsFeed>) {
                callback.onLoaded(news)
            }
        })
    }

    override fun getNewsDetail(userToken: String, newsId: Int, callback: GitsDataSource.GetNewsDetailCallback) {
        remoteDataSource.getNewsDetail("Bearer " + getToken(), newsId, object : GitsDataSource.GetNewsDetailCallback {
            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(newsDetail: NewsFeed, similarNews: List<NewsFeed>?) {
                callback.onLoaded(newsDetail, similarNews)
            }
        })
    }

    override fun getNewsFeeds(userToken: String, callback: GitsDataSource.GetNewsFeedsCallback) {
        remoteDataSource.getNewsFeeds("Bearer " + getToken(), object : GitsDataSource.GetNewsFeedsCallback {
            override fun onDataNotAvailable(type: Int) {
                callback.onDataNotAvailable(type)
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(headlines: List<NewsFeed>?, topNews: List<NewsFeed>?) {
                callback.onLoaded(headlines, topNews)
            }
        })
    }

    override fun getChallengeDashboard(userId: Int, userToken: String, callback: GitsDataSource.GetChallengeDashboardCallback) {
        remoteDataSource.getChallengeDashboard(userId, "Bearer " + getToken(), object : GitsDataSource.GetChallengeDashboardCallback {
            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onLoaded(challengeDashboard: GetChallengeDashboardDao) {
                callback.onLoaded(challengeDashboard)
            }
        })
    }

    override fun getChallenges(userToken: String, callback: GitsDataSource.GetChallengesCallback) {
        remoteDataSource.getChallenges("Bearer " + getToken(), object : GitsDataSource.GetChallengesCallback {
            override fun onDataLoaded(daoChallenge: GetChallengesDao) {
                callback.onDataLoaded(daoChallenge)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }
        })
    }

    override fun getRunHistories(userToken: String, userId: Int, page: Int, callback: GitsDataSource.GetRunHistoriesCallback, context: Context) {
        remoteDataSource.getRunHistories("Bearer " + getToken(), userId, page, object : GitsDataSource.GetRunHistoriesCallback {
            override fun onDataLoaded(histories: List<ItemType>, page: Int) {
                callback.onDataLoaded(histories, page)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }
        }, context)
    }

    override fun userValidation(userId: Int, callback: GitsDataSource.UserValidationCallback) {
        remoteDataSource.userValidation(userId, object : GitsDataSource.UserValidationCallback {
            override fun onValidationSuccess() {
                callback.onValidationSuccess()
            }

            override fun onValidationFailed(msg: String) {
                callback.onValidationFailed(msg)
            }
        })
    }

    override fun getEventDetail(token: String?, eventId: Int, userId: Int, callback: GitsDataSource.GetEventDetailCallback) {
        if (getToken() != null)
            remoteDataSource.getEventDetail("Bearer " + getToken(), eventId, userId, object : GitsDataSource.GetEventDetailCallback {
                override fun onEventLoaded(eventDetail: EventDetail) {
                    callback.onEventLoaded(eventDetail)
                }

                override fun onError(errorMessage: String?) {
                    callback.onError(errorMessage)
                }
            })
    }

    override fun getUser(): User? = localDataSource.getUser()

    override fun getForgotPasswordDao(): ForgotPasswordDao = localDataSource.getForgotPasswordDao()

    override fun showUser(token: String?, userId: Int, callback: GitsDataSource.ShowUserCallback) {
        if (getToken() != null)
            remoteDataSource.showUser("Bearer " + getToken(), userId, object : GitsDataSource.ShowUserCallback {
                override fun onUserLoaded(user: User) {
                    callback.onUserLoaded(user)
                }

                override fun onUserLoadFail(errorMessage: String?) {
                    callback.onUserLoadFail(errorMessage)
                }
            })
    }

    override fun getShareLink(userToken: String, eventId: Int, callback: GitsDataSource.GetShareLinkCallback) {
        remoteDataSource.getShareLink("Bearer " + getToken(), eventId, object : GitsDataSource.GetShareLinkCallback {
            override fun onError(errorMsg: String?) {
                callback.onError(errorMsg)
            }

            override fun onFinish() {
                callback.onFinish()
            }

            override fun onSuccess(link: String) {
                callback.onSuccess(link)
            }
        })
    }

    override fun getEventList(token: String?, ownerId: Int, callback: GitsDataSource.GetEventListCallback) {
        if (getToken() != null) {
            remoteDataSource.getEventList("Bearer " + getToken(), ownerId, object : GitsDataSource.GetEventListCallback {
                override fun onEventLoaded(eventList: List<Event>) {
                    callback.onEventLoaded(eventList)
                }

                override fun onDataNotAvailable() {
                    callback.onDataNotAvailable()
                }

                override fun onError(errorMessage: String?) {
                    callback.onError(errorMessage)
                }
            })
        } else
            Log.i("TOKEN", "no token " + getToken())
    }

    override fun getOnBoardImage(callback: GitsDataSource.GetOnBoardImageCallback) {
        remoteDataSource.getOnBoardImage(object : GitsDataSource.GetOnBoardImageCallback {
            override fun onImageLoaded(images: List<OnBoardImage>) {
                callback.onImageLoaded(images)
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

        })
    }

    override fun getImagesGallery(eventId: Int, pid: String, token: String?, callback: GitsDataSource.GetImagesGalleryCallback) {
        if (getToken() != null)
            remoteDataSource.getImagesGallery(eventId, pid, "Bearer " + getToken(), object : GitsDataSource.GetImagesGalleryCallback {
                override fun onImagesLoaded(images: List<ImageGallery>?) {
                    callback.onImagesLoaded(images)
                }

                override fun onDataNotAvailable() {
                    callback.onDataNotAvailable()
                }

                override fun onError(errorMessage: String?) {
                    callback.onError(errorMessage)
                }
            })
    }

    override fun getMedalHistory(userToken: String, userId: Int, page: Int, callback: GitsDataSource.GetMedalCallback) {
        remoteDataSource.getMedalHistory("Bearer " + getToken(), userId, page, object : GitsDataSource.GetMedalCallback {
            override fun onDataLoaded(challengesMedal: List<ChallengeMedal>, currentMedal: CurrentMedal?, page: Int) {
                callback.onDataLoaded(challengesMedal, currentMedal, page)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }
        })
    }

    override fun getChallengeById(userToken: String, idChallenge: Int, userId: Int, callback: GitsDataSource.GetChallengeByIdCallback) {
        remoteDataSource.getChallengeById("Bearer " + getToken(), idChallenge, userId, object : GitsDataSource.GetChallengeByIdCallback {
            override fun onDataLoaded(challenge: Challenge) {
                callback.onDataLoaded(challenge)
            }

            override fun onError(errorMessage: String?) {
                callback.onError(errorMessage)
            }

            override fun onFinish() {
                callback.onFinish()
            }
        })
    }


    /**
     *  ========================================================================================================================
     *
     *                                        PUT
     *
     *  ========================================================================================================================
     */
    override fun updatePersonalBio(userId: String, name: String, gender: String, dob: String, callback: GitsDataSource.UpdatePersonalBioCallback) {
        remoteDataSource.updatePersonalBio(userId, name, gender, dob, object : GitsDataSource.UpdatePersonalBioCallback {
            override fun onUpdateSuccess(accessToken: String?) {
                callback.onUpdateSuccess(accessToken)
            }

            override fun onUpdateFailed(msg: String) {
                callback.onUpdateFailed(msg)
            }
        })
    }

    /**
     *  ========================================================================================================================
     *
     *                                        ADDITIONAL FUNCTION
     *
     *  ========================================================================================================================
     */
    override fun clearDisposable() {
        remoteDataSource.clearDisposable()
    }

    companion object {

        private var INSTANCE: GitsRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.

         * @param gitsRemoteDataSourcethe backend data source
         * *
         * @return the [GitsRepository] instance
         */
        @JvmStatic
        fun getInstance(gitsRemoteDataSource: GitsDataSource, gitsLocalDataSource: GitsDataSource) =
                INSTANCE
                        ?: synchronized(GitsRepository::class.java) {
                            INSTANCE
                                    ?: GitsRepository(gitsRemoteDataSource, gitsLocalDataSource)
                                            .also { INSTANCE = it }
                        }

        /**
         * Used to force [getInstance] to create a new instance
         * next time it's called.
         */
        @JvmStatic
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}