package id.otsuka.gits.sweatgen.base

import android.app.Service
import android.content.Intent
import android.os.IBinder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


/**
 * Created by irfanirawansukirman on 14/12/17.
 */
abstract class BaseService : Service() {

    private var compositeDisposable: CompositeDisposable? = null

    override fun onCreate() {
        super.onCreate()

        initCompositeDisposable()
    }

    override fun onDestroy() {

        clearSubscribe()

        super.onDestroy()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    /**
     * Initiate composite disposable rx java
     */
    private fun initCompositeDisposable() {

        if (compositeDisposable == null) {

            compositeDisposable = CompositeDisposable()
        }
    }

    /**
     * Clear subscribe when app has destroyed
     */
    private fun clearSubscribe() {

        if (compositeDisposable != null) {

            compositeDisposable?.clear()
        }
    }

    /**
     * Register the disposable for subscribe process call api
     */
    fun addSubscribe(disposable: Disposable) {

        compositeDisposable?.add(disposable)

    }
}