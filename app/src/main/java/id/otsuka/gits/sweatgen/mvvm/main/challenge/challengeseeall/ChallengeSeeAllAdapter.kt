package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.ItemChallengeSeeallBinding

class ChallengeSeeAllAdapter(val context: Context,
                             val challenges: ArrayList<Challenge>,
                             val mViewModel: ChallengeSeeAllViewModel,
                             val listener: ChallengeSeeAllUserActionListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceData(data: List<Challenge>) {
        challenges.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = challenges.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ItemHolder).bind(challenges[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemHolder(ItemChallengeSeeallBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = listener
        })
    }

    class ItemHolder(val mBinding: ItemChallengeSeeallBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(obj: Challenge) {
            mBinding.challenge = obj
        }
    }
}