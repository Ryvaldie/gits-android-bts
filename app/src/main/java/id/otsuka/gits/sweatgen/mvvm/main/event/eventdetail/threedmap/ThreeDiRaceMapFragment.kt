package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.threedmap


import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import id.otsuka.gits.sweatgen.databinding.FragmentThreeDiRaceMapBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.threedmap.ThreeDiRaceMapViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.withArgs
import kotlinx.android.synthetic.main.fragment_three_di_race_map.*


class ThreeDiRaceMapFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentThreeDiRaceMapBinding
    lateinit var mViewModel: ThreeDiRaceMapViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentThreeDiRaceMapBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            bUrl.set(arguments?.getString(MAP_URL, ""))
        }

        mViewDataBinding.mListener = object : ThreeDiRaceMapUserActionListener {

        }



        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webview_3d.webViewClient = object : android.webkit.WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)

                if (progressBar1 != null)
                    progressBar1.visibility = View.GONE
            }
        }

        toolbar_3d.setNavigationOnClickListener { activity!!.onBackPressed() }
    }

    fun obtainViewModel(): ThreeDiRaceMapViewModel = obtainViewModel(ThreeDiRaceMapViewModel::class.java)

    companion object {
        val MAP_URL = "mapUrl"
        fun newInstance(mapUrl: String) = ThreeDiRaceMapFragment().withArgs {
            putString(MAP_URL, mapUrl)
        }

    }

}
