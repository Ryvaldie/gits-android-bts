package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail


import android.arch.lifecycle.Observer
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.databinding.FragmentEventDetailBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.eventrundown.EventRundownFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery.GalleryFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.howtogetthere.HowToGetThereFragmentDialog
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.livetracker.LiveTrackerFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment.Companion.RACE_CENTRAL
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment.Companion.RACE_GUIDE
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceresult.RaceResultFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.threedmap.ThreeDiRaceMapFragment
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import kotlinx.android.synthetic.main.fragment_event_detail.*

class EventDetailFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentEventDetailBinding
    lateinit var mViewModel: EventDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentEventDetailBinding.inflate(inflater, container!!, false)

        mViewModel = (activity as EventDetailActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

            eventId.set((requireActivity() as EventDetailActivity).eventId)

            getEventDetail()

            bExpanded.set(false)
        }

        mViewDataBinding.mListener = object : EventDetailUserActionListener {
            override fun raceCentral() {

                if (mViewModel.obsEeventDetail.get()?.race_central != null)

                    if (mViewModel.obsEeventDetail.get()?.race_central!!.isEmpty())
                        showDataNotAvailable()
                    else {

                        val jsonList = GsonBuilder().setPrettyPrinting().create().toJson(mViewModel.obsEeventDetail.get()?.race_central)

                        requireActivity().replaceFragmentAndAddToBackStack(
                                RaceGuideCentralFragment.newInstance(jsonList, RACE_CENTRAL), R.id.frame_main_content)
                    }
                else
                    showDataNotAvailable()
            }

            override fun raceGuide() {
                if (mViewModel.obsEeventDetail.get()?.race_guide != null)
                    if (mViewModel.obsEeventDetail.get()?.race_guide!!.isEmpty())
                        showDataNotAvailable()
                    else {

                        val jsonList = GsonBuilder().setPrettyPrinting().create().toJson(mViewModel.obsEeventDetail.get()?.race_guide)

                        requireActivity().replaceFragmentAndAddToBackStack(
                                RaceGuideCentralFragment.newInstance(jsonList, RACE_GUIDE), R.id.frame_main_content)
                    }
                else
                    showDataNotAvailable()

            }

            override fun seeMore() {
                tv_desc_event.expandCollapse()

                if (mViewDataBinding.tvDescEvent.maxLines > 5) {
                    mViewDataBinding.tvBtnExpandCollapse.text = getString(R.string.text_see_more)
                } else
                    mViewDataBinding.tvBtnExpandCollapse.text = getString(R.string.text_less_more)
            }

            override fun back() {
                requireActivity().onBackPressed()
            }

            override fun registerEvent() {
                if (mViewModel.obsEeventDetail.get()?.status == 0)
                    Snackbar.make(view!!, R.string.text_ticket_sold_out, Snackbar.LENGTH_LONG).show()
                else {

                    requireActivity().replaceFragmentAndAddToBackStack(LiveTrackerFragment.newInstance(mViewModel.obsEeventDetail.get()?.status_link
                            ?: "", R.string.text_register_event), R.id.frame_main_content)

                }
            }

            override fun eventSchedule() {
                if (mViewModel.obsEeventDetail.get()?.schedule != null) {
                    if (mViewModel.obsEeventDetail.get()?.schedule!!.isNotEmpty()) {

                        val eventString = GsonBuilder().setPrettyPrinting().create().toJson(mViewModel.obsEeventDetail.get()?.schedule)

                        requireActivity().replaceFragmentAndAddToBackStack(EventRundownFragment.newInstance(eventString), R.id.frame_main_content)

                    } else
                        showDataNotAvailable()
                } else
                    showDataNotAvailable()

            }

            override fun getThere() {
                if (mViewModel.obsLocation.isEmpty())
                    showDataNotAvailable()
                else
                    requireActivity().showImageDialog(HowToGetThereFragmentDialog())
            }

            override fun parkingPoint() {
                if (!mViewModel.obsEeventDetail.get()?.link_map.isNullOrEmpty())
                    requireActivity().replaceFragmentAndAddToBackStack(ThreeDiRaceMapFragment.newInstance(mViewModel.obsEeventDetail.get()?.link_map!!), R.id.frame_main_content)
                else
                    showDataNotAvailable()
            }

            override fun galleryPhoto() {
                if (mViewModel.obsEeventDetail.get()?.link_gallery != null)
                    activity!!.replaceFragmentAndAddToBackStack(GalleryFragment.newInstance(mViewModel.obsEeventDetail.get()?.link_gallery!!), R.id.frame_main_content)
                else
                    showDataNotAvailable()
            }

            override fun liveTracker() {
                if (!mViewModel.obsEeventDetail.get()?.link_track.isNullOrEmpty()) {
                    activity!!.replaceFragmentAndAddToBackStack(LiveTrackerFragment.newInstance(mViewModel.obsEeventDetail.get()?.link_track!!), R.id.frame_main_content)
                } else {
                    showDataNotAvailable()
                }
            }

            override fun raceResult() {
                if (mViewModel.obsEeventDetail.get()?.link_result.isNullOrEmpty())
                    getString(R.string.text_race_will_be).showSnackBar(mViewDataBinding.root)
                else
                    requireActivity().replaceFragmentAndAddToBackStack(RaceResultFragment.newInstance(mViewModel.obsEeventDetail.get()?.link_result!!), R.id.frame_main_content)
            }

            override fun call() {
                if (!mViewModel.obsEeventDetail.get()?.contact?.phone.isNullOrEmpty()) {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mViewModel.obsEeventDetail.get()?.contact!!.phone!!))
                    context!!.startActivity(intent)
                } else
                    showDataNotAvailable()
            }

            override fun mail() {
                if (!mViewModel.obsEeventDetail.get()?.contact?.email.isNullOrEmpty()) {
                    val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mViewModel.obsEeventDetail.get()?.contact!!.email!!, null))
                    context!!.startActivity(intent)
                } else
                    showDataNotAvailable()
            }

            override fun web() {
                if (!mViewModel.obsEeventDetail.get()?.contact?.website.isNullOrEmpty()) {
                    try {
                        val url = mViewModel.obsEeventDetail.get()?.contact?.website

                        if (Patterns.WEB_URL.matcher(url).matches()) {

                            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))

                            context!!.startActivity(intent)

                        } else
                            showDataNotAvailable()

                    } catch (e: Exception) {
                        showDataNotAvailable()
                    }
                } else
                    showDataNotAvailable()
            }

            override fun bookmark() {
                mViewModel.bBookmarked.set(!mViewModel.bBookmarked.get())
                mViewModel.bookmarkEvent()

            }

            override fun share() {
                mViewModel.getShareLink()
            }

        }

        return mViewDataBinding.root

    }

    private fun showDataNotAvailable() {
        getString(R.string.text_data_not_available).showSnackBar(mViewDataBinding.root)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.snackbarMsg.observe(this, Observer {
            it?.showCustomSnackBar(mViewDataBinding.root, color = ContextCompat.getColor(context!!, R.color.colorGpsActivated))
        })

        mViewModel.snackbarError.observe(this, Observer {
            if (it != null) {
                if (it == Other.TOKEN_EXPIRE) {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                } else {
                    it.showSnackBar(mViewDataBinding.root)
                }
            }
        })
        mViewModel.eventLoading.observe(this, Observer { it ->
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {
                Handler().postDelayed({
                    hideProgressDialog()
                }, 300)

                if (!mViewModel.bMessage.get().isNullOrEmpty()) {
                    if (mViewModel.bMessage.get() == TOKEN_EXPIRE) {
                        mViewModel.repository.logout(null, null, true)
                        activity!!.redirectToLogin()
                    } else
                        mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
                }

            }
        })

        mViewModel.shareEvent.observe(this, Observer {
            Handler().postDelayed({
                context!!.shareContent(url = it
                        ?: "", title = mViewModel.obsEeventDetail.get()?.name
                        ?: "")
            }, 700)
        })

        mViewModel.loadingShare.observe(this, Observer {
            if (it != null) {
                if (it) {
                    showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
                } else {
                    Handler().postDelayed({
                        hideProgressDialog()
                    }, 1000)

                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_event_details)
        (activity as AppCompatActivity).supportActionBar!!.apply {
            title = getString(R.string.text_event_detail)
        }
        toolbar_event_details.setNavigationOnClickListener { mViewDataBinding.mListener?.back() }

    }

    companion object {
        fun newInstance() = EventDetailFragment().apply {

        }

    }

}

