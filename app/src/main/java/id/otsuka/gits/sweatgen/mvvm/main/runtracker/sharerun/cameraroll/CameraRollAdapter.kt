package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll

import android.net.Uri
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import id.otsuka.gits.sweatgen.databinding.ItemImageGalleryBinding
import java.io.File

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class CameraRollAdapter(val data: ArrayList<String>,
                        val mListener: CameraRollUserActionListener,
                        val activity: FragmentActivity,
                        val vm: CameraRollViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = data.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val mViewBinding = ItemImageGalleryBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = this@CameraRollAdapter.mListener
        }

        mViewBinding.apply {
            val displayMetrics = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(displayMetrics)
            val width = displayMetrics.widthPixels
            val layoutParam = FrameLayout.LayoutParams(width, width.div(3))
            imgv.layoutParams = layoutParam
        }

        return ImageVH(mViewBinding)
    }

    fun replaceData(data: List<String>) {
        if (data.isNotEmpty()) {
            this.data.apply {
                clear()
                addAll(data)
            }
            notifyDataSetChanged()
            mListener.onItemClick(this.data[0], 0)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ImageVH).bind(data[position], (vm.bSelectedPos == position), position)
    }

    class ImageVH(val mViewBinding: ItemImageGalleryBinding) : RecyclerView.ViewHolder(mViewBinding.root) {

        fun bind(obj: String, isSelected: Boolean, position: Int) {
            val file = File(obj)
            val uri = Uri.fromFile(file)
            mViewBinding.path = obj
          //  mViewBinding.image = uri
            mViewBinding.pos = position
            mViewBinding.selected = isSelected
        }
    }

}