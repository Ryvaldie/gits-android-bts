package id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.support.v4.content.ContextCompat
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class PersonalBioViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val bDateOfBirth = ObservableField("")
    val bFullName = ObservableField("")
    val bGender = ObservableField(0)
    val bStringGender = ObservableField("")
    val bUserId = ObservableInt(0)
    val eventLoading = SingleLiveEvent<Boolean>()
    val bMessage = ObservableField("")
    val isSuccess = ObservableBoolean(false)

    fun validateFields(): Boolean = (!bFullName.get().isNullOrEmpty() && bGender.get() != 0 && !bDateOfBirth.get().isNullOrEmpty())

    fun setGender() {
        if (bGender.get() == R.id.radio_male)
            bStringGender.set(context.getString(R.string.text_male))
        else
            bStringGender.set(context.getString(R.string.text_female))
    }

    fun getTempUser() {
        bUserId.set((repository.getUser() ?: User()).id)
    }

    fun updatePersonalBio() {
        eventLoading.value = true
        repository.updatePersonalBio(bUserId.get().toString(), bFullName.get()!!, bStringGender.get()!!, bDateOfBirth.get()!!, object : GitsDataSource.UpdatePersonalBioCallback {
            override fun onUpdateSuccess(accessToken: String?) {
                repository.saveUser(User(token = accessToken, id = bUserId.get()))
                isSuccess.set(true)
                eventLoading.value = false
            }

            override fun onUpdateFailed(msg: String) {
                isSuccess.set(false)
                bMessage.set(msg)
                eventLoading.value = false
            }
        })
    }
}