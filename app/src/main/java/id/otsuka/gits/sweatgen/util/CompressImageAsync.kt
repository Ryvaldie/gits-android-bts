package id.otsuka.gits.sweatgen.util

import android.os.AsyncTask
import android.util.Log
import java.io.File

class CompressImageAsync(delegate: AsyncResponse) : AsyncTask<String, Void, File>() {

    override fun doInBackground(vararg params: String?): File? {
        var file = File(params[0])

        val length = file.length()

        if (length.div(1024) >= 1024) {
            do {

                Log.d("File Length", file.length().div(1024).toString())

                file = file.reduceImageSize()

            } while (file.length().div(1024) >= 1024)
        }
        return file
    }

    var delegate: AsyncResponse? = null

    // you may separate this or combined to caller class.
    interface AsyncResponse {
        fun processFinish(output: File?)
    }

    init {
        this.delegate = delegate
    }

    override fun onPostExecute(result: File?) {
        delegate!!.processFinish(result)
    }
}