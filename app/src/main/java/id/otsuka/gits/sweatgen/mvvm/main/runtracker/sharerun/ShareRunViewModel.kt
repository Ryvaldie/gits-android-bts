package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.AddHistoryRunParams


class ShareRunViewModel(context: Application) : AndroidViewModel(context) {
    val bCapturedImage = ObservableField<Any>()
    val bEditedImage = ObservableField<Any>()
    val obsAddRunHistory = ObservableField<AddHistoryRunParams>()
}


/**
 * TODO add to bindings class
 * function for bindingsList
 *
 * object ShareRunBindings {
 *
 * 	@BindingAdapter("app:listDataShareRun")
 *     @JvmStatic
 *     fun setListDataShareRun(recyclerView: RecyclerView, data: List<ShareRunModel>) {
 *         with(recyclerView.adapter as ShareRunAdapter) {
 *             replaceData(data)
 *         }
 *     }
 *
 * }
 *
 **/