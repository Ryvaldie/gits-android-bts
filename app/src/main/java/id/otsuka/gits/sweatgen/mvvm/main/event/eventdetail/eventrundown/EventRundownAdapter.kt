package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.eventrundown


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.EventRundown
import id.otsuka.gits.sweatgen.databinding.ItemEventRundownBinding


class EventRundownAdapter(data: List<EventRundown>, viewModel: EventRundownViewModel) : RecyclerView.Adapter<EventRundownAdapter.EventRundownItem>() {

    var mData = data
    val mViewModel = viewModel

    override fun onBindViewHolder(holder: EventRundownItem, position: Int) {
        holder!!.bind(mData[position], mViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventRundownItem {
        val binding = ItemEventRundownBinding.inflate(LayoutInflater.from(parent!!.context), parent!!, false)
        return EventRundownItem(binding)
    }

    override fun getItemCount(): Int {
        return mData.size
    }


    fun replaceData(data: List<EventRundown>) {
        mData = data
        notifyDataSetChanged()
    }


    class EventRundownItem(binding: ItemEventRundownBinding) : RecyclerView.ViewHolder(binding.root) {
        val mBinding = binding;

        fun bind(data: EventRundown, viewModel: EventRundownViewModel) {
            mBinding.mData = data
        }

    }

}