package id.otsuka.gits.sweatgen.data.model

import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeType
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeType.Companion.ITEM_TYPE

data class Challenge(
        val id: Int = 0,
        val name: String? = "",
        val from_date_challenge: String? = "",
        val end_date_challenge: String? = "",
        val description: String? = "",
        val category: String? = "",
        val duration: Int = 0,
        val distance: Double = 0.0,
        val point: Int = 0,
        val logo: String? = "",
        val banner: String? = "",
        val address: String? = "",
        var mItemType: Int = ITEM_TYPE,
        var mChallengeTitle: String = "",
        val days_left: String? = "",
        val is_continues: Int? = 0,
        val run_status: String? = null,
        val distance_sum: Double? = 0.0,
        val distance_total: Double? = 0.0,
        val participant: String? = null,
        val leaderboard: String? = null,
        val duration_sum:Long?=null

) : ChallengeType {

    override fun getItemType(): Int = mItemType

    override fun setItemType(itemType: Int) {
        mItemType = itemType
    }

    override fun setTitle(title: String) {
        mChallengeTitle = title
    }
}