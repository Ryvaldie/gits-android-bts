package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.graphics.Bitmap


class CameraRollViewModel(context: Application) : AndroidViewModel(context) {
    val bSelectedImage = ObservableField<Bitmap>()
    var bSelectedPos = -1
    val isOpen = ObservableField(true)

    val listImages = ObservableArrayList<String>()
}


/**
 * TODO add to bindings class
 * function for bindingsList
 *
 * object CameraRollBindings {
 *
 * 	@BindingAdapter("app:listDataCameraRoll")
 *     @JvmStatic
 *     fun setListDataCameraRoll(recyclerView: RecyclerView, data: List<CameraRollModel>) {
 *         with(recyclerView.adapter as CameraRollAdapter) {
 *             replaceData(data)
 *         }
 *     }
 *
 * }
 *
 **/