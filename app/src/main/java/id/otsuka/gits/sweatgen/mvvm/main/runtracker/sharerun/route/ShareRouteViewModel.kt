package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.route;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.otsuka.gits.sweatgen.util.convertToLatLng


class ShareRouteViewModel(context: Application) : AndroidViewModel(context) {
    val listLocations = ObservableArrayList<Location?>()
    val listLatLng = ObservableArrayList<LatLng?>()

    fun start(jsonLocation: String) {

        try {
            val collectionType = object : TypeToken<List<Location?>>() {}.type
            val locations = Gson().fromJson<ArrayList<Location?>>(jsonLocation, collectionType)
            listLocations.clear()
            listLocations.addAll(locations)
        } catch (e: Exception) {
        }

        listLatLng.clear()
        listLatLng.addAll(listLocations.convertToLatLng())
    }
}