package id.otsuka.gits.sweatgen.mvvm.onboard


import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentOnBoardBinding
import id.otsuka.gits.sweatgen.mvvm.onboard.login.LoginFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.register.RegisterFragment
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.*
import kotlinx.android.synthetic.main.fragment_on_board.*
import java.util.*

class OnBoardFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentOnBoardBinding
    lateinit var mViewModel: OnBoardViewModel
    lateinit var swipeTimer: Timer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentOnBoardBinding.inflate(inflater, container, false)

        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            start()
        }

        mViewDataBinding.mListener = object : OnBoardUserActionListener {
            override fun onSignupClick() {
                activity?.replaceFragmentAndAddToBackStack(RegisterFragment.newInstance(), R.id.frame_main_content)
            }

            override fun onSigninClick() {
                activity?.replaceFragmentAndAddToBackStack(LoginFragment.newInstance(), R.id.frame_main_content)
            }
        }

        return mViewDataBinding.root
    }

    fun obtainViewModel(): OnBoardViewModel = obtainViewModel(OnBoardViewModel::class.java)

    override fun onPause() {
        super.onPause()
        swipeTimer.cancel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.eventLoading.observe(this, android.arch.lifecycle.Observer { it ->
            if (it!!) {

            } else {
                mViewDataBinding.linVpIndicator.setupViewPagerIndicator(mViewModel.pagesContent.size)
            }
        })

        mViewModel.snackbarMsg.observe(this, android.arch.lifecycle.Observer {
            (it ?: "-").showSnackBar(mViewDataBinding.root)
        })
    }

    private fun slideShow() {

        swipeTimer = Timer()
        swipeTimer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                activity?.runOnUiThread {
                    mViewDataBinding.vpOnboard.apply {
                        try {
                            currentItem = if (currentItem == adapter?.count?.minus(1))
                                0
                            else
                                currentItem.plus(1)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    }
                }
            }
        }, 4000, 4000)
    }

    override fun onResume() {
        super.onResume()
        slideShow()
    }

    override fun onStart() {
        super.onStart()

    }

    companion object {
        fun newInstance() = OnBoardFragment().apply {

        }

    }

}
