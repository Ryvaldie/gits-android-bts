package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.data.model.Challenge

object ChallengeSeeAllBindings {

    @BindingAdapter("app:seeAllItems")
    @JvmStatic
    fun setupAllItems(recyclerView: RecyclerView, data: List<Challenge>) {
        with(recyclerView.adapter as ChallengeSeeAllAdapter) {
           replaceData(data)
        }
    }

}