package id.otsuka.gits.sweatgen.mvvm.main.runtracker.metrics


import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentMetricsTrackerModeBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerViewModel
import id.otsuka.gits.sweatgen.util.Other.INTENT_EXTRA_LAST_DURATION
import id.otsuka.gits.sweatgen.util.Other.INTENT_START_TIMER

class MetricsTrackerModeFragment : Fragment() {

    val TAG = MetricsTrackerModeFragment::class.java.simpleName

    lateinit var mViewDataBinding: FragmentMetricsTrackerModeBinding

    lateinit var mViewModel: RunTrackerViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentMetricsTrackerModeBinding.inflate(inflater, container, false)

        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel = (activity as RunTrackerActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel


        (activity as RunTrackerActivity).obtainViewModel().startPauseEvent.observe(this, Observer { state ->

            if (!state!!) {
                mViewModel.trackerMode.set(false)
                mViewModel.afterPause = true

            } else {
                mViewModel.trackerMode.set(true)
            }
            val intent = Intent(INTENT_START_TIMER)

            intent.putExtra("state", state)

            intent.putExtra(INTENT_EXTRA_LAST_DURATION, (activity as RunTrackerActivity).obtainViewModel().bLastDuration.get())

            LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent)

            (activity as RunTrackerActivity).obtainViewModel().bLastDuration.set(0)

        })
    }

    companion object {
        fun newInstance() = MetricsTrackerModeFragment().apply {

        }

    }

}
