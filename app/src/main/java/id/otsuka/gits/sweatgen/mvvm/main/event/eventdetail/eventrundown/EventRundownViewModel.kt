package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.eventrundown;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import id.otsuka.gits.sweatgen.data.model.EventRundown


class EventRundownViewModel(context: Application) : AndroidViewModel(context) {

    val eventRundownList: ObservableList<EventRundown> = ObservableArrayList()

}