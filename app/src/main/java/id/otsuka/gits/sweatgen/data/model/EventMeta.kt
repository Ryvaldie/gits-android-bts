package id.otsuka.gits.sweatgen.data.model

import java.io.Serializable

data class EventMeta(val logo: String? = null,
                     val banner: String? = null
):Serializable