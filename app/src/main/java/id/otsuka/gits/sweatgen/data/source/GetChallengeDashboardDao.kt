package id.otsuka.gits.sweatgen.data.source

import com.google.gson.annotations.SerializedName
import id.otsuka.gits.sweatgen.data.model.Challenge

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class GetChallengeDashboardDao(val highlight: List<Challenge>? = null,
                                    @SerializedName("myChallanges")
                                    val myChallenges: List<Challenge>? = null,
                                    @SerializedName("allChallanges")
                                    val allChallenges: List<Challenge>? = null,
                                    @SerializedName("previousChallanges")
                                    val prevChallenges: List<Challenge>? = null
)