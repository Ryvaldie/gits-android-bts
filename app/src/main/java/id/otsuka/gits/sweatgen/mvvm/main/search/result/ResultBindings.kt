package id.otsuka.gits.sweatgen.mvvm.main.search.result;

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.data.model.Challenge

/**
 * @author radhikayusuf.
 */

object ResultBindings {

    @BindingAdapter("app:result")
    @JvmStatic
    fun setResult(recyclerView: RecyclerView, result: ArrayList<Challenge>?) {
        if (result != null) {
            recyclerView.apply {
                (adapter as RecyclerAdapter).replaceResult(result)
            }
        }
    }
}