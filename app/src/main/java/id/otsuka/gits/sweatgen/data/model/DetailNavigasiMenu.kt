package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class DetailNavigasiMenu(
        val id: Int,
        val title: String
)