package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.otsuka.gits.sweatgen.databinding.FragmentStickersBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.sharefinalpage.ShareFinalPageFragment
import id.otsuka.gits.sweatgen.util.GridSpacesItemDecoration
import id.otsuka.gits.sweatgen.util.dpToPx
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.showSnackBar

class StickersFragment : Fragment(), StickersUserActionListener {

    lateinit var mViewDataBinding: FragmentStickersBinding
    lateinit var mViewModel: StickersViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentStickersBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            snackbarMsg.observe(this@StickersFragment, Observer {
                it?.showSnackBar(mViewDataBinding.root)
            })
            start()
        }

        mViewDataBinding.mListener = this@StickersFragment

        return mViewDataBinding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
    }

    private fun setupRecycler() {
        mViewDataBinding.recyclerStickers.apply {
            layoutManager = GridLayoutManager(context, 6)

            addItemDecoration(GridSpacesItemDecoration(2.dpToPx(context), true))

            val parentFragment = parentFragment as ShareEditImageFragment

            adapter = StickerAdapter(ArrayList(), parentFragment.mViewModel)
        }
    }

    override fun onItemClick(url: String) {

    }


    // TODO import obtainViewModel & add StickersViewModel to ViewModelFactory
    fun obtainViewModel(): StickersViewModel = obtainViewModel(StickersViewModel::class.java)

    companion object {
        fun newInstance() = StickersFragment().apply {

        }

    }

}
