package id.otsuka.gits.sweatgen.mvvm.main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentMainBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainViewModel


class MainFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentMainBinding
    lateinit var mViewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentMainBinding.inflate(inflater!!, container!!, false)
        mViewModel = (activity as MainActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : MainUserActionListener {
            override fun onSearchClick() {

            }

            override fun onTryAgain() {

            }
        }



        return mViewDataBinding.root

    }

    companion object {
        fun newInstance() = MainFragment().apply {

        }

    }

}
