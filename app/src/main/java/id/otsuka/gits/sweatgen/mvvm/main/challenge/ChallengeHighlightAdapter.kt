package id.otsuka.gits.sweatgen.mvvm.main.challenge

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.ItemHighlightChallengeBinding
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail.ChallengeDetailActivity

class ChallengeHighlightAdapter(val context: Context,
                                var mData: ArrayList<Challenge>
) : PagerAdapter() {

    val mListener = object : ItemClickListener {
        override fun itemOnClick(idChallenge: Int) {
            ChallengeDetailActivity.startActivity(idChallenge, context)
        }
    }

    fun replaceData(data: List<Challenge>) {
        mData.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun getCount(): Int = mData.size

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val mViewBinding = ItemHighlightChallengeBinding.inflate(LayoutInflater.from(container.context), container, false).apply {
            challenge = mData[position]
            mListener = this@ChallengeHighlightAdapter.mListener
        }
        container.addView(mViewBinding.root)
        return mViewBinding.root
    }

    interface ItemClickListener {
        fun itemOnClick(idChallenge: Int)
    }
}
