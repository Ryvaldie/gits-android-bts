package id.otsuka.gits.sweatgen.util.customView

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.widget.LinearLayout
import id.otsuka.gits.sweatgen.mvvm.main.event.ViewPagerAdapter.Companion.BIG_SCALE

/**
 * Created by azkiyaq on 3/16/18.
 */
/**
 * Created by Azkiya Qolbi on 11/22/2017.
 */
class CarouselLinearLayout(context: Context, attrs: AttributeSet)
    : LinearLayout(context, attrs) {
    private var scale = BIG_SCALE

    fun setScaleBoth(scale: Float) {
        this.scale = scale
        this.invalidate()
    }

    override fun onDraw(canvas: Canvas?) {

        if (canvas != null) {
            val w = this.width.toFloat()
            val h = this.height.toFloat()
            canvas.scale(scale, scale, w / 2, h / 2)

        }
        super.onDraw(canvas)
    }
}
