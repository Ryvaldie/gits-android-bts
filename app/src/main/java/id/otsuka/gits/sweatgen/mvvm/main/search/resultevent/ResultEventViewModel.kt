package id.otsuka.gits.sweatgen.mvvm.main.search.resultevent;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.util.searchEventFromList


class ResultEventViewModel(context: Application) : AndroidViewModel(context) {

    val resultEventList = ObservableArrayList<Event>()

    fun start(result: ArrayList<Event>) {
        resultEventList.apply {

            clear()
            addAll(result)
        }
    }

    fun search(temp: ArrayList<Event>, keyword: String) {
        if (keyword.isEmpty())
            resultEventList.clear()
        else
            if (temp.isNotEmpty()) {
                resultEventList.apply {
                    clear()
                    if (keyword.isEmpty()) {
                        addAll(temp)
                    } else {
                        addAll(temp.searchEventFromList(keyword))
                    }
                }
            }
    }

}