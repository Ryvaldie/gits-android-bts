package id.otsuka.gits.sweatgen.mvvm.main.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.ItemSeeallNewsBinding
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.SeeAllNewsUserActionListener

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class SearchNewsAdapter(var data: List<NewsItemModel>,
                        val vm: SearchViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = data.size

    fun replaceData(data: List<NewsItemModel>){
        this.data = data
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NewsHolder).bind(data[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsHolder(ItemSeeallNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = object : SeeAllNewsUserActionListener {
                override fun onItemClick(newsId: Int) {
                    vm.moveToNewsDetail.value = newsId
                }
            }
        })
    }

    class NewsHolder(val mViewBinding: ItemSeeallNewsBinding) : RecyclerView.ViewHolder(mViewBinding.root) {
        fun bind(obj: NewsItemModel) {
            mViewBinding.news = obj
        }
    }
}