package id.otsuka.gits.sweatgen.data.model

data class GetShareLinkDao(val content: ShareLink)