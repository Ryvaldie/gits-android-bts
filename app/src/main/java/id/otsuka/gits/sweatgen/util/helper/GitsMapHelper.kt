package id.otsuka.gits.sweatgen.util.helper

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerViewModel
import id.otsuka.gits.sweatgen.util.bitmapDescriptorFromVector
import id.otsuka.gits.sweatgen.util.checkLocationPermission
import id.otsuka.gits.sweatgen.util.convertToLatLng
import java.util.*

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class GitsMapHelper(val mGoogleMap: GoogleMap,
                    val mViewModel: RunTrackerViewModel,
                    val context: Context,
                    val mListener: GitsMapCallback
) {

    val TAG = GitsMapHelper::class.java.simpleName

    private var userPositionMarker: Marker? = null

    var runningPathPolyline: Polyline? = null

    private var zoomable = true

    var zoomBlockingTimer: Timer? = null

    var didInitialzoom = false

    fun updateLocation(location: Location, state: Boolean) {
        if (state) {
            if (mViewModel.obsLocations.isNotEmpty())
                if (mViewModel.obsLocations.last()!!.distanceTo(location) > 15 && mViewModel.afterPause) {

                    mViewModel.afterPause = false

                    mViewModel.obsLocations.add(null)

                    mViewModel.obsLocations.add(location)

                    runningPathPolyline = null
                } else {
                    mViewModel.obsLocations.add(location)
                }
            else {
                mViewModel.obsLocations.add(location)
            }

        }

        drawUserPositionMarker(location)

        zoomMapTo(location)

        addPolyline()

        addPolyline()

        if (mViewModel.obsLocations.isNotEmpty())
            if (mViewModel.obsLocations.size < 2)
                drawStartPositionMarker()

        if (mViewModel.obsLocations.size > 1) {
            mListener.calculateData()
        }
    }

    fun setupMap() {
        mGoogleMap.apply {
            mGoogleMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
            uiSettings.isZoomControlsEnabled = false
            if (context.checkLocationPermission()) {
                isMyLocationEnabled = false
            }
            uiSettings.isCompassEnabled = true
            setOnCameraMoveStartedListener {
                if (it == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    Log.d(TAG, "onCameraMoveStarted after user's zoom action")

                    zoomable = false
                    if (zoomBlockingTimer != null) {
                        zoomBlockingTimer!!.cancel()
                    }

                    val task = object : TimerTask() {
                        override fun run() {
                            (context as AppCompatActivity).runOnUiThread {
                                zoomBlockingTimer = null
                                zoomable = true
                            }
                        }
                    }

                    zoomBlockingTimer = Timer()
                    zoomBlockingTimer!!.schedule(task, 10 * 1000)


                }
            }


        }
        setupLocation()
    }


    private fun addPolyline() {
        val polyLineWidth = 8
        val locationList = mViewModel.obsLocations

        if (runningPathPolyline == null) {
            if (locationList.size > 1) {
                val fromLocation = locationList[locationList.size - 2]
                val toLocation = locationList[locationList.size - 1]

                if (fromLocation != null && toLocation != null) {
                    val from = LatLng(((fromLocation.latitude)),
                            ((fromLocation.longitude)))

                    val to = LatLng(((toLocation.latitude)),
                            ((toLocation.longitude)))

                    this.runningPathPolyline = mGoogleMap.addPolyline(PolylineOptions()
                            .add(from, to)
                            .width(polyLineWidth.toFloat()).color(ContextCompat.getColor(context, R.color.colorBlueRadioCircle)).geodesic(true))

                }
            }
        } else {
            val toLocation = locationList.get(locationList.size - 1)
            val to = LatLng(((toLocation!!.latitude)),
                    ((toLocation.longitude)))

            val points = runningPathPolyline!!.points
            points.add(to)

            runningPathPolyline!!.points = points
        }
    }

    private fun drawUserPositionMarker(location: Location) {

        val userPositionMarkerBitmapDescriptor = R.drawable.ic_my_location_indicator
                .bitmapDescriptorFromVector(context)

        val latLong = LatLng(location.latitude, location.longitude)

        if (userPositionMarker == null) {
            userPositionMarker = mGoogleMap.addMarker(MarkerOptions()
                    .position(latLong)
                    .flat(true)
                    .anchor(0.5f, 0.5f)
                    .icon(userPositionMarkerBitmapDescriptor))

            mViewModel.gpsState.set(2)

        } else {
            userPositionMarker!!.position = latLong
        }
    }


    private fun zoomMapTo(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)

        if (!didInitialzoom) {
            try {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.5f))
                didInitialzoom = true
                return
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if (zoomable) {
            try {
                zoomable = false
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng),
                        object : GoogleMap.CancelableCallback {
                            override fun onFinish() {
                                zoomable = true
                            }

                            override fun onCancel() {
                                zoomable = true
                            }
                        })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun putMarker(location: android.location.Location, resId: Int) {
        val userPositionMarkerBitmapDescriptor = resId
                .bitmapDescriptorFromVector(context)

        val latLng = LatLng(location.latitude, location.longitude)

        mGoogleMap.addMarker(MarkerOptions()
                .position(latLng)
                .flat(true)
                .anchor(0.3f, 0.3f)
                .icon(userPositionMarkerBitmapDescriptor))
    }


    fun requestPermission() {
        ActivityCompat.requestPermissions(context as Activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST_CODE)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onRequestPermissionResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {

            PERMISSION_REQUEST_CODE -> {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    mListener.startService()
                else if (!(context as Activity).shouldShowRequestPermissionRationale(permissions[0])) {
                    mListener.showGeneralAlert()
                } else {
                    requestPermission()
                }
            }
        }
    }

    fun drawRoute() {

        val polyLineWidth = 6

        var polyline: Polyline? = null

        val listLatLng = mViewModel.obsLocations.convertToLatLng()

        for (index in 0..listLatLng.lastIndex) {
            if (index == 0) {
                putMarker(mViewModel.obsLocations[index]!!, R.drawable.ic_start_user_position)
                zoomMapTo(mViewModel.obsLocations[index]!!)
            }

            if (listLatLng.size > 1) {
                if (index != listLatLng.lastIndex) {
                    if (polyline == null) {
                        if (listLatLng[index] != null && listLatLng[index.plus(1)] != null)
                            if (index != listLatLng.lastIndex)
                                polyline = mGoogleMap.addPolyline(PolylineOptions()
                                        .add(listLatLng[index], listLatLng[index.plus(1)])
                                        .width(polyLineWidth.toFloat()).color(ContextCompat.getColor(context, R.color.colorBlueRadioCircle)).geodesic(true))
                    } else {
                        if (listLatLng[index] != null) {
                            val points = polyline.points

                            points?.add(listLatLng[index])

                            polyline.points = points
                        } else {
                            polyline = null
                        }

                    }
                }

            }
        }
    }

    private fun setupLocation() {
        if (ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mListener.startService()
        } else {
            requestPermission()
        }
    }

    private fun drawStartPositionMarker() {

        val userPositionMarkerBitmapDescriptor = R.drawable.ic_start_user_position
                .bitmapDescriptorFromVector(context)

        val latLng = LatLng(mViewModel.obsLocations.first()!!.latitude, mViewModel.obsLocations.first()!!.longitude)

        mGoogleMap.addMarker(MarkerOptions()
                .position(latLng)
                .flat(true)
                .anchor(0.5f, 0.5f)
                .icon(userPositionMarkerBitmapDescriptor))

    }

    interface GitsMapCallback {
        fun calculateData()
        fun startService()
        fun showGeneralAlert()
    }

    companion object {
        const val PERMISSION_REQUEST_CODE = 3110
    }
}