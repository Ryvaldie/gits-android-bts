package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail;


interface NewsDetailUserActionListener {

    // Example
    // fun onClickItem()

    fun seeAll()

    fun itemOnClick(newsId: Int)
}