package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail

import android.databinding.BindingAdapter
import android.widget.TextView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.customView.CustomFontTextView
import id.otsuka.gits.sweatgen.util.cutExactly2Num
import id.otsuka.gits.sweatgen.util.timeFromSecondFormat

/**
 * @author radhikayusuf.
 */

object ChallengeDetailBindings {
    @BindingAdapter("app:daysLeft")
    @JvmStatic
    fun setDaysLeft(textView: TextView, daysLeft: String?) {
        if (!daysLeft.isNullOrEmpty()) {
            textView.text = (daysLeft ?: "").replace(" days left", "")
        } else
            textView.text = "-"
    }

    @BindingAdapter("app:distance")
    @JvmStatic
    fun setupDistance(textView: CustomFontTextView, distance: Double?) {
        textView.apply {
            text = if (distance != null && distance > 0.0) {
                val distance = distance.div(1000).cutExactly2Num().replace(".00", "")
                context.getString(R.string.text_nkm).replace("%", distance)
            } else {
                context.getString(R.string.text_infinity)
            }
        }
    }

    @BindingAdapter("app:duration")
    @JvmStatic
    fun setupDuration(textView: CustomFontTextView, duration: Int?) {
        textView.apply {
            text = if (duration != null && duration > 0) {
                duration.toLong().timeFromSecondFormat("%dh %dm %ds")
            } else {
                context.getString(R.string.text_infinity)
            }
        }
    }
}