package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.otsuka.gits.sweatgen.databinding.FragmentShareRunBinding

class ShareRunFragment : Fragment(), ShareRunUserActionListener {

    lateinit var mViewDataBinding: FragmentShareRunBinding
    lateinit var mViewModel: ShareRunViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentShareRunBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as ShareRunActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = this@ShareRunFragment

        setupTab()

        return mViewDataBinding.root

    }

    private fun setupTab() {
        mViewDataBinding.viewpagerShare.apply {
            adapter = ShareTabAdapter(requireContext(), childFragmentManager)
            offscreenPageLimit = 3
        }
        mViewDataBinding.tabShare.apply {
            setupWithViewPager(mViewDataBinding.viewpagerShare)
        }
    }

    override fun onClickTest(text: String) {
        // TODO if you have a toast extension, you can replace this
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }


    companion object {

        fun newInstance() = ShareRunFragment().apply {

        }

    }

}
