package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class ChallengeSeeAllViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    var challengeType = 1
    val listChallenges = ObservableArrayList<Challenge>()
    val isRequesting = ObservableBoolean(false)
    val snackbarMsg = SingleLiveEvent<String>()

    fun loadChallenges() {
        isRequesting.set(true)

        if (challengeType == 1) {
            repository.getAvailableChallenges("", repository.getUser()?.id
                    ?: 0, object : GitsDataSource.GetAllChallengeCallback {
                override fun onDataNotAvailable() {
                    isRequesting.set(false)
                }

                override fun onError(errorMessage: String?) {
                    isRequesting.set(false)
                    snackbarMsg.value = errorMessage
                }

                override fun onLoaded(challenges: List<Challenge>) {
                    listChallenges.apply {
                        clear()
                        addAll(challenges)
                    }
                    isRequesting.set(false)
                }
            })
        } else {
            repository.getPreviousChallenges("", repository.getUser()?.id
                    ?: 0, object : GitsDataSource.GetAllChallengeCallback {
                override fun onDataNotAvailable() {
                    isRequesting.set(false)
                }

                override fun onError(errorMessage: String?) {
                    isRequesting.set(false)
                    snackbarMsg.value = errorMessage
                }

                override fun onLoaded(challenges: List<Challenge>) {
                    listChallenges.apply {
                        clear()
                        addAll(challenges)
                    }
                    isRequesting.set(false)
                }
            })
        }
    }

}