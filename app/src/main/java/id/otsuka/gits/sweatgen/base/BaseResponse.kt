package id.otsuka.gits.sweatgen.base

/**
 * Created by irfanirawansukirman on 22/03/18.
 */
data class BaseResponse<T, M>(val status_code: Int, val status: Any, val message: M, val data: T)
