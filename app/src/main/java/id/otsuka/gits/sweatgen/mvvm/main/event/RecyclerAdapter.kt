package id.otsuka.gits.sweatgen.mvvm.main.event

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.data.model.EventDetail
import id.otsuka.gits.sweatgen.databinding.CustomItemRviewEventBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailActivity

class RecyclerAdapter(var events: List<Event>, val listner: ItemClickListener)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replaceEvents(events: List<Event>) {
        this.events = events
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind(events[position], listner)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val mViewDataBinding = CustomItemRviewEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(mViewDataBinding)

    }

    interface ItemClickListener {
        fun onItemSelected(eventId: Int)
    }

    class MyViewHolder(val mViewDataBinding: CustomItemRviewEventBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {

        fun bind(obj: Event, listner: ItemClickListener) {
            mViewDataBinding.apply {
                event = obj
                mListener = listner
            }
        }
    }

    override fun getItemCount(): Int = events.size
}