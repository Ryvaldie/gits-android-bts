package id.otsuka.gits.sweatgen.data.model

data class HistoryContent(val id: Int = 0,
                          val challenge_id: Int? = 0,
                          val user_id: Int = 0,
                          val activity_name: String? = "",
                          val type_run: String? = "",
                          val distance: Double? = 0.0,
                          val step: Int? = 0,
                          val calories: Double? = 0.0,
                          val duration: String? = "",
                          val point: Int? = 0,
                          val long_lat: String? = "",
                          val created_at: String? = "",
                          val event_id: Int = 0,
                          val name: String? = null,
                          val category: String? = null,
                          val logo: String? = null,
                          val banner: String? = null,
                          val daysLeft: String? = null,
                          val type: String? = null
)