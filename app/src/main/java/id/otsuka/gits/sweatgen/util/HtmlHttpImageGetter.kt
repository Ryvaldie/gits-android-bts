package id.otsuka.gits.sweatgen.util

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.TextView
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference
import java.net.URI
import java.net.URL

@Suppress("DEPRECATION")
class HtmlHttpImageGetter : Html.ImageGetter {

    internal var container: TextView? = null
    internal var baseUri: URI? = null
    internal var matchParentWidth: Boolean = false

    private var compressImage = false
    private var qualityImage = 50

    constructor(textView: TextView) {
        this.container = textView
        this.matchParentWidth = false
    }

    constructor(textView: TextView, matchParentWidth: Boolean) {
        this.container = textView
        this.matchParentWidth = matchParentWidth
    }

    fun enableCompressImage(enable: Boolean) {
        enableCompressImage(enable, 50)
    }

    fun enableCompressImage(enable: Boolean, quality: Int) {
        compressImage = enable
        qualityImage = quality
    }

    override fun getDrawable(source: String): Drawable {
        val urlDrawable = UrlDrawable()
        val base64 = source.contains("base64")
        val asyncTask = ImageGetterAsyncTask(urlDrawable, this, container!!,
                matchParentWidth, compressImage, qualityImage, base64)

        asyncTask.execute(source)
        return urlDrawable
    }

    private class ImageGetterAsyncTask(d: UrlDrawable, imageGetter: HtmlHttpImageGetter, container: View,
                                       private val matchParentWidth: Boolean, compressImage: Boolean, qualityImage: Int, private val base64: Boolean?) : AsyncTask<String, Void, Drawable>() {

        private val drawableReference: WeakReference<UrlDrawable> = WeakReference(d)
        private val imageGetterReference: WeakReference<HtmlHttpImageGetter> = WeakReference(imageGetter)
        private val containerReference: WeakReference<View> = WeakReference(container)
        private val resources: WeakReference<Resources> = WeakReference(container.resources)
        private var source: String? = null
        private var scale: Float = 0.toFloat()

        private var compressImage = false
        private var qualityImage = 50

        init {
            this.compressImage = compressImage
            this.qualityImage = qualityImage
        }

        override fun doInBackground(vararg params: String): Drawable? {
            source = params[0]

            if (resources.get() != null) {
                return if (base64!!) {
                    val base = source!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    val decodedString = Base64.decode(base[1].toByteArray(), Base64.DEFAULT)
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inSampleSize = 3;
//                    options.outMimeType = base[0].split(":")[1].split(";")[0];
                    val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size/*, options*/)
                    val drawable = BitmapDrawable(resources.get(), decodedByte)
                    scale = getScale(drawable)
                    drawable.setBounds(0, 0, (drawable.intrinsicWidth * scale).toInt(), (drawable.intrinsicHeight * scale).toInt())
                    drawable
                } else {
                    if (compressImage) {
                        fetchCompressedDrawable(resources.get()!!, source)
                    } else {
                        fetchDrawable(resources.get()!!, source)
                    }
                }
            }

            return null
        }

        override fun onPostExecute(result: Drawable?) {
            if (result == null) {
                Log.w("HtmlHttpImageGetter", "Drawable result is null! (source: $source)")
                return
            }
            val urlDrawable = drawableReference.get() ?: return
            urlDrawable.drawable = result
            urlDrawable.setBounds(0, 0, result.intrinsicWidth, result.intrinsicHeight)
            urlDrawable.level = 1

            val imageGetter = imageGetterReference.get() ?: return

            imageGetter.container!!.invalidate()
            imageGetter.container!!.text = imageGetter.container!!.text
//            imageGetter.container.setCompoundDrawablesWithIntrinsicBounds(urlDrawable.drawable, null, null, null);
        }

        fun fetchDrawable(res: Resources, urlString: String?): Drawable? {
            return try {
                val `is` = fetch(urlString)
                val drawable = BitmapDrawable(res, `is`)
                scale = getScale(drawable)
                drawable.setBounds(0, 0, (drawable.intrinsicWidth * scale).toInt(), (drawable.intrinsicHeight * scale).toInt())
                drawable
            } catch (e: Exception) {
                null
            }

        }

        fun fetchCompressedDrawable(res: Resources, urlString: String?): Drawable? {
            try {
                val `is` = fetch(urlString)
                val original = BitmapDrawable(res, `is`).bitmap

                val out = ByteArrayOutputStream()
                original.compress(Bitmap.CompressFormat.JPEG, qualityImage, out)
                original.recycle()
                `is`!!.close()

                val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(out.toByteArray()))
                out.close()

                scale = getScale(decoded)
                val b = BitmapDrawable(res, decoded)

                b.setBounds(0, 0, (b.intrinsicWidth * scale).toInt(), (b.intrinsicHeight * scale).toInt())
                return b
            } catch (e: Exception) {
                return null
            }

        }

        private fun getScale(bitmap: Bitmap): Float {
            val container = containerReference.get() ?: return 1f

            val maxWidth = container.width.toFloat()
            val originalDrawableWidth = bitmap.width.toFloat()

            return maxWidth / originalDrawableWidth
        }

        private fun getScale(drawable: Drawable): Float {
            val container = containerReference.get()
            if (!matchParentWidth || container == null) {
                return 1f
            }

            val maxWidth = container.width.toFloat()
            val originalDrawableWidth = drawable.intrinsicWidth.toFloat()

            return maxWidth / originalDrawableWidth
        }

        @Throws(IOException::class)
        private fun fetch(urlString: String?): InputStream? {
            val url: URL
            val imageGetter = imageGetterReference.get() ?: return null
            url = if (imageGetter.baseUri != null) {
                imageGetter.baseUri!!.resolve(urlString!!).toURL()
            } else {
                URI.create(urlString!!).toURL()
            }

            return url.content as InputStream
        }
    }

    inner class UrlDrawable : BitmapDrawable() {

        var drawable: Drawable? = null

        override fun draw(canvas: Canvas) {
            if (drawable != null) {
                drawable!!.draw(canvas)
            }
        }
    }

}