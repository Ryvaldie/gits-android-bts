package id.otsuka.gits.sweatgen.mvvm.main.search.result


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentResultBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.challenge.ChallengeViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel
import kotlinx.android.synthetic.main.fragment_result.*
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail.ChallengeDetailActivity

class ResultFragment : Fragment(),ResultUserActionListener {

    lateinit var mViewDataBinding: FragmentResultBinding
    lateinit var mViewModel: ResultViewModel
    lateinit var challengeVm: ChallengeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentResultBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            (activity as MainActivity).mViewModel.searchChallenge.observe(this@ResultFragment, Observer {
                search((activity as MainActivity).resultChallengeList, it!!)
            })
        }

        prepareSearchData()

        mViewDataBinding.mListener = this@ResultFragment

        return mViewDataBinding.root

    }

    override fun onItemClick(challengeId: Int) {
        ChallengeDetailActivity.startActivity(challengeId, requireContext())
    }

    private fun prepareSearchData() {
        challengeVm = obtainChallVm()

        observeSearchDataEvent()

        challengeVm.loadData()
    }

    private fun observeSearchDataEvent() {
        challengeVm.eventChallengesLoaded.observe(this, Observer {
            (requireActivity() as MainActivity).resultChallengeList.apply {
                if (it != null) {
                    clear()
                    addAll(it)
                }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler_search.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = RecyclerAdapter(context, ArrayList(),this@ResultFragment)
        }
    }

    fun obtainViewModel(): ResultViewModel = obtainViewModel(ResultViewModel::class.java)

    private fun obtainChallVm(): ChallengeViewModel = obtainViewModel(ChallengeViewModel::class.java)

    companion object {
        fun newInstance() = ResultFragment().apply {

        }

    }

}
