package id.otsuka.gits.sweatgen.data.source.jobs

import android.content.Context
import id.otsuka.gits.sweatgen.data.model.*
import id.otsuka.gits.sweatgen.data.source.GetChallengeDashboardDao
import id.otsuka.gits.sweatgen.data.source.GetRunningHistoryHeaderContent
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardModel
import id.otsuka.gits.sweatgen.mvvm.main.profile.ItemType
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunModel
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface GitsDataSource {

    /**
     *  ========================================================================================================================
     *
     *                                        ADDITIONAL FUNCTION
     *
     *  ========================================================================================================================
     */
    fun clearDisposable()

    /**
     *  ========================================================================================================================
     *
     *                                        POST
     *
     *  ========================================================================================================================
     */

    fun saveContinuesRun(id: Int, savedVirtualRun: SavedVirtualRun)

    fun getContinuesRun(id: Int): SavedVirtualRun?

    fun getAllContinuesRun(): List<SavedVirtualRun>

    fun register(registerParams: RegisterParams, callback: RegisterCallback)

    fun login(loginParams: LoginParams, callback: LoginCallback)

    fun forgotPassword(forgotPasswordParams: ForgotPasswordParams, callback: ForgotPasswordCallback)

    fun setNewPassword(newPasswordParams: NewPasswordParams, callback: SetNewPasswordCallback)

    fun resendCode(tokenForgot: String? = null, callback: ResendCodeCallback)

    fun saveUser(user: User)

    fun saveForgotPassword(forgotPasswordDao: ForgotPasswordDao)

    fun logout(token: String?, callback: LogoutCallback?, isExpired: Boolean = false)

    fun updateUser(accessToken: String? = null, userId: Int = 0, name: RequestBody?, gender: RequestBody?, dob: RequestBody?, picture: MultipartBody.Part, callback: EditProfileCallback)

    fun addRunProgress(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: AddRunHistoryCallback)

    fun addRunHistory(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: AddRunHistoryCallback)

    fun bookmarkEvent(userToken: String, params: BookmarkEventParams, callback: BookmarkEventCallback)
    /**
     *  ========================================================================================================================
     *
     *                                        PUT
     *
     *  ========================================================================================================================
     */
    fun updatePersonalBio(userId: String, name: String, gender: String, dob: String, callback: UpdatePersonalBioCallback)


    /**
     *  ========================================================================================================================
     *
     *                                       GET
     *
     *  ========================================================================================================================
     */

    fun getLatestAppVersionCode(deviceVersionCode: Int, callback: GetLatestAppVersionCallback)

    fun resendEmail(token: String, callback: ResendEmailCallback)

    fun userValidation(userId: Int, callback: UserValidationCallback)

    fun showUser(token: String?, userId: Int, callback: ShowUserCallback)

    fun getEventList(token: String?, ownerId: Int, callback: GetEventListCallback)

    fun getEventDetail(token: String?, eventId: Int, userId: Int, callback: GetEventDetailCallback)

    fun getUser(): User?

    fun getForgotPasswordDao(): ForgotPasswordDao

    fun getToken(): String?

    fun getOnBoardImage(callback: GetOnBoardImageCallback)

    fun getImagesGallery(eventId: Int, pid: String, token: String?, callback: GetImagesGalleryCallback)

    fun getRunHistories(userToken: String, userId: Int, page: Int = 1, callback: GetRunHistoriesCallback, context: Context)

    fun getChallenges(userToken: String, callback: GetChallengesCallback)

    fun getMedalHistory(userToken: String, userId: Int = 0, page: Int = 1, callback: GetMedalCallback)

    fun getChallengeById(userToken: String, idChallenge: Int, userId: Int, callback: GetChallengeByIdCallback)

    fun getShareLink(userToken: String, eventId: Int, callback: GetShareLinkCallback)

    fun getNewsFeeds(userToken: String, callback: GetNewsFeedsCallback)

    fun getNewsDetail(userToken: String, newsId: Int, callback: GetNewsDetailCallback)

    fun getAllNewsByCategory(userToken: String, categoryId: Int, page: Int, callback: GetAllNewsCallback)

    fun getAllNews(userToken: String, page: Int, callback: GetAllNewsCallback)

    fun getAllNews(userToken: String, callback: GetAllNewsCallback)

    fun getStickers(userToken: String, callback: GetStickersCallback)

    fun getVirturunById(userToken: String, virturunId: Int, userId: Int, callback: GetVirturunByIdCallback)

    fun getContinuesRunningHistory(userId: Int, challengeId: Int, userToken: String, callback: GetContinuesRunningHistoryCallback)

    fun getChallengeDashboard(userId: Int, userToken: String, callback: GetChallengeDashboardCallback)

    fun getLeaderboard(userToken: String, challengeId: Int, callback: GetLeaderboardCallback)

    fun postJoinChallenge(userToken: String, challengeId: Int, userId: Int, callback: PostJoinChallengeCallback)

    fun postJoinVirturun(userToken: String, virturunId: Int, userId: Int, callback: PostJoinChallengeCallback)

    fun getAvailableChallenges(userToken: String, userId: Int, callback: GetAllChallengeCallback)

    fun getPreviousChallenges(userToken: String, userId: Int, callback: GetAllChallengeCallback)

    fun getVirtualRunList(userToken: String, userId: Int, callback: GetVirtualrunListCallback)

    fun getHistoryVirtualRun(userToken: String, userId: Int, callback: GetVirtualrunListCallback)

    fun getVirturunBanner(userToken: String, callback: GetVirturunBannerCallback)

    fun getVirturunLeaderboard(userToken: String, virturunId: Int, callback: GetLeaderboardCallback)

    fun redeemValid(userToken: String, virturunId: Int, userId: Int, accessCode: String, callback: GeneralPostCallback)

    fun saveVirtualRunGuide(guide: List<RaceCentralGuide>)

    fun getVirtualRunGuide(): List<RaceCentralGuide>

    interface GeneralPostCallback {
        fun onSuccess(message: String?)
        fun onFailed(errorMessage: String?)
        fun onFinish()
    }

    interface GetVirturunBannerCallback {

        fun onLoaded(data: GetVirturunBannerDao)

        fun onError(errorMessage: String?)

    }

    interface GetVirtualrunListCallback {

        fun onLoaded(virturuns: List<VirtualRunModel>)

        fun onDataNotAvailable()

        fun onError(errorMessage: String?)
    }

    interface GetAllChallengeCallback {

        fun onLoaded(challenges: List<Challenge>)

        fun onDataNotAvailable()

        fun onError(errorMessage: String?)
    }

    interface PostJoinChallengeCallback {
        fun onSuccess()
        fun onFinish()
        fun onFailed(errorMessage: String?)
    }

    interface GetLeaderboardCallback {
        fun onLoaded(users: List<LeaderBoardModel>)
        fun onDataNotAvailable()
        fun onError(errorMessage: String?)
    }

    interface GetChallengeDashboardCallback {
        fun onLoaded(challengeDashboard: GetChallengeDashboardDao)
        fun onError(errorMessage: String?)
    }

    interface GetContinuesRunningHistoryCallback {
        fun onLoaded(headerData: GetRunningHistoryHeaderContent, histories: List<HistoryRun>)
        fun onDataNotAvailable(headerData: GetRunningHistoryHeaderContent)
        fun onError(errorMessage: String?)
    }

    interface GetVirturunByIdCallback {
        fun onError(errorMessage: String?)
        fun onLoaded(virturun: VirtualRunDetail)
        fun onFinish()
    }

    interface GetStickersCallback {
        fun onLoaded(stickers: List<String>)
        fun onError(errorMessage: String?)
        fun onDataNotAvailable()
    }

    interface GetAllNewsCallback {
        fun onLoaded(news: List<NewsFeed>)
        fun onError(errorMessage: String?)
        fun onDataNotAvailable()
    }

    interface GetLatestAppVersionCallback {
        fun onAppsNeedToUpdate()
        fun onAppsAlreadyUpdated()
        fun onError(errorMessage: String?)
    }

    interface GetNewsDetailCallback {
        fun onLoaded(newsDetail: NewsFeed, similarNews: List<NewsFeed>? = null)
        fun onError(errorMessage: String?)
        fun onDataNotAvailable()
    }

    interface GetNewsFeedsCallback {
        fun onLoaded(headlines: List<NewsFeed>? = null, topNews: List<NewsFeed>? = null)
        fun onError(errorMessage: String?)
        /*
        * 0 -> headline
        * 1 -> top news
        * 2 -> both
        * */
        fun onDataNotAvailable(type: Int)
    }

    interface GetShareLinkCallback {
        fun onSuccess(link: String)
        fun onError(errorMsg: String?)
        fun onFinish()
    }

    interface GetMedalCallback {
        fun onDataLoaded(challengesMedal: List<ChallengeMedal>, currentMedal: CurrentMedal?, page: Int)
        fun onDataNotAvailable()
        fun onError(errorMessage: String?)
    }

    interface GetChallengeByIdCallback {
        fun onDataLoaded(challenge: Challenge)
        fun onError(errorMessage: String?)
        fun onFinish()
    }

    interface BookmarkEventCallback {
        fun postSuccess(dao: BookmarkEventDao)
        fun postError(errorMessage: String?)
        fun onFinish()
    }

    interface GetChallengesCallback {
        fun onDataLoaded(daoChallenge: GetChallengesDao)
        fun onDataNotAvailable()
        fun onError(errorMessage: String?)
    }

    interface GetRunHistoriesCallback {
        fun onDataLoaded(histories: List<ItemType>, page: Int)
        fun onError(errorMessage: String?)
        fun onDataNotAvailable()
        fun onFinish()
    }

    interface AddRunHistoryCallback {
        fun postSuccess()
        fun postError(errorMessage: String?)
        fun onFinish()
    }

    interface LogoutCallback {
        fun logoutSuccess()
        fun logoutError(errorMessage: String?)
    }

    interface GetImagesGalleryCallback {
        fun onImagesLoaded(images: List<ImageGallery>?)
        fun onError(errorMessage: String?)
        fun onDataNotAvailable()
    }

    interface GetOnBoardImageCallback {
        fun onImageLoaded(images: List<OnBoardImage>)
        fun onError(errorMessage: String?)
    }

    interface GetEventDetailCallback {
        fun onEventLoaded(eventDetail: EventDetail)
        fun onError(errorMessage: String?)
    }

    interface GetEventListCallback {
        fun onEventLoaded(eventList: List<Event>)
        fun onError(errorMessage: String?)
        fun onDataNotAvailable()
    }

    interface ShowUserCallback {
        fun onUserLoaded(user: User)
        fun onUserLoadFail(errorMessage: String?)
    }

    interface EditProfileCallback {
        fun onUpdateSuccess(user: User, msg: String?)
        fun onUpdateFailed(errorMessage: String?)
        fun onTokenNull()
    }

    interface UserValidationCallback {
        fun onValidationSuccess()
        fun onValidationFailed(msg: String)
    }

    interface ResendEmailCallback {

        fun onResendSuccess(msg: String?)
        fun onResendFail(msg: String)

    }

    interface UpdatePersonalBioCallback {

        fun onUpdateSuccess(accessToken: String?)

        fun onUpdateFailed(msg: String)
    }

    interface LoginCallback {

        fun onLoginSuccess(loginDao: LoginDao, msg: String? = null)

        fun onloginFailed(message: String?)
    }

    interface RegisterCallback {
        fun onRegisterSuccess(registerDao: RegisterDao)
        fun onRegisterFailed(errorMessage: String?, errorMsg: String?)
    }

    interface ForgotPasswordCallback {
        fun onForgotPasswordSuccess(forgotPasswordDao: ForgotPasswordDao, msg: String? = null)
        fun onForgotPasswordFailed(errorMessage: String?)
    }

    interface ResendCodeCallback {
        fun onSendCodeSuccess(msg: String?)
        fun onSendCodeFailed(message: String?)
    }

    interface SetNewPasswordCallback {
        fun onSetNewPasswordSuccess(msg: String?)
        fun onSetNewPasswordFailed(errorMessage: String?)
    }

}