package id.otsuka.gits.sweatgen.deeplink

import com.airbnb.deeplinkdispatch.DeepLinkModule

/**
 * @author radhikayusuf.
 */

/** This will generate a AppDeepLinkModuleLoader class */
@DeepLinkModule
class AppDeepLinkModule{

}