package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.eventrundown


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentEventRundownBinding
import id.otsuka.gits.sweatgen.util.jsonStringToList2
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.withArgs
import kotlinx.android.synthetic.main.fragment_event_rundown.*


class EventRundownFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentEventRundownBinding
    lateinit var mViewModel: EventRundownViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentEventRundownBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            val events = arguments?.getString(EVENT_RUNDOWN,"").jsonStringToList2()
            eventRundownList.apply {
                clear()
                addAll(events)
            }
        }

        mViewDataBinding.mListener = object : EventRundownUserActionListener {
            override fun back() {
                activity!!.onBackPressed()
            }

        }
        return mViewDataBinding.root

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupListData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).setSupportActionBar(toolbar_event_rundown)
        (activity as AppCompatActivity).supportActionBar?.apply {
            title = getString(R.string.text_event_rundown)
        }
        toolbar_event_rundown.setNavigationOnClickListener { mViewDataBinding.mListener?.back() }
    }


    fun obtainViewModel(): EventRundownViewModel = obtainViewModel(EventRundownViewModel::class.java)


    fun setupListData() {
        mViewDataBinding.recyclerEventRundown.adapter = EventRundownAdapter(mViewModel.eventRundownList, mViewModel)
        mViewDataBinding.recyclerEventRundown.layoutManager = LinearLayoutManager(context)
    }


    companion object {
        val EVENT_RUNDOWN = "events"
        fun newInstance(events: String) = EventRundownFragment().withArgs {
            putString(EVENT_RUNDOWN, events)
        }

    }

}
