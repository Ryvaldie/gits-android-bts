package id.otsuka.gits.sweatgen.data.model

data class LoginDao(val access_token: String?=null,
                    val access_user: AccessUser
                    )