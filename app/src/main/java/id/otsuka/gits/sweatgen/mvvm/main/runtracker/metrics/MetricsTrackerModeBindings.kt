package id.otsuka.gits.sweatgen.mvvm.main.runtracker.metrics;

import android.databinding.BindingAdapter
import android.widget.TextView

/**
 * @author radhikayusuf.
 */

object MetricsTrackerModeBindings {

    @BindingAdapter("app:decimalRule")
    @JvmStatic
    fun setDecimalRule(textView: TextView, value: String) {
        textView.apply {
            if (value.contains(",0"))
                value.replace(",0", "A")

            text = value
        }
    }
}