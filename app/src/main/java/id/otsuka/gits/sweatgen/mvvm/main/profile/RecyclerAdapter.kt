package id.otsuka.gits.sweatgen.mvvm.main.profile

import android.content.Context
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.HistoryBookmarkType
import id.otsuka.gits.sweatgen.data.model.HistoryMapType
import id.otsuka.gits.sweatgen.databinding.ItemListLoadingBinding
import id.otsuka.gits.sweatgen.databinding.ItemRunHistoryEventTypeBinding
import id.otsuka.gits.sweatgen.databinding.ItemRunHistoryMapTypeBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailActivity
import id.otsuka.gits.sweatgen.mvvm.main.profile.ItemType.Companion.TYPE_EVENT
import id.otsuka.gits.sweatgen.util.bitmapDescriptorFromVector
import id.otsuka.gits.sweatgen.util.checkLocationPermission
import id.otsuka.gits.sweatgen.util.saveLayoutToImage

class RecyclerAdapter(var histories: List<ItemType>,
                      val savedInstanceState: Bundle?,
                      val context: Context,
                      val viewModel: ProfileViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var showProgress = true

    companion object {
        const val ITEM_PER_PAGE = 10
        const val TYPE_CONTENT = 1
        const val TYPE_PROGBAR = 2
    }

    override fun getItemCount(): Int {
        return if (histories.isEmpty()) 0 else histories.size + (if (showProgress) 1 else 0)
    }

    fun replaceHistories(histories: List<ItemType>) {
        if (this.histories.isEmpty()) {
            this.histories = histories
            notifyDataSetChanged()
        } else {
            if (histories.size > this.histories.size) {
                val lastPosition = (ITEM_PER_PAGE.times(viewModel.currentPage)).minus(1)
                this.histories = histories
                notifyItemRangeInserted(lastPosition, this.histories.size.minus(1))
            }

            viewModel.currentPage += 1
        }

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MapTypeVH -> holder.bind(histories[position])
            is ProgbarVH -> holder.bind(!(viewModel.isLastPage.get()))
            is BookmarkTypeVH -> holder.bind(histories[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var mViewDataBinding: ViewDataBinding = ItemRunHistoryMapTypeBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mapView.onCreate(savedInstanceState)
            mapView.isClickable = false
            mListener = object : ProfileUserActionListener {
                override fun convertMapToImage(v: View) {
                    v.saveLayoutToImage("Map${System.currentTimeMillis()}.png")
                }

                override fun toReward() {

                }
            }
        }
        when (viewType) {
            TYPE_PROGBAR -> {
                mViewDataBinding = ItemListLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                return ProgbarVH(mViewDataBinding)
            }
            TYPE_EVENT -> {

                mViewDataBinding = ItemRunHistoryEventTypeBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = object : ItemClickListener {
                        override fun onItemClicked(eventId: Int) {
                            EventDetailActivity.startActivity(context, eventId)
                        }
                    }
                }

                return BookmarkTypeVH(mViewDataBinding)
            }
        }

        return MapTypeVH(mViewDataBinding, context)
    }

    override fun getItemViewType(position: Int): Int {
        return if (showProgress) {
            if (histories.size == position) {
                TYPE_PROGBAR
            } else {
                histories[position].getItemType()
            }
        } else {
            histories[position].getItemType()
        }
    }

    fun showProgress(show: Boolean) {
        showProgress = show
//        notifyItemChanged(histories.size)
        notifyDataSetChanged()
    }

    class ProgbarVH(val mBinding: ItemListLoadingBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(isVisible: Boolean) {
            mBinding.isVisible = isVisible
        }
    }

    interface ItemClickListener {
        fun onItemClicked(eventId: Int)
    }

    class BookmarkTypeVH(val mViewDataBinding: ItemRunHistoryEventTypeBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {
        fun bind(obj: ItemType) {
            mViewDataBinding.apply {
                bookmark = obj as HistoryBookmarkType
            }
        }
    }

    class MapTypeVH(val mViewDataBinding: ViewDataBinding, val context: Context) : RecyclerView.ViewHolder(mViewDataBinding.root), OnMapReadyCallback {

        val locations = ArrayList<android.location.Location?>()

        val latLngs = ArrayList<LatLng?>()

        val polylineOptions = ArrayList<PolylineOptions>()

        lateinit var mmMap: GoogleMap

        fun bind(obj: ItemType) {

            (mViewDataBinding as ItemRunHistoryMapTypeBinding).apply {

                history = (obj as HistoryMapType).copy(distance = obj.distance.div(1000))

                locations.apply {
                    clear()
                    addAll(obj.listLocations)
                }

                latLngs.apply {
                    clear()
                    addAll(obj.listLatLng)
                }

                polylineOptions.apply {
                    clear()
                    addAll(obj.listPolyOptions)
                }

                mapView.getMapAsync(this@MapTypeVH)

                mapView.onResume()
            }
        }


        fun zoomMapTo(location: android.location.Location, mMap: GoogleMap) {
            val latLng = LatLng(location.latitude, location.longitude)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
        }

        private fun fixZoom() {

            val latLngBounds = LatLngBounds.Builder()

            latLngs.forEach {
                if (it != null)
                    latLngBounds.include(it)
            }
            mmMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 100))
        }

        fun setupMap(map: GoogleMap) {
            map.apply {
                uiSettings.isMyLocationButtonEnabled = false

                if (context.checkLocationPermission()) {
                    isMyLocationEnabled = false
                }
            }
        }

        fun putMarker(location: android.location.Location, mMap: GoogleMap, resId: Int) {
            val userPositionMarkerBitmapDescriptor = resId
                    .bitmapDescriptorFromVector(context)

            val latLng = LatLng(location.latitude, location.longitude)

            mMap.addMarker(MarkerOptions()
                    .position(latLng)
                    .flat(true)
                    .anchor(0.3f, 0.3f)
                    .icon(userPositionMarkerBitmapDescriptor))
        }

        fun drawTrack(mMap: GoogleMap) {

            if (locations.first() != null) {
                putMarker(locations.first()!!, mMap, R.drawable.ic_start_user_position)
                zoomMapTo(locations.first()!!, mMap)
                fixZoom()
            }

            if (locations.last() != null)
                putMarker(locations.last()!!, mMap, R.drawable.ic_finish)

            polylineOptions.map {
                mMap.addPolyline(it)
            }
        }

        override fun onMapReady(mMap: GoogleMap) {

            mMap.clear()

            mmMap = mMap

            setupMap(mMap)

            drawTrack(mMap)
        }
    }


}