package id.otsuka.gits.sweatgen.data.model

data class ImageGallery(val id: Int = 0,
                        val image: String? = null,
                        val marker: String? = null
)