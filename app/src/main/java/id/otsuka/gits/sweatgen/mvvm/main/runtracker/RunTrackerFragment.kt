package id.otsuka.gits.sweatgen.mvvm.main.runtracker


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentRunTrackerBinding


class RunTrackerFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentRunTrackerBinding
    lateinit var mViewModel: RunTrackerViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRunTrackerBinding.inflate(inflater, container, false)

        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel = (activity as RunTrackerActivity).obtainViewModel()
        mViewDataBinding.mViewModel = mViewModel


    }

    companion object {
        fun newInstance() = RunTrackerFragment().apply {

        }

    }

}
