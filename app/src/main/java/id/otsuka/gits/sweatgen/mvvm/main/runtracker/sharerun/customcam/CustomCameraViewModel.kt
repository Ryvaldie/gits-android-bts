package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.customcam;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.graphics.Bitmap


class CustomCameraViewModel(context: Application) : AndroidViewModel(context) {
    /*
    * true -> camera front mode
    * false -> camera back mode
    * */
    val bCameraFront = ObservableBoolean(true)

    val isRendering = ObservableBoolean(false)

}