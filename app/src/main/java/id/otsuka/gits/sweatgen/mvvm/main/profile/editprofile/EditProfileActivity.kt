package id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile;

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity


class EditProfileActivity : AppCompatActivity(), EditProfileNavigator {
  override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        setupFragment()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val fragment = supportFragmentManager.findFragmentByTag(EditProfileFragment::class.java.simpleName)
        fragment.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        EditProfileFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, EditProfileActivity::class.java))
        }
    }
}
