package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage


import android.arch.lifecycle.Observer
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.xiaopo.flying.sticker.DrawableSticker
import com.xiaopo.flying.sticker.Sticker
import com.xiaopo.flying.sticker.StickerView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentShareEditImageBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.sharefinalpage.ShareFinalPageFragment
import id.otsuka.gits.sweatgen.util.*
import java.io.File
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

class ShareEditImageFragment : Fragment(), ShareEditImageUserActionListener {

    lateinit var mViewDataBinding: FragmentShareEditImageBinding
    lateinit var mViewModel: ShareEditImageViewModel
    lateinit var mParentVm: ShareRunViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentShareEditImageBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            itemClickEvent.observe(this@ShareEditImageFragment, Observer {
                if (it != null) {
                    Glide.with(requireContext())
                            .asBitmap()
                            .load(it)
                            .listener(object : RequestListener<Bitmap> {
                                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                                    Log.d(ShareEditImageFragment::class.java.simpleName, e?.message)
                                    return false
                                }

                                override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                    return false
                                }
                            }
                            ).into(object : SimpleTarget<Bitmap>() {
                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                    val drawableImg = BitmapDrawable(resources, resource)
                                    val sticker = DrawableSticker(drawableImg)
                                    mViewDataBinding.stickerView.apply {
                                        addSticker(sticker)
                                        onStickerOperationListener = object : StickerView.OnStickerOperationListener {
                                            override fun onStickerDragFinished(sticker: Sticker) {

                                            }

                                            override fun onStickerAdded(sticker: Sticker) {

                                            }

                                            override fun onStickerClicked(sticker: Sticker) {

                                            }

                                            override fun onStickerDeleted(sticker: Sticker) {

                                            }

                                            override fun onStickerDoubleTapped(sticker: Sticker) {
                                                mViewDataBinding.stickerView.remove(sticker)
                                            }

                                            override fun onStickerFlipped(sticker: Sticker) {

                                            }

                                            override fun onStickerZoomFinished(sticker: Sticker) {

                                            }
                                        }
                                    }
                                }
                            })
                }
            })
        }

        mViewDataBinding.mListener = this@ShareEditImageFragment

        setupTab()

        return mViewDataBinding.root

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_share_run, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_next -> {
                val filename = "share_run_${System.currentTimeMillis()}.png"

                mViewDataBinding.stickerView.saveLayoutToImage(filename)

                mParentVm.bEditedImage.set(File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_PICTURES).absolutePath + "/sweatgen/$filename"))

                (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(ShareFinalPageFragment.newInstance(), R.id.frame_main_content)
            }
        }
        return true
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        mParentVm = (requireActivity() as ShareRunActivity).obtainViewModel()
        val mDistance = String.format("%.2f", mParentVm.obsAddRunHistory.get()?.distance)
        val mCalories = String.format("%.1f", mParentVm.obsAddRunHistory.get()?.calories)
        val mDuration = TimeUnit.SECONDS.toMillis(mParentVm.obsAddRunHistory.get()?.duration?.toLong()
                ?: 0).timeMillisFormat("%dh %dm %ds")
        mViewDataBinding.apply {
            activityVm = mParentVm
            duration = mDuration
            distance = mDistance
            calories = mCalories
        }
    }

    private fun setupTab() {
        val displayMetrics = DisplayMetrics()

        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)

        val height = (displayMetrics.heightPixels)

        val width = displayMetrics.widthPixels

        val imgFinalHeight = height.times(0.6).roundToInt()

        val tabHeight = height.minus(imgFinalHeight).minus(72.dpToPx(requireContext()))

        mViewDataBinding.imgFinal.apply {
            val layoutParam = FrameLayout.LayoutParams(width, imgFinalHeight)
            layoutParams = layoutParam
        }

        mViewDataBinding.bottomLayout?.apply {

            viewpagerSticker?.adapter = StickerTabAdapter(requireContext(), childFragmentManager)

            tabSticker?.setupWithViewPager(viewpagerSticker)

            BottomSheetBehavior.from(linSheet).apply {
                peekHeight = tabHeight
                setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                    override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    }

                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        linSheet.post {
                            linSheet.requestLayout()
                            linSheet.invalidate()
                        }
                    }
                })
            }
        }
    }

    override fun onClickTest(text: String) {
        // TODO if you have a toast extension, you can replace this
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }

    // TODO import obtainViewModel & add ShareEditImageViewModel to ViewModelFactory
    fun obtainViewModel(): ShareEditImageViewModel = obtainViewModel(ShareEditImageViewModel::class.java)

    companion object {
        fun newInstance() = ShareEditImageFragment().apply {

        }

    }

}
