package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.threedmap;

import android.databinding.BindingAdapter
import android.os.Build
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebView

/**
 * @author radhikayusuf.
 */

object ThreeDiRaceMapBindings {
    @BindingAdapter("app:url")
    @JvmStatic
    fun loadUrl(webView: WebView, url: String?) {
        webView.apply {
            val cookieManager = CookieManager.getInstance()
            cookieManager.setAcceptCookie(true)

            settings.apply {
                javaScriptEnabled = true
                cacheMode = WebSettings.LOAD_NO_CACHE
                domStorageEnabled = true
                builtInZoomControls = true
                displayZoomControls = true
                javaScriptCanOpenWindowsAutomatically = true
                setSupportMultipleWindows(true)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cookieManager.setAcceptThirdPartyCookies(webView, true); }
            }
            if (Build.VERSION.SDK_INT >= 21) {
                settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;
            }
            isHorizontalScrollBarEnabled = false
            if (url != null) {
                loadUrl(url)
            }
        }
    }
}