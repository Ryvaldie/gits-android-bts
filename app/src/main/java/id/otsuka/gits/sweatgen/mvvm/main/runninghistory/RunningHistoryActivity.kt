package id.otsuka.gits.sweatgen.mvvm.main.runninghistory;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import kotlinx.android.synthetic.main.activity_running_history.*

class RunningHistoryActivity : AppCompatActivity(), RunningHistoryNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_running_history)
        setupFragment()
        setupToolbar()
    }

    private fun setupToolbar() {
        toolbar.setNavigationOnClickListener { super.onBackPressed() }
    }

    // TODO add RunningHistoryViewModel to ViewModelFactory & if template have an error, please reimport obtainViewModel
    fun obtainViewModel(): RunningHistoryViewModel = obtainViewModel(RunningHistoryViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        RunningHistoryFragment.newInstance().let {
            // TODO if template have an error, please reimport replaceFragmentInActivity
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {

        val CHALLENGE_ID = "challengeId"

        val TOTAL_PROGRESS = "totalProgress"

        val CURRENT_PROGRESS = "currentProgress"

        val PROGRESS = "progress"

        fun startActivity(context: Context, challengeId: Int,
                          currentProgress: String,
                          totalProgress: String,
                          progress: Int
        ) {
            context.startActivity(Intent(context, RunningHistoryActivity::class.java).apply {
                putExtra(CHALLENGE_ID, challengeId)
                putExtra(PROGRESS, progress)
                putExtra(CURRENT_PROGRESS, currentProgress)
                putExtra(TOTAL_PROGRESS, totalProgress)
            })
        }
    }
}
