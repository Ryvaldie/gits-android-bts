package id.otsuka.gits.sweatgen.mvvm.main.newsfeed;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.NewsFeed
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.databinding.HeadlineSectionBinding
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.agoFormat
import id.otsuka.gits.sweatgen.util.toFormattedDate

class NewsFeedViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bHeadlineList = ObservableArrayList<NewsItemModel>()

    val createSliderIndicator = SingleLiveEvent<HeadlineSectionBinding>()

    val bRegularNews = ObservableArrayList<NewsItemModel>()

    val showMessage = SingleLiveEvent<String>()

    val isRequesting = ObservableBoolean(false)

    val isFirstTime = ObservableBoolean(true)

    val stopRefresh = SingleLiveEvent<Void>()

    fun start() {

        isRequesting.set(true)

        repository.getNewsFeeds("", object : GitsDataSource.GetNewsFeedsCallback {
            override fun onDataNotAvailable(type: Int) {
                isRequesting.set(false)
                isFirstTime.set(false)
                stopRefresh.call()
            }

            override fun onError(errorMessage: String?) {
                isRequesting.set(false)
                showMessage.value = errorMessage
                isFirstTime.set(false)
                stopRefresh.call()
            }

            override fun onLoaded(headlines: List<NewsFeed>?, topNews: List<NewsFeed>?) {
                bHeadlineList.clear()
                bRegularNews.clear()

                if (headlines != null) {
                    bHeadlineList.addAll(headlines.map {
                        NewsItemModel(
                                it.id,
                                it.title ?: "-",
                                (it.date_publish
                                        ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").agoFormat(context)
                                ,
                                it.content ?: "-",
                                it.banner ?: "")
                    })
                }
                if (topNews != null) {
                    bRegularNews.addAll(topNews.mapIndexed { x, it ->
                        NewsItemModel(
                                layoutCode = if (x == 0) R.layout.item_newsfeed_header else R.layout.item_newsfeed_regular,
                                id = it.id,
                                title = it.title ?: "-",
                                publish_date = (it.date_publish
                                        ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").agoFormat(context) + " - " + it.category,
                                content = it.content ?: "-",
                                banner = it.banner ?: ""
                        )
                    })
                }

                isRequesting.set(false)
                isFirstTime.set(false)
                stopRefresh.call()
            }
        })
    }

}