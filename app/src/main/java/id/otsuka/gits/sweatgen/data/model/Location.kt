package id.otsuka.gits.sweatgen.data.model

data class Location(val id: Int = 0,
                    val event_id: Int = 0,
                    val address: String? = null,
                    val longitude: String? = null,
                    val laititude: String? = null,
                    val category:String?=null
)