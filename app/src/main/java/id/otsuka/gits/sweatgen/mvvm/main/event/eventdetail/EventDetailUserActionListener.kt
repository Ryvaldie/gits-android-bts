package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail;

import android.view.View
import android.widget.TextView


interface EventDetailUserActionListener {

    fun registerEvent()

    fun eventSchedule()

    fun getThere()

    fun parkingPoint()

    fun galleryPhoto()

    fun liveTracker()

    fun raceResult()

    fun raceCentral()

    fun raceGuide()

    fun call()

    fun mail()

    fun web()

    fun seeMore()

    fun back()

    fun share()

    fun bookmark()

}