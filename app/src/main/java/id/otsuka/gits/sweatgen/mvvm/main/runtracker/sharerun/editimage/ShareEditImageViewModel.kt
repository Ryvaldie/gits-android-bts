package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField
import android.databinding.ObservableInt
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class ShareEditImageViewModel(context: Application) : AndroidViewModel(context) {
    /*
       * 0 -> blue style
       * 1 -> white style
       * */
    val bSelectedSticker = ObservableInt(-1)

    val itemClickEvent = SingleLiveEvent<String>()

    val bStickerUrl = ObservableField<String>("")
}