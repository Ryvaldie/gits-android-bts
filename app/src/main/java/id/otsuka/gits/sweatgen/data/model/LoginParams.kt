package id.otsuka.gits.sweatgen.data.model

data class LoginParams(val email: String?=null,
                       val password: String?=null
)