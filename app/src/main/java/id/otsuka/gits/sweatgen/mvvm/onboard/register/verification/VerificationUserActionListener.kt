package id.otsuka.gits.sweatgen.mvvm.onboard.register.verification;


interface VerificationUserActionListener {

    // Example
    // fun onClickItem()

    fun onResendEmail()

    fun goToPersonalBio()

    fun backToOnBoard()

}