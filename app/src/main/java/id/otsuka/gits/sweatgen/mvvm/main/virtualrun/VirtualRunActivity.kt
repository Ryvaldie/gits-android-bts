package id.otsuka.gits.sweatgen.mvvm.main.virtualrun;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunFragment.Companion.JOIN_VIRTURUN_TYPE
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import kotlinx.android.synthetic.main.activity_virtual_run.*


class VirtualRunActivity : AppCompatActivity(), VirtualRunNavigator {

    lateinit var mViewModel: VirtualRunViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_virtual_run)

        mViewModel = obtainViewModel().apply {

        }

        setupFragment()

        setupToolbar()

    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_virtual_run)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_left_arrow_white)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    fun showHideToolbar(show: Boolean) {
        if (show)
            supportActionBar?.show()
        else
            supportActionBar?.hide()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.fragments[1]
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    // TODO add VirtualRunViewModel to ViewModelFactory & if template have an error, please reimport obtainViewModel
    fun obtainViewModel(): VirtualRunViewModel = obtainViewModel(VirtualRunViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        VirtualRunFragment.newInstance(JOIN_VIRTURUN_TYPE).let {
            // TODO if template have an error, please reimport replaceFragmentInActivity
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, VirtualRunActivity::class.java))
        }
    }
}
