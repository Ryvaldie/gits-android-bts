package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail;

import id.otsuka.gits.sweatgen.R
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.support.v4.content.ContextCompat
import id.otsuka.gits.sweatgen.data.model.*
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.howtogetthere.RecyclerviewAdapter
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.yyyyMMddToEEEEddyyyy


class EventDetailViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val obsEeventDetail = ObservableField<EventDetail>()
    val obsLocation = ObservableArrayList<Location>()
    val obsEventRundown = ObservableArrayList<EventRundown>()
    val eventId = ObservableInt()
    val obsListener = ObservableField<RecyclerviewAdapter.onClickItemListener>()
    val bExpanded = ObservableField(false)
    val eventLoading = SingleLiveEvent<Boolean>()
    val sameDate = ObservableField(false)
    val bMessage = ObservableField("")
    val snackbarMsg = SingleLiveEvent<String?>()
    val snackbarError = SingleLiveEvent<String?>()
    val bBookmarked = ObservableBoolean(false)
    val shareEvent = SingleLiveEvent<String?>()
    val loadingShare = SingleLiveEvent<Boolean>()


    fun getShareLink() {
        loadingShare.value = true

        repository.getShareLink("", eventId.get(), object : GitsDataSource.GetShareLinkCallback {
            override fun onError(errorMsg: String?) {
                snackbarError.value = errorMsg
            }

            override fun onFinish() {
                loadingShare.value = false
            }

            override fun onSuccess(link: String) {
                shareEvent.value = link
            }
        })
    }

    fun bookmarkEvent() {

        val params = BookmarkEventParams((repository.getUser() ?: User()).id, eventId.get())
        repository.bookmarkEvent("", params, object : GitsDataSource.BookmarkEventCallback {
            override fun postSuccess(dao: BookmarkEventDao) {
                if (dao.isBookmark == 0)
                    snackbarMsg.value = context.getString(R.string.text_unbookmarked)
                else
                    snackbarMsg.value = context.getString(R.string.text_bookmarked)
            }

            override fun postError(errorMessage: String?) {
                bBookmarked.set(!bBookmarked.get())
                snackbarError.value = errorMessage
            }

            override fun onFinish() {}
        })

    }


    fun getEventDetail() {
        eventLoading.value = true
        repository.getEventDetail(null, eventId.get(), (repository.getUser()
                ?: User()).id, object : GitsDataSource.GetEventDetailCallback {
            override fun onEventLoaded(eventDetail: EventDetail) {
                if (eventDetail.location!!.isNotEmpty())
                    obsLocation.apply {
                        clear()
                        addAll(eventDetail.location)
                    }
                if (eventDetail.schedule!!.isNotEmpty())
                    obsEventRundown.apply {
                        clear()
                        addAll(eventDetail.schedule)
                    }

                obsEeventDetail.set(eventDetail)

                bBookmarked.set((eventDetail.isBookmark ?: 0) != 0)
                eventLoading.value = false


            }

            override fun onError(errorMessage: String?) {
                bMessage.set(errorMessage)
                eventLoading.value = false
            }
        })
    }
}