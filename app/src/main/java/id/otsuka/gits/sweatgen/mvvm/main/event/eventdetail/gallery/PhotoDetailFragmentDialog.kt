package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.DialogImageDetailBinding
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.view.Window
import android.view.Window.FEATURE_NO_TITLE
import android.widget.RelativeLayout
import id.otsuka.gits.sweatgen.R
import kotlinx.android.synthetic.main.dialog_image_detail.*


class PhotoDetailFragmentDialog : DialogFragment() {

    lateinit var mViewDataBinding: DialogImageDetailBinding
    lateinit var mViewModel: GalleryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val mParentFragment = activity?.supportFragmentManager?.findFragmentByTag(GalleryFragment::class.java.simpleName)

        mViewDataBinding = DialogImageDetailBinding.inflate(layoutInflater, container, false).apply {
         //   image = (mParentFragment as GalleryFragment).mViewModel.bPhotoDetail.get()
        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_photo_detail.setNavigationOnClickListener {
            this.dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        // creating the fullscreen dialog
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window!!.setBackgroundDrawable(ContextCompat.getDrawable(context!!, R.color.colorOpacityBlackGallery))
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        return dialog
    }
}