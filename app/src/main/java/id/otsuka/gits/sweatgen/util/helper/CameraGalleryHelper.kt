package id.otsuka.gits.sweatgen.util.helper

import android.Manifest
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.theartofdev.edmodo.cropper.CropImage
import java.io.File


/**
 * Created by IDUA on 15-Mar-18.
 */
class CameraGalleryHelper {
    companion object {
        const val REQUEST_CAMERA_CODE = 123
        const val REQUEST_GALLERY_CODE = 456
        const val REQUEST_GALLERY_AND_CAMERA_CODE = 789
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        private val PERMISSIONS_EXTERNAL_STORAGE = arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
        private val PERMISSIONS_CAMERA = arrayOf(Manifest.permission.CAMERA)
        private val PERMISSIONS_CAMERA_AND_STORAGE = arrayOf(Manifest.permission.CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)

        fun openImagePickerOption(activity: Activity) {
            if (checkCameraAndStoragePermission(activity)) {
                try {
                    CropImage.startPickImageActivity(activity)
                } catch (ex: android.content.ActivityNotFoundException) {
                    Toast.makeText(activity, "Please install a file manager", Toast.LENGTH_SHORT).show()
                }
            }
        }


        fun isImage(file: File): Boolean {
            val imageExtension = arrayOf("jpg", "png", "gif", "jpeg")
            val extension = file.extension
            Log.wtf("TAG", extension)
            return if (extension.isEmpty()) true else imageExtension.any { extension.toLowerCase().endsWith(it) }
        }


        fun openImagePickerOption(fm: Fragment) {
            if (checkCameraAndStoragePermission(fm.requireActivity())) {
                try {
                    CropImage.startPickImageActivity(fm.requireActivity())
                } catch (ex: android.content.ActivityNotFoundException) {
                    Toast.makeText(fm.requireActivity(), "Please install a file manager", Toast.LENGTH_SHORT).show()
                }
            }
        }

        @SuppressLint("ObsoleteSdkInt")
        fun checkCameraAndStoragePermission(activity: Activity): Boolean {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                return true
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    activity.requestPermissions(PERMISSIONS_CAMERA_AND_STORAGE, REQUEST_GALLERY_AND_CAMERA_CODE)
                    false
                }
            }
            return true
        }

        @SuppressLint("ObsoleteSdkInt")
        fun checkCameraPermission(activity: Activity): Boolean {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                return true
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    true
                } else {
                    activity.requestPermissions(PERMISSIONS_CAMERA, REQUEST_CAMERA_CODE)
                    false
                }
            }
            return true
        }

        @SuppressLint("ObsoleteSdkInt")
        fun checkExternalStoragePermission(activity: Activity): Boolean {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                return true
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val readStoragePermissionState = ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE)
                val writeStoragePermissionState = ContextCompat.checkSelfPermission(activity, WRITE_EXTERNAL_STORAGE)
                val externalStoragePermissionGranted = readStoragePermissionState == PackageManager.PERMISSION_GRANTED
                        && writeStoragePermissionState == PackageManager.PERMISSION_GRANTED
                if (!externalStoragePermissionGranted) {
                    activity.requestPermissions(PERMISSIONS_EXTERNAL_STORAGE, REQUEST_GALLERY_CODE)
                    return false
                }
                return true
            }
            return true
        }
    }
}