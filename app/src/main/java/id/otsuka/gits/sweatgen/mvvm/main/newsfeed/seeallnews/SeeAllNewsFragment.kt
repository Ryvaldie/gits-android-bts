package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentSeeAllNewsBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail.NewsDetailActivity
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.AllNewsAdapter.Companion.ITEM_PER_PAGE
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.SeeAllNewsActivity.Companion.CATEGORY_ID
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.showSnackBar


class SeeAllNewsFragment : Fragment(), SeeAllNewsUserActionListener {

    lateinit var mViewDataBinding: FragmentSeeAllNewsBinding
    lateinit var mViewModel: SeeAllNewsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentSeeAllNewsBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

            categoryId.set(requireActivity().intent.getIntExtra(CATEGORY_ID, 0))

            showProgress.observe(this@SeeAllNewsFragment, Observer {
                if (it != null)
                    (mViewDataBinding.recyclerNews.adapter as AllNewsAdapter).showProgress(!it)
            })
            if (mViewModel.categoryId.get() == -1)
                getAllNews()
            else
                getAllNewsByCategory()
        }

        mViewDataBinding.mListener = this@SeeAllNewsFragment

        setupRecycler()
        setupToolbar()

        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.showMessage.observe(this, Observer {
            when (it) {
              TOKEN_EXPIRE -> {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                }
                else -> {

                    (it ?: "-").showSnackBar(mViewDataBinding.root)
                }
            }
        })
    }

    override fun onItemClick(newsId: Int) {
        NewsDetailActivity.startActivity(newsId, requireContext())
    }

    fun setupToolbar() {
        mViewDataBinding.toolbarNav.setOnClickListener { requireActivity().onBackPressed() }
    }

    fun setupRecycler() {
        mViewDataBinding.recyclerNews.apply {
            adapter = AllNewsAdapter(ArrayList(0), this@SeeAllNewsFragment, mViewModel)
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val layoutManager = mViewDataBinding.recyclerNews.layoutManager
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisibleItemPosition = (mViewDataBinding.recyclerNews.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                    if (!(mViewModel.isLastPage.get())) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                                firstVisibleItemPosition >= 0
                                && totalItemCount >= ITEM_PER_PAGE) {

                            if (!mViewModel.isRequesting.get()) {
                                mViewModel.isRequesting.set(true)
                                Handler().postDelayed({
                                    if (mViewModel.categoryId.get() == -1)
                                        mViewModel.getAllNews()
                                    else
                                        mViewModel.getAllNewsByCategory()
                                }, 1000)
                            }

                        }
                    }
                }
            })

        }
    }


    fun obtainViewModel(): SeeAllNewsViewModel = obtainViewModel(SeeAllNewsViewModel::class.java)


    companion object {
        fun newInstance() = SeeAllNewsFragment().apply {

        }

    }

}
