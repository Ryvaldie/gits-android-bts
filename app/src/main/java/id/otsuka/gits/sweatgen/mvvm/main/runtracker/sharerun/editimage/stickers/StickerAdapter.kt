package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.ItemStickerBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageViewModel

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class StickerAdapter(val stickers: ArrayList<String>,
                     val vm: ShareEditImageViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int = stickers.size

    fun fillData(stickers: List<String>) {
        this.stickers.apply {
            clear()
            addAll(stickers)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as StickerVH).bind(stickers[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return StickerVH(ItemStickerBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = object : StickersUserActionListener {
                override fun onItemClick(url: String) {
                    vm.itemClickEvent.value = url
                }
            }
        })
    }

    class StickerVH(val mViewBinding: ItemStickerBinding) : RecyclerView.ViewHolder(mViewBinding.root) {
        fun bind(obj: String) {
            mViewBinding.imageUrl = obj
        }
    }
}