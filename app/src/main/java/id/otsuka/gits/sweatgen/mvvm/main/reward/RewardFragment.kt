package id.otsuka.gits.sweatgen.mvvm.main.reward


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentRewardBinding
import id.otsuka.gits.sweatgen.mvvm.main.reward.RecyclerAdapter.Companion.ITEM_PER_PAGE
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import kotlinx.android.synthetic.main.fragment_reward.*


class RewardFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentRewardBinding
    lateinit var mViewModel: RewardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRewardBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            showProgress.observe(this@RewardFragment, Observer {
                (mViewDataBinding.recyclerMedals.adapter as RecyclerAdapter).showProgress(it != true)
            })
        }

        mViewDataBinding.mListener = object : RewardUserActionListener {
            override fun onBack() {
                activity!!.onBackPressed()
            }
        }

        return mViewDataBinding.root
    }

    private fun setupRecycler() {
        mViewDataBinding.apply {
            recyclerMedals.apply {

                adapter = RecyclerAdapter(ArrayList(0), mViewModel!!)

                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)

                        val layoutManager = recyclerMedals.layoutManager
                        val visibleItemCount = layoutManager.childCount
                        val totalItemCount = layoutManager.itemCount
                        val firstVisibleItemPosition = (recyclerMedals.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                        if (!(mViewModel!!.isLastPage.get())) {
                            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                                    firstVisibleItemPosition >= 0
                                    && totalItemCount >= ITEM_PER_PAGE) {

                                mViewModel!!.isRequesting.set(true)

                                Handler().postDelayed({
                                    mViewModel!!.start()
                                }, 1000)
                            }
                        }
                    }
                })

            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()

        toolbar_nav.setOnClickListener { requireActivity().onBackPressed() }

        mViewModel.eventMessage.observe(this, Observer {

            when (it) {
                TOKEN_EXPIRE -> {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                }
                else -> {
                    (it ?: "-").showSnackBar(mViewDataBinding.root)
                }
            }
        })

        mViewModel.loadingEvent.observe(this, Observer {
            if (it == true) {
                activity?.showLoading()
            } else {
                swipe_refresh_reward.isRefreshing = false
                activity?.dismissLoading()
            }
        })

        swipe_refresh_reward.setOnRefreshListener {
            mViewModel.apply {
                start()
                mViewModel.currentPage = 1
            }
        }

        mViewModel.start()
    }


    fun obtainViewModel(): RewardViewModel = obtainViewModel(RewardViewModel::class.java)

    companion object {
        fun newInstance() = RewardFragment().apply {

        }

    }

}
