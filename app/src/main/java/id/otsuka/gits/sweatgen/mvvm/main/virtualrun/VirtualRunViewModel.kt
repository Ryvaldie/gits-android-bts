package id.otsuka.gits.sweatgen.mvvm.main.virtualrun;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class VirtualRunViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val listVirtualRun = ObservableArrayList<VirtualRunModel>()

    val listMyVirtualRun = ObservableArrayList<VirtualRunModel>()

    val eventItemClick = SingleLiveEvent<Int>()

    val isRequesting = ObservableBoolean(false)

    val snackbarMsg = SingleLiveEvent<String>()

    var virtuRunType = -1

    fun getGuideString(): String {
        return Gson().toJson(repository.getVirtualRunGuide())
    }

    fun loadJoinVirturunList() {
        isRequesting.set(true)
        if (virtuRunType == 1)
            repository.getVirtualRunList("", repository.getUser()?.id
                    ?: 0, object : GitsDataSource.GetVirtualrunListCallback {
                override fun onDataNotAvailable() {
                    isRequesting.set(false)
                }

                override fun onError(errorMessage: String?) {
                    isRequesting.set(false)
                    snackbarMsg.value = errorMessage
                }

                override fun onLoaded(virturuns: List<VirtualRunModel>) {
                    listVirtualRun.apply {
                        clear()
                        addAll(virturuns)
                    }
                    isRequesting.set(false)
                }
            })
        else
            repository.getHistoryVirtualRun("", repository.getUser()?.id
                    ?: 0, object : GitsDataSource.GetVirtualrunListCallback {
                override fun onDataNotAvailable() {
                    isRequesting.set(false)
                }

                override fun onError(errorMessage: String?) {
                    isRequesting.set(false)
                    snackbarMsg.value = errorMessage
                }

                override fun onLoaded(virturuns: List<VirtualRunModel>) {
                    listMyVirtualRun.apply {
                        clear()
                        addAll(virturuns)
                    }
                    isRequesting.set(false)
                }
            })
    }
}


/**
 * TODO add to bindings class
 * function for bindingsList
 *
 * object VirtualRunBindings {
 *
 * 	@BindingAdapter("app:listDataVirtualRun")
 *     @JvmStatic
 *     fun setListDataVirtualRun(recyclerView: RecyclerView, data: List<VirtualRunModel>) {
 *         with(recyclerView.adapter as VirtualRunAdapter) {
 *             replaceData(data)
 *         }
 *     }
 *
 * }
 *
 **/