package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.DetailNavigasiMenu
import id.otsuka.gits.sweatgen.databinding.ItemNavVirtualrunDetailBinding

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class NavigasiAdapter(val data: ArrayList<DetailNavigasiMenu>,
                      val mListener: DetailVirtualRunUserActionListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NavHolder).bind(data[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NavHolder(ItemNavVirtualrunDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = this@NavigasiAdapter.mListener
        })
    }

    class NavHolder(val mViewBinding: ItemNavVirtualrunDetailBinding) : RecyclerView.ViewHolder(mViewBinding.root) {
        fun bind(obj: DetailNavigasiMenu) {
            mViewBinding.nav = obj
        }
    }
}