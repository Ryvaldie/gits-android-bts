package id.otsuka.gits.sweatgen.data.model

import id.otsuka.gits.sweatgen.util.Other.TYPE_FREE_RUN

data class AddHistoryRunParams(
        val challenge_id: Int = 0,
        val user_id: Int = 0,
        val activity_name: String? = null,
        val distance: Double = 0.0,
        val step: Double = 0.0,
        val calories: Double = 0.0,
        val duration: Int = 0,
        val point: Int = 0,
        val type_run: String? = TYPE_FREE_RUN,
        val long_lat: String? = null,
        val virturunId: Int = 0
)