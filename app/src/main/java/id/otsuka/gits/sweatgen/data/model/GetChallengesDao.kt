package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class GetChallengesDao(val highlight_challenges: List<Challenge>?,
                            val my_challenges : List<Challenge>?,
                            val previous_challenges : List<Challenge>?
                            )