package id.otsuka.gits.sweatgen.data.model

import java.io.Serializable

data class Event(val id: Int = 0,
                 val name: String? = null,
                 val from_date_event: String? = null,
                 val end_date_event: String? = null,
                 val from_date_race: String? = null,
                 val end_date_race: String? = null,
                 val category: String? = null,
                 val address: String? = null,
                 val meta: EventMeta? = null,
                 val isExpire:Boolean

) : Serializable