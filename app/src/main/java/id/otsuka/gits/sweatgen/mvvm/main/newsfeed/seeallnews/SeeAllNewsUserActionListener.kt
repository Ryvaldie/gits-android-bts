package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews;


interface SeeAllNewsUserActionListener {

    // Example
    // fun onClickItem()

    fun onItemClick(newsId: Int)
}