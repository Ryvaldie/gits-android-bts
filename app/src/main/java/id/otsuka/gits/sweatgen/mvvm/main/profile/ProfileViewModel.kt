package id.otsuka.gits.sweatgen.mvvm.main.profile;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.graphics.Bitmap
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.profile.RecyclerAdapter.Companion.ITEM_PER_PAGE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class ProfileViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val bQrCode = ObservableField<Bitmap?>()

    var bStringCode = ObservableField("")

    var bUser = ObservableField<User>()

    val progressState = ObservableField(false)

    val eventLoading2 = SingleLiveEvent<Boolean>()

    val bMessage2 = ObservableField("")

    val eventLoading = SingleLiveEvent<Boolean>()

    val bMessage = ObservableField("")

    var bRunHistories = ObservableArrayList<ItemType>()

    val snackbarMsg = SingleLiveEvent<String>()

    var isLastPage = ObservableBoolean(false)

    val isRequesting = ObservableBoolean(false)

    val showProgress = SingleLiveEvent<Boolean>()

    var currentPage = 1

    var firstTime = true

    fun getUser(): User = (repository.getUser()) ?: User()

    fun showUser() {

        eventLoading.value = true

        repository.showUser(null, userId = getUser().id, callback = object : GitsDataSource.ShowUserCallback {
            override fun onUserLoaded(user: User) {
                repository.saveUser(user.copy(id = getUser().id, token = getUser().token))
                bUser.set(user)
                bStringCode.set(user.code)
                eventLoading.value = false
            }

            override fun onUserLoadFail(errorMessage: String?) {
                bMessage.set(errorMessage)
                eventLoading.value = false
            }
        })
    }

    fun getRunHistories() {

        isLastPage.set(false)

        if (firstTime) {
            progressState.set(true)
            firstTime = false
        }

        repository.getRunHistories("", (repository.getUser()
                ?: User()).id, currentPage, object : GitsDataSource.GetRunHistoriesCallback {
            override fun onDataLoaded(histories: List<ItemType>, page: Int) {
                if (currentPage == 1)
                    bRunHistories.clear()

                bRunHistories.addAll(histories)

                isLastPage.set(bRunHistories.size < ITEM_PER_PAGE)

                showProgress.value = isLastPage.get()

                progressState.set(false)

                isRequesting.set(false)
            }

            override fun onDataNotAvailable() {
                showProgress.value = true
                progressState.set(false)
                isLastPage.set(true)
                isRequesting.set(false)
            }

            override fun onFinish() {
              //  isRequesting.set(false)
            }

            override fun onError(errorMessage: String?) {
                progressState.set(false)
                snackbarMsg.value = errorMessage
                isLastPage.set(false)
                isRequesting.set(false)
            }
        },context)
    }


    fun logout() {
        eventLoading2.value = true
        repository.logout(null, object : GitsDataSource.LogoutCallback {
            override fun logoutSuccess() {
                eventLoading2.value = false
            }

            override fun logoutError(errorMessage: String?) {
                bMessage2.set(errorMessage)
                eventLoading2.value = false
            }
        }
        )
    }


}