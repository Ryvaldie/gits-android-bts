package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.databinding.ObservableInt
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.ImageGallery
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import io.reactivex.Observable


class GalleryViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val bUrl = ObservableField("")
}