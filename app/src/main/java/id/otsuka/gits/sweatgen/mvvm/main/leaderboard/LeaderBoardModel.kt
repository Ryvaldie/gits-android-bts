package id.otsuka.gits.sweatgen.mvvm.main.leaderboard


class LeaderBoardModel(
        val badgeSrc: Int = 0,
        val userImage: String,
        val achievement: String,
        val rank:String,
        val userName:String
)