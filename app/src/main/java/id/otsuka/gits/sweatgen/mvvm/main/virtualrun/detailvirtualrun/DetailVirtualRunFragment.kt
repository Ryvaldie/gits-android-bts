package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun

import android.arch.lifecycle.Observer
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentDetailVirtualRunBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment.Companion.HYDRATE_POINT
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment.Companion.PRIZE
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment.Companion.RULES
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunActivity
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.startvirtualrun.StartVirtualRunFragment
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.Other.TOKO_OTSUKA_URL
import id.otsuka.gits.sweatgen.util.customView.InputAccessCodeDialog

class DetailVirtualRunFragment : Fragment(), DetailVirtualRunUserActionListener {

    lateinit var mViewDataBinding: FragmentDetailVirtualRunBinding
    lateinit var mViewModel: DetailVirtualRunViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentDetailVirtualRunBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            if (arguments != null) {
                virturunId = arguments!!.getInt(VIRTUALRUN_ID)
            }
            start()
        }

        mViewDataBinding.virtuRun = mViewModel.bDetailVirtuRun

        mViewDataBinding.mListener = this@DetailVirtualRunFragment

        setupRecyclerNav()

        (requireActivity() as VirtualRunActivity).showHideToolbar(true)

        return mViewDataBinding.root

    }


    override fun onDialogCancel() {

    }

    override fun onDialogSubmit() {
        mViewModel.redeemSuccess()
    }

    override fun toStartRun() {
        startVirtualRun()
    }

    private fun startVirtualRun() {
        (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(StartVirtualRunFragment.newInstance(mViewModel.virturunId), R.id.frame_main_content)
    }

    override fun triggerDialog() {
        requireActivity().showImageDialog(InputAccessCodeDialog())

    }

    override fun call() {
        showDataNotAvailable()
    }

    override fun mail() {
        showDataNotAvailable()
    }

    override fun web() {
        showDataNotAvailable()
    }

    override fun toTokoOtsuka() {
        val httpIntent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(TOKO_OTSUKA_URL)
        }
        startActivity(httpIntent)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            hide()
        }

        mViewModel.snackbarMsg.observe(this, Observer {
            when (it) {
                TOKEN_EXPIRE -> {

                    mViewModel.repository.logout(null, null, true)

                    requireActivity().redirectToLogin()

                }
                else -> it?.showSnackBar(mViewDataBinding.root)
            }
        })
        mViewModel.submitNewDataEvent.observe(this, Observer {
            mViewDataBinding.virtuRun = mViewModel.bDetailVirtuRun
        })
        mViewDataBinding.toolbarVirturunDetail.setNavigationOnClickListener { requireActivity().onBackPressed() }

        mViewModel.afterRedeemSuccess.observe(this, Observer {
            mViewModel.start()
        })
    }


    private fun setupRecyclerNav() {
        mViewDataBinding.recyclerNav.apply {
            adapter = NavigasiAdapter(mViewModel.listNav, this@DetailVirtualRunFragment)
            layoutManager = GridLayoutManager(requireContext(), 3)
            addItemDecoration(GridSpacesItemDecoration(2.dpToPx(requireContext()), true))
        }
    }

    override fun seeMore() {
        mViewDataBinding.tvDescVirturun.expandCollapse()
        if (mViewDataBinding.tvDescVirturun.maxLines > 5) {
            mViewDataBinding.tvBtnExpandCollapse.text = getString(R.string.text_see_more)
        } else
            mViewDataBinding.tvBtnExpandCollapse.text = getString(R.string.text_less_more)

    }

    fun showDataNotAvailable() {
        mViewModel.snackbarMsg.value = getString(R.string.text_data_not_available)
    }

    override fun navItemClick(id: Int) {
        var idTitle = -1
        var jsonData = ""

        when (id) {
            R.drawable.ic_document -> {
                idTitle = RULES
                jsonData = Gson().toJson(mViewModel.rulesImageList)
            }

            R.drawable.ic_location2 -> {
                idTitle = HYDRATE_POINT
                jsonData = Gson().toJson(mViewModel.hydratePointImageList)
            }

            R.drawable.ic_gift -> {
                idTitle = PRIZE
                jsonData = Gson().toJson(mViewModel.prizeImageList)
            }
        }

        if (jsonData == "[]")
            showDataNotAvailable()
        else {
            requireActivity().replaceFragmentAndAddToBackStack(
                    RaceGuideCentralFragment.newInstance(jsonData, idTitle), R.id.frame_main_content)

            (requireActivity() as VirtualRunActivity).showHideToolbar(false)
        }
    }

    override fun onClickTest(text: String) {
        // TODO if you have a toast extension, you can replace this
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }


    // TODO import obtainViewModel & add DetailVirtualRunViewModel to ViewModelFactory
    fun obtainViewModel(): DetailVirtualRunViewModel = obtainViewModel(DetailVirtualRunViewModel::class.java)

    companion object {
        val VIRTUALRUN_ID = "id"
        fun newInstance(virturunId: Int) = DetailVirtualRunFragment().withArgs {
            putInt(VIRTUALRUN_ID, virturunId)
        }

    }

}
