package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.startvirtualrun

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.CustomDialogChallengeInfoBinding
import id.otsuka.gits.sweatgen.databinding.FragmentStartVirtualRunBinding
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity.Companion.REQ_TO_RUN_TRACKER
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.PARTICIPANT
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.Other.TYPE_VIRTURUN

class StartVirtualRunFragment : Fragment(), StartVirtualRunUserActionListener {

    lateinit var mViewDataBinding: FragmentStartVirtualRunBinding
    lateinit var mViewModel: StartVirtualRunViewModel
    val mDialog by lazy { buildDialog() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentStartVirtualRunBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            if (arguments != null) {
                virturunId = arguments!!.getInt(VIRTUALRUN_ID)
            }
        }

        mViewDataBinding.mListener = this@StartVirtualRunFragment

        return mViewDataBinding.root

    }

    override fun info() {
        mDialog.show()
    }

    private fun buildDialog(): AlertDialog {
        val dialog = AlertDialog.Builder(context!!).create()

        val inflater = layoutInflater

        val alertView = CustomDialogChallengeInfoBinding.inflate(inflater).apply {
            desc = getString(R.string.text_lorem)
            tviewYes.setOnClickListener { dialog.dismiss() }
            tvDescChallenge.movementMethod = ScrollingMovementMethod()
        }

        dialog.setView(alertView.root)

        dialog.setCancelable(false)

        return dialog
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_TO_RUN_TRACKER) {
                data?.getStringExtra("message")!!.showCustomSnackBar(mViewDataBinding.root, color = ContextCompat.getColor(requireContext(), R.color.colorGpsActivated))
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mViewModel.start()
    }

    override fun startRun() {

        buildParam()

        if ((mViewModel.bVirtualRun.get()?.run_status ?: "") == PARTICIPANT)
            if (!mViewModel.isStarted)
                mViewModel.snackbarMsg.value = getString(R.string.text_not_today)
            else
                mViewModel.joinVirtualRun()
        else
            RunTrackerActivity.startActivityForChallenge(context!!,3, Gson().toJson(mViewModel.challenge))
    }

    private fun buildParam() {
        mViewModel.bVirtualRun.get()?.apply {
            mViewModel.challenge = Challenge(
                    this.id,
                    name,
                    from_date_event,
                    end_date_event,
                    challenge_description,
                    category,
                    duration ?: 0,
                    distance ?: 0.0,
                    points ?: 0,
                    icon,
                    banner,
                    "",
                    0,
                    "",
                    "",
                    0,
                    "",
                    0.0,
                    0.0,
                    "",
                    "",
                    0
            )
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            hide()
        }

        mViewModel.eventStartRun.observe(this, Observer {
            if (mViewModel.bVirtualRun.get() != null) {

                RunTrackerActivity.startActivityForChallenge(context!!, 3, Gson().toJson(mViewModel.challenge))

            } else {
                getString(R.string.text_please_refresh).showSnackBar(mViewDataBinding.root)
            }
        })
        mViewModel.snackbarMsg.observe(this, Observer {
            when (it) {
                TOKEN_EXPIRE -> {

                    mViewModel.repository.logout(null, null, true)

                    requireActivity().redirectToLogin()

                }
                else -> it?.showSnackBar(mViewDataBinding.root)
            }
        })
        mViewDataBinding.toolbarStartVirturun.setNavigationOnClickListener { requireActivity().onBackPressed() }
    }

    override fun toRunningHistory() {
        //  RunningHistoryActivity.startActivityForChallenge(requireContext(), 0)
    }

    override fun toLeaderBoard() {
        LeaderBoardActivity.startActivity(
                LeaderBoardActivity.VIRTURUN_TYPE,
                requireContext(),
                mViewModel.bVirtualRun.get()?.daysLeft ?: "",
                "0",
                "0",
                mViewModel.bVirtualRun.get()?.id ?: 0,
                0,
                false
        )
    }


    override fun onClickTest(text: String) {
        // TODO if you have a toast extension, you can replace this
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }


    // TODO import obtainViewModel & add StartVirtualRunViewModel to ViewModelFactory
    fun obtainViewModel(): StartVirtualRunViewModel = obtainViewModel(StartVirtualRunViewModel::class.java)

    companion object {
        val VIRTUALRUN_ID = "id"
        fun newInstance(virturunId: Int) = StartVirtualRunFragment().withArgs {
            putInt(VIRTUALRUN_ID, virturunId)
        }

    }

}
