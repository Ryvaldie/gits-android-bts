package id.otsuka.gits.sweatgen.mvvm.onboard.login.verificationpassword


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.databinding.FragmentVerificationPasswordBinding
import id.otsuka.gits.sweatgen.mvvm.onboard.login.LoginFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.login.verificationpassword.VerificationPasswordViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentAndAddToBackStack
import id.otsuka.gits.sweatgen.util.showSnackBar
import kotlinx.android.synthetic.main.fragment_verification_password.*


class VerificationPasswordFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentVerificationPasswordBinding
    lateinit var mViewModel: VerificationPasswordViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentVerificationPasswordBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : VerificationPasswordUserActionListener {
            override fun backToOnBoard() {
                activity?.onBackPressed()
            }

            override fun setPassword() {
                if (mViewModel.validateFields()) {
                    if (mViewModel.validateRetype())
                        mViewModel.setNewPassword()
                    else
                        getString(R.string.text_check_retype_pass).showSnackBar(mViewDataBinding.root)
                } else {
                    getString(R.string.text_please_fill_info).showSnackBar(mViewDataBinding.root)
                }
            }

            override fun onResendCode() {
                mViewModel.resendCode()
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer {
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {
                hideProgressDialog()
                if (mViewModel.isSuccess.get()) {
                    getString(R.string.text_set_new_password_success).showSnackBar(mViewDataBinding.root)
                    activity?.onBackPressed()
                } else {
                    mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
                }
            }
        })

        mViewModel.resendLoading.observe(this, Observer {
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {
                hideProgressDialog()
                getString(R.string.text_resend_code_success).showSnackBar(mViewDataBinding.root)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).setSupportActionBar(toolbar_verification_password)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)


        toolbar_verification_password.setNavigationOnClickListener { mViewDataBinding.mListener?.backToOnBoard() }
    }

    fun obtainViewModel(): VerificationPasswordViewModel = obtainViewModel(VerificationPasswordViewModel::class.java)

    companion object {
        fun newInstance() = VerificationPasswordFragment().apply {

        }

    }

}
