package id.otsuka.gits.sweatgen.mvvm.onboard

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.OnBoardImage
import id.otsuka.gits.sweatgen.util.customView.ViewpagerCustomDuration

/**
 * @author radhikayusuf.
 */

object OnBoardBindings {

    @BindingAdapter("app:imageUrl", "app:listener", requireAll = false)
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?, listener: RequestListener<Drawable>?) {

        val finalImage: Any = imageUrl ?: R.drawable.img_slider_event

        if (listener != null)
            Glide.with(view.context)
                    .load(finalImage)
                    .listener(listener)
                    .into(view)
                    .apply {
                        RequestOptions().diskCacheStrategy
                    }
        else
            Glide.with(view.context)
                    .load(finalImage)
                    .into(view)
                    .apply {
                        RequestOptions().diskCacheStrategy
                    }


    }

    @BindingAdapter("app:vpAdapter")
    @JvmStatic
    fun setupViewpagerAdapter(viewPager: ViewpagerCustomDuration, pagesContent: List<OnBoardImage>) {

        viewPager.apply {
            setScrollDuration(1000)

            adapter = ViewPagerAdapter(viewPager.context, ArrayList(0))

            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageSelected(position: Int) {

                    val linDots = (parent as View).findViewById<LinearLayout>(R.id.lin_vp_indicator)
                    if (linDots.childCount > 0) {
                        for (x in 0..viewPager.adapter?.count!!.minus(1)) {
                            if (linDots.getChildAt(x) != null) {
                                (linDots.getChildAt(x) as ImageView).apply {
                                    setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_inactive_vp_indicator))
                                }
                            }

                        }
                        if (linDots.getChildAt(position) != null) {
                            (linDots.getChildAt(position) as ImageView).apply {
                                setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_active_vp_indicator))
                            }
                        }
                    }


                }

                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }
            })
            (adapter as ViewPagerAdapter).replacePagesContent(pagesContent)

        }
    }

}