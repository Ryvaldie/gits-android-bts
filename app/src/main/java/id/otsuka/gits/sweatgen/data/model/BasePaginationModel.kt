package id.otsuka.gits.sweatgen.data.model

data class BasePaginationModel<T>(
        var content: T? = null,
        val paginate: Paginate? = null

        //Todo code above just for testing. Change it with real base response from API
)