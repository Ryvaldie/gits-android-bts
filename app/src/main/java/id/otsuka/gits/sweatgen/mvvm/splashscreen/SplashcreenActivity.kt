package id.otsuka.gits.sweatgen.mvvm.splashscreen;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.onboard.OnBoardActivity
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity

class SplashcreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashcreen)
        setupFragment()
        moveToOnBoard()
    }

    private fun moveToOnBoard() {
        Handler().postDelayed({ OnBoardActivity.startActivity(this) }, 2000)

    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        SplashcreenFragment.newInstance().let {
            // TODO if template have an error, please reimport replaceFragmentInActivity
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, SplashcreenActivity::class.java))
        }
    }
}
