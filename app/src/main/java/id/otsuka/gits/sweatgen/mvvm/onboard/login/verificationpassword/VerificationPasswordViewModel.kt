package id.otsuka.gits.sweatgen.mvvm.onboard.login.verificationpassword;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.ForgotPasswordDao
import id.otsuka.gits.sweatgen.data.model.NewPasswordParams
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import okhttp3.MediaType
import okhttp3.RequestBody


class VerificationPasswordViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bCode = ObservableField("")
    val bPassword= ObservableField("")
    val bRetypePassword= ObservableField("")
    val bMessage = ObservableField("")
    val isSuccess = ObservableBoolean(false)

    val eventLoading = SingleLiveEvent<Boolean>()

    val resendLoading = SingleLiveEvent<Boolean>()

    fun validateFields(): Boolean = (!bCode.get().isNullOrEmpty() && !bPassword.get().isNullOrEmpty() && !bRetypePassword.get().isNullOrEmpty())

    fun validateRetype(): Boolean = bPassword.get().toString() == bRetypePassword.get().toString()

    fun setNewPassword() {

        eventLoading.value = true

        val newPasswordParams = NewPasswordParams(bCode.get(), bPassword.get())

        repository.setNewPassword(newPasswordParams, object : GitsDataSource.SetNewPasswordCallback {
            override fun onSetNewPasswordSuccess(msg: String?) {
                isSuccess.set(true)
                eventLoading.value = false
            }

            override fun onSetNewPasswordFailed(errorMessage: String?) {
                isSuccess.set(false)
                bMessage.set(errorMessage)
                eventLoading.value = false
            }
        })
    }

    fun resendCode() {

        eventLoading.value = true

        repository.resendCode(repository.getForgotPasswordDao().forgot_token, object : GitsDataSource.ResendCodeCallback {
            override fun onSendCodeSuccess(msg: String?) {
                isSuccess.set(true)
                resendLoading.value = false
            }

            override fun onSendCodeFailed(errorMessage: String?) {
                isSuccess.set(false)
                bMessage.set(errorMessage)
                resendLoading.value = false

            }
        })
    }



}