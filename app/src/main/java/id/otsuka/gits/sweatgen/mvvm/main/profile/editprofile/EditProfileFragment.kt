package id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile


import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.databinding.FragmentEditProfileBinding
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.helper.CameraGalleryHelper
import id.otsuka.gits.sweatgen.util.helper.FileHelper
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


class EditProfileFragment : BaseFragment(), ChooseImageSourceListener, DatePickerDialog.OnDateSetListener {

    lateinit var mViewDataBinding: FragmentEditProfileBinding
    lateinit var mViewModel: EditProfileViewModel
    val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentEditProfileBinding.inflate(inflater, container!!, false).apply {
            mViewModel = obtainVm().apply {

                mUser = repository.getUser()

                with(repository.getUser() ?: User()) {

                    bUserPhoto.set(picture)

                    bFullName.set(name)

                    bDateOfBirth.set(dob)

                    if (gender == getString(R.string.text_male))
                        bGender.set(R.id.radio_male)
                    else
                        bGender.set(R.id.radio_female)
                }
            }
        }

        this.mViewModel = mViewDataBinding.mViewModel!!

        mViewDataBinding.mListener = object : EditProfileUserActionListener {
            override fun saveClicked() {

                mViewModel.setGender()

                if (mViewModel.validateFields()) {
                    mViewModel.eventLoading.value = true

                    when (mViewModel.bUserPhoto.get()) {
                        is File -> {
                            mViewModel.editMyProfile()
                        }
                        is String -> {
                            if (!hasPermission(permissions)) {
                                mViewModel.eventLoading.value = false
                                permissionRequest(permissions)
                            } else {
                                if (mViewDataBinding.imgvUser.drawable != null) {
                                    saveTempImg((mViewDataBinding.imgvUser.drawable as BitmapDrawable).convertToByteArray())
                                    mViewModel.editMyProfile()
                                } else {
                                    getString(R.string.text_please_upload_picture).showSnackBar(mViewDataBinding.root)
                                }
                            }
                        }
                    }

                } else {
                    getString(R.string.text_please_upload_picture).showSnackBar(mViewDataBinding.root)
                }
            }

            override fun triggerDatePicker(view: View) {
                showDatePicker(this@EditProfileFragment, (view as EditText).text.toString())
            }

            override fun triggerPickImage() {
                CameraGalleryHelper.openImagePickerOption(this@EditProfileFragment)
            }

            override fun onNavigationBack() {
                activity?.onBackPressed()
            }
        }



        return mViewDataBinding.root

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        if (year > (Calendar.getInstance().get(Calendar.YEAR) - 5)) {
            showToast(getString(R.string.text_minimum_age_require))
        } else {
            mViewModel.bDateOfBirth.set(year.toString() + "-" + month.plus(1).toString().mToMm() + "-" + dayOfMonth.toString())
        }
    }

    private fun saveTempImg(byteImage: ByteArray) {
        val imgFile = "img-${System.currentTimeMillis()}.jpg"
        val image = saveImge(byteImage, imgFile)
        mViewModel.bUserPhoto.set(image)
    }

    private fun permissionCheck() {

        if (!hasPermission(permissions)) {
            permissionRequest(permissions)
        } else {
            chooseImageSource(this)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer {
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)

            } else {
                hideProgressDialog()
                if (mViewModel.isSuccess.get()) {
                    activity?.onBackPressed()
                } else {
                    if (!mViewModel.bMessage.get().isNullOrEmpty())
                        if (mViewModel.bMessage.get() == TOKEN_EXPIRE) {
                            mViewModel.repository.logout(null, null, true)
                            activity!!.redirectToLogin()
                        } else
                            mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE -> {

                    val imageUri = CropImage.getPickImageResultUri(requireActivity(), data)

                    val file = File(FileHelper.getRealPathFromURI(requireActivity(), imageUri))
                    if (!CameraGalleryHelper.isImage(file)) {
                        // getString(R.string.text_is_not_image).showSnackBar(mViewDataBinding.root)
                        return
                    }

                    CropImage.activity(imageUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAllowRotation(false)
                            .setRequestedSize(500, 500)
                            .setCropShape(CropImageView.CropShape.OVAL)
                            .setAspectRatio(1, 1)
                            .start(requireContext(), this@EditProfileFragment)
                }
                else -> {
                    val intentResult = CropImage.getActivityResult(data)
                    val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, intentResult.uri)
                    val bytes = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
                    val byteArray = bytes.toByteArray()
                    val imgFile = "img-${System.currentTimeMillis()}.jpg"
                    mViewModel.bUserPhoto.set(saveImge(byteArray, imgFile))
                }
            }
        }
    }

    private fun shareToStory(file: File) {
        val uri = Uri.fromFile(file)
        val shareIntent = Intent("com.instagram.share.ADD_TO_STORY")
        shareIntent.setDataAndType(uri, "image/*")
        shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        if (requireActivity().packageManager.resolveActivity(shareIntent, 0) != null) {
            requireActivity().startActivityForResult(shareIntent, 0)
        }
    }

    private fun hasPermission(permissions: Array<String>): Boolean {
        if (permissions.isNotEmpty()) {
            for (permission in permissions) {
                if (ContextCompat.checkSelfPermission(activity!!, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }

        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_edit_profile.setNavigationOnClickListener { mViewDataBinding.mListener?.onNavigationBack() }


    }

    private fun permissionRequest(permissions: Array<String>) {
        ActivityCompat.requestPermissions(activity!!,
                permissions,
                1001)
    }

    override fun onItemSelected(itemId: Int) {
        when (itemId) {
            0 -> {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                activity!!.startActivityForResult(intent, REQUEST_CAMERA)
            }
            1 -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT

                activity!!.startActivityForResult(Intent.createChooser(intent, "Select Image"), REQUEST_GALLERY)
            }
        }
    }

    fun obtainVm(): EditProfileViewModel = obtainViewModel(EditProfileViewModel::class.java)


    companion object {

        val REQUEST_CAMERA = 111

        val REQUEST_GALLERY = 112

        fun newInstance() = EditProfileFragment().apply {

        }

    }

}
