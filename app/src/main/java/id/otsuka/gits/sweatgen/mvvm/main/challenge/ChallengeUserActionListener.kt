package id.otsuka.gits.sweatgen.mvvm.main.challenge;


interface ChallengeUserActionListener {

    fun onItemSelected(id: Int)
    fun onSeeAllClicked(challengeType: Int)
}