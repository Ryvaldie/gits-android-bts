package id.otsuka.gits.sweatgen.mvvm.main.search.resultevent


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.databinding.ItemResultEventBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailActivity


class ResultEventAdapter(val context: Context,
                         var result: List<Event>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = result.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ResultVH).bind(result[position])
    }

    fun replaceData(result: List<Event>) {
        this.result = result
        notifyDataSetChanged()
    }

    interface ItemClickListener {
        fun onItemClicked(idChallenge: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mViewDataBinding: ItemResultEventBinding = ItemResultEventBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = object : ResultEventAdapter.ItemClickListener {
                override fun onItemClicked(idChallenge: Int) {
                    EventDetailActivity.startActivity(context, idChallenge)
                }
            }
        }
        return ResultVH(mViewDataBinding)
    }

    class ResultVH(val mViewDataBinding: ItemResultEventBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {
        fun bind(obj: Event) {
            mViewDataBinding.mData = obj
        }
    }


}