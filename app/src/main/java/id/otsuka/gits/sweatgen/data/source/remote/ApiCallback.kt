package id.otsuka.gits.sweatgen.data.source.remote

import com.google.gson.Gson
import id.otsuka.gits.sweatgen.base.BaseResponse
import id.otsuka.gits.sweatgen.data.model.Message
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by irfanirawansukirman on 17/01/18.
 */

abstract class ApiCallback<M> : Observer<M> {

    abstract fun onSuccess(model: M)

    abstract fun onFailure(errorMessage: String, errorMsg: String?)

    abstract fun onFinish()

    override fun onComplete() {
        onFinish()
    }

    override fun onNext(t: M) {
        onSuccess(t)
    }

    override fun onSubscribe(d: Disposable) {

    }

    override fun onError(e: Throwable) {
        e.printStackTrace()
        when (e) {
            is HttpException -> {
                val httpException = e
                //httpException.response().errorBody().string()
                val code = httpException.code()
                var msg = httpException.message()
                var msg2: String? = ""
                var baseDao: BaseResponse<M, M>? = null
                var body: ResponseBody? = null

                try {
                    body = httpException.response().errorBody()

                    baseDao = Gson().fromJson<BaseResponse<M, M>>(body!!.string(), BaseResponse::class.java)
                } catch (exception: Exception) {
                    onFailure(exception.message!!, null)
                }

                when (code) {
                    502 -> {
                        msg = httpException.message()
                    }
                    426 -> {
                        msg = code.toString()
                    }
                    else -> {
                        if (baseDao?.message != null) {

                            if (baseDao.message.toString().contains("{")) {

                                val json = Gson().toJson(baseDao.message)

                                val dao = Gson().fromJson<Message>(json, Message::class.java)

                                with(dao) {

                                    if (baseDao.status.toString().replace(".0", "").toInt() == 404) {

                                        msg = this.code ?: "-"
                                    } else {
                                        msg = if (email.isNullOrEmpty()) "" else email
                                        msg2 = if (password.isNullOrEmpty()) "" else password
                                    }
                                }
                            } else {
                                msg = if (code == 401)
                                    TOKEN_EXPIRE
                                else
                                    baseDao.message.toString()
                            }

                        } else {
                            if (code == 401)
                                msg = TOKEN_EXPIRE
                        }
                    }
                }
                onFailure(msg, msg2)

            }
            is UnknownHostException -> onFailure("An error occurred while connecting to the server", null)
            is SocketTimeoutException -> onFailure("An error occurred while connecting to the server", null)
            is ConnectException -> onFailure("No Internet Connection", null)
            else -> onFailure(e.message ?: "", null)
        }
        onFinish()
    }
}