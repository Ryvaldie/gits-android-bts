package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.AddHistoryRunParams
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.sharefinalpage.ShareFinalPageFragment
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import kotlinx.android.synthetic.main.activity_share_run.*


class ShareRunActivity : AppCompatActivity(), ShareRunNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_run)
        setupFragment()
        setupToolbar()
        strictMode()
        getRunDatas()
    }

    private fun getRunDatas() {
        if (intent != null) {
            obtainViewModel().apply {
                val json = intent.getStringExtra(RUN_HISTORY_PARAM)
                val param = Gson().fromJson<AddHistoryRunParams>(json, AddHistoryRunParams::class.java)
                obsAddRunHistory.set(param)
            }
        }
    }

    private fun strictMode() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar_share)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_left_arrow_white)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            android.R.id.home -> {
                detectLatestFragment()
                super.onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }

    }

    private fun detectLatestFragment() {
        val fragment: Fragment? = supportFragmentManager.findFragmentById(R.id.frame_main_content)
        if (fragment != null) {
            when (fragment) {
                is ShareEditImageFragment -> obtainViewModel().bCapturedImage.set(null)
                is ShareFinalPageFragment -> obtainViewModel().bEditedImage.set(null)
            }
        }
    }

    // TODO add ShareRunViewModel to ViewModelFactory & if template have an error, please reimport obtainViewModel
    fun obtainViewModel(): ShareRunViewModel = obtainViewModel(ShareRunViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        ShareRunFragment.newInstance().let {
            // TODO if template have an error, please reimport replaceFragmentInActivity
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        const val LOCATION_INTENT = "locations"
        const val RUN_HISTORY_PARAM = "runHistoryParam"

        fun startActivity(locationJson: String?, runHistoryParamJson: String, context: Context) {
            context.startActivity(
                    Intent(context, ShareRunActivity::class.java).apply {
                        putExtra(LOCATION_INTENT, locationJson)
                        putExtra(RUN_HISTORY_PARAM, runHistoryParamJson)
                    }
            )
        }
    }
}
