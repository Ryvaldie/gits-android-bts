package id.otsuka.gits.sweatgen.mvvm.main.discussion


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentDiscussionBinding
import id.otsuka.gits.sweatgen.mvvm.main.discussion.DiscussionViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel


class DiscussionFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentDiscussionBinding
    lateinit var mViewModel: DiscussionViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentDiscussionBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : DiscussionUserActionListener {

        }



        return mViewDataBinding.root

    }

    fun obtainViewModel(): DiscussionViewModel = obtainViewModel(DiscussionViewModel::class.java)

    companion object {
        fun newInstance() = DiscussionFragment().apply {

        }

    }

}
