package id.otsuka.gits.sweatgen.mvvm.main.virtualrun

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.databinding.ItemJoinVirtualRunBinding

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class VirtualRunAdapter(var data: ArrayList<VirtualRunModel>,
                        val vm: VirtualRunViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = data.size

    fun replaceData(data: List<VirtualRunModel>) {
        this.data.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as EventHolder).bind(data[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return EventHolder(ItemJoinVirtualRunBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = object : VirtualRunUserActionListener {
                override fun onItemClick(id: Int) {
                    vm.eventItemClick.value = id
                }

                override fun toInfo() {

                }
            }
        })
    }

    class EventHolder(val mViewBinding: ItemJoinVirtualRunBinding) : RecyclerView.ViewHolder(mViewBinding.root) {
        fun bind(obj: VirtualRunModel) {
            mViewBinding.virturun = obj
        }
    }
}