package id.otsuka.gits.sweatgen.mvvm.main.virtualrun;


interface VirtualRunUserActionListener {

    fun onItemClick(id: Int)

    fun toInfo()

}