package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.startvirtualrun;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.text.format.DateUtils
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.model.VirtualRunDetail
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.Other.JOINED_RUN
import id.otsuka.gits.sweatgen.util.Other.PARTICIPANT
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.toFormattedDate
import java.util.*

class StartVirtualRunViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bVirtualRun = ObservableField<VirtualRunDetail>()

    val bProgressString = ObservableField("")

    val bProgress = ObservableInt(0)

    val bDaysLeft = ObservableField("3")

    var virturunId = 0

    var isStarted = true

    val bCurrentDistance = ObservableField("")

    val isFinished = ObservableBoolean(false)

    val snackbarMsg = SingleLiveEvent<String>()

    var challenge = Challenge()

    val isRequesting = ObservableBoolean(false)

    val isButtonEnable = ObservableBoolean(true)

    val bButtonText = ObservableField("")

    val isBtnRequesting = ObservableBoolean(false)

    val eventStartRun = SingleLiveEvent<Void>()

    fun blockAccess() {
        isFinished.set(true)
        isButtonEnable.set(false)
    }

    fun joinVirtualRun() {
        isBtnRequesting.set(true)
        repository.postJoinVirturun("", virturunId, repository.getUser()?.id
                ?: 0, object : GitsDataSource.PostJoinChallengeCallback {
            override fun onFailed(errorMessage: String?) {
                snackbarMsg.value = errorMessage
            }

            override fun onFinish() {
                isBtnRequesting.set(false)
            }

            override fun onSuccess() {
                eventStartRun.call()
            }
        })
    }

    fun start() {

        isRequesting.set(true)

        repository.getVirturunById("", virturunId, repository.getUser()?.id
                ?: 0, object : GitsDataSource.GetVirturunByIdCallback {
            override fun onError(errorMessage: String?) {
                snackbarMsg.value = errorMessage
            }

            override fun onFinish() {
                isRequesting.set(false)
            }

            override fun onLoaded(virturun: VirtualRunDetail) {

                bDaysLeft.set(virturun.daysLeft)

                bVirtualRun.set(virturun)

                val date = virturun.from_date_event?.toFormattedDate("yyyy-MM-dd", Locale("id", "ID"))

                if (date != null) {
                    isStarted = date.before(Date()) || DateUtils.isToday(date.time)
                }

                if (virturun.run_status != null) {

                    var btnText = ""

                    when (virturun.run_status) {
                        PARTICIPANT -> btnText = context.getString(R.string.text_join_now)
                        JOINED_RUN -> btnText = context.getString(R.string.text_start_now)
                        else -> {
                            btnText = context.getString(R.string.text_finished)
                            blockAccess()
                        }
                    }
                    bButtonText.set(btnText)
                }
            }
        })
    }

}