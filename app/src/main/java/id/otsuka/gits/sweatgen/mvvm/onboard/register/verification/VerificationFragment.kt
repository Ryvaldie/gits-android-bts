package id.otsuka.gits.sweatgen.mvvm.onboard.register.verification


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.databinding.FragmentVerificationBinding
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.withArgs
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.replaceFragmentAndAddToBackStack
import id.otsuka.gits.sweatgen.util.showSnackBar
import id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio.PersonalBioFragment
import kotlinx.android.synthetic.main.fragment_verification.*

class VerificationFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentVerificationBinding
    lateinit var mViewModel: VerificationViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentVerificationBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            getTempUser()
        }

        mViewDataBinding.mListener = object : VerificationUserActionListener {
            override fun onResendEmail() {
                mViewModel.resendEmail()
            }

            override fun backToOnBoard() {
                activity?.onBackPressed()
            }

            override fun goToPersonalBio() {
                mViewModel.userValidation()
            }
        }



        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar_verification)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)


        toolbar_verification.setNavigationOnClickListener { mViewDataBinding.mListener?.backToOnBoard() }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer { it ->
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {

                hideProgressDialog()

                if (mViewModel.isValid.get())
                    activity?.replaceFragmentAndAddToBackStack(PersonalBioFragment.newInstance(), R.id.frame_main_content)
                else
                    mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
            }
        })
    }

    fun obtainViewModel(): VerificationViewModel = obtainViewModel(VerificationViewModel::class.java)

    companion object {
        fun newInstance() = VerificationFragment().withArgs {
        }

    }

}
