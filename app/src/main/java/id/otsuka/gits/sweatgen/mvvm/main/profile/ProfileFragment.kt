package id.otsuka.gits.sweatgen.mvvm.main.profile


import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentProfileBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.profile.RecyclerAdapter.Companion.ITEM_PER_PAGE
import id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile.EditProfileActivity
import id.otsuka.gits.sweatgen.mvvm.main.reward.RewardActivity
import id.otsuka.gits.sweatgen.mvvm.onboard.OnBoardActivity
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.NO_INET_CONNECTION
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment(), RequestListener<Drawable> {

    lateinit var mViewDataBinding: FragmentProfileBinding
    lateinit var mViewModel: ProfileViewModel
    var editResume = false

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_toolbar_profile, menu)

        if (menu is MenuBuilder) {

            menu.setOptionalIconsVisible(true)
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
        return false
    }

    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_qr -> {
                showQRDialog(mViewModel)
            }
            R.id.menu_edit_profile -> {
                editResume = true
                EditProfileActivity.startActivity(context!!)
            }

            R.id.menu_logout -> {

                mViewModel.logout()

                mViewModel.eventLoading2.observe(this, Observer {
                    if (it!!) {

                    } else {
                        if (mViewModel.bMessage2.get().isNullOrEmpty()) {
                            OnBoardActivity.startActivity(context!!)
                            activity!!.finish()
                        } else {
                            mViewModel.bMessage2.get()?.showSnackBar(mViewDataBinding.root)
                        }


                    }
                })

            }
        }
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        setupRecycler(savedInstanceState)

        mViewModel.eventLoading.observe(this, Observer { it ->
            if (it!!) {
                if (!swipe_refresh_profile.isRefreshing) {
                    frame_progbar.visibility = View.VISIBLE
                }
            } else {
                if (!swipe_refresh_profile.isRefreshing) {
                    frame_progbar.visibility = View.GONE
                } else {
                    swipe_refresh_profile.isRefreshing = false
                }

                if (mViewModel.bMessage.get().isNullOrEmpty())
                    mViewModel.bQrCode.set(generateQRCode(mViewModel.getUser().code))
                else {

                    when (mViewModel.bMessage.get()) {

                        TOKEN_EXPIRE -> {

                            mViewModel.repository.logout(null, null, true)

                            activity!!.redirectToLogin()
                        }
                        NO_INET_CONNECTION -> {
                            (activity as MainActivity).showNoInetConnection()
                        }
                        else -> {
                            mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
                        }
                    }

                    mViewModel.bMessage.set("")
                }
            }
        })

        mViewModel.snackbarMsg.observe(this, Observer {
            when (it) {
                TOKEN_EXPIRE -> {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                }
                Other.NO_INET_CONNECTION -> {
                    if (mViewModel.currentPage == 1)
                        (activity as MainActivity).showNoInetConnection()
                    else
                        it.showSnackBar(mViewDataBinding.root)
                }
                else -> {
                    (it ?: "-").showSnackBar(mViewDataBinding.root)
                }
            }
        })

        swipe_refresh_profile.setOnRefreshListener {
            refreshData()
        }

        mViewModel.showUser()
        mViewModel.getRunHistories()

    }

    fun refreshData() {
        mViewModel.showUser()
        mViewModel.currentPage = 1
        mViewModel.getRunHistories()
    }

    private fun setupRecycler(savedInstanceState: Bundle?) {
        recyview_history.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            layoutManager.supportsPredictiveItemAnimations()
            adapter = RecyclerAdapter(ArrayList(0), savedInstanceState, requireActivity(), mViewModel)

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val layoutManager = mViewDataBinding.recyviewHistory.layoutManager
                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisibleItemPosition = (mViewDataBinding.recyviewHistory.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                    if (!(mViewModel.isLastPage.get())) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                                firstVisibleItemPosition >= 0
                                && totalItemCount >= ITEM_PER_PAGE) {

                            if (!mViewModel.isRequesting.get()) {
                                mViewModel.isRequesting.set(true)
                                Handler().postDelayed({
                                    mViewModel.getRunHistories()
                                }, 1000)
                            }

                        }
                    }
                }
            })

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentProfileBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            showProgress.observe(this@ProfileFragment, Observer {
                if (it != null)
                    (mViewDataBinding.recyviewHistory.adapter as RecyclerAdapter).showProgress(!it)
            })
        }

        mViewDataBinding.mListener = object : ProfileUserActionListener {
            override fun toReward() {
                RewardActivity.startActivity(requireContext())
            }

            override fun convertMapToImage(v: View) {

            }
        }
        mViewDataBinding.listener = this@ProfileFragment

        return mViewDataBinding.root
    }

    fun obtainViewModel(): ProfileViewModel = obtainViewModel(ProfileViewModel::class.java)

    override fun onResume() {
        super.onResume()
        if (editResume) {
            refreshData()
            editResume = false
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            (activity as MainActivity).mViewDataBinding?.mViewModel?.loadLayout?.set(false)
        }
    }

    companion object {
        fun newInstance() = ProfileFragment().apply {

        }

    }

}
