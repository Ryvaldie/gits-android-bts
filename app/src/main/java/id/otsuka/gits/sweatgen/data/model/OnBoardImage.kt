package id.otsuka.gits.sweatgen.data.model

import java.io.Serializable

data class OnBoardImage (
        val id: Int = 0,
        val title: String? = null,
        val desc: String? = null,
        val image: String? = null
) : Serializable