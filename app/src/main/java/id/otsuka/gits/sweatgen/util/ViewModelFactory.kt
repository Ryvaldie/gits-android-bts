package id.otsuka.gits.sweatgen.util

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.MainViewModel
import id.otsuka.gits.sweatgen.mvvm.main.challenge.ChallengeViewModel
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail.ChallengeDetailViewModel
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllViewModel
import id.otsuka.gits.sweatgen.mvvm.main.discussion.DiscussionViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.EventViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.eventrundown.EventRundownViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery.GalleryViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.livetracker.LiveTrackerViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceresult.RaceResultViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.threedmap.ThreeDiRaceMapViewModel
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardViewModel
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsFeedViewModel
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail.NewsDetailViewModel
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.SeeAllNewsViewModel
import id.otsuka.gits.sweatgen.mvvm.main.profile.ProfileViewModel
import id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile.EditProfileViewModel
import id.otsuka.gits.sweatgen.mvvm.main.reward.RewardViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.runningdetail.RunningDetailsViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.challengeresult.ChallengeResultViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.postingpage.PostingPageViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll.CameraRollViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.customcam.CustomCameraViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.rundata.RunDataViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers.StickersViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.route.ShareRouteViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.sharefinalpage.ShareFinalPageViewModel
import id.otsuka.gits.sweatgen.mvvm.main.search.SearchViewModel
import id.otsuka.gits.sweatgen.mvvm.main.search.result.ResultViewModel
import id.otsuka.gits.sweatgen.mvvm.main.search.resultevent.ResultEventViewModel
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun.DetailVirtualRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.startvirtualrun.StartVirtualRunViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.OnBoardViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.login.LoginViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.login.forgotpassword.ForgotPasswordViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.login.verificationpassword.VerificationPasswordViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.register.RegisterViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio.PersonalBioFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio.PersonalBioViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.register.verification.VerificationViewModel

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

class ViewModelFactory private constructor(
        private val mApplication: Application,
        private val gitsRepository: GitsRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
                    isAssignableFrom(RegisterViewModel::class.java) ->
                        RegisterViewModel(mApplication, gitsRepository)
                    isAssignableFrom(VerificationViewModel::class.java) ->
                        VerificationViewModel(mApplication, gitsRepository)
                    isAssignableFrom(LoginViewModel::class.java) ->
                        LoginViewModel(mApplication, gitsRepository)
                    isAssignableFrom(PersonalBioFragment::class.java) ->
                        PersonalBioViewModel(mApplication, gitsRepository)
                    isAssignableFrom(OnBoardViewModel::class.java) ->
                        OnBoardViewModel(mApplication, gitsRepository)
                    isAssignableFrom(MainViewModel::class.java) ->
                        MainViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ProfileViewModel::class.java) ->
                        ProfileViewModel(mApplication, gitsRepository)
                    isAssignableFrom(PersonalBioViewModel::class.java) ->
                        PersonalBioViewModel(mApplication, gitsRepository)
                    isAssignableFrom(EditProfileViewModel::class.java) ->
                        EditProfileViewModel(mApplication, gitsRepository)
                    isAssignableFrom(EventViewModel::class.java) ->
                        EventViewModel(mApplication, gitsRepository)
                    isAssignableFrom(EventDetailViewModel::class.java) ->
                        EventDetailViewModel(mApplication, gitsRepository)
                    isAssignableFrom(GalleryViewModel::class.java) ->
                        GalleryViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ThreeDiRaceMapViewModel::class.java) ->
                        ThreeDiRaceMapViewModel(mApplication)
                    isAssignableFrom(ChallengeViewModel::class.java) ->
                        ChallengeViewModel(mApplication, gitsRepository)
                    isAssignableFrom(RewardViewModel::class.java) ->
                        RewardViewModel(mApplication, gitsRepository)
                    isAssignableFrom(DiscussionViewModel::class.java) ->
                        DiscussionViewModel(mApplication)
                    isAssignableFrom(RaceGuideCentralViewModel::class.java) ->
                        RaceGuideCentralViewModel(mApplication)
                    isAssignableFrom(ForgotPasswordViewModel::class.java) ->
                        ForgotPasswordViewModel(mApplication, gitsRepository)
                    isAssignableFrom(VerificationPasswordViewModel::class.java) ->
                        VerificationPasswordViewModel(mApplication, gitsRepository)
                    isAssignableFrom(EventRundownViewModel::class.java) ->
                        EventRundownViewModel(mApplication)
                    isAssignableFrom(LiveTrackerViewModel::class.java) ->
                        LiveTrackerViewModel(mApplication)
                    isAssignableFrom(RaceResultViewModel::class.java) ->
                        RaceResultViewModel(mApplication)
                    isAssignableFrom(RunTrackerViewModel::class.java) ->
                        RunTrackerViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ChallengeSeeAllViewModel::class.java) ->
                        ChallengeSeeAllViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ChallengeDetailViewModel::class.java) ->
                        ChallengeDetailViewModel(mApplication, gitsRepository)
                    isAssignableFrom(PostingPageViewModel::class.java) ->
                        PostingPageViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ChallengeResultViewModel::class.java) ->
                        ChallengeResultViewModel(mApplication)
                    isAssignableFrom(SearchViewModel::class.java) ->
                        SearchViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ResultViewModel::class.java) ->
                        ResultViewModel(mApplication)
                    isAssignableFrom(ResultEventViewModel::class.java) ->
                        ResultEventViewModel(mApplication)
                    isAssignableFrom(NewsFeedViewModel::class.java) ->
                        NewsFeedViewModel(mApplication, gitsRepository)
                    isAssignableFrom(NewsDetailViewModel::class.java) ->
                        NewsDetailViewModel(mApplication, gitsRepository)
                    isAssignableFrom(SeeAllNewsViewModel::class.java) ->
                        SeeAllNewsViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ShareRunViewModel::class.java) ->
                        ShareRunViewModel(mApplication)
                    isAssignableFrom(CameraRollViewModel::class.java) ->
                        CameraRollViewModel(mApplication)
                    isAssignableFrom(CustomCameraViewModel::class.java) ->
                        CustomCameraViewModel(mApplication)
                    isAssignableFrom(ShareEditImageViewModel::class.java) ->
                        ShareEditImageViewModel(mApplication)
                    isAssignableFrom(ShareRouteViewModel::class.java) ->
                        ShareRouteViewModel(mApplication)
                    isAssignableFrom(RunDataViewModel::class.java) ->
                        RunDataViewModel(mApplication)
                    isAssignableFrom(StickersViewModel::class.java) ->
                        StickersViewModel(mApplication, gitsRepository)
                    isAssignableFrom(ShareFinalPageViewModel::class.java) ->
                        ShareFinalPageViewModel(mApplication)
                    isAssignableFrom(VirtualRunViewModel::class.java) ->
                        VirtualRunViewModel(mApplication, gitsRepository)
                    isAssignableFrom(DetailVirtualRunViewModel::class.java) ->
                        DetailVirtualRunViewModel(mApplication, gitsRepository)
                    isAssignableFrom(StartVirtualRunViewModel::class.java) ->
                        StartVirtualRunViewModel(mApplication, gitsRepository)
                    isAssignableFrom(RunningHistoryViewModel::class.java) ->
                        RunningHistoryViewModel(mApplication, gitsRepository)
                    isAssignableFrom(LeaderBoardViewModel::class.java) ->
                        LeaderBoardViewModel(mApplication, gitsRepository)
                    isAssignableFrom(RunningDetailsViewModel::class.java) ->
                        RunningDetailsViewModel(mApplication)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(mApplication: Application) =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(mApplication,
                            Injection.provideGitsRepository(mApplication))
                            .also { INSTANCE = it }
                }
    }
}