package id.otsuka.gits.sweatgen.data.source

import id.otsuka.gits.sweatgen.base.BaseResponse
import id.otsuka.gits.sweatgen.data.model.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiService {


    /**
     *  ========================================================================================================================
     *
     *                                        POST
     *
     *  ========================================================================================================================
     */

    @POST("register")
    fun register(@Header("Accept") accept: String = "application/json",
                 @Body registerParams: RegisterParams
    ): Observable<BaseResponse<RegisterDao, Any>>

    @POST("login")
    fun login(@Header("Accept") accept: String = "application/json",
              @Body loginParams: LoginParams
    ): Observable<BaseResponse<LoginDao, String>>

    @POST("virtual-run/redeem")
    @FormUrlEncoded
    fun redeemValid(@Header("Authorization") accessToken: String,
                    @Field("virtual_run_id") virturunId: Int,
                    @Field("user_id") userId: Int,
                    @Field("access_code") accessCode: String
    ): Observable<BaseResponse<Any, String>>

    @POST("forgot-password")
    fun forgotPassword(@Body forgotPasswordParams: ForgotPasswordParams): Observable<BaseResponse<ForgotPasswordDao, String>>

    @POST("verify-password")
    fun setNewPassword(@Header("Content-Type") contentType: String = "application/json",
                       @Body newPasswordParams: NewPasswordParams
    ): Observable<BaseResponse<Any, Any>>

    @POST("send-code-forgot")
    @FormUrlEncoded
    fun resendCode(@Field("token_forgot") token_forgot: String): Observable<BaseResponse<Any, String>>

    @POST("event-bookmark")
    fun bookmarkEvent(@Header("Authorization") accessToken: String,
                      @Body param: BookmarkEventParams
    ): Observable<BaseResponse<BookmarkEventDao, String>>

    /**
     *  ========================================================================================================================
     *
     *                                      GET
     *
     *  ========================================================================================================================
     */

    @GET("challenge-list-by/{id}")
    fun getChallengeById(@Header("Authorization") accessToken: String,
                         @Path("id") challengeId: Int,
                         @Query("userId") userId: Int
    ): Observable<BaseResponse<Challenge, String>>

    @GET("challenge-list")
    fun getChallengeDashboard(@Header("Authorization") accessToken: String,
                              @Query("userId") userId: Int)
            : Observable<BaseResponse<GetChallengeDashboardDao, String>>

    @GET("history-challange-progress")
    fun getCotinuesRunHistory(@Header("Authorization") accessToken: String,
                              @Query("userID") userId: Int,
                              @Query("challangeID") challengeId: Int
    ): Observable<BaseResponse<BasePaginationModel<GetCotinuesRunHistoryDao>, String>>

    @GET("list-stiker")
    fun getStickers(@Header("Authorization") accessToken: String): Observable<BaseResponse<List<String>?, String>>


    @GET("newsfeed/all")
    fun getAllNews(@Header("Authorization") accessToken: String): Observable<BaseResponse<BasePaginationModel<List<NewsFeed>>, String>>

    @GET("version")
    fun getLatestApkVersionCode(@Query("device_key") deviceVersionCode: Int): Observable<BaseResponse<Any, String>>

    @GET("logout")
    fun postLogout(
            @Query("token") accessToken: String
    ): Observable<BaseResponse<Any, String>>

    @GET("newsfeed")
    fun getNewsFeeds(@Header("Authorization") accessToken: String)
            : Observable<BaseResponse<BasePaginationModel<GetNewsFeedDao>, String>>

    @GET("newsfeed/{id}")
    fun getNewsDetail(
            @Header("Authorization") accessToken: String,
            @Path("id") newsId: Int
    ): Observable<BaseResponse<BasePaginationModel<GetNewsDetailDao>, String>>

    @GET("newsfeed/regular")
    fun getAllNews(@Header("Authorization") accessToken: String,
                   @Query("page") page: Int
    ): Observable<BaseResponse<BasePaginationModel<List<NewsFeed>>, String>>

    @GET("newsfeed/category/{id}")
    fun getNewsByCategory(@Header("Authorization") accessToken: String,
                          @Path("id") categoryId: Int,
                          @Query("page") page: Int)
            : Observable<BaseResponse<BasePaginationModel<List<NewsFeed>>, String>>


    @GET("resend-email/{token}")
    fun resendEmail(@Path("token") token: String)
            : Observable<BaseResponse<Any, String>>

    @GET("user-validation/{userId}")
    fun userValidation(@Path("userId") userId: Int)
            : Observable<BaseResponse<Any, String>>

    @GET("users/{userId}")
    fun showUser(@Header("Accept") accept: String = "application/json",
                 @Header("Authorization") accessToken: String,
                 @Path("userId") userId: Int
    ): Observable<BaseResponse<User, String>>

    @GET("event-list/{ownerId}")
    fun getEventList(@Header("Authorization") accessToken: String,
                     @Path("ownerId") ownerId: Int,
                     @Query("page") page: Int = 0
    ): Observable<BaseResponse<GetEventListDao, String>>

    @GET("event-order-by")
    fun getEventDetail(@Header("Authorization") accessToken: String,
                       @Query("id") eventId: Int,
                       @Query("user_id") userId: Int
    ): Observable<BaseResponse<GetEventDetailDao, String>>

    @GET("screen-meta-image")
    fun getOnBoardImage(): Observable<BaseResponse<List<OnBoardImage>, String>>

    @GET("event-gallery-by")
    fun getImagesGallery(@Header("Authorization") accessToken: String,
                         @Query("event") eventId: Int,
                         @Query("pid") pid: String
    ): Observable<BaseResponse<List<ImageGallery>?, String>>

    @GET("history-run/{userId}")
    fun getRunHistories(@Header("Authorization") accessToken: String,
                        @Path("userId") userId: Int,
                        @Query("page") page: Int
    ): Observable<BaseResponse<GetRunHistoriesDao, String>>

    @GET("challenge")
    fun getChallenges(@Header("Authorization") accessToken: String)
            : Observable<BaseResponse<BasePaginationModel<GetChallengesDao>, String>>

    @GET("user-medal/{userId}")
    fun getMedalHistory(@Header("Authorization") accessToken: String,
                        @Path("userId") userId: Int,
                        @Query("page") page: Int
    ): Observable<BaseResponse<GetMedalHistoryDao, String>>

    @GET("share")
    fun getShareLink(@Header("Authorization") accessToken: String,
                     @Query("id") id: Int
    ): Observable<BaseResponse<GetShareLinkDao, String>>

    @GET("challenge-leaderboard/{id}")
    fun getLeaderboard(@Header("Authorization") accessToken: String,
                       @Path("id") challengeId: Int
    ): Observable<BaseResponse<List<LeaderBoardDao>?, String>>

    @GET("challenge-list/all_challenge/{id}")
    fun getAvailableChallenges(@Header("Authorization") accessToken: String,
                               @Path("id") userId: Int):
            Observable<BaseResponse<BasePaginationModel<List<Challenge>>, String>>

    @GET("challenge-list/previous_challenges/{id}")
    fun getPreviousChallenges(@Header("Authorization") accessToken: String,
                              @Path("id") userId: Int):
            Observable<BaseResponse<BasePaginationModel<List<Challenge>>, String>>

    @GET("virtual-run-list")
    fun getVirtualRunList(@Header("Authorization") accessToken: String,
                          @Query("userId") userId: Int
    ):
            Observable<BaseResponse<BasePaginationModel<List<VirtualRun>>, String>>

    @GET("virtual-run-list/history/{id}")
    fun getHistoryVirtualRun(@Header("Authorization") accessToken: String,
                             @Path("id") userId: Int
    ): Observable<BaseResponse<BasePaginationModel<List<VirtualRun>>, String>>

    @GET("virtual-run-list/{id}")
    fun getVirtualRunById(@Header("Authorization") accessToken: String,
                          @Path("id") virturunId: Int,
                          @Query("userId") userId: Int
    ): Observable<BaseResponse<VirtualRunDetail, String>>

    @GET("virtual-run/banner")
    fun getVirtualRunBanner(@Header("Authorization") accessToken: String)
            : Observable<BaseResponse<BasePaginationModel<GetVirturunBannerDao>, String>>

    @GET("virtual-run-list/leaderboard/{id}")
    fun getVirturunLeaderboard(@Header("Authorization") accessToken: String,
                               @Path("id") virturunId: Int
    ): Observable<BaseResponse<BasePaginationModel<List<LeaderBoardDao>?>, String>>

    /**
     *  ========================================================================================================================
     *
     *                                     PUT
     *
     *  ========================================================================================================================
     */
    @PUT("update-bio/{userId}")
    fun updatePersonalBio(
            @Header("Content-Type") contentType: String = "application/x-www-form-urlencoded",
            @Path("userId") userId: String,
            @Query("name") name: String,
            @Query("gender") gender: String,
            @Query("dob") dob: String): Observable<BaseResponse<UpdatePersonalBioDao, String>>

    @Multipart
    @POST("users-update/{userId}")
    fun updateUser(
            @Header("Authorization") accessToken: String,
            @Path("userId") userId: Int,
            @Part("name") name: RequestBody?,
            @Part("gender") gender: RequestBody?,
            @Part("dob") dob: RequestBody?,
            @Part picture: MultipartBody.Part
    ): Observable<BaseResponse<User, String>>

    @POST("history-run")
    @FormUrlEncoded
    fun postRunHistory(@Header("Content-Type") contentType: String = "application/x-www-form-urlencoded",
                       @Header("Authorization") accessToken: String,
                       @Field("user_id") userId: Int = 0,
                       @Field("activity_name") activity_name: String? = null,
                       @Field("distance") distance: Double = 0.0,
                       @Field("step") step: Double = 0.0,
                       @Field("calories") calories: Double = 0.0,
                       @Field("duration") duration: Int = 0,
                       @Field("point") point: Int = 0,
                       @Field("type_run") type_run: String,
                       @Field("long_lat") long_lat: String? = null,
                       @Field("challenge_id") challengeId: Int,
                       @Field("virtual_run_id") virturunId: Int
    ): Observable<BaseResponse<Any, String>>

    @POST("join-virtual-run")
    @FormUrlEncoded
    fun joinVirtualRun(@Header("Content-Type") contentType: String = "application/x-www-form-urlencoded",
                       @Header("Authorization") accessToken: String,
                       @Field("virtual_run_id") virturunId: Int = 0,
                       @Field("user_id") userId: Int = 0
    ): Observable<BaseResponse<Any, String>>

    @POST("join-challenge")
    @FormUrlEncoded
    fun joinChallenge(@Header("Content-Type") contentType: String = "application/x-www-form-urlencoded",
                      @Header("Authorization") accessToken: String,
                      @Field("challenge_id") challengeId: Int = 0,
                      @Field("user_id") userId: Int = 0
    ): Observable<BaseResponse<Any, String>>

    @POST("challange-progress-run")
    @FormUrlEncoded
    fun postRunProgress(@Header("Content-Type") contentType: String = "application/x-www-form-urlencoded",
                        @Header("Authorization") accessToken: String,
                        @Field("challange_id") challengeId: Int = 0,
                        @Field("user_id") userId: Int = 0,
                        @Field("distance") distance: Double = 0.0,
                        @Field("duration") duration: Int = 0,
                        @Field("calories") calories: Double = 0.0,
                        @Field("step") step: Double = 0.0,
                        @Field("long_lat") long_lat: String = ""
    ): Observable<BaseResponse<Any, String>>

    companion object Factory {

        fun createService(baseUrl: String): ApiService {
            val mLoggingInterceptor = HttpLoggingInterceptor()
            mLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val mClient = OkHttpClient.Builder()
                    .addInterceptor(mLoggingInterceptor)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .build()

            val mRetrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(mClient) //Todo comment if app release
                    .build()

            return mRetrofit.create(ApiService::class.java)
        }
    }
}