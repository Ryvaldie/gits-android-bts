package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail

import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Location
import id.otsuka.gits.sweatgen.util.getDd
import id.otsuka.gits.sweatgen.util.toDate
import id.otsuka.gits.sweatgen.util.toDdMMMMyyyy
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.howtogetthere.RecyclerviewAdapter
import id.otsuka.gits.sweatgen.util.yyyyMMddToEEEEddyyyy

/**
 * @author radhikayusuf.
 */

object EventDetailBindings {
    @BindingAdapter("app:dateStart", "app:dateEnd", "app:viewmodel", requireAll = false)
    @JvmStatic
    fun setDate(textView: TextView, dateStart: String?, dateEnd: String?, viewmodel: EventDetailViewModel) {
        if (dateStart != null && dateEnd != null) {
            val start = dateStart.toDate()
            val end = dateEnd.toDate()

            textView.apply {
                val dateStartFormatted = dateStart.yyyyMMddToEEEEddyyyy()
                val dateEndFormatted = dateEnd.yyyyMMddToEEEEddyyyy()

                text = context.getString(R.string.text_start_date)
                        .replace("#", dateStartFormatted)

                if (start.getDd() != end.getDd()) {
                    text = text.toString()
                            .replace("*", dateEndFormatted)

                } else {
                    text = text.toString()
                            .replace("\n\n\nEnd:\n*", "").replace("Start:\n", "")
                    viewmodel.sameDate.set(true)
                }

            }
        }

    }

    @BindingAdapter("app:adapter", "app:listener")
    @JvmStatic
    fun setPlacesList(recyclerView: RecyclerView, locations: List<Location>?, listener: RecyclerviewAdapter.onClickItemListener?) {
        recyclerView.apply {
            if (locations != null && listener != null) {
                adapter = RecyclerviewAdapter(locations, listener)
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                (adapter as RecyclerviewAdapter).replacePlaces(locations)
            }

        }
    }


    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?) {

        val finalImage: Any = imageUrl ?: R.drawable.img_slider_event

        Glide.with(view.context)
                .load(finalImage)
                .into(view)
                .apply { RequestOptions().error(R.color.greyLineProfile).placeholder(R.color.greyLineProfile) }
    }
}