package id.otsuka.gits.sweatgen.data.model

data class GetNewsDetailDao(val detail: NewsFeed? = null,
                            val similar: List<NewsFeed>? = null)