package id.otsuka.gits.sweatgen.mvvm.main.challenge

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.ItemChallengeSectionOneBinding
import id.otsuka.gits.sweatgen.databinding.ItemChallengeSectionTwoBinding
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllActivity.Companion.AVAILABLE_CHALLENGE
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllActivity.Companion.PREVIOUS_CHALLENGE
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeType.Companion.HEADER_TYPE

class ChallengeAdapter(val context: Context,
                       var mData: ArrayList<Challenge>,
                       val mViewModel: ChallengeViewModel,
                       val listener: ChallengeUserActionListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> R.layout.item_challenge_section_one
            else -> mData[position.minus(1)].getItemType()
        }
    }

    fun replaceData(data: List<Challenge>) {
        mData.apply {
            clear()
            addAll(data)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_challenge_section_one -> {
                SectionOneHolder(ItemChallengeSectionOneBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    empty = (this@ChallengeAdapter.mViewModel.highlightChallenges.isEmpty())
                    vpChallHighlight.apply {
                        adapter = ChallengeHighlightAdapter(this@ChallengeAdapter.context, ArrayList())
                        offscreenPageLimit = 3
                        pageMargin = -(resources.displayMetrics.widthPixels / 15)
                        clipToPadding = false
                       // (adapter as ChallengeHighlightAdapter).replaceData(this@ChallengeAdapter.mViewModel.highlightChallenges)
                    }
                })
            }
            else -> {
                SectionTwo(context, ItemChallengeSectionTwoBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = listener
                }, viewType)
            }
        }
    }

    override fun getItemCount(): Int = mData.size.plus(1)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SectionTwo)
            holder.bind(mData[position.minus(1)])
        else
            (holder as SectionOneHolder).refresh(mViewModel)
    }

    class SectionOneHolder(val mBinding: ItemChallengeSectionOneBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun refresh(vm: ChallengeViewModel) {
            mBinding.vpChallHighlight.apply {
                mBinding.empty = (vm.highlightChallenges.isEmpty())
                (adapter as ChallengeHighlightAdapter).replaceData(vm.highlightChallenges)
            }
        }
    }

    class SectionTwo(val context: Context, val mBinding: ItemChallengeSectionTwoBinding, val viewType: Int)
        : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(data: Challenge) {
            mBinding.apply {
                challenge = data
                headerVisible = (viewType == HEADER_TYPE)
                seeallVisible = (data.mChallengeTitle != context.getString(R.string.text_mychallenge))
                headerTitle = data.mChallengeTitle
                challengeType = if (data.mChallengeTitle == context.getString(R.string.text_join_challenge)) AVAILABLE_CHALLENGE else PREVIOUS_CHALLENGE
            }
        }
    }


}


