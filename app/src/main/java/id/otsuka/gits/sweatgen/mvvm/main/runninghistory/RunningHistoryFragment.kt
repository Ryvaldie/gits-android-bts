package id.otsuka.gits.sweatgen.mvvm.main.runninghistory


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentRunningHistoryBinding
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryActivity.Companion.CHALLENGE_ID
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryActivity.Companion.CURRENT_PROGRESS
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryActivity.Companion.PROGRESS
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.RunningHistoryActivity.Companion.TOTAL_PROGRESS
import id.otsuka.gits.sweatgen.mvvm.main.runninghistory.runningdetail.RunningDetailsFragment
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.replaceFragmentAndAddToBackStack
import id.otsuka.gits.sweatgen.util.showSnackBar
import id.otsuka.gits.sweatgen.util.withArgs

class RunningHistoryFragment : Fragment(), RunningHistoryUserActionListener {

    lateinit var mViewDataBinding: FragmentRunningHistoryBinding
    lateinit var mViewModel: RunningHistoryViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRunningHistoryBinding.inflate(inflater, container!!, false)
        mViewModel = (activity as RunningHistoryActivity).obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            challengeId = requireActivity().intent.getIntExtra(CHALLENGE_ID, 0)
            getHistories()
        }

        mViewDataBinding.mListener = this@RunningHistoryFragment

        setupRecycler()

        return mViewDataBinding.root

    }

    private fun setupRecycler() {
        mViewDataBinding.recyclerRunHistory.apply {
            adapter = RunningHistoryAdapter(ArrayList(), mViewModel, this@RunningHistoryFragment)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeEvents()
        val mIntent = requireActivity().intent
        mViewModel.apply {
            bCurrentDistance.set(mIntent.getStringExtra(CURRENT_PROGRESS))
            bTotalDistance.set(mIntent.getStringExtra(TOTAL_PROGRESS))
            bProgress.set(mIntent.getIntExtra(PROGRESS, 0))
        }
    }

    private fun observeEvents() {
        mViewModel.snackbarMessage.observe(this, Observer {
            if (it != null) {
                when (it) {
                    TOKEN_EXPIRE -> {
                        mViewModel.repository.logout(null, null, true)

                        requireActivity().redirectToLogin()
                    }
                    else -> {
                        it.showSnackBar(mViewDataBinding.root)
                    }
                }
            }
        })
    }

    override fun onItemClick(index: Int) {
        val jsonItem = Gson().toJson(mViewModel.listHistoryRun[index])
        (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(RunningDetailsFragment.newInstance(jsonItem), R.id.frame_main_content)
    }

    companion object {
        fun newInstance() = RunningHistoryFragment().withArgs {

        }

    }

}
