package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class VirtualRun(val id: Int = 0,
                      val name: String? = null,
                      val category: String? = null,
                      val ticket_code: String? = null,
                      val start_date: String? = null,
                      val end_date: String? = null,
                      val duration: Int? = null,
                      val distance: Double? = null,
                      val banner: String? = null,
                      val icon: String? = null,
                      val leaderboard: String? = null,
                      val event_description: String? = null,
                      val challenge_description: String? = null,
                      val days_left: String? = null,
                      val expired: Boolean? = null,
                      val run_status: String? = null,
                      val information: VirturunDetailInformation? = null
)