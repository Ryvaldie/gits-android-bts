package id.otsuka.gits.sweatgen.mvvm.splashscreen


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentSplashcreenBinding

class SplashcreenFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentSplashcreenBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentSplashcreenBinding.inflate(inflater, container!!, false)

        return mViewDataBinding.root

    }

    companion object {
        fun newInstance() = SplashcreenFragment().apply {

        }

    }

}
