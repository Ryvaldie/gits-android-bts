package id.otsuka.gits.sweatgen.data.model

data class User(val id: Int = 0,
                val name: String? = null,
                val code: String? = null,
                val email: String? = null,
                val provider_id: Any? = null,
                val token: String? = null,
                val temporary_token: String? = null,
                val picture: String? = null,
                val gender: String? = null,
                val dob: String? = null,
                val age: Int = 0,
                val total_challenge: Int = 0,
                val total_event: Int = 0,
                val total_point: Int = 0
)