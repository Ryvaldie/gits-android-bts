package id.otsuka.gits.sweatgen.mvvm.onboard.login;


interface LoginUserActionListener {

    fun onBtnLoginClick()

    fun onForgotPasswordClick()

    fun onSignupClick()

    fun onLoginFbClick()

}