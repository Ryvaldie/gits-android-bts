package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class StickersViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val snackbarMsg = SingleLiveEvent<String>()

    val isRequesting = ObservableBoolean(false)

    val listStickers = ObservableArrayList<String>()

    fun start() {
        isRequesting.set(true)
        repository.getStickers("", object : GitsDataSource.GetStickersCallback {
            override fun onDataNotAvailable() {
                isRequesting.set(false)
            }

            override fun onError(errorMessage: String?) {
                isRequesting.set(false)
                snackbarMsg.value = errorMessage
            }

            override fun onLoaded(stickers: List<String>) {
                isRequesting.set(false)
                listStickers.apply {
                    clear()
                    addAll(stickers)
                }
            }
        })
    }
}