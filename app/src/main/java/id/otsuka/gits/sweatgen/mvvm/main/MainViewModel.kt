package id.otsuka.gits.sweatgen.mvvm.main;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.util.Log
import android.widget.EditText
import com.jakewharton.rxbinding3.widget.textChanges
import id.otsuka.gits.sweatgen.BuildConfig
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit


class MainViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val saveStateEvent = SingleLiveEvent<Int>()
    val bFocusable = ObservableBoolean(false)
    val bNoInet = ObservableBoolean(false)
    val loadLayout = ObservableBoolean(false)
    val searchEvent = SingleLiveEvent<String>()
    val searchChallenge = SingleLiveEvent<String>()
    val searchNewsfeed = SingleLiveEvent<String>()
    val showUpdateAppsDialog = SingleLiveEvent<Void>()

    fun getLatestAppsVersion() {
        repository.getLatestAppVersionCode(BuildConfig.VERSION_CODE, object : GitsDataSource.GetLatestAppVersionCallback {
            override fun onAppsAlreadyUpdated() {
            }

            override fun onAppsNeedToUpdate() {
                showUpdateAppsDialog.call()
            }

            override fun onError(errorMessage: String?) {
                Log.d(MainViewModel::class.java.simpleName, errorMessage)
            }
        })
    }


    fun observeSearchField(edt: EditText) {

        edt.textChanges()
                .debounce(300, TimeUnit.MILLISECONDS)
                .map { it.toString() }
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    searchEvent.value = it
                    searchChallenge.value = it
                    searchNewsfeed.value = it
                }, {
                    // edt.text = SpannableStringBuilder("")
                })
    }
}