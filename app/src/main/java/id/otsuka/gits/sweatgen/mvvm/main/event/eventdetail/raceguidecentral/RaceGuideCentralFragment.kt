package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentRaceGuideCentralBinding
import id.otsuka.gits.sweatgen.util.jsonStringToList
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.setupViewPagerIndicator
import id.otsuka.gits.sweatgen.util.withArgs
import kotlinx.android.synthetic.main.fragment_race_guide_central.*


class RaceGuideCentralFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentRaceGuideCentralBinding
    lateinit var mViewModel: RaceGuideCentralViewModel
    val listOfTitle = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRaceGuideCentralBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            val imageList = arguments?.getString(RACE_GUIDE_CENTRAL, "").jsonStringToList()
            obsRaceGuideCentral.apply {
                clear()
                addAll(imageList)
            }
        }

        mViewDataBinding.mListener = object : RaceGuideCentralUserActionListener {
            override fun back() {

            }
        }

        setupViewPager()
        setupTitles()

        return mViewDataBinding.root

    }

    private fun setupTitles() {
        listOfTitle.apply {
            add(getString(R.string.text_race_central))
            add(getString(R.string.text_race_guide))
            add(getString(R.string.text_rules))
            add(getString(R.string.text_hydrate_point))
            add(getString(R.string.text_prize))
            add(getString(R.string.text_information))
        }
    }

    private fun setupViewPager() {

        mViewDataBinding.vpRace.apply {

            adapter = ViewPagerAdapter(requireContext(), ArrayList(0), mViewModel)

            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageSelected(position: Int) {

                    val linDots = (parent as View).findViewById<LinearLayout>(R.id.lin_vp_indicator)

                    for (x in 0..mViewModel.obsRaceGuideCentral.size.minus(1)) {
                        (linDots.getChildAt(x) as ImageView).apply {
                            setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_inactive_vp_indicator_race))
                        }
                    }

                    (linDots.getChildAt(position) as ImageView).apply {
                        setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_active_vp_indicator_race))
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }
            })
        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).apply {

          //  setSupportActionBar(toolbar_race)

            toolbar_race.title = listOfTitle[arguments?.getInt(RACE_TYPE) ?: 0]
        }
        toolbar_race.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.createIndicator.observe(this, Observer {
            mViewDataBinding.linVpIndicator.removeAllViews()
            mViewDataBinding.linVpIndicator.setupViewPagerIndicator(mViewModel.obsRaceGuideCentral.size, 0)
        })
    }

    fun obtainViewModel(): RaceGuideCentralViewModel = obtainViewModel(RaceGuideCentralViewModel::class.java)

    companion object {

        const val RACE_GUIDE_CENTRAL = "raceGuideCentral"
        const val RACE_TYPE = "raceType"
        val RACE_CENTRAL = 0
        val RACE_GUIDE = 1
        val RULES = 2
        val HYDRATE_POINT = 3
        val PRIZE = 4
        val VIRTUAL_RUN_GUIDE = 5

        fun newInstance(raceGuideCentralList: String, raceType: Int) = RaceGuideCentralFragment().withArgs {
            putString(RACE_GUIDE_CENTRAL, raceGuideCentralList)
            putInt(RACE_TYPE, raceType)
        }

    }

}
