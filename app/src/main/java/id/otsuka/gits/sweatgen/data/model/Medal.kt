package id.otsuka.gits.sweatgen.data.model

data class Medal(
        val id: Int = 0,
        val medal_name: String? = "",
        val point: Int = 0,
        val medal_logo: String? = ""
)