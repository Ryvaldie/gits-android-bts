package id.otsuka.gits.sweatgen.util

import android.annotation.SuppressLint
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.annotation.IdRes
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import id.otsuka.gits.sweatgen.data.model.EventRundown
import id.otsuka.gits.sweatgen.data.model.RaceCentralGuide
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by irfanirawansukirman on 26/01/18.
 */

/**
 * The `fragment` is added to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.replaceFragmentInActivity(fragment: Fragment, frameId: Int) {
    supportFragmentManager.transact {
        replace(frameId, fragment, fragment::class.java.simpleName)
    }
}

fun String?.jsonStringToList(): List<RaceCentralGuide> {

    var initList = emptyList<RaceCentralGuide>()

    try {
        val collectionType = object : TypeToken<List<RaceCentralGuide>>() {}.type
        initList = Gson().fromJson<List<RaceCentralGuide>>(this, collectionType)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return initList
}

fun String?.jsonStringToList2(): List<EventRundown> {

    var initList = emptyList<EventRundown>()

    try {
        val collectionType = object : TypeToken<List<EventRundown>>() {}.type
        initList = Gson().fromJson<List<EventRundown>>(this, collectionType)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return initList
}

fun Fragment.showToast(message: String = "", duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this.context, message, duration).show()
}

fun Fragment.getImageFromCamera(data: Intent?): ByteArray {
    val thumbnail = data?.extras!!.get("data") as Bitmap
    val bytes = ByteArrayOutputStream()
    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
    return bytes.toByteArray()
}

fun Any.cutExactly2Num(): String {
    val df = DecimalFormat("#.##")
    df.roundingMode = RoundingMode.FLOOR
    return df.format(this)
}

fun Any.cutExactly2NumKm(): String {
    val df = DecimalFormat("#.## KM")
    df.roundingMode = RoundingMode.FLOOR
    return df.format(this)
}

fun Any.cutExactly1Num(): String {
    val df = DecimalFormat("#.#")
    df.roundingMode = RoundingMode.FLOOR
    return df.format(this)
}

fun ByteArray.rotate(degrees: Float): ByteArray {

    val bitmap = BitmapFactory.decodeByteArray(this, 0, this.size)

    val matrix = Matrix().apply { postRotate(degrees) }

    val rotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)

    val byteArrayStream = ByteArrayOutputStream()

    rotated.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayStream)

    return byteArrayStream.toByteArray()
}


fun Fragment.saveImge(image: ByteArray, fileName: String): File? {

    val parentDir = File(Environment.getExternalStoragePublicDirectory
    (Environment.DIRECTORY_PICTURES).absolutePath + "/sweatgen/")

    if (!parentDir.exists()) {
        parentDir.mkdirs()
    }

    if (parentDir.exists()) {
        val file = File(Environment.getExternalStoragePublicDirectory
        (Environment.DIRECTORY_PICTURES).absolutePath + "/sweatgen/" + fileName)

        var stream: FileOutputStream? = null
        try {
            stream = FileOutputStream(file)

            stream.write(image)
        } finally {
            if (stream != null)
                stream.close()
        }
        return file
    }

    return null
}

fun BitmapDrawable.convertToByteArray(): ByteArray {
    val stream = ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
    return stream.toByteArray()
}


fun File.filePathToMultipart(): MultipartBody.Part {

    val reqFile = RequestBody.create(MediaType.parse("image/*"), this)
    val image = MultipartBody.Part.createFormData("picture", this.name, reqFile)
    return image
}

fun getDataColumn(context: Context, uri: Uri, selection: String?,
                  selectionArgs: Array<String>?): String? {
    var cursor: Cursor? = null
    val projection = arrayOf<String>(MediaStore.Files.FileColumns.DATA)
    try {
        cursor = context.contentResolver.query(
                uri, projection, selection, selectionArgs, null)
        if (cursor != null && cursor.moveToFirst()) {
            val cindex = cursor.getColumnIndexOrThrow(projection[0])
            return cursor.getString(cindex)
        }
    } finally {
        if (cursor != null)
            cursor.close()
    }
    return null
}

fun Uri.getActualPath2(context: Context): String? {
    var cursor: Cursor? = null
    try {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        cursor = context.contentResolver.query(this, proj, null, null, null)
        val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    } catch (e: Exception) {
        Log.e("GitsExt", "getRealPathFromURI Exception : " + e.toString())
        return ""
    } finally {
        if (cursor != null) {
            cursor.close()
        }
    }
}

fun Uri.getActualPath3(context: Context): String? {
    return RealPathUtil.getRealPath(context, this)
}

fun File.reduceImageSize(): File {

    val outputStream = ByteArrayOutputStream()

    val imgBitmap = BitmapFactory.decodeFile(this.absolutePath)

    val bitmap2 = Bitmap.createScaledBitmap(imgBitmap, imgBitmap.width.times(8).div(10), imgBitmap.height.times(8).div(10), false)

    val fileOutputStream = FileOutputStream(this)

    bitmap2.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)

    fileOutputStream.write(outputStream.toByteArray())

    fileOutputStream.close()

    return this
}

fun Uri.getActualPath(context: Context): String? {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        if (DocumentsContract.isDocumentUri(context, this)) {
            when {
                "com.android.externalstorage.documents" == this.authority -> {// ExternalStorageProvider
                    val docId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        DocumentsContract.getDocumentId(this)
                    } else {
                        ""
                    }
                    val split = docId.split((":").toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                    val type = split[0]
                    return when {
                        "primary".equals(type, ignoreCase = true) -> Environment.getExternalStorageDirectory().absolutePath + "/" + split[1]
                        else -> "/stroage/" + type + "/" + split[1]
                    }
                }
                "com.android.providers.downloads.documents" == this.authority -> {// DownloadsProvider
                    val id = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        DocumentsContract.getDocumentId(this)
                    } else {
                        ""
                    }
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                    return getDataColumn(context, contentUri, null, null)
                }
                "com.android.providers.media.documents" == this.authority -> {// MediaProvider
                    val docId = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        DocumentsContract.getDocumentId(this)
                    } else {
                        ""
                    }
                    val split = docId.split((":").toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    contentUri = MediaStore.Files.getContentUri("external")
                    val selection = "_id=?"
                    val selectionArgs = arrayOf<String>(split[1])
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
                else -> {
                }
            }
        }

    } else if ("content".equals(this.scheme, ignoreCase = true)) {//MediaStore
        return getDataColumn(context, this, null, null)
    } else if ("file".equals(this.scheme, ignoreCase = true)) {// File
        return this.path
    }
    return null
}

fun String.yyyyMMddToEEEEddyyyy(): String {
    var pattern = SimpleDateFormat("yyyy-MM-dd")
    val date = pattern.parse(this)
    pattern = SimpleDateFormat("EEEE,\ndd MMMM yyyy")
    return pattern.format(date)
}

fun String.yyyyMMddToddMMMMyyyy(): String {
    var pattern = SimpleDateFormat("yyyy-MM-dd")
    val date = pattern.parse(this)
    pattern = SimpleDateFormat("dd MMMM yyyy")
    return pattern.format(date)
}

fun String.toDate(): Date {
    var pattern = SimpleDateFormat("yyyy-MM-dd")
    return pattern.parse(this)
}

fun Long.timeMillisFormat(): String {
    val simpleDataFormat = SimpleDateFormat("E, dd MM yyyy hh:mm a")
    return simpleDataFormat.format(Date(this))
}

fun Int.dpToPx(context: Context): Int = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics))

fun FragmentActivity.replaceFragmentAndAddToBackStack(fragment: Fragment, frameId: Int, tag: String = "") {
    supportFragmentManager.transact {
        //    setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        replace(frameId, fragment, if (tag.isEmpty()) fragment::class.java.simpleName else tag)
                .addToBackStack(if (tag.isEmpty()) fragment::class.java.simpleName else tag)
    }
    supportFragmentManager.executePendingTransactions()
}

fun FragmentActivity.replaceFragmentAndNotToBackStack(fragment: Fragment, frameId: Int, tag: String = "") {
    supportFragmentManager.transact {
        //    setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        replace(frameId, fragment, if (tag.isEmpty()) fragment::class.java.simpleName else tag)
                .disallowAddToBackStack()
    }
    supportFragmentManager.executePendingTransactions()
}

fun Date.getDd(): String {
    return android.text.format.DateFormat.format("dd", this).toString()
}

fun Date.getD(): String {
    return android.text.format.DateFormat.format("d", this).toString()
}

fun Date.getYyyy(): String {
    return android.text.format.DateFormat.format("yyyy", this).toString()
}


fun Date.getMm(): String {
    return android.text.format.DateFormat.format("MMM", this).toString()
}

fun Date.toDdMMMMyyyy(): String {
    return android.text.format.DateFormat.format("dd MMMM yyyy", this).toString()
}

fun Date.getDdMmmm(): String {
    return android.text.format.DateFormat.format("dd MMMM", this).toString()
}

fun Date.toAtMMMMdd(): String {
    return android.text.format.DateFormat.format("MMMM dd, yyyy 'at' hh:mm a", this).toString()
}

fun Date.toOrdinalFormat(): String {
    val ordinal = this.getD().toInt().getOrdinal()

    return android.text.format.DateFormat.format("MMMM d'$ordinal'", this).toString()
}

fun FragmentActivity.addFragmentAndAddToBackStack(fragment: Fragment, frameId: Int) {
    supportFragmentManager.transact {
        //  setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        add(frameId, fragment, fragment::class.java.simpleName)
                .addToBackStack(fragment::class.java.simpleName)
    }
    supportFragmentManager.executePendingTransactions()
}

fun FragmentActivity.addFragmentAndAddToBackStack(fragment: Fragment, frameId: Int, listFragment: List<Int>) {
    supportFragmentManager.beginTransaction().apply {
        //    setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
        for (child in listFragment) {
            hide(supportFragmentManager.findFragmentById(child))
        }
        show(fragment)
        commit()
//        replace(frameId, fragment, fragment::class.java.simpleName)
//                .addToBackStack(fragment::class.java.simpleName)
    }


//    supportFragmentManager.executePendingTransactions()
}

/**
 * Runs a FragmentTransaction, then calls commit().
 */
private inline fun FragmentManager.transact(action: FragmentTransaction.() -> Unit) {
    beginTransaction().apply {
        action()
    }.commit()
}

fun AppCompatActivity.setupActionBar(@IdRes toolbarId: Int, action: ActionBar.() -> Unit) {
    setSupportActionBar(findViewById(toolbarId))
    supportActionBar?.setDisplayShowTitleEnabled(false)
    supportActionBar?.run {
        action()
    }
}

fun String.mToMm(): String {
    return if (this.length == 1) {
        val oldVal = this
        "0$oldVal"
    } else
        this

}

fun <T : ViewModel> AppCompatActivity.obtainViewModel(viewModelClass: Class<T>) =
        ViewModelProviders.of(this, ViewModelFactory.getInstance(application)).get(viewModelClass)


fun <T : ViewModel> Fragment.obtainViewModel(viewModelClass: Class<T>) =
        ViewModelProviders.of(this, ViewModelFactory.getInstance(activity!!.application)).get(viewModelClass)

fun <T : ViewModel> FragmentActivity.obtainViewModel(viewModelClass: Class<T>) =
        ViewModelProviders.of(this, ViewModelFactory.getInstance(application)).get(viewModelClass)

fun View.showSnackbar(snackbarText: String, timeLength: Int) {
    Snackbar.make(this, snackbarText, timeLength).show()
}

@SuppressLint("RestrictedApi")
fun BottomNavigationView.disableShiftMode() {
    val menuView = this.getChildAt(0) as BottomNavigationMenuView
    try {

        val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
        shiftingMode.isAccessible = true
        shiftingMode.setBoolean(menuView, false)
        shiftingMode.isAccessible = false
        for (i in 0..menuView.childCount.minus(1)) {
            val item = menuView.getChildAt(i) as BottomNavigationItemView
            //noinspection RestrictedApi
            item.setShiftingMode(false)
            // set once again checked value, so view will be updated
            //noinspection RestrictedApi
            item.setChecked(item.itemData.isChecked)
        }
    } catch (e: NoSuchFieldException) {
        Log.e("BNVHelper", "Unable to get shift mode field", e)
    } catch (e: IllegalAccessException) {
        Log.e("BNVHelper", "Unable to change value of shift mode", e)
    }

}

fun <T : Fragment> T.withArgs(
        argsBuilder: Bundle.() -> Unit): T =
        this.apply {
            arguments = Bundle().apply(argsBuilder)
        }

/**
 * Triggers a snackbar message when the value contained by snackbarTaskMessageLiveEvent is modified.
 */
fun View.setupSnackbar(lifecycleOwner: LifecycleOwner,
                       snackbarMessageLiveEvent: SingleLiveEvent<String>, timeLength: Int) {
    snackbarMessageLiveEvent.observe(lifecycleOwner, Observer {
        it?.let { showSnackbar(it, timeLength) }
    })
}