package id.otsuka.gits.sweatgen.data.model

data class AccessUser(val token: String? = null,
                      val id: Int = 0,
                      val email:String?=null
)