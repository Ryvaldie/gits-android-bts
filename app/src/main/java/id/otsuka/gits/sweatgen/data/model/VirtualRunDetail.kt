package id.otsuka.gits.sweatgen.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class VirtualRunDetail(val id: Int = 0,
                            val name: String? = null,
                            val ticket_code: String? = null,
                            @SerializedName("start_date")
                            val from_date_event: String? = null,
                            @SerializedName("end_date")
                            val end_date_event: String? = null,
                            val category: String? = null,
                            val address: String? = null,
                            val banner: String? = null,
                            val icon: String? = null,
                            val leaderboard: String? = null,
                            val event_description: String? = null,
                            val challenge_description: String? = null,
                            val contact: Contact? = null,
                            val status: Int? = 0,
                            val isBookmark: Int? = 0,
                            val run_status: String? = null,
                            val isContinues: Boolean? = null,
                            val participant: String? = null,
                            @SerializedName("point")
                            val points: Int? = null,
                            @SerializedName("days_left")
                            val daysLeft: String? = null,
                            val duration: Int? = 0,
                            val distance: Double? = 0.0,
                            val information: VirturunDetailInformation?=null
) : Serializable