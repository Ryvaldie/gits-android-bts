package id.otsuka.gits.sweatgen.mvvm.main.newsfeed

data class NewsItemModel(
        val id: Int = 0,
        val title: String,
        val publish_date: String,
        val content: String,
        val banner: String,
        val layoutCode: Int = 0
)