package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.ItemSimilarArticleBinding
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel

class SimilarNewsAdapter(var data: List<NewsItemModel>,
                         val listener: NewsDetailUserActionListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SimilarNewsVH).bind(data[position], (position == 0))
    }

    fun replaceData(data: List<NewsItemModel>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SimilarNewsVH(ItemSimilarArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = listener
        })
    }

    class SimilarNewsVH(val binding: ItemSimilarArticleBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: NewsItemModel, first: Boolean) {
            binding.first = first
            binding.news = obj
        }
    }
}