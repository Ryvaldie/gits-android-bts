package id.otsuka.gits.sweatgen.mvvm.onboard;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.util.Preference.KEY_LOGIN
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity


class OnBoardActivity : AppCompatActivity(), OnBoardNavigator {
    val pref by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_board)

        if (pref.getBoolean(KEY_LOGIN, false))
            MainActivity.startActivity(this)
        else {
            setupFragment()
        }

    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        OnBoardFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, OnBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }

        fun startActivity(context: Context, eventId: Int) {

            val intent = Intent(context, OnBoardActivity::class.java)

            intent.putExtra("event_id", eventId)

            intent.putExtra("openEventDetail", true)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

            context.startActivity(intent)

            (context as AppCompatActivity).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        }
    }
}
