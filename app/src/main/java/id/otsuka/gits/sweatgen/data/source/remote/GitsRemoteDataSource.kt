package id.otsuka.gits.sweatgen.data.source.remote

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.android.gms.maps.model.PolylineOptions
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseResponse
import id.otsuka.gits.sweatgen.data.model.*
import id.otsuka.gits.sweatgen.data.source.ApiService
import id.otsuka.gits.sweatgen.data.source.GetChallengeDashboardDao
import id.otsuka.gits.sweatgen.data.source.GetCotinuesRunHistoryDao
import id.otsuka.gits.sweatgen.data.source.GetRunningHistoryHeaderContent
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardModel
import id.otsuka.gits.sweatgen.mvvm.main.profile.ItemType
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.VirtualRunModel
import id.otsuka.gits.sweatgen.util.Other.LEADERBOARD_BY_DISTANCE
import id.otsuka.gits.sweatgen.util.Other.TYPE_RUN
import id.otsuka.gits.sweatgen.util.Preference.BASE_URL
import id.otsuka.gits.sweatgen.util.convertToLatLng
import id.otsuka.gits.sweatgen.util.cutExactly2NumKm
import id.otsuka.gits.sweatgen.util.parseStringToLocations
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.concurrent.TimeUnit


object GitsRemoteDataSource : GitsDataSource {

    val apiService = ApiService.createService(BASE_URL)
    var compositeDisposable: CompositeDisposable? = null

    override fun saveContinuesRun(id: Int, savedVirtualRun: SavedVirtualRun) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getVirturunBanner(userToken: String, callback: GitsDataSource.GetVirturunBannerCallback) {
        apiService.getVirtualRunBanner(userToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map {
                    val guidesList = ArrayList<RaceCentralGuide>()
                    if (it.data.content?.guide_content != null) {
                        it.data.content?.guide_content!!.mapTo(guidesList) {
                            RaceCentralGuide(image = it)
                        }
                    }
                    it.data.content?.copy(guide_contents = guidesList)
                }
                .subscribe(object : ApiCallback<GetVirturunBannerDao?>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: GetVirturunBannerDao?) {
                        if (model != null) {
                            callback.onLoaded(model)
                        }
                    }
                })
    }

    override fun getVirtualRunGuide(): List<RaceCentralGuide> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveVirtualRunGuide(guide: List<RaceCentralGuide>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getVirturunById(userToken: String, virturunId: Int, userId: Int, callback: GitsDataSource.GetVirturunByIdCallback) {
        apiService.getVirtualRunById(userToken, virturunId, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<VirtualRunDetail, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<VirtualRunDetail, String>) {
                        callback.onLoaded(model.data)
                    }
                })
    }

    override fun getAllContinuesRun(): List<SavedVirtualRun> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getContinuesRun(id: Int): SavedVirtualRun? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getHistoryVirtualRun(userToken: String, userId: Int, callback: GitsDataSource.GetVirtualrunListCallback) {
        apiService.getHistoryVirtualRun(userToken, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { it ->
                    val virturuns = ArrayList<VirtualRunModel>()
                    if (it.data.content != null) {
                        if (it.data.content!!.isNotEmpty()) {
                            it.data.content!!.mapIndexed { index, it ->
                                val item = VirtualRunModel(
                                        it.id,
                                        it.name,
                                        it.start_date,
                                        it.end_date,
                                        it.category,
                                        EventMeta(it.icon, it.banner),
                                        it.expired ?: false
                                )
                                virturuns.add(item)
                            }
                        }
                    }
                    virturuns
                }
                .subscribe(object : ApiCallback<ArrayList<VirtualRunModel>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: ArrayList<VirtualRunModel>) {
                        if (model.isNotEmpty())
                            callback.onLoaded(model)
                        else
                            callback.onDataNotAvailable()
                    }
                })
    }

    override fun getVirtualRunList(userToken: String, userId: Int, callback: GitsDataSource.GetVirtualrunListCallback) {
        apiService.getVirtualRunList(userToken, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { it ->
                    val virturuns = ArrayList<VirtualRunModel>()
                    if (it.data.content != null) {
                        if (it.data.content!!.isNotEmpty()) {
                            it.data.content!!.mapIndexed { index, it ->
                                val item = VirtualRunModel(
                                        it.id,
                                        it.name,
                                        it.start_date,
                                        it.end_date,
                                        it.category,
                                        EventMeta(it.icon, it.banner),
                                        it.expired ?: false,
                                        isJoined = ((it.run_status ?: "").isNotEmpty())
                                )
                                virturuns.add(item)
                            }
                        }
                    }
                    virturuns
                }
                .subscribe(object : ApiCallback<ArrayList<VirtualRunModel>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: ArrayList<VirtualRunModel>) {
                        if (model.isNotEmpty())
                            callback.onLoaded(model)
                        else
                            callback.onDataNotAvailable()
                    }
                })
    }

    override fun getVirturunLeaderboard(userToken: String, virturunId: Int, callback: GitsDataSource.GetLeaderboardCallback) {
        apiService.getVirturunLeaderboard(userToken, virturunId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { t ->
                    val leaderboard = ArrayList<LeaderBoardModel>()
                    if (t.data.content != null) {
                        if (t.data.content!!.isNotEmpty()) {

                            t.data.content!!.mapIndexed { index, leaderBoardDao ->

                                val item = LeaderBoardModel(
                                        when (index) {
                                            0 -> R.drawable.ic_leaderboard_first_rank
                                            1 -> R.drawable.ic_leaderboard_second_rank
                                            2 -> R.drawable.ic_leaderboard_third_rank
                                            else -> 0
                                        },
                                        leaderBoardDao.picture ?: "",
                                        if ((leaderBoardDao.leaderboard
                                                        ?: LEADERBOARD_BY_DISTANCE) == LEADERBOARD_BY_DISTANCE)
                                            ((leaderBoardDao.distance
                                                    ?: 0.0) / 1000).cutExactly2NumKm()
                                        else String.format("%dmin", TimeUnit.SECONDS.toMinutes(leaderBoardDao.duration
                                                ?: 0)),
                                        index.plus(1).toString(),
                                        leaderBoardDao.name ?: "-")

                                leaderboard.add(item)
                            }
                        }
                    }
                    leaderboard
                }
                .subscribe(object : ApiCallback<ArrayList<LeaderBoardModel>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: ArrayList<LeaderBoardModel>) {
                        if (model.isEmpty())
                            callback.onDataNotAvailable()
                        else
                            callback.onLoaded(model)
                    }
                })
    }

    override fun getLeaderboard(userToken: String, challengeId: Int, callback: GitsDataSource.GetLeaderboardCallback) {
        apiService.getLeaderboard(userToken, challengeId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { t ->
                    val leaderboard = ArrayList<LeaderBoardModel>()
                    if (t.data != null) {

                        if (t.data.isNotEmpty()) {

                            t.data.mapIndexed { index, leaderBoardDao ->

                                val item = LeaderBoardModel(
                                        when (index) {
                                            0 -> R.drawable.ic_leaderboard_first_rank
                                            1 -> R.drawable.ic_leaderboard_second_rank
                                            2 -> R.drawable.ic_leaderboard_third_rank
                                            else -> 0
                                        },
                                        leaderBoardDao.picture ?: "",
                                        if ((leaderBoardDao.leaderboard
                                                        ?: LEADERBOARD_BY_DISTANCE) == LEADERBOARD_BY_DISTANCE)
                                            ((leaderBoardDao.distance
                                                    ?: 0.0) / 1000).cutExactly2NumKm()
                                        else String.format("%dmin", TimeUnit.SECONDS.toMinutes(leaderBoardDao.duration
                                                ?: 0)),
                                        index.plus(1).toString(),
                                        leaderBoardDao.name ?: "-")

                                leaderboard.add(item)
                            }
                        }

                    }
                    leaderboard
                }
                .subscribe(object : ApiCallback<ArrayList<LeaderBoardModel>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: ArrayList<LeaderBoardModel>) {
                        if (model.isEmpty())
                            callback.onDataNotAvailable()
                        else
                            callback.onLoaded(model)
                    }
                })
    }

    override fun getChallengeDashboard(userId: Int, userToken: String, callback: GitsDataSource.GetChallengeDashboardCallback) {
        apiService.getChallengeDashboard(userToken, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<GetChallengeDashboardDao, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: BaseResponse<GetChallengeDashboardDao, String>) {
                        callback.onLoaded(model.data)
                    }
                })
    }

    override fun getContinuesRunningHistory(userId: Int, challengeId: Int, userToken: String, callback: GitsDataSource.GetContinuesRunningHistoryCallback) {
        apiService.getCotinuesRunHistory(userToken, userId, challengeId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<GetCotinuesRunHistoryDao>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<GetCotinuesRunHistoryDao>, String>) {
                        var header = GetRunningHistoryHeaderContent(0.0, 0.0)

                        if (model.data.content != null) {

                            if (model.data.content!!.header != null) {
                                header = model.data.content!!.header!!
                            }

                            if (model.data.content!!.body != null) {
                                callback.onLoaded(header, model.data.content!!.body!!)
                            } else {
                                callback.onDataNotAvailable(header)
                            }
                        } else {
                            callback.onDataNotAvailable(header)
                        }
                    }
                })
    }

    override fun getStickers(userToken: String, callback: GitsDataSource.GetStickersCallback) {
        apiService.getStickers(userToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<List<String>?, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }

                    override fun onSuccess(model: BaseResponse<List<String>?, String>) {
                        if (model.data != null) {
                            callback.onLoaded(model.data)
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }
                })
    }

    override fun login(loginParams: LoginParams, callback: GitsDataSource.LoginCallback) {
        apiService.login(loginParams = loginParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<LoginDao, String>>() {
                    override fun onSuccess(model: BaseResponse<LoginDao, String>) {
                        callback.onLoginSuccess(model.data, model.message)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onloginFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun register(registerParams: RegisterParams, callback: GitsDataSource.RegisterCallback) {
        apiService.register(registerParams = registerParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<RegisterDao, Any>>() {
                    override fun onSuccess(model: BaseResponse<RegisterDao, Any>) {
                        callback.onRegisterSuccess(model.data)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onRegisterFailed(errorMessage, errorMsg)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun bookmarkEvent(userToken: String, params: BookmarkEventParams, callback: GitsDataSource.BookmarkEventCallback) {
        apiService.bookmarkEvent(userToken, params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BookmarkEventDao, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.postError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<BookmarkEventDao, String>) {
                        callback.postSuccess(model.data)
                    }
                })
    }

    override fun getAllNews(userToken: String, callback: GitsDataSource.GetAllNewsCallback) {
        apiService.getAllNews(userToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<List<NewsFeed>>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<List<NewsFeed>>, String>) {
                        if (model.data.content != null) {
                            if (model.data.content!!.isNotEmpty()) {
                                callback.onLoaded(model.data.content!!)
                            } else
                                callback.onDataNotAvailable()
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }
                })
    }

    override fun getMedalHistory(userToken: String, userId: Int, page: Int, callback: GitsDataSource.GetMedalCallback) {
        apiService.getMedalHistory(userToken, userId, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<GetMedalHistoryDao, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {}

                    override fun onSuccess(model: BaseResponse<GetMedalHistoryDao, String>) {
                        if (model.data.content.list_medal != null)
                            if (model.data.content.list_medal.isNotEmpty())
                                callback.onDataLoaded(model.data.content.list_medal,
                                        model.data.content.current_medal, (model.data.paginate
                                        ?: Paginate()).current_page)
                            else
                                callback.onDataNotAvailable()
                        else
                            callback.onDataNotAvailable()

                    }
                })
    }

    override fun forgotPassword(forgotPasswordParams: ForgotPasswordParams, callback: GitsDataSource.ForgotPasswordCallback) {
        apiService.forgotPassword(forgotPasswordParams = forgotPasswordParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<ForgotPasswordDao, String>>() {
                    override fun onSuccess(model: BaseResponse<ForgotPasswordDao, String>) {
                        callback.onForgotPasswordSuccess(model.data, model.message)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onForgotPasswordFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun resendCode(tokenForgot: String?, callback: GitsDataSource.ResendCodeCallback) {
        apiService.resendCode(token_forgot = tokenForgot!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onSendCodeSuccess(model.message)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onSendCodeFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun addRunProgress(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: GitsDataSource.AddRunHistoryCallback) {
        apiService.postRunProgress(accessToken = userToken,
                userId = addHistoryRunParams.user_id,
                long_lat = addHistoryRunParams.long_lat ?: "",
                duration = addHistoryRunParams.duration,
                calories = addHistoryRunParams.calories,
                step = addHistoryRunParams.step,
                distance = addHistoryRunParams.distance,
                challengeId = addHistoryRunParams.challenge_id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.postSuccess()
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.postError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun addRunHistory(userToken: String, addHistoryRunParams: AddHistoryRunParams, callback: GitsDataSource.AddRunHistoryCallback) {
        apiService.postRunHistory(
                accessToken = userToken,
                userId = addHistoryRunParams.user_id,
                long_lat = addHistoryRunParams.long_lat,
                activity_name = addHistoryRunParams.activity_name,
                duration = addHistoryRunParams.duration,
                calories = addHistoryRunParams.calories,
                step = addHistoryRunParams.step,
                distance = addHistoryRunParams.distance,
                point = addHistoryRunParams.point,
                type_run = addHistoryRunParams.type_run ?: "",
                challengeId = addHistoryRunParams.challenge_id,
                virturunId = addHistoryRunParams.challenge_id
        )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.postSuccess()
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.postError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun setNewPassword(newPasswordParams: NewPasswordParams, callback: GitsDataSource.SetNewPasswordCallback) {
        apiService.setNewPassword(newPasswordParams = newPasswordParams)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, Any>>() {
                    override fun onSuccess(model: BaseResponse<Any, Any>) {
                        callback.onSetNewPasswordSuccess(model.message.toString())
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onSetNewPasswordFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun updateUser(accessToken: String?, userId: Int, name: RequestBody?, gender: RequestBody?, dob: RequestBody?, picture: MultipartBody.Part, callback: GitsDataSource.EditProfileCallback) {
        apiService.updateUser(accessToken = accessToken!!, userId = userId, name = name, dob = dob, gender = gender, picture = picture)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<User, String>>() {
                    override fun onSuccess(model: BaseResponse<User, String>) {
                        callback.onUpdateSuccess(model.data, model.message)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onUpdateFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun logout(token: String?, callback: GitsDataSource.LogoutCallback?, isExpired: Boolean) {
        apiService.postLogout(accessToken = token!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback?.logoutSuccess()
                        Log.d(GitsRemoteDataSource::class.java.simpleName, Gson().toJson(model))
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback?.logoutError(errorMessage)
                        Log.d(GitsRemoteDataSource::class.java.simpleName, errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getAvailableChallenges(userToken: String, userId: Int, callback: GitsDataSource.GetAllChallengeCallback) {
        apiService.getAvailableChallenges(userToken, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<List<Challenge>>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<List<Challenge>>, String>) {
                        if (model.data.content != null) {
                            if (model.data.content!!.isNotEmpty()) {
                                callback.onLoaded(model.data.content!!)
                            } else
                                callback.onDataNotAvailable()
                        } else
                            callback.onDataNotAvailable()
                    }
                })
    }

    override fun getPreviousChallenges(userToken: String, userId: Int, callback: GitsDataSource.GetAllChallengeCallback) {
        apiService.getPreviousChallenges(userToken, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<List<Challenge>>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<List<Challenge>>, String>) {
                        if (model.data.content != null) {
                            if (model.data.content!!.isNotEmpty()) {
                                callback.onLoaded(model.data.content!!)
                            } else
                                callback.onDataNotAvailable()
                        } else
                            callback.onDataNotAvailable()
                    }
                })
    }

    override fun getLatestAppVersionCode(deviceVersionCode: Int, callback: GitsDataSource.GetLatestAppVersionCallback) {
        apiService.getLatestApkVersionCode(deviceVersionCode).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onAppsAlreadyUpdated()
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        if (errorMessage == "426")
                            callback.onAppsNeedToUpdate()
                        else
                            callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }
                })
    }

    override fun getChallengeById(userToken: String, idChallenge: Int, userId: Int, callback: GitsDataSource.GetChallengeByIdCallback) {
        apiService.getChallengeById(userToken, idChallenge, userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Challenge, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<Challenge, String>) {
                        callback.onDataLoaded(model.data)
                    }
                })
    }

    override fun saveUser(user: User) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveForgotPassword(forgotPasswordDao: ForgotPasswordDao) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  ========================================================================================================================
     *
     *                                        PUT
     *
     *  ========================================================================================================================
     */
    override fun updatePersonalBio(userId: String, name: String, gender: String, dob: String, callback: GitsDataSource.UpdatePersonalBioCallback) {
        apiService.updatePersonalBio(userId = userId, name = name, gender = gender, dob = dob)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<UpdatePersonalBioDao, String>>() {
                    override fun onSuccess(model: BaseResponse<UpdatePersonalBioDao, String>) {
                        callback.onUpdateSuccess(model.data.access_token)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onUpdateFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getForgotPasswordDao(): ForgotPasswordDao {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getShareLink(userToken: String, eventId: Int, callback: GitsDataSource.GetShareLinkCallback) {
        apiService.getShareLink(userToken, eventId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<GetShareLinkDao, String>>() {

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<GetShareLinkDao, String>) {
                        callback.onSuccess(model.data.content.link ?: "")
                    }
                })
    }

    override fun resendEmail(token: String, callback: GitsDataSource.ResendEmailCallback) {
        apiService.resendEmail(token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onResendSuccess(model.message)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onResendFail(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun userValidation(userId: Int, callback: GitsDataSource.UserValidationCallback) {
        apiService.userValidation(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onValidationSuccess()
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onValidationFailed(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun showUser(token: String?, userId: Int, callback: GitsDataSource.ShowUserCallback) {
        apiService.showUser(accessToken = token!!, userId = userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<User, String>>() {
                    override fun onSuccess(model: BaseResponse<User, String>) {
                        callback.onUserLoaded(model.data)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onUserLoadFail(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getEventDetail(token: String?, eventId: Int, userId: Int, callback: GitsDataSource.GetEventDetailCallback) {
        apiService.getEventDetail(token!!, eventId, userId).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<GetEventDetailDao, String>>() {
                    override fun onSuccess(model: BaseResponse<GetEventDetailDao, String>) {
                        callback.onEventLoaded(model.data.event.copy(isBookmark = (model.data.bookmark
                                ?: BookmarkEventDao(isBookmark = null)).isBookmark))
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })

    }

    override fun getEventList(token: String?, ownerId: Int, callback: GitsDataSource.GetEventListCallback) {
        apiService.getEventList(token!!, ownerId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<GetEventListDao, String>>() {
                    override fun onSuccess(model: BaseResponse<GetEventListDao, String>) {
                        if (model.data.data != null) {
                            if (model.data.data.isNotEmpty()) {
                                callback.onEventLoaded(model.data.data)
                            } else
                                callback.onDataNotAvailable()
                        } else
                            callback.onDataNotAvailable()

                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getOnBoardImage(callback: GitsDataSource.GetOnBoardImageCallback) {
        apiService.getOnBoardImage()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<List<OnBoardImage>, String>>() {
                    override fun onSuccess(model: BaseResponse<List<OnBoardImage>, String>) {
                        callback.onImageLoaded(model.data)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getImagesGallery(eventId: Int, pid: String, token: String?, callback: GitsDataSource.GetImagesGalleryCallback) {
        apiService.getImagesGallery(token!!, eventId, pid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<List<ImageGallery>?, String>>() {
                    override fun onSuccess(model: BaseResponse<List<ImageGallery>?, String>) {
                        if (!model.data!!.isEmpty())
                            callback.onImagesLoaded(model.data)
                        else
                            callback.onDataNotAvailable()
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getChallenges(userToken: String, callback: GitsDataSource.GetChallengesCallback) {
        apiService.getChallenges(userToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<GetChallengesDao>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<GetChallengesDao>, String>) {
                        if (model.data.content != null) {
                            callback.onDataLoaded(model.data.content!!)
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }
                })
    }


    override fun getRunHistories(userToken: String, userId: Int, page: Int, callback: GitsDataSource.GetRunHistoriesCallback, context: Context) {
        apiService.getRunHistories(userToken, userId, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map {
                    val newHistoryAndBookmarks = ArrayList<ItemType>()

                    if (it.data.content != null) {
                        val oldHistoryAndBookmark = it.data.content
                        oldHistoryAndBookmark.mapIndexed { x, historyContent ->
                            historyContent.apply {
                                val item = if (historyContent.type == TYPE_RUN) {

                                    val listLocation = long_lat.parseStringToLocations()
                                    val listLatLng = listLocation.convertToLatLng()
                                    val polyOptionList = ArrayList<PolylineOptions>()
                                    var polyOption: PolylineOptions? = null
                                    val polyLineWidth = 6

                                    for (index in 0..listLatLng.lastIndex) {

                                        if (listLatLng.size > 1) {
                                            if (index != listLatLng.lastIndex) {
                                                if (polyOption == null) {
                                                    if (listLatLng[index] != null && listLatLng[index.plus(1)] != null)
                                                        if (index != listLatLng.lastIndex) {
                                                            polyOption = PolylineOptions()
                                                                    .add(listLatLng[index], listLatLng[index.plus(1)])
                                                                    .width(polyLineWidth.toFloat()).color(ContextCompat.getColor(context, R.color.colorBlueRadioCircle)).geodesic(true)

                                                            polyOptionList.add(polyOption)
                                                        }
                                                } else {
                                                    if (listLatLng[index] != null) {
                                                        polyOptionList.last().add(listLatLng[index])
                                                    } else {
                                                        polyOption = null
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    HistoryMapType(
                                            id,
                                            challenge_id ?: 0,
                                            user_id,
                                            activity_name,
                                            type_run,
                                            distance ?: 0.0,
                                            step ?: 0,
                                            calories ?: 0.0,
                                            duration,
                                            point ?: 0,
                                            long_lat,
                                            created_at,
                                            listLocation,
                                            listLatLng,
                                            polyOptionList
                                    )
                                } else {
                                    HistoryBookmarkType(id, event_id, name, category, logo, banner, daysLeft)
                                }
                                newHistoryAndBookmarks.add(item)

                            }
                        }
                    }

                    newHistoryAndBookmarks
                }
                .subscribe(object : ApiCallback<ArrayList<ItemType>>() {
                    override fun onSuccess(model: ArrayList<ItemType>) {
                        if (model.isNotEmpty()) {
                            callback.onDataLoaded(model, 0)
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }
                })
    }

    override fun getNewsFeeds(userToken: String, callback: GitsDataSource.GetNewsFeedsCallback) {
        apiService.getNewsFeeds(userToken)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<GetNewsFeedDao>, String>>() {
                    override fun onSuccess(model: BaseResponse<BasePaginationModel<GetNewsFeedDao>, String>) {
                        if (model.data.content != null) {

                            val topNews: List<NewsFeed>? = model.data.content!!.top_news
                            val headlines: List<NewsFeed>? = model.data.content!!.headline_news

                            callback.onLoaded(headlines, topNews)

                            if (topNews == null && headlines != null) {
                                callback.onDataNotAvailable(1)
                            } else if (topNews != null && headlines == null)
                                callback.onDataNotAvailable(0)

                            callback.onLoaded(headlines, topNews)
                        } else
                            callback.onDataNotAvailable(2)
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }
                })
    }

    override fun postJoinChallenge(userToken: String, challengeId: Int, userId: Int, callback: GitsDataSource.PostJoinChallengeCallback) {
        apiService.joinChallenge(accessToken = userToken, challengeId = challengeId, userId = userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onFailed(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onSuccess()
                    }
                })
    }

    override fun postJoinVirturun(userToken: String, virturunId: Int, userId: Int, callback: GitsDataSource.PostJoinChallengeCallback) {
        apiService.joinVirtualRun(accessToken = userToken, virturunId = virturunId, userId = userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onFailed(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onSuccess()
                    }
                })
    }


    override fun redeemValid(userToken: String, virturunId: Int, userId: Int, accessCode: String, callback: GitsDataSource.GeneralPostCallback) {
        apiService.redeemValid(userToken, virturunId, userId, accessCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<Any, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onFailed(errorMessage)
                    }

                    override fun onFinish() {
                        callback.onFinish()
                    }

                    override fun onSuccess(model: BaseResponse<Any, String>) {
                        callback.onSuccess(model.message)
                    }
                })
    }

    override fun getAllNewsByCategory(userToken: String, categoryId: Int, page: Int, callback: GitsDataSource.GetAllNewsCallback) {
        apiService.getNewsByCategory(userToken, categoryId, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<List<NewsFeed>>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<List<NewsFeed>>, String>) {
                        if (model.data.content != null) {
                            if (model.data.content!!.isNotEmpty()) {
                                callback.onLoaded(model.data.content!!)
                            } else
                                callback.onDataNotAvailable()
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }
                })
    }

    override fun getAllNews(userToken: String, page: Int, callback: GitsDataSource.GetAllNewsCallback) {
        apiService.getAllNews(userToken, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<List<NewsFeed>>, String>>() {
                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {
                    }

                    override fun onSuccess(model: BaseResponse<BasePaginationModel<List<NewsFeed>>, String>) {
                        if (model.data.content != null) {
                            if (model.data.content!!.isNotEmpty()) {
                                callback.onLoaded(model.data.content!!)
                            } else
                                callback.onDataNotAvailable()
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }
                })
    }

    override fun getNewsDetail(userToken: String, newsId: Int, callback: GitsDataSource.GetNewsDetailCallback) {
        apiService.getNewsDetail(userToken, newsId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiCallback<BaseResponse<BasePaginationModel<GetNewsDetailDao>, String>>() {
                    override fun onSuccess(model: BaseResponse<BasePaginationModel<GetNewsDetailDao>, String>) {
                        if (model.data.content != null) {
                            if (model.data.content!!.detail != null) {
                                callback.onLoaded(model.data.content!!.detail!!, model.data.content!!.similar)
                            } else
                                callback.onDataNotAvailable()
                        } else
                            callback.onDataNotAvailable()
                    }

                    override fun onFailure(errorMessage: String, errorMsg: String?) {
                        callback.onError(errorMessage)
                    }

                    override fun onFinish() {

                    }
                })
    }

    override fun getToken(): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getUser(): User? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    /**
     *  ========================================================================================================================
     *
     *                                        ADDITIONAL FUNCTION
     *
     *  ========================================================================================================================
     */


    private fun addDisposable(disposable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
            compositeDisposable!!.add(disposable)
        }
    }

    private fun clearSubscribe() {
        if (compositeDisposable != null) {
            compositeDisposable!!.clear()
        }
    }

    override fun clearDisposable() {
        clearSubscribe()
    }

}