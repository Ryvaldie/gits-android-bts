package id.otsuka.gits.sweatgen.mvvm.main.runtracker.postingpage;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableDouble
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.AddHistoryRunParams
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class PostingPageViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val obsAddRunHistory = ObservableField<AddHistoryRunParams>()

    val bDistance = ObservableField("")

    val bStep = ObservableField("")

    val bCalories = ObservableField("")

    val bDoubleCal = ObservableDouble(0.0)

    val bDuration = ObservableField("")

    val bCurrentDate = ObservableField("")

    val bActivityName = ObservableField("")

    val loadingEvent = SingleLiveEvent<Boolean>()

    val redirectHome = SingleLiveEvent<Void>()

    val snackbarMessage = SingleLiveEvent<String>()

    fun validateField(): Boolean = !(bActivityName.get().isNullOrEmpty())

    fun postRunHistory() {

        loadingEvent.value = true

        obsAddRunHistory.set(obsAddRunHistory.get()?.copy(activity_name = bActivityName.get()
                ?: ""))

        repository.addRunHistory("", obsAddRunHistory.get()!!, object : GitsDataSource.AddRunHistoryCallback {
            override fun postSuccess() {
                redirectHome.call()
            }

            override fun onFinish() {
                loadingEvent.value = false
            }

            override fun postError(errorMessage: String?) {
                snackbarMessage.value = errorMessage
            }
        })
    }
}