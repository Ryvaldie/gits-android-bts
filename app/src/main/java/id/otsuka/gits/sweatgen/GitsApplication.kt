package id.otsuka.gits.sweatgen

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import java.util.*
/**
 * Created by irfanirawansukirman on 26/01/18.
 */

class GitsApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        setupGlobalFontDefault()
        setupLocale()
    }
    private fun setupLocale() {
        val config = resources.configuration

        config.setLocale(Locale.ENGLISH)

        baseContext?.resources?.updateConfiguration(config, baseContext?.resources?.displayMetrics)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

    private fun setupGlobalFontDefault() {
        /*  CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                  .setDefaultFontPath("fonts/Raleway-Regular.ttf")
                  .setFontAttrId(R.attr.fontPath)
                  .build()
          )*/
    }
}