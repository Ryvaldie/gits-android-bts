package id.otsuka.gits.sweatgen.data.model

data class ForgotPasswordParams (val email: String? = null)