package id.otsuka.gits.sweatgen.mvvm.onboard;


interface OnBoardUserActionListener {

    fun onSignupClick()
    fun onSigninClick()
    // Example
    // fun onClickItem()

}