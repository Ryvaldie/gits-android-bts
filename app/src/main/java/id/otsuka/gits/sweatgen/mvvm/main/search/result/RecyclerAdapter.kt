package id.otsuka.gits.sweatgen.mvvm.main.search.result

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.ItemResultBinding

class RecyclerAdapter(val context: Context,
                      var result: ArrayList<Challenge>,
                      val listener: ResultUserActionListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = result.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ResultVH).bind(result[position])
    }

    fun replaceResult(result: ArrayList<Challenge>) {
        this.result.apply {
            clear()
            addAll(result)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mViewDataBinding: ItemResultBinding = ItemResultBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            mListener = listener
        }
        return ResultVH(mViewDataBinding)
    }

    class ResultVH(val mViewDataBinding: ItemResultBinding) : RecyclerView.ViewHolder(mViewDataBinding.root) {
        fun bind(obj: Challenge) {
            mViewDataBinding.challenge = obj
        }
    }
}