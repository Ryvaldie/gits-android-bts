package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import kotlinx.android.synthetic.main.activity_challenge_see_all.*


class ChallengeSeeAllActivity : AppCompatActivity(), ChallengeSeeAllNavigator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenge_see_all)
        setupFragment()
        setupToolbar()
    }


    fun obtainViewModel(): ChallengeSeeAllViewModel = obtainViewModel(ChallengeSeeAllViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        ChallengeSeeAllFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    private fun setupToolbar() {
        toolbar_see_all_challenge.setNavigationOnClickListener { super.onBackPressed() }
        if (intent.getIntExtra(CHALLENGE_TYPE, -1) == PREVIOUS_CHALLENGE)
            toolbar_see_all_challenge.title = getString(R.string.text_previous_challenge)
    }

    companion object {
        val CHALLENGE_TYPE = "challengeType"
        val AVAILABLE_CHALLENGE = 1
        val PREVIOUS_CHALLENGE = 2
        fun startActivity(context: Context, type: Int) {
            context.startActivity(Intent(context, ChallengeSeeAllActivity::class.java)
                    .putExtra(CHALLENGE_TYPE, type))
        }
    }
}
