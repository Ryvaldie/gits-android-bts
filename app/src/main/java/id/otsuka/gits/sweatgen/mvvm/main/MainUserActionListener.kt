package id.otsuka.gits.sweatgen.mvvm.main;


interface MainUserActionListener {

    // Example
    // fun onClickItem()

    fun onSearchClick()

    fun onTryAgain()
}