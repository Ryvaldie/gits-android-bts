package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers;


interface StickersUserActionListener {

    fun onItemClick(url: String)

}