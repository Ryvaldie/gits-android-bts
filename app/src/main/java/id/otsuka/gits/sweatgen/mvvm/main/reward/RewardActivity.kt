package id.otsuka.gits.sweatgen.mvvm.main.reward;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import kotlinx.android.synthetic.main.activity_reward.*


class RewardActivity : AppCompatActivity(), RewardNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reward)
        replaceFragmentInActivity(RewardFragment.newInstance(), R.id.frame_main_content)
           }


    fun obtainViewModel(): RewardViewModel = obtainViewModel(RewardViewModel::class.java)

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, RewardActivity::class.java))
        }
    }
}
