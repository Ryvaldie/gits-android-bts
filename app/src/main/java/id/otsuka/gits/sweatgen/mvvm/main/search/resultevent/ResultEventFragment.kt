package id.otsuka.gits.sweatgen.mvvm.main.search.resultevent


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentResultEventBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.event.EventViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel


class ResultEventFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentResultEventBinding
    lateinit var mViewModel: ResultEventViewModel
    lateinit var eventVm: EventViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = FragmentResultEventBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            (requireActivity() as MainActivity).mViewModel.searchEvent.observe(this@ResultEventFragment, Observer {
                search((requireActivity() as MainActivity).resultEventList, it!!)
            })
        }

        mViewDataBinding.mListener = object : ResultEventUserActionListener {

        }

        eventVm = obtainEventVm().apply {

            this.countData.set(0)

            getPocariEventList(1)

            searchResult.observe(this@ResultEventFragment, Observer {

                if (this.countData.get() < 2)
                    (activity as MainActivity).resultEventList.clear()

                (activity as MainActivity).resultEventList.addAll(it!!)

                if (this.countData.get() == 2) {
                    this.countData.set(-1)
                    //    mViewModel.start((activity as MainActivity).resultEventList)
                } else {
                    getPocariEventList(0)
                }


            })
        }

        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupListData()
    }


    fun obtainViewModel(): ResultEventViewModel = obtainViewModel(ResultEventViewModel::class.java)
    private fun obtainEventVm(): EventViewModel = obtainViewModel(EventViewModel::class.java)


    private fun setupListData() {
        mViewDataBinding.recyclerResultEvent.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = ResultEventAdapter(context, arrayListOf())
        }
    }


    companion object {
        fun newInstance() = ResultEventFragment().apply {

        }

    }

}
