package id.otsuka.gits.sweatgen.util

/**
 * Created by irfanirawansukirman on 26/01/18.
 */

object Preference {
    //Todo Diisi dengan key preference
    const val KEY = "PREFERENCE_KEY"
    //  val BASE_URL = "http://43.245.180.185/sweatgen_api/api/v2/"
    val BASE_URL = "http://149.28.157.167/api/v2/"
    val KEY_LOGIN = "login"
    const val USER_KEY = "user"
    const val SAVED_RUN_LIST = "savedRun"
    const val FORGOT_KEY = "forgot"
    const val DEEPLINK_BASE = "otsuka://sweatgen.app/event"
    const val VIRTURUN_GUIDE = "virturunGuide"
}

object Other {
    //Todo Diisi dengan key Intent, Argument, Database name etc
    const val ENGLISH_LOCALE = "en_US"
    const val LEADERBOARD_BY_DISTANCE = "by_distance"
    const val LEADERBOARD_BY_TIME = "by_time"
    const val JOINED_RUN = "joined"
    const val PARTICIPANT = "participant"
    const val FINISHED_RUN = "finished"
    const val INDONESIAN_LOCALE = "id_ID"
    const val KEY_NAME = "GradMaker"
    const val INTENT_FILTER_GPS = "intentGps"
    const val TOKO_OTSUKA_URL = "https://toko-otsuka.id/"
    const val DEEPLINK_ID = "id"
    const val INTENT_PERMISSION = "intentPermission"
    const val INTENT_LOCATION_CHANGED = "intentLocationChanged"
    const val INTENT_START_TIMER = "intentStartTimer"
    const val INTENT_EXTRA_LAST_DURATION = "lastDuration"
    const val NOTIF_CHANNEL = "SweatGen"
    const val NOTIF_RUN_TRACKER = "Running Tracker"
    const val INTENT_TIMER_TICK = "intentTimerTick"
    const val CHALLENGE_EVENT_TYPE = "EVENT"
    const val CHALLENGE_NON_EVENT = "NONEVENT"
    const val NOTIF_FOREGROUND_SERVICE_ID = 5515
    const val TOKEN_EXPIRE = "token_expire"
    const val NO_INET_CONNECTION = "No Internet Connection"
    const val TYPE_FREE_RUN = "free_run"
    const val TYPE_CHALLENGE = "challenge"
    const val TYPE_VIRTURUN = "virtual_run"
    const val TYPE_BOOKMARK = "bookmark"
    const val TYPE_RUN = "history_run"
    const val PI_REQUEST_CODE = 324325
    const val RESOLUTION_ACTIVATE_GPS = "resolution"
    const val KEY_TIME_STRING_TICKING = "time"
    const val KEY_TIME_MILLIS_TICKING = "millis"
    const val KEY_UPDATE_LOCATION = "location"
}