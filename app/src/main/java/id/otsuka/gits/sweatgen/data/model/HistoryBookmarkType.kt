package id.otsuka.gits.sweatgen.data.model

import id.otsuka.gits.sweatgen.mvvm.main.profile.ItemType

data class HistoryBookmarkType(val id: Int = 0,
                               val event_id: Int = 0,
                               val name: String? = null,
                               val category: String? = null,
                               val logo: String? = null,
                               val banner: String? = null,
                               val daysLeft: String? = null
) : ItemType {
    override fun getItemType(): Int = ItemType.TYPE_EVENT
}