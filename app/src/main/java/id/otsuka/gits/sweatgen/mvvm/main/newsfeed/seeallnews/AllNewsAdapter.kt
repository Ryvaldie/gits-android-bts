package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.ItemListLoadingBinding
import id.otsuka.gits.sweatgen.databinding.ItemSeeallNewsBinding
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel

class AllNewsAdapter(var data: List<NewsItemModel>,
                     val listener: SeeAllNewsUserActionListener,
                     val viewModel: SeeAllNewsViewModel
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        val ITEM_PER_PAGE = 10
    }

    var showProgress = true

    fun replaceData(data: List<NewsItemModel>) {
        if (this.data.isEmpty()) {
            this.data = data
            notifyDataSetChanged()
        } else {
            if (data.size > this.data.size) {
                val lastPosition = (ITEM_PER_PAGE.times(viewModel.currentPage)).minus(1)
                this.data = data
                notifyItemRangeInserted(lastPosition, this.data.size.minus(1))
            }
            viewModel.currentPage += 1
        }
    }

    class ProgbarVH(val mBinding: ItemListLoadingBinding) : RecyclerView.ViewHolder(mBinding.root) {
        fun bind(isVisible: Boolean) {
            mBinding.isVisible = isVisible
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (showProgress) {
            if (data.size == position) {
                R.layout.item_list_loading
            } else {
                R.layout.item_seeall_news
            }
        } else {
            R.layout.item_seeall_news
        }
    }

    fun showProgress(show: Boolean) {
        showProgress = show
        notifyItemChanged(data.size)
        //   notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return if (data.isEmpty()) 0 else data.size + (if (showProgress) 1 else 0)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProgbarVH -> holder.bind(!(viewModel.isLastPage.get()))
            is NewsVH -> {
                holder.bind(data[position])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_list_loading -> {
                val binding = ItemListLoadingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                ProgbarVH(binding)
            }
            else -> {
                val binding = ItemSeeallNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
                    mListener = listener
                }
                NewsVH(binding)
            }
        }
    }

    class NewsVH(val binding: ItemSeeallNewsBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: NewsItemModel) {
            binding.apply {
                news = obj
            }
        }
    }


}