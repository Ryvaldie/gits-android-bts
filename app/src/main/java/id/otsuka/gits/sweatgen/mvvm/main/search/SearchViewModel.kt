package id.otsuka.gits.sweatgen.mvvm.main.search;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import id.otsuka.gits.sweatgen.data.model.NewsFeed
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.agoFormat
import id.otsuka.gits.sweatgen.util.getSpanString
import id.otsuka.gits.sweatgen.util.toFormattedDate


class SearchViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    /*
    * 0 -> untuk tipe event
    * 1 -> untuk tipe challenge
     */
    val searchType = ObservableInt(0)

    val moveToNewsDetail = SingleLiveEvent<Int>()

    val bIsNewsFeed = ObservableField<Boolean>()

    val listAllNews = ObservableArrayList<NewsItemModel>()

    val filterdNews = ObservableArrayList<NewsItemModel>()

    val searchValue = SingleLiveEvent<String>()

    val progbarState = ObservableBoolean(false)

    val snackbarMsg = SingleLiveEvent<String>()

    var mThread = Thread()

    fun getAllNews() {
        progbarState.set(true)

        repository.getAllNews("", object : GitsDataSource.GetAllNewsCallback {
            override fun onDataNotAvailable() {
                progbarState.set(false)
            }

            override fun onError(errorMessage: String?) {
                progbarState.set(false)
                snackbarMsg.value = errorMessage
            }

            override fun onLoaded(news: List<NewsFeed>) {

                listAllNews.clear()

                mThread = Thread(Runnable {
                    listAllNews.addAll(news.map {
                        NewsItemModel(
                                it.id,
                                it.title ?: "-",
                                (it.date_publish
                                        ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").agoFormat(context) + " - " + it.category,
                                (it.content ?: "-").getSpanString(context).toString(),
                                it.banner ?: "")
                    })

                    progbarState.set(false)
                })

                mThread.start()
            }
        })
    }

}