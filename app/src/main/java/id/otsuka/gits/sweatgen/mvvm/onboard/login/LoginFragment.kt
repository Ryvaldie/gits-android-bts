package id.otsuka.gits.sweatgen.mvvm.onboard.login


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.databinding.FragmentLoginBinding
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailActivity
import id.otsuka.gits.sweatgen.mvvm.onboard.register.RegisterFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.register.verification.VerificationFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio.PersonalBioFragment
import id.otsuka.gits.sweatgen.mvvm.onboard.login.forgotpassword.ForgotPasswordFragment
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Preference.KEY_LOGIN

class LoginFragment : BaseFragment() {

    lateinit var mViewDataBinding: FragmentLoginBinding
    lateinit var mViewModel: LoginViewModel
    val pref by lazy { PreferenceManager.getDefaultSharedPreferences(context) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentLoginBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {

        }

        mViewDataBinding.mListener = object : LoginUserActionListener {

            override fun onBtnLoginClick() {

                requireActivity().hideKeyboard()

                if (mViewModel.validateFields()) {
                    mViewModel.postLogin()
                } else {
                    getString(R.string.text_please_fill_info).showSnackBar(mViewDataBinding.root)
                }
            }

            override fun onForgotPasswordClick() {
                activity?.replaceFragmentAndAddToBackStack(ForgotPasswordFragment.newInstance(), R.id.frame_main_content)
            }

            override fun onLoginFbClick() {

            }

            override fun onSignupClick() {
                (activity as AppCompatActivity).replaceFragmentInActivity(RegisterFragment.newInstance(), R.id.frame_main_content)
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.loadingEvent.observe(this, Observer { it ->
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {
                Handler().postDelayed({
                    hideProgressDialog()
                    if (mViewModel.bSuccess.get()) {
                        if (mViewModel.bVerified.get()) {
                            if (mViewModel.bMessage.get() == "Personal bio has not been updated") {
                                activity?.replaceFragmentAndAddToBackStack(PersonalBioFragment.newInstance(), R.id.frame_main_content)
                            } else {

                                pref.edit().putBoolean(KEY_LOGIN, true).apply()

                                if (activity!!.intent.getBooleanExtra("openEventDetail", false))
                                    EventDetailActivity.startActivity(context!!, activity!!.intent.getIntExtra("event_id", -1))
                                else
                                    MainActivity.startActivity(context!!)

                                activity?.finish()
                            }

                        } else {
                            activity?.replaceFragmentAndAddToBackStack(VerificationFragment.newInstance(), R.id.frame_main_content)
                        }

                    } else
                        mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
                }, 1000)

            }

        })
    }

    fun obtainViewModel(): LoginViewModel = obtainViewModel(LoginViewModel::class.java)

    companion object {
        fun newInstance() = LoginFragment().apply {

        }

    }

}
