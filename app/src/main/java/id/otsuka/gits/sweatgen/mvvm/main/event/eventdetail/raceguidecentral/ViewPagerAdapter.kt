package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import id.otsuka.gits.sweatgen.data.model.RaceCentralGuide
import id.otsuka.gits.sweatgen.databinding.CustomVpItemRaceBinding

class ViewPagerAdapter(val context: Context,
                       var pagesContent: List<RaceCentralGuide>,
                       var viewModel: RaceGuideCentralViewModel
) : PagerAdapter() {

    fun replacePagesContent(pagesContent: List<RaceCentralGuide>) {
        this.pagesContent = pagesContent
        notifyDataSetChanged()
        if (this.pagesContent.isNotEmpty())
            viewModel.createIndicator.call()
    }

    override fun getCount(): Int = pagesContent.size

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val viewDataBinding = CustomVpItemRaceBinding.inflate(layoutInflater, container, false).apply {
            race = pagesContent[position]
            listener = object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    progbar.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    progbar.visibility = View.GONE
                    return false
                }
            }
        }

        container.addView(viewDataBinding.root, 0)

        return viewDataBinding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}