package id.otsuka.gits.sweatgen.data.model

class NewPasswordParams (val code: String? = null,
                         val password: String? = null)