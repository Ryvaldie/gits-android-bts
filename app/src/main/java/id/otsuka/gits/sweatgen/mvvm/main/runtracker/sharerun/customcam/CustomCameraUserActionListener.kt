package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.customcam;


interface CustomCameraUserActionListener {

    fun onCapture()
    fun onSwitchCam()

}