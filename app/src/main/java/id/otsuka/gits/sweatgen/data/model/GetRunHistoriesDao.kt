package id.otsuka.gits.sweatgen.data.model

data class GetRunHistoriesDao(val content: List<HistoryContent>?,
                              val pagination: Paginate
)