package id.otsuka.gits.sweatgen.data.model

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import id.otsuka.gits.sweatgen.mvvm.main.profile.ItemType

data class HistoryMapType(
        val id: Int = 0,
        val challenge_id: Int = 0,
        val user_id: Int = 0,
        val activity_name: String? = "",
        val type_run: String? = "",
        val distance: Double = 0.0,
        val step: Int = 0,
        val calories: Double = 0.0,
        val duration: String? = "",
        val point: Int = 0,
        val long_lat: String? = "",
        val created_at: String? = "",
        val listLocations: ArrayList<Location?>,
        val listLatLng: ArrayList<LatLng?>,
        val listPolyOptions: ArrayList<PolylineOptions>
) : ItemType {
    override fun getItemType(): Int {
        return ItemType.TYPE_MAP
    }
}