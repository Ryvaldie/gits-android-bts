package id.otsuka.gits.sweatgen.mvvm.main.event;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.support.v4.app.FragmentManager
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.data.model.GetVirturunBannerDao
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.toDate
import java.util.*


class EventViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    var obsEventPocari = ObservableArrayList<Event>()
    var obsEventLain = ObservableArrayList<Event>()
    val bMesage = ObservableField("")
    val firstPageEvent = SingleLiveEvent<Int>()
    val bSliderCount = ObservableField(0)
    val eventLoading = SingleLiveEvent<Boolean>()
    val obsFragmentManager = ObservableField<FragmentManager>()
    val searchResult = SingleLiveEvent<ArrayList<Event>>()
    val countData = ObservableInt(-1)
    val bBannerVirturun = ObservableField("https://chat.gits.id/files/xe3e8jpnubb8tdor1sxazb6oyw/public?h=REcwNjrIecjse0Vxqvf8yA5H5pXtJ6Q0-w5Oko4I11U")

    fun getVirturunBanner() {
        repository.getVirturunBanner("", object : GitsDataSource.GetVirturunBannerCallback {
            override fun onError(errorMessage: String?) {
                bMesage.set(errorMessage)
            }

            override fun onLoaded(data: GetVirturunBannerDao) {
                bBannerVirturun.set(data.banner_event)
            }
        })
    }

    fun getPocariEventList(ownerId: Int) {
        eventLoading.value = true

        repository.getEventList(null, ownerId, object : GitsDataSource.GetEventListCallback {
            override fun onEventLoaded(eventList: List<Event>) {

                bMesage.set("")

                if (ownerId == 1)
                    obsEventPocari.apply {
                        clear()
                        addAll(eventList)
                        bSliderCount.set(this.size)
                        eventLoading.value = false
                    }
                else
                    obsEventLain.apply {
                        clear()
                        eventList.mapTo(obsEventLain) {
                            val dateEvent = it.end_date_event?.toDate()
                            val currentDate = Date(System.currentTimeMillis())
                            it.copy(isExpire = currentDate.after(dateEvent))
                        }
                    }

                if (countData.get() != -1) {
                    if (countData.get() < 2)
                        countData.set(countData.get().plus(1))

                    val result: ArrayList<Event> = arrayListOf()

                    if (ownerId == 1)
                        result.addAll(obsEventPocari)
                    else
                        result.addAll(obsEventLain)

                    searchResult.value = result

                }

            }

            override fun onDataNotAvailable() {
                eventLoading.value = false

                if (countData.get() < 2)
                    countData.set(countData.get().plus(1))

                searchResult.value = ArrayList(0)
            }

            override fun onError(errorMessage: String?) {
                if (errorMessage != null)
                    bMesage.set(errorMessage)
                eventLoading.value = false

            }
        })
    }
}