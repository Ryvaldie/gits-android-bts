package id.otsuka.gits.sweatgen.mvvm.main.leaderboard


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentLeaderBoardBinding
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.CHALLENGE_ID
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.CURRENT_PROGRESS
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.DAYS_LEFT
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.IS_CONTINUES
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.LEADERBOARD_TYPE
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.PROGRESS
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.TOTAL_PROGRESS
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.showSnackBar

class LeaderBoardFragment : Fragment(), LeaderBoardUserActionListener {

    lateinit var mViewDataBinding: FragmentLeaderBoardBinding
    lateinit var mViewModel: LeaderBoardViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentLeaderBoardBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            eventClickItem.observe(this@LeaderBoardFragment, Observer {
                if (it != null) {
                    onClickItem(it)
                }
            })
        }

        mViewDataBinding.mListener = this@LeaderBoardFragment

        return mViewDataBinding.root

    }


    override fun onClickItem(data: LeaderBoardModel) {
        // TODO if you have a toast extension, you can replace this
        //  Toast.makeText(activity, data.title, Toast.LENGTH_SHORT).show()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeEvents()
        setupListData()
        val mIntent = requireActivity().intent
        mViewModel.apply {
            leaderboardType = mIntent.getIntExtra(LEADERBOARD_TYPE, 0)
            bDaysLeft.set(mIntent.getStringExtra(DAYS_LEFT))
            bCurrentProgress.set(mIntent.getStringExtra(CURRENT_PROGRESS))
            bTotalProgress.set(mIntent.getStringExtra(TOTAL_PROGRESS))
            challengeId = mIntent.getIntExtra(CHALLENGE_ID, 0)
            bProgress.set(mIntent.getIntExtra(PROGRESS, 0))
            isContinues.set(mIntent.getBooleanExtra(IS_CONTINUES, false))
            loadData()
        }
    }

    private fun observeEvents() {
        mViewModel.snackbarMessage.observe(this@LeaderBoardFragment, Observer {
            if (it != null) {
                when (it) {
                    TOKEN_EXPIRE -> {
                        mViewModel.repository.logout(null, null, true)

                        requireActivity().redirectToLogin()
                    }
                    else -> {
                        it.showSnackBar(mViewDataBinding.root)
                    }
                }
            }
        })
    }


    // TODO add LeaderBoardViewModel to ViewModelFactory & if template have an error, please reimport obtainViewModel
    fun obtainViewModel(): LeaderBoardViewModel = obtainViewModel(LeaderBoardViewModel::class.java)


    fun setupListData() {
        mViewDataBinding.recyclerLeaderBoard.adapter = LeaderBoardAdapter(ArrayList(), mViewModel)
        mViewDataBinding.recyclerLeaderBoard.layoutManager = LinearLayoutManager(context)
    }


    companion object {
        fun newInstance() = LeaderBoardFragment().apply {

        }

    }

}
