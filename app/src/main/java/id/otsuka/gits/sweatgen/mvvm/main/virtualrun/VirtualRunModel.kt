package id.otsuka.gits.sweatgen.mvvm.main.virtualrun

import id.otsuka.gits.sweatgen.data.model.EventMeta
import id.otsuka.gits.sweatgen.data.model.Extras


data class VirtualRunModel(
        val id: Int = 0,
        val name: String? = null,
        val from_date_event: String? = null,
        val end_date_event: String? = null,
        val category: String? = null,
        val meta: EventMeta? = null,
        val isExpire: Boolean,
        val isContinues: Boolean? = null,
        val description: String? = null,
        val participant: String? = null,
        val extras: Extras? = null,
        val distance: Double? = null,
        val duration: Int? = null,
        val points: Int? = null,
        val isJoined: Boolean = false,
        val isFinished: Boolean = false
)