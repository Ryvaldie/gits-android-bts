package id.otsuka.gits.sweatgen.mvvm.main.event;

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.fenchtose.nocropper.CropperView
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.util.yyyyMMddToddMMMMyyyy

/**
 * @author radhikayusuf.
 */

object EventBindings {

    @BindingAdapter("app:eventItems")
    @JvmStatic
    fun setupEventItems(recyclerView: RecyclerView, events: List<Event>) {
        recyclerView.apply {
            (adapter as RecyclerAdapter).replaceEvents(events)
        }
    }

    @BindingAdapter("app:sliderItems", "app:fragmentManager", "app:viewmodel", requireAll = false)
    @JvmStatic
    fun setupViewpagerAdapter(viewPager: ViewPager, pagesContent: List<Event>, fragmentManager: FragmentManager, vm: EventViewModel) {
        viewPager.apply {

            adapter = ViewPagerAdapter(context, fragmentManager, ArrayList(0), vm)

            offscreenPageLimit = 3

            pageMargin = -(resources.displayMetrics.widthPixels / 15)

            clipToPadding = false

            setPadding(32, 0, 32, 0)

            setPageTransformer(false, adapter as ViewPagerAdapter)

            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageSelected(position: Int) {
/*
                    //ini untuk memindahkan indicator ke current position
                    val linDots = (parent as View).findViewById<LinearLayout>(R.id.lin_vp_indicator)

                    if (linDots.childCount > 0) {
                        for (x in 0..pagesContent.size.minus(1)) {
                            (linDots.getChildAt(x) as ImageView).apply {
                                setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_inactive_vp_indicator))
                            }
                        }

                        (linDots.getChildAt(position % pagesContent.size) as ImageView).apply {
                            setImageDrawable(ContextCompat.getDrawable(this.context, R.drawable.custom_active_vp_indicator))
                        }
                    }
*/
                }

                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }
            })

            (adapter as ViewPagerAdapter).replacePagesContent(pagesContent)
        }

    }

    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?) {

        val finalImage: Any = imageUrl ?: R.drawable.img_slider_event

        Glide.with(view.context)
                .load(finalImage)
                .into(view)
                .apply { RequestOptions().error(R.color.greyLineProfile).placeholder(R.color.greyLineProfile) }
    }

    @BindingAdapter("app:imageNoPlaceholder")
    @JvmStatic
    fun loadImageNoPlaceholder(view: ImageView, imageUrl: Any?) {

        Glide.with(view.context)
                .load(imageUrl)
                .into(view)
    }

    @BindingAdapter("app:cropImgUrl")
    @JvmStatic
    fun loadImageCrop(view: CropperView, image: Bitmap?) {
        view.setImageBitmap(image)
    }

    @BindingAdapter("app:address", "app:date", requireAll = false)
    @JvmStatic
    fun setAddressAndDate(textView: TextView, address: String, date: String?) {
        with(textView) {
            val addressDate = context.getString(R.string.text_event_date_dummy)

            if (date != null) {
                if (address.isEmpty()) {
                    text = addressDate.replace("%, *", date.yyyyMMddToddMMMMyyyy())
                } else
                    text = addressDate.replace("%", address).replace("*", date.yyyyMMddToddMMMMyyyy())
            } else {
                text = "-"
            }

        }
    }
}