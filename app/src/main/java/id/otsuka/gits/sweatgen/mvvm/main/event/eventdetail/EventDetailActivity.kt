package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail;

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.airbnb.deeplinkdispatch.DeepLink
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailViewModel
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailNavigator
import id.otsuka.gits.sweatgen.mvvm.onboard.OnBoardActivity
import id.otsuka.gits.sweatgen.util.Other.DEEPLINK_ID
import id.otsuka.gits.sweatgen.util.Preference.DEEPLINK_BASE
import id.otsuka.gits.sweatgen.util.setupActionBar
import id.otsuka.gits.sweatgen.util.showToast

@DeepLink(DEEPLINK_BASE)
class EventDetailActivity : AppCompatActivity(), EventDetailNavigator {

    var eventId = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)
        if (intent.getBooleanExtra(DeepLink.IS_DEEP_LINK, false)) {
            intent.data.apply {

                eventId = try {
                    getQueryParameter(DEEPLINK_ID).toInt()
                } catch (e: NumberFormatException) {
                    -1
                }
            }
            if (obtainViewModel().repository.getUser() == null) {
                OnBoardActivity.startActivity(this, eventId)
                finish()
                getString(R.string.text_must_login).showToast(this)
                return
            }
        } else {
            eventId = intent.getIntExtra(EVENT_ID, -1)
        }
        setupFragment()
    }


    fun obtainViewModel(): EventDetailViewModel = obtainViewModel(EventDetailViewModel::class.java)

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        EventDetailFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        val EVENT_ID = "eventId"
        fun startActivity(context: Context, eventId: Int) {
            context.startActivity(Intent(context, EventDetailActivity::class.java).putExtra(EVENT_ID, eventId))
        }
    }
}
