package id.otsuka.gits.sweatgen.mvvm.main.runtracker.postingpage;

import android.databinding.BindingAdapter
import android.widget.TextView
import id.otsuka.gits.sweatgen.util.cutExactly1Num

/**
 * @author radhikayusuf.
 */

object PostingPageBindings {

    @BindingAdapter("app:cal")
    @JvmStatic
    fun setCalories(textView: TextView, calories: Double?) {
        if (calories != null) {

            if (calories >= 1000) {

                textView.text = calories.div(1000).cutExactly1Num().replace(".0", "")

            } else {
                textView.text = calories.cutExactly1Num().replace(".0", "")
            }
        } else {
            textView.text = "0"
        }
    }
}