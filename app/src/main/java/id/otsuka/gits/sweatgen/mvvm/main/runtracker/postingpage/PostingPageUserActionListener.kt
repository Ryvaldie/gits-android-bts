package id.otsuka.gits.sweatgen.mvvm.main.runtracker.postingpage;


interface PostingPageUserActionListener {

    // Example
    // fun onClickItem()
    fun post()

    fun onBack()
}