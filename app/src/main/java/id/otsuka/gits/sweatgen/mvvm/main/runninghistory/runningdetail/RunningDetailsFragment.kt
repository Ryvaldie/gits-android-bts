package id.otsuka.gits.sweatgen.mvvm.main.runninghistory.runningdetail


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.HistoryRun
import id.otsuka.gits.sweatgen.databinding.FragmentRunningDetailsBinding
import id.otsuka.gits.sweatgen.util.*

class RunningDetailsFragment : Fragment(), RunningDetailsUserActionListener, OnMapReadyCallback {

    lateinit var mViewDataBinding: FragmentRunningDetailsBinding
    lateinit var mViewModel: RunningDetailsViewModel
    val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"
    var mGoogleMap: GoogleMap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRunningDetailsBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            if (arguments != null) {

                val jsonItem = arguments!!.getString(RUNNING_HISTORY)

                if (!jsonItem.isNullOrEmpty()) {

                    val runHistory = Gson().fromJson<HistoryRun>(jsonItem, HistoryRun::class.java)

                    bDistance.set(getString(R.string.text_nkm).replace("%", ((runHistory.distance
                            ?: 0.0) / 1000).cutExactly2Num()))

                    bDuration.set((runHistory.duration ?: 0).timeFromSecondtoHourMinute("%dh %dm"))

                    var calories = runHistory.calories

                    var stringParent = ""

                    if ((calories ?: 0.0) > 1000) {

                        calories = (calories ?: 0.0) / 1000

                        stringParent = getString(R.string.text_nkkal)

                    } else {

                        stringParent = getString(R.string.text_nkal)

                    }

                    bCalories.set(stringParent.replace("%s", (calories ?: 0.0).cutExactly2Num()))

                    val route = runHistory.long_lat

                    if (!route.isNullOrEmpty()) {

                        start(route ?: "")

                    }
                }
            }

        }

        mViewDataBinding.mListener = this@RunningDetailsFragment

        setupMapview(savedInstanceState)

        return mViewDataBinding.root

    }

    private fun drawTrack() {
        val polyLineWidth = 6

        var polyline: Polyline? = null
        mViewModel.apply {
            for (index in 0..listLatLng.lastIndex) {
                if (index == 0) {
                    putMarker(listLocations[index]!!, R.drawable.ic_start_user_position)
                    zoomMapTo(listLocations[index]!!)
                    if (listLocations.size > 1)
                        fixZoom()
                }

                if (listLatLng.size > 1) {
                    if (index != listLatLng.lastIndex) {
                        if (polyline == null) {
                            if (listLatLng[index] != null && listLatLng[index.plus(1)] != null)
                                if (index != listLatLng.lastIndex)
                                    polyline = mGoogleMap?.addPolyline(PolylineOptions()
                                            .add(listLatLng[index], listLatLng[index.plus(1)])
                                            .width(polyLineWidth.toFloat()).color(ContextCompat.getColor(requireContext(), R.color.colorBlueRadioCircle)).geodesic(true))
                        } else {
                            if (listLatLng[index] != null) {
                                val points = polyline?.points

                                points?.add(listLatLng[index])

                                polyline?.points = points
                            } else {
                                polyline = null
                            }

                        }
                    } else {
                        if (listLocations[index] != null)
                            putMarker(listLocations[index]!!, R.drawable.ic_finish)
                    }

                }
            }
        }

    }

    private fun setupMapview(savedInstanceState: Bundle?) {
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }
        mViewDataBinding.mapview.apply {
            onCreate(mapViewBundle)
            getMapAsync(this@RunningDetailsFragment)
        }
    }

    private fun zoomMapTo(location: android.location.Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
    }

    override fun onStart() {
        super.onStart()
        mViewDataBinding.mapview.onStart()
    }

    override fun onStop() {
        super.onStop()
        mViewDataBinding.mapview.onStop()
    }

    override fun onResume() {
        super.onResume()
        mViewDataBinding.mapview.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewDataBinding.mapview.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        mViewDataBinding.mapview.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mViewDataBinding.mapview.onLowMemory()
    }

    override fun onMapReady(map: GoogleMap?) {
        map?.clear()

        mGoogleMap = map

        setupMap()

        drawTrack()
    }

    private fun fixZoom() {

        val latLngBounds = LatLngBounds.Builder()

        mViewModel.listLatLng.forEach {
            if (it != null)
                latLngBounds.include(it)
        }
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 100))
    }

    private fun putMarker(location: android.location.Location, resId: Int) {
        val userPositionMarkerBitmapDescriptor = resId
                .bitmapDescriptorFromVector(requireContext())

        val latLng = LatLng(location.latitude, location.longitude)

        mGoogleMap?.addMarker(MarkerOptions()
                .position(latLng)
                .flat(true)
                .anchor(0.3f, 0.3f)
                .icon(userPositionMarkerBitmapDescriptor))
    }


    private fun setupMap() {
        mGoogleMap?.apply {
            uiSettings.isMyLocationButtonEnabled = false

            if (requireContext().checkLocationPermission()) {
                isMyLocationEnabled = false
            }
        }
    }


    override fun onClickTest(text: String) {
        // TODO if you have a toast extension, you can replace this
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }


    // TODO import obtainViewModel & add RunningDetailsViewModel to ViewModelFactory
    fun obtainViewModel(): RunningDetailsViewModel = obtainViewModel(RunningDetailsViewModel::class.java)

    companion object {
        val RUNNING_HISTORY = "runHistory"
        fun newInstance(jsonRunHistory: String) = RunningDetailsFragment().withArgs {
            putString(RUNNING_HISTORY, jsonRunHistory)
        }

    }

}
