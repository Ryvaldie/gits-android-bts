package id.otsuka.gits.sweatgen.data.model

import android.location.Location
import java.io.Serializable

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class SavedVirtualRun(val id: Int = 0,
                           val mDistance: Double = 0.0,
                           val mStep: Int = 0,
                           val mCalories: Double = 0.0,
                           val mDuration: Long = 0,
                           val route: List<Location?> = ArrayList()
) : Serializable