package id.otsuka.gits.sweatgen.mvvm.main.newsfeed;


interface NewsFeedUserActionListener {

    // Example
    // fun onClickItem()

    fun itemOnClick(newsId: Int)

    /*
    *0->see all headline
    * 1 -> see all regular news
    * */
    fun seeAll()

}