package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.howtogetthere

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Location
import id.otsuka.gits.sweatgen.databinding.CustomPopupPlacesItemBinding

class RecyclerviewAdapter(var locations: List<Location>,
                          val listener: onClickItemListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun replacePlaces(places: List<Location>) {
        this.locations = places
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as MyViewHolder).bind(locations[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = CustomPopupPlacesItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(view)
    }

    interface onClickItemListener {
        fun onItemSelected(location: Location)
    }

    class MyViewHolder(val mView: CustomPopupPlacesItemBinding) : RecyclerView.ViewHolder(mView.root) {

        fun bind(location: Location, listener: onClickItemListener) {
            mView.location = location
            mView.mListener = listener
        }
    }

    override fun getItemCount(): Int = locations.size
}