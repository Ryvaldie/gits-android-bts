
package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail;

import id.otsuka.gits.sweatgen.data.model.Challenge


interface ChallengeDetailUserActionListener {

    // Example
    // fun onClickItem()
    fun startChallenge(challenge: Challenge?)

    fun toLeaderboard()

    fun showInfo()
    fun toRunHistory()

}