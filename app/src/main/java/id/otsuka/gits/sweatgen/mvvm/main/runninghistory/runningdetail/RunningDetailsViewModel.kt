package id.otsuka.gits.sweatgen.mvvm.main.runninghistory.runningdetail;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import id.otsuka.gits.sweatgen.util.convertToLatLng
import id.otsuka.gits.sweatgen.util.parseStringToLocations


class RunningDetailsViewModel(context: Application) : AndroidViewModel(context) {
    val listLocations = ObservableArrayList<Location?>()
    val listLatLng = ObservableArrayList<LatLng?>()
    val bDistance = ObservableField("")
    val bCalories = ObservableField("")
    val bDuration = ObservableField("")

    fun start(jsonLocation: String) {
        listLocations.apply {
            clear()
            addAll(jsonLocation.parseStringToLocations()
            )
        }
        listLatLng.apply {
            clear()
            addAll(listLocations.convertToLatLng())
        }
    }

}