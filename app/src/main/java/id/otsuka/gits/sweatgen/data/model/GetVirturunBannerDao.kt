package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */
data class GetVirturunBannerDao(val banner_event: String? = null,
                                val guide_content: List<String>? = null,
                                val guide_contents: List<RaceCentralGuide>
)