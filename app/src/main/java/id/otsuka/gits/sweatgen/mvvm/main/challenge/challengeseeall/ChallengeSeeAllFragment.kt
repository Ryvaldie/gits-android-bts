package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentChallengeSeeAllBinding
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengedetail.ChallengeDetailActivity
import id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall.ChallengeSeeAllActivity.Companion.CHALLENGE_TYPE

class ChallengeSeeAllFragment : Fragment(), ChallengeSeeAllUserActionListener {

    lateinit var mViewDataBinding: FragmentChallengeSeeAllBinding
    lateinit var mViewModel: ChallengeSeeAllViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentChallengeSeeAllBinding.inflate(inflater, container!!, false)

        mViewModel = (requireActivity() as ChallengeSeeAllActivity).obtainViewModel()


        mViewModel.challengeType = requireActivity().intent.getIntExtra(CHALLENGE_TYPE, 0)

        mViewDataBinding.mViewModel = mViewModel.apply {
            loadChallenges()
        }

        mViewDataBinding.mListener = this@ChallengeSeeAllFragment

        return mViewDataBinding.root

    }

    override fun onItemClick(challengeId: Int) {
        ChallengeDetailActivity.startActivity(challengeId, requireContext())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecycler()
    }

    private fun setupRecycler() {
        with(mViewDataBinding) {
            recAllChalenges.apply {
                adapter = ChallengeSeeAllAdapter(requireContext(), ArrayList(), this@ChallengeSeeAllFragment.mViewModel, this@ChallengeSeeAllFragment)
                layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            }
        }
    }


    companion object {
        fun newInstance() = ChallengeSeeAllFragment().apply {

        }

    }

}
