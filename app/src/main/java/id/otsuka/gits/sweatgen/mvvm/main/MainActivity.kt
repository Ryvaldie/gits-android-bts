package id.otsuka.gits.sweatgen.mvvm.main

import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ObservableArrayList
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.crashlytics.android.Crashlytics
import com.google.android.gms.maps.MapsInitializer
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.databinding.ActivityMainBinding
import id.otsuka.gits.sweatgen.mvvm.main.challenge.ChallengeFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.EventFragment
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsFeedFragment
import id.otsuka.gits.sweatgen.mvvm.main.profile.ProfileFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity.Companion.REQ_TO_RUN_TRACKER
import id.otsuka.gits.sweatgen.mvvm.main.search.SearchFragment
import id.otsuka.gits.sweatgen.mvvm.main.search.SearchFragment.Companion.IS_NEWSFEED
import id.otsuka.gits.sweatgen.util.customView.NotifyUpdateAppsDialogFragment
import id.otsuka.gits.sweatgen.util.disableShiftMode
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.showCustomSnackBar
import id.otsuka.gits.sweatgen.util.showImageDialog
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), MainNavigator, MainUserActionListener {

    var mViewDataBinding: ActivityMainBinding? = null
    val resultChallengeList = ObservableArrayList<Challenge>()
    val resultEventList = ObservableArrayList<Event>()
    lateinit var mViewModel: MainViewModel
    private val mFragmentList = ArrayList<Fragment>()
    private lateinit var mSelectedFragment: Fragment

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        edt_search.clearFocus()
        logo.animate().alpha(1.0f).duration = 800
        logo.visibility = View.VISIBLE
        btn_back.animate().alpha(0.0f).duration = 800
        btn_back.visibility = View.GONE
        if (mViewModel.bFocusable.get()) {
            mViewModel.bFocusable.set(false)
            edt_search.setText("")
        }
        when (item.itemId) {
            R.id.nav_event -> {
                mViewModel.saveStateEvent.value = 0
                mViewModel.loadLayout.set(true)
                changeFragment(mFragmentList[0])
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_run_track -> {
                RunTrackerActivity.startActivityForFreeRun(this, 1)
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_newsfeed -> {
                mViewModel.saveStateEvent.value = 3
                mViewModel.loadLayout.set(true)
                changeFragment(mFragmentList[1])
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_challange -> {
                mViewModel.saveStateEvent.value = 1
                mViewModel.loadLayout.set(true)
                changeFragment(mFragmentList[2])
                return@OnNavigationItemSelectedListener true
            }

            R.id.nav_profile -> {
                mViewModel.saveStateEvent.value = 2
                mViewModel.loadLayout.set(true)
                changeFragment(mFragmentList[3])
                return@OnNavigationItemSelectedListener true
            }
            else -> {
                return@OnNavigationItemSelectedListener false
            }
        }
    }

    private fun observeCheckVersion() {
        mViewModel.showUpdateAppsDialog.observe(this, android.arch.lifecycle.Observer {
            showImageDialog(NotifyUpdateAppsDialogFragment())
        })
    }


    fun checkAppVersion() {
        mViewModel.getLatestAppsVersion()
    }


    override fun onResume() {
        super.onResume()

        if (mViewDataBinding != null) {
            val idItem = when (mViewModel.saveStateEvent.value) {
                0 -> R.id.nav_event
                1 -> R.id.nav_challange
                2 -> R.id.nav_profile
                3 -> R.id.nav_newsfeed
                else -> 0
            }
            mViewDataBinding!!.bnavMain.selectedItemId = idItem
            mOnNavigationItemSelectedListener.onNavigationItemSelected(mViewDataBinding!!.bnavMain.menu.findItem(idItem))
        }

    }

    override fun onBackPressed() {
        logo.animate().alpha(1.0f).duration = 800
        logo.visibility = View.VISIBLE
        btn_back.animate().alpha(0.0f).duration = 800
        btn_back.visibility = View.GONE

        if (mSelectedFragment is SearchFragment) {

            (mFragmentList[4] as SearchFragment).mViewModel.apply {
                listAllNews.clear()
                mThread.interrupt()
            }

            edt_search.setText("")
            edt_search.clearFocus()

            mViewModel.bFocusable.set(false)

            changeFragment(mFragmentList[when (mViewModel.saveStateEvent.value ?: 0) {
                1 -> 2
                3 -> 1
                else -> 0
            }])

        } else {
            if (mViewModel.saveStateEvent.value ?: 0 > 0) {
                if (mViewModel.bFocusable.get()) {
                    mViewModel.bFocusable.set(false)

                    edt_search.setText("")
                    edt_search.clearFocus()

                    val index = when (mViewModel.saveStateEvent.value ?: 0) {
                        0 -> 0
                        1 -> 2
                        3 -> {
                            1
                        }
                        else -> 3
                    }
                    changeFragment(mFragmentList[index])
                } else {
                    bnav_main.selectedItemId = R.id.nav_event
                }
            } else {
                if (mViewModel.bFocusable.get()) {
                    mViewModel.bFocusable.set(false)
                    edt_search.setText("")
                    edt_search.clearFocus()
                    val index = when (mViewModel.saveStateEvent.value ?: 0) {
                        0 -> 0
                        1 -> 2
                        3 -> 1
                        else -> 3
                    }
                    changeFragment(mFragmentList[index])
                } else {
                    finishAffinity()
                    super.onBackPressed()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Fabric.with(this, Crashlytics())

        mViewDataBinding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main).apply {
            mViewModel = obtainViewModel()
            mListener = this@MainActivity
        }

        mViewModel = obtainViewModel().apply {
            observeSearchField(mViewDataBinding!!.edtSearch)
        }

        observeCheckVersion()

        checkAppVersion()

        MapsInitializer.initialize(this)

        mFragmentList.apply {
            add(supportFragmentManager.findFragmentById(R.id.fragmentEvent) as EventFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentNewsfeed) as NewsFeedFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentChallenge) as ChallengeFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentProfile) as ProfileFragment)
            add(supportFragmentManager.findFragmentById(R.id.fragmentSearch) as SearchFragment)
        }

        setupToolbar()

        obtainViewModel().saveStateEvent.observe(this, Observer { it ->
            if (it == 2) {
                hideSearchBar()
            } else {
                showSearchBar(it)
            }
        })

        bnav_main.disableShiftMode()

        bnav_main.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        bnav_main.selectedItemId = R.id.nav_challange

        obtainViewModel().saveStateEvent.value = 1

        btn_back.setOnClickListener {
            onBackPressed()
        }

        changeFragment(mFragmentList[2])

    }


    private fun changeFragment(fragment: Fragment, bundle: Bundle? = null) {

        mSelectedFragment = fragment

        fragment.apply {
            if (bundle != null)
                arguments = bundle
            onResume()
        }
        val transaction = supportFragmentManager.beginTransaction()
        for (f in mFragmentList) {
            transaction.hide(f)
        }
        transaction.show(fragment)
        super.onPostResume()
        transaction.commit()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.apply {
            setDisplayShowTitleEnabled(false)
        }


    }

    fun showSearchBar(position: Int?) {
        relative_search_bar.visibility = View.VISIBLE
        edt_search.hint = when (position) {
            1 -> getString(R.string.text_search_your_challenge)
            3 -> getString(R.string.text_search_your_newsfeed)
            else -> getString(R.string.text_search_your_event)

        }
    }

    override fun onTryAgain() {
        hideNoInetConnection()
        val index = when (obtainViewModel().saveStateEvent.value ?: 0) {
            0 -> 0
            1 -> 2
            3 -> 1
            else -> 3
        }
//        changeFragment(mFragmentList[index])
        if (mFragmentList[index].isVisible) {
            when (index) {
                0 -> {
                    val mViewModel = (mFragmentList[index] as EventFragment).mViewModel
                    mViewModel.getPocariEventList(0)
                    mViewModel.getPocariEventList(1)
                }
                1 -> {
                    val mViewModel = (mFragmentList[index] as NewsFeedFragment).mViewModel
                    mViewModel.start()
                }
                2 -> {
                    val mViewModel = (mFragmentList[index] as ChallengeFragment).mViewModel
                    mViewModel.loadData()
                }
                else -> {
                    val mViewModel = (mFragmentList[index] as ProfileFragment).mViewModel
                    mViewModel.showUser()
                    mViewModel.getRunHistories()
                }
            }
        }
    }

    fun showNoInetConnection() {
        obtainViewModel().bNoInet.set(true)
    }

    private fun hideNoInetConnection() {
        obtainViewModel().bNoInet.set(false)
    }

    private fun hideSearchBar() {
        relative_search_bar.visibility = View.INVISIBLE
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_TO_RUN_TRACKER) {
                Handler().postDelayed({
                    try {
                        obtainViewModel().saveStateEvent.value = 2
                        bnav_main.selectedItemId = R.id.nav_profile
                        (mFragmentList[3] as ProfileFragment).refreshData()
                        changeFragment(mFragmentList[3])
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, 500)

                data?.getStringExtra("message")!!.showCustomSnackBar(rellayout_main, color = ContextCompat.getColor(this, R.color.colorGpsActivated))

            }
        }
    }

    // private fun search
    override fun onSearchClick() {

        if (!mViewModel.bFocusable.get()) {
            mViewModel.bFocusable.set(true)
            logo.animate().alpha(0.0f).duration = 800
            logo.visibility = View.GONE
            btn_back.visibility = View.VISIBLE
            btn_back.animate().alpha(1.0f).duration = 800

            changeFragment(mFragmentList[4], Bundle().apply { putBoolean(IS_NEWSFEED, mViewModel.saveStateEvent.value == 3) })
        }

    }


    fun obtainViewModel(): MainViewModel = obtainViewModel(MainViewModel::class.java)

    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))

            (context as AppCompatActivity).overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)

        }
    }
}
