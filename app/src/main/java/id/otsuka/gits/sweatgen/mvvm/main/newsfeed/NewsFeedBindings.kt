package id.otsuka.gits.sweatgen.mvvm.main.newsfeed;

import android.databinding.BindingAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView

/**
 * @author radhikayusuf.
 */

object NewsFeedBindings {

    @BindingAdapter("app:headlines")
    @JvmStatic
    fun setupHeadlines(viewPager: ViewPager, data: List<NewsItemModel>) {
        (viewPager.adapter as HeadlineAdapter).apply {
            replaceData(data)
        }
    }

    @BindingAdapter("app:news")
    @JvmStatic
    fun setupNews(recyclerView: RecyclerView, data: List<NewsItemModel>) {
        (recyclerView.adapter as NewsAdapter).apply {
            replaceData(data)
        }
    }
}
