package id.otsuka.gits.sweatgen.mvvm.main.search;

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel

/**
 * @author radhikayusuf.
 */

object SearchBindings {

    @BindingAdapter("app:filterednews")
    @JvmStatic
    fun setNewsList(recyclerView: RecyclerView, data: List<NewsItemModel>) {
        (recyclerView.adapter as SearchNewsAdapter).apply {
            replaceData(data)
        }
    }

}