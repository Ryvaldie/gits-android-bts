package id.otsuka.gits.sweatgen.mvvm.main.leaderboard;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.*
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.leaderboard.LeaderBoardActivity.Companion.CHALLENGE_TYPE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent

class LeaderBoardViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bDaysLeft = ObservableField("-")

    var leaderboardType = -1

    val bCurrentProgress = ObservableField("")

    val bTotalProgress = ObservableField("")

    val leaderBoardList: ObservableList<LeaderBoardModel> = ObservableArrayList()

    val bProgress = ObservableInt(0)

    val isRequesting = ObservableBoolean(false)

    var challengeId = 0

    val isContinues = ObservableBoolean(false)

    val snackbarMessage = SingleLiveEvent<String>()

    // TODO if template have an error, please reimport SingleLiveEvent
    val eventClickItem = SingleLiveEvent<LeaderBoardModel>()

    fun loadData() {
        isRequesting.set(true)

        if (leaderboardType == CHALLENGE_TYPE)

            repository.getLeaderboard("", challengeId, object : GitsDataSource.GetLeaderboardCallback {
                override fun onDataNotAvailable() {
                    isRequesting.set(false)
                }

                override fun onError(errorMessage: String?) {
                    isRequesting.set(false)
                    snackbarMessage.value = errorMessage
                }

                override fun onLoaded(users: List<LeaderBoardModel>) {
                    leaderBoardList.apply {
                        clear()
                        addAll(users)
                    }
                    isRequesting.set(false)
                }
            })
        else
            repository.getVirturunLeaderboard("", challengeId, object : GitsDataSource.GetLeaderboardCallback {
                override fun onDataNotAvailable() {
                    isRequesting.set(false)
                }

                override fun onError(errorMessage: String?) {
                    isRequesting.set(false)
                    snackbarMessage.value = errorMessage
                }

                override fun onLoaded(users: List<LeaderBoardModel>) {
                    leaderBoardList.apply {
                        clear()
                        addAll(users)
                    }
                    isRequesting.set(false)
                }
            })

    }


}