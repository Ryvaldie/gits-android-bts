package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.rundata;


interface RunDataUserActionListener {

    fun onSelectedItem(styleCode: Int)

}