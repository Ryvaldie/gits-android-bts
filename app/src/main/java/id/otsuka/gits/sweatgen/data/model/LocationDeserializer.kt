package id.otsuka.gits.sweatgen.data.model

import android.location.Location
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type


/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class LocationDeserializer() : JsonDeserializer<Location> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Location {
        val jo = json?.asJsonObject
        val l = Location(jo?.getAsJsonPrimitive("mProvider")?.asString)
        l.accuracy = (jo?.getAsJsonPrimitive("mAccuracy")?.asFloat) ?: 0f
        // etc, getting and setting all the data
        return l
    }
}