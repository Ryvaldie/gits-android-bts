package id.otsuka.gits.sweatgen.data.model

data class Schedule(val start_time: String? = null,
                    val end_time: String? = null
)