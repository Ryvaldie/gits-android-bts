package id.otsuka.gits.sweatgen.data.model

data class GetEventListDao(val current_page: Int = 0,
                           val data: List<Event>? = null
)