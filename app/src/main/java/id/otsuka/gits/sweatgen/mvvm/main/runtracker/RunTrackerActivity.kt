package id.otsuka.gits.sweatgen.mvvm.main.runtracker

import android.app.Activity
import android.app.PendingIntent
import android.arch.lifecycle.Observer
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.gson.Gson
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.AddHistoryRunParams
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.databinding.ActivityRunTrackerBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.challengeresult.ChallengeResultFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.postingpage.PostingPageFragment
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.INTENT_FILTER_GPS
import id.otsuka.gits.sweatgen.util.Other.INTENT_LOCATION_CHANGED
import id.otsuka.gits.sweatgen.util.Other.INTENT_PERMISSION
import id.otsuka.gits.sweatgen.util.Other.INTENT_TIMER_TICK
import id.otsuka.gits.sweatgen.util.Other.KEY_TIME_MILLIS_TICKING
import id.otsuka.gits.sweatgen.util.Other.KEY_TIME_STRING_TICKING
import id.otsuka.gits.sweatgen.util.Other.KEY_UPDATE_LOCATION
import id.otsuka.gits.sweatgen.util.Other.TYPE_CHALLENGE
import id.otsuka.gits.sweatgen.util.Other.TYPE_VIRTURUN
import id.otsuka.gits.sweatgen.util.helper.GitsMapHelper
import kotlinx.android.synthetic.main.activity_run_tracker.*
import kotlinx.android.synthetic.main.custom_fake_gps_alert.view.*
import kotlinx.android.synthetic.main.custom_run_alert.view.*
import java.util.concurrent.TimeUnit


class RunTrackerActivity : AppCompatActivity(), RunTrackerNavigator, OnMapReadyCallback, GitsMapHelper.GitsMapCallback {

    lateinit var mViewModel: RunTrackerViewModel

    lateinit var mapFragment: SupportMapFragment

    lateinit var mGitsMapHelper: GitsMapHelper

    lateinit var myLocationUpdateService: LocationUpdateService

    val TAG = RunTrackerActivity::class.java.simpleName

    private val mAlertDialog by lazy { buildDialog("RUNALERT") }

    private val mFakeGpsAlertDialog by lazy { buildDialog("FAKEGPS") }

    private val mGeneralAlert by lazy {
        buildGeneralDialog(R.string.text_location_permission_required, R.string.text_location_message, PERMISSION_TYPE)
    }

    private val mGpsAlert by lazy {
        buildGeneralDialog(R.string.text_location_service_required, R.string.text_loc_service_msg, GPS_TYPE)
    }

    private lateinit var mViewDataBinding: ActivityRunTrackerBinding

    private var challengeString = ""

    /*
    * this broadcast receiver is used to recieve message/data from the 'LocationUpdateService"
    * */
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {

            when (intent?.action) {

                INTENT_FILTER_GPS -> {
                    triggerGpsResolutionDialog(intent)
                }

                Other.INTENT_PERMISSION -> {
                    mGitsMapHelper.requestPermission()
                }

                INTENT_TIMER_TICK -> {
                    mViewModel.bTime.set(intent.getStringExtra(KEY_TIME_STRING_TICKING))

                    mViewModel.timeInMillis.set(intent.getLongExtra(KEY_TIME_MILLIS_TICKING, 0))
                }

                INTENT_LOCATION_CHANGED -> {
                    onReceivedNewLocationFromService(intent)
                }
            }
        }
    }

    private fun onReceivedNewLocationFromService(intent: Intent?) {
        val newLocation = intent?.getParcelableExtra<Location>(KEY_UPDATE_LOCATION)

        if (newLocation != null) {
            if (newLocation.isFromMockProvider) {
                if (!(this@RunTrackerActivity).isFinishing)
                    mFakeGpsAlertDialog.show()
            } else {
                if (mViewModel.obsLocations.isEmpty()) {

                    mGitsMapHelper.updateLocation(newLocation, true)

                } else {
                    if (mViewModel.obsBtnState.get()!!) {
                        mGitsMapHelper.updateLocation(newLocation, true)
                    } else {
                        mGitsMapHelper.updateLocation(newLocation, false)
                    }
                }
            }
        }
    }

    /*
    * this method works for triggering the 'enabling gps'
    * */
    private fun triggerGpsResolutionDialog(intent: Intent?) {

        val pendingIntent = intent?.getParcelableExtra(Other.RESOLUTION_ACTIVATE_GPS) as PendingIntent

        startIntentSenderForResult(
                pendingIntent.intentSender,
                RESOLUTION_GPS_REQUEST_CODE,
                null,
                0,
                0,
                0)
    }

    override fun startService() {
        startLocationUpdateService()
    }

    override fun showGeneralAlert() {
        mGeneralAlert.show()
    }

    override fun calculateData() {
        calculateDistance()

        calculateSteps()

        calculateCalories()
    }

    private fun calculateDistance() {
        with(mViewModel.obsLocations) {

            if (this[lastIndex.minus(1)] != null && last() != null) {

                val prevDistance = mViewModel.bDoubleDistance.get()

                val newDistance = this[lastIndex.minus(1)]!!.distanceTo(last()).div(1000)

                mViewModel.bDoubleDistance.set(prevDistance?.plus(newDistance))

                mViewModel.bDistance.set((mViewModel.bDoubleDistance.get() ?: 0f).cutExactly2Num())
            }
        }
    }

    private fun calculateSteps() {
        val steps = mViewModel.bDoubleDistance.get()!!.times(1000).times(3.2808)
        mViewModel.bSteps.set(Math.round(steps).toString())
    }

    private fun calculateCalories() {
        val calories = mViewModel.bDoubleDistance.get()!!.times(30)
        mViewModel.bDoubleCalories.set(calories)
        mViewModel.bCalories.set(calories.cutExactly1Num())
    }

    private fun registerMessageReceiver() {
        val intentFilter = IntentFilter()

        intentFilter.addAction(INTENT_FILTER_GPS)
        intentFilter.addAction(INTENT_PERMISSION)
        intentFilter.addAction(INTENT_LOCATION_CHANGED)
        intentFilter.addAction(INTENT_TIMER_TICK)

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, intentFilter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        initVariables()

        observeEvent()

        mViewDataBinding = DataBindingUtil.setContentView<ActivityRunTrackerBinding>(this, R.layout.activity_run_tracker).apply {
            mViewModel = obtainViewModel().apply {
                runningType.set(intent.getIntExtra(RUN_TYPE, 0))
            }
            mListener = object : RunTrackerUserActionListener {
                override fun onStartPauseBtnClick(state: Boolean) {
                    startOrPauseRun(state)
                }

                override fun changeTrackerMode(mode: Boolean) {
                    this@RunTrackerActivity.mViewModel.trackerMode.set(mode)
                }

                override fun onFinish() {
                    if (this@RunTrackerActivity.mViewModel.runningType.get() == 1)
                        finishRun()
                    else {
                        finishChallenge()
                    }

                }
            }
        }

        setupMapFragment()

        setupNavigationClick()

        mViewModel.start()
    }

    private fun initVariables() {
        mViewModel = obtainViewModel()
    }

    private fun finishChallenge() {

        challengeString = intent.getStringExtra(CHALLENGE_JSON)

        val challenge = Gson().fromJson<Challenge>(challengeString, Challenge::class.java)

        if (challenge != null) {
            buildParams(challenge)

            if (challenge.is_continues != null) {
                if (challenge.is_continues != 0) {
                    validateContinuesRun(challenge)
                } else {
                    validateSingleRun(challenge)
                }
            } else {
                validateSingleRun(challenge)
            }
        }
    }

    private fun validateContinuesRun(challenge: Challenge) {
        mViewModel.bCompleted.set(isFinishedContinuesChallenge(challenge.distance_sum
                ?: 0.0, challenge.distance_total ?: 0.0, challenge.duration_sum
                ?: 0, challenge.duration))

        if (mViewModel.bCompleted.get()) {
            mViewModel.postRunHistory(true)
        } else
            mViewModel.postRunProgress(false)

    }

    private fun validateSingleRun(challenge: Challenge) {
        mViewModel.bCompleted.set(validateChallenge(challenge.duration, challenge.distance))

        if (mViewModel.bCompleted.get()) {
            mViewModel.postRunHistory(false)
        } else {
            mViewModel.redirectResultPage.call()
        }
    }

    private fun isFinishedContinuesChallenge(lastDistance: Double, challDistance: Double, lastDuration: Long, challDuration: Int): Boolean {
        val mDistance = mViewModel.bDoubleDistance.get()!!.times(1000) + lastDistance
        val mDuration = TimeUnit.MILLISECONDS.toSeconds(mViewModel.timeInMillis.get()
                ?: 0) + lastDuration
        return (mDistance >= challDistance)
    }

    private fun validateChallenge(challDuration: Int, challDistance: Double): Boolean {

        val mDuration = TimeUnit.MILLISECONDS.toSeconds(mViewModel.timeInMillis.get() ?: 0)

        val mDistance = mViewModel.bDoubleDistance.get()!!.times(1000)

        if (challDuration != 0 && challDistance != 0.0 || challDuration != 0 && challDistance == 0.0) {
            return (mDuration < challDuration && mDistance >= challDistance)
        } else if (challDuration == 0 && challDistance != 0.0) {
            return (mDistance >= challDistance)
        } else {
            return true
        }
    }

    private fun buildParams(challenge: Challenge) {
        val runHistory = AddHistoryRunParams(
                challenge.id,
                activity_name = challenge.name,
                distance = mViewModel.bDoubleDistance.get()!!.times(1000).toDouble(),
                duration = TimeUnit.MILLISECONDS.toSeconds(mViewModel.timeInMillis.get()
                        ?: 0).toInt(),
                step = mViewModel.bSteps.get()?.toDouble() ?: 0.0,
                calories = mViewModel.bDoubleCalories.get().toDouble(),
                point = challenge.point,
                type_run = if (intent.getIntExtra(RUN_TYPE, 0) == 2) TYPE_CHALLENGE else TYPE_VIRTURUN,
                long_lat = ArrayList(mViewModel.obsLocations).parseLatLong(),
                virturunId = challenge.id
        )
        mViewModel.obsAddRunHistory.set(runHistory)
    }

    private fun setupNavigationClick() {
        card_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun finishRun() {
        replaceFragmentAndAddToBackStack(PostingPageFragment.newInstance(), R.id.frame_main_content)
    }

    private fun startOrPauseRun(state: Boolean) {

        if (mViewModel.obsStartFragmentVisibility.get() == 1)
            mViewModel.trackerMode.set(true)

        mViewModel.obsStartFragmentVisibility.set(0)

        mViewModel.obsBtnState.set(state)

        mViewModel.startPauseEvent.value = state

    }

    private fun observeEvent() {

        mViewModel.loadingEvent.observe(this, android.arch.lifecycle.Observer {
            if (it!!) {
                showLoading()
            } else {
                dismissLoading()
            }
        })

        mViewModel.snackbarMessage.observe(this, android.arch.lifecycle.Observer {
            if (it == Other.TOKEN_EXPIRE) {
                mViewModel.repository.logout(null, null, true)
                redirectToLogin()
            } else {
                (it ?: "-").showSnackBar(mViewDataBinding.root)
            }
        })

        mViewModel.redirectToChallengeDetail.observe(this, Observer {
            stopService()
            val message = Intent()
            message.putExtra("message", getString(R.string.text_challenge_finished))
            setResult(Activity.RESULT_OK, message)
            finish()
        })

        mViewModel.redirectResultPage.observe(this, android.arch.lifecycle.Observer {
            replaceFragmentInActivity(ChallengeResultFragment.newInstance(mViewModel.bCompleted.get()), R.id.frame_main_content)
        })
    }


    fun obtainViewModel(): RunTrackerViewModel = obtainViewModel(RunTrackerViewModel::class.java)

    private fun setupMapFragment() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment_map_mode) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }


    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            mGitsMapHelper = GitsMapHelper(googleMap, mViewModel, this, this)

            mGitsMapHelper.setupMap()

            if (mViewModel.obsLocations.size > 1) {
                mGitsMapHelper.drawRoute()
            }

            registerMessageReceiver()
        }
    }

    override fun onBackPressed() {
        val fragment = if (mViewModel.runningType.get() == 1)
            supportFragmentManager.findFragmentByTag(PostingPageFragment::class.java.simpleName)
        else
            supportFragmentManager.findFragmentByTag(ChallengeResultFragment::class.java.simpleName)

        if (fragment != null) {
            if (fragment is PostingPageFragment) {
                supportFragmentManager.popBackStack()
            } else {

            }

        } else {
            if (mViewModel.obsStartFragmentVisibility.get() == 1) {
                super.onBackPressed()
                stopService()
            } else {
                mAlertDialog.show()
            }
        }
    }

    fun stopService() {
        val intent = Intent(applicationContext, LocationUpdateService::class.java)
        stopService(intent)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        mGitsMapHelper.onRequestPermissionResult(requestCode, permissions, grantResults)
    }

    private fun buildGeneralDialog(title: Int, message: Int, type: Int): AlertDialog {

        val builder = AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
        builder.setTitle(getString(title))
        builder.setMessage(getString(message))
        builder.setPositiveButton(getString(R.string.text_go_settings)) { dialog, which ->

            val intent: Intent

            if (type == 0) {
                intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
            } else {
                intent = Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            }

            startActivityForResult(intent, 3110)
        }

        builder.setCancelable(false)

        return builder.create()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESOLUTION_GPS_REQUEST_CODE) {
            mGeneralAlert.dismiss()
            recreate()
        } else {
            when (resultCode) {
                Activity.RESULT_OK -> {
                }
                Activity.RESULT_CANCELED -> {
                    mGpsAlert.show()
                }
            }
        }

    }


    private fun startLocationUpdateService() {

        myLocationUpdateService = LocationUpdateService()

        val intent = Intent(applicationContext, LocationUpdateService::class.java)

        intent.putExtra(RUN_TYPE, mViewModel.runningType.get())

        intent.putExtra(CHALLENGE_JSON, challengeString)

        startService(intent)
    }


    private fun buildDialog(type: String): AlertDialog {
        val dialog = AlertDialog.Builder(this@RunTrackerActivity).create()

        val inflater = layoutInflater

        if (type == "RUNALERT") {
            val alertView = inflater.inflate(R.layout.custom_run_alert, null)
            with(alertView) {

                tview_yes.setOnClickListener {
                    startOrPauseRun(false)

                    dialog.dismiss()

                    if (obtainViewModel().runningType.get() == 1)
                        finishRun()
                    else
                        finishChallenge()
                }

                tview_no.setOnClickListener { dialog.dismiss() }

            }

            dialog.setView(alertView)

            dialog.setCancelable(false)
        } else {

            val alertView = inflater.inflate(R.layout.custom_fake_gps_alert, null)

            alertView.tview_okay.setOnClickListener {

                dialog.dismiss()

                finish()
            }

            dialog.setView(alertView)

            dialog.setCancelable(false)
        }

        return dialog
    }


    companion object {
        const val REQ_TO_RUN_TRACKER = 7031
        const val PERMISSION_TYPE = 0
        const val GPS_TYPE = 1
        const val CHALLENGE_JSON = "challenge"
        const val RUN_TYPE = "runType"
        const val RESOLUTION_GPS_REQUEST_CODE = 911

        fun startActivityForChallenge(context: Context, runType: Int, challenge: String) {
            (context as Activity).startActivityForResult(Intent(context, RunTrackerActivity::class.java)
                    .putExtra(RUN_TYPE, runType)
                    .putExtra(CHALLENGE_JSON, challenge), REQ_TO_RUN_TRACKER)
        }

        fun startActivityForFreeRun(context: Context, runType: Int) {
            (context as Activity).startActivityForResult(Intent(context, RunTrackerActivity::class.java).putExtra(RUN_TYPE, runType)
                    , REQ_TO_RUN_TRACKER)
        }
    }
}
