package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.startvirtualrun;


interface StartVirtualRunUserActionListener {

    fun onClickTest(text: String)

    fun toRunningHistory()

    fun toLeaderBoard()

    fun startRun()

    fun info()

}