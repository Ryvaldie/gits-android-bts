package id.otsuka.gits.sweatgen.data.model

data class ForgotPasswordDao (
        val email: String? = null,
        val forgot_token: String? = null,
        val name: String? = null
)