package id.otsuka.gits.sweatgen.data.model

data class Contact(val id: Int = 0,
                   val event_id: Int = 0,
                   val website: String? = null,
                   val email: String? = null,
                   val phone: String? = null
)