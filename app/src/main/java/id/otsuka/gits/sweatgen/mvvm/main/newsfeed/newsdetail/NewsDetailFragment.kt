package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail


import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentNewsDetailBinding
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail.NewsDetailActivity.Companion.NEWS_ID
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews.SeeAllNewsActivity
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.dpToPx
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.showSnackBar

class NewsDetailFragment : Fragment(), NewsDetailUserActionListener {

    lateinit var mViewDataBinding: FragmentNewsDetailBinding
    lateinit var mViewModel: NewsDetailViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentNewsDetailBinding.inflate(inflater, container, false)

        mViewModel = obtainVm()

        mViewDataBinding.mViewModel = mViewModel.apply {
            newsId = requireActivity().intent.getIntExtra(NEWS_ID, 0)
            start()
        }

        mViewDataBinding.mListener = this@NewsDetailFragment

        setupRecycler()
        setupToolbar()

        return mViewDataBinding.root

    }


    fun setupRecycler() {
        mViewDataBinding.recyclerSimilar.apply {
            adapter = SimilarNewsAdapter(ArrayList(0), this@NewsDetailFragment)
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
    }

    fun setupToolbar() {
        mViewDataBinding.toolbarNewsDetail.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    override fun itemOnClick(newsId: Int) {
        NewsDetailActivity.startActivity(newsId, requireContext())
    }

    override fun seeAll() {

        val catId = mViewModel.newsId

        SeeAllNewsActivity.startActivity(catId, requireContext())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.showMessage.observe(this, Observer {
            when (it) {
                TOKEN_EXPIRE -> {
                    mViewModel.repository.logout(null, null, true)
                    activity!!.redirectToLogin()
                }
                else -> {
                    (it ?: "-").showSnackBar(mViewDataBinding.root)
                }
            }
        })
    }

    fun obtainVm(): NewsDetailViewModel = obtainViewModel(NewsDetailViewModel::class.java)

    companion object {
        fun newInstance() = NewsDetailFragment().apply {

        }

    }

}
