package id.otsuka.gits.sweatgen.mvvm.main.event

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.util.customView.CarouselLinearLayout

class ViewPagerAdapter(val context: Context,
                       fragmentManager: FragmentManager,
                       var pagesContent: List<Event>,
                       val vm: EventViewModel
) : FragmentPagerAdapter(fragmentManager), ViewPager.PageTransformer {

    val LOOP_SIZE = 1000

    var firstPage = 0

    private var scale = 0f

    private var mCount = 0

    companion object {
        val BIG_SCALE = 1.0f
        val SMALL_SCALE = 0.9f
        val DIFF_SCALE = BIG_SCALE - SMALL_SCALE
    }

    fun replacePagesContent(pagesContent: List<Event>) {

        this.pagesContent = pagesContent

        this.mCount = pagesContent.size

        if (this.mCount > 1)
            this.firstPage = (this.mCount * LOOP_SIZE) / 2

        notifyDataSetChanged()

        vm.firstPageEvent.value = this@ViewPagerAdapter.firstPage
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getItem(position: Int): Fragment {
        val pos = position % this.mCount

        try {
            scale = if (position == firstPage) {
                BIG_SCALE
            } else SMALL_SCALE

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return (EventSliderItemFragment.newInstance(pagesContent[pos], scale))
    }

    override fun getCount(): Int {
        var count = 0
        if (pagesContent.size > 1)
            try {
                count = this.mCount * LOOP_SIZE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        else
            count = pagesContent.size
        return count
    }

    override fun transformPage(page: View, position: Float) {
        var scale = BIG_SCALE

        scale = if (position > 0) {
            scale - position * DIFF_SCALE
        } else {
            scale + position * DIFF_SCALE
        }
        if (scale < 0) scale = 0f

        (page as CarouselLinearLayout).setScaleBoth(scale)

    }
}