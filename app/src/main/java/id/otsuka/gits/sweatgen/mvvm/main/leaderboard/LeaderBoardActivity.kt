package id.otsuka.gits.sweatgen.mvvm.main.leaderboard;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity
import kotlinx.android.synthetic.main.activity_leader_board.*


class LeaderBoardActivity : AppCompatActivity() {

    val listTitle = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leader_board)
        setupTitle()
        setupFragment()
        setupToolbar()
    }

    private fun setupTitle() {
        listTitle.addAll(listOf(getString(R.string.text_leaderboard_challenge), getString(R.string.text_leaderboard_virtual)))
    }

    private fun setupToolbar() {
        toolbar_leaderboard.title = listTitle[intent.getIntExtra(LEADERBOARD_TYPE, 0)]
        toolbar_leaderboard.setNavigationOnClickListener { super.onBackPressed() }
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        LeaderBoardFragment.newInstance().let {
            // TODO if template have an error, please reimport replaceFragmentInActivity
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        val LEADERBOARD_TYPE = "leaderboardType"
        val CHALLENGE_TYPE = 0
        val VIRTURUN_TYPE = 1
        val DAYS_LEFT = "daysLeft"
        val CURRENT_PROGRESS = "currentProgress"
        val TOTAL_PROGRESS = "totalProgress"
        val CHALLENGE_ID = "challengeId"
        val PROGRESS = "progress"
        val IS_CONTINUES = "isContinues"

        fun startActivity(
                leaderboardType: Int,
                context: Context,
                daysLeft: String,
                currentProgress: String,
                totalProgress: String,
                challengeId: Int,
                progress: Int,
                isContinues:Boolean
        ) {
            context.startActivity(Intent(context, LeaderBoardActivity::class.java).apply {
                putExtra(LEADERBOARD_TYPE, leaderboardType)
                putExtra(DAYS_LEFT, daysLeft)
                putExtra(CURRENT_PROGRESS, currentProgress)
                putExtra(TOTAL_PROGRESS, totalProgress)
                putExtra(CHALLENGE_ID, challengeId)
                putExtra(PROGRESS, progress)
                putExtra(IS_CONTINUES, isContinues)
            })
        }
    }
}
