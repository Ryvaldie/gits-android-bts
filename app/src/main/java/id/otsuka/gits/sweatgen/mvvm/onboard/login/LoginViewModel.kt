package id.otsuka.gits.sweatgen.mvvm.onboard.login;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.util.Log
import id.otsuka.gits.sweatgen.data.model.LoginDao
import id.otsuka.gits.sweatgen.data.model.LoginParams
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class LoginViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bEmail = ObservableField("")
    val bPassword = ObservableField("")
    val bSuccess = ObservableBoolean(false)
    val bMessage = ObservableField("")
    val loadingEvent = SingleLiveEvent<Boolean>()
    val bVerified = ObservableBoolean(false)

    fun validateFields(): Boolean = (!bEmail.get().isNullOrEmpty() && !bPassword.get().isNullOrEmpty())

    fun postLogin() {

        loadingEvent.value = true

        val loginParams = LoginParams(bEmail.get(), bPassword.get())

        repository.login(loginParams, object : GitsDataSource.LoginCallback {
            override fun onLoginSuccess(loginDao: LoginDao, msg: String?) {

                bSuccess.set(true)

                if (loginDao.access_user.token == null) {
                    bVerified.set(true)
                    bMessage.set(msg)
                    if (loginDao.access_token != null) {
                        repository.saveUser(User(token = loginDao.access_token, id = loginDao.access_user.id))
                    } else
                        repository.saveUser(User(token = "", id = loginDao.access_user.id))
                } else {
                    repository.saveUser(User(temporary_token = loginDao.access_user.token, id = loginDao.access_user.id))
                }
                loadingEvent.value = false
            }

            override fun onloginFailed(message: String?) {
                bSuccess.set(false)
                if (message == TOKEN_EXPIRE)
                    bMessage.set("Wrong username or password")
                else
                    bMessage.set(message)
                loadingEvent.value = false
            }
        })
    }
}