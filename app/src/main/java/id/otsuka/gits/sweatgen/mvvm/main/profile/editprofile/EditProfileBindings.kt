package id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.otsuka.gits.sweatgen.R

/**
 * @author radhikayusuf.
 */

object EditProfileBindings {
    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?) {

        val finalImage: Any = imageUrl ?: R.drawable.image_user_dummy

        Glide.with(view.context)
                .load(finalImage)
                .into(view)
                .apply { RequestOptions().error(R.color.greyLineProfile).placeholder(R.color.greyLineProfile) }
    }
}