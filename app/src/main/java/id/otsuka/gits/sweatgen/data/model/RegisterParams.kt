package id.otsuka.gits.sweatgen.data.model

data class RegisterParams(val name: String? = null,
                          val email: String? = null,
                          val password: String? = null,
                          val provider: String? = null
)