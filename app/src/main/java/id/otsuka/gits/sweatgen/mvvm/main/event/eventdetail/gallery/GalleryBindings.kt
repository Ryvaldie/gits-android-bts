package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery;

import android.databinding.BindingAdapter
import android.os.Build
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.ImageGallery
import id.otsuka.gits.sweatgen.util.GridSpacesItemDecoration
import id.otsuka.gits.sweatgen.util.dpToPx

/**
 * @author radhikayusuf.
 */

object GalleryBindings {

    @BindingAdapter("app:photos", "app:ItemListener")
    @JvmStatic
    fun setupPhotos(recyclerView: RecyclerView, photos: List<ImageGallery>, itemListener: RecyclerAdapter.onClickItemListener) {
        recyclerView.apply {
            adapter = RecyclerAdapter(ArrayList(0), itemListener)
            layoutManager = GridLayoutManager(context, 3)
            addItemDecoration(GridSpacesItemDecoration(2.dpToPx(context), false))
            (adapter as RecyclerAdapter).replacePhotos(photos)
        }
    }

    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?) {

        val finalImage: Any = imageUrl ?: R.drawable.img_slider_event

        Glide.with(view.context)
                .load(finalImage)
                .into(view)
                .apply { RequestOptions().error(R.color.greyLineProfile).placeholder(R.color.greyLineProfile) }
    }

    @BindingAdapter("app:url")
    @JvmStatic
    fun loadUrl(webView: WebView, url: String?) {
        webView.apply {
            val cookieManager = CookieManager.getInstance()
            cookieManager.setAcceptCookie(true)

            settings.apply {
                javaScriptEnabled = true
                cacheMode = WebSettings.LOAD_NO_CACHE
                domStorageEnabled = true
                builtInZoomControls = true
                displayZoomControls = true
                javaScriptCanOpenWindowsAutomatically = true
                setSupportMultipleWindows(true)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cookieManager.setAcceptThirdPartyCookies(webView, true); }
            }
            if (Build.VERSION.SDK_INT >= 21) {
                settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;
            }
            isHorizontalScrollBarEnabled = false
            if (url != null) {
                loadUrl(url)
            }
        }
    }

    @BindingAdapter("app:age")
    @JvmStatic
    fun setupAge(textView: TextView, age: Int) {
        textView.apply {
            text = context.getString(R.string.text_dummy_user_detail).replace("%", age.toString())

        }
    }
}