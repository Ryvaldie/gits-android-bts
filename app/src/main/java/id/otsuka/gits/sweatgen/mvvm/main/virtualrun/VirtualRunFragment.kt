package id.otsuka.gits.sweatgen.mvvm.main.virtualrun

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.*
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentVirtualRunBinding
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun.DetailVirtualRunFragment
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.startvirtualrun.StartVirtualRunFragment
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.raceguidecentral.RaceGuideCentralFragment.Companion.VIRTUAL_RUN_GUIDE

class VirtualRunFragment : Fragment(), VirtualRunUserActionListener {

    lateinit var mViewDataBinding: FragmentVirtualRunBinding
    lateinit var mViewModel: VirtualRunViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentVirtualRunBinding.inflate(inflater, container!!, false)

        mViewModel = (requireActivity() as VirtualRunActivity).mViewModel
        mViewDataBinding.mViewModel = mViewModel.apply {
            virtuRunType = arguments?.getInt(VIRTURUN_TYPE, -1) ?: -1
            loadJoinVirturunList()
        }

        mViewDataBinding.mListener = this@VirtualRunFragment

        setupRecycler()

        (requireActivity() as VirtualRunActivity).showHideToolbar(true)

        return mViewDataBinding.root

    }

    override fun toInfo() {
        (requireActivity() as VirtualRunActivity).showHideToolbar(false)

        requireActivity().replaceFragmentAndAddToBackStack(
                RaceGuideCentralFragment.newInstance(mViewModel.getGuideString(), VIRTUAL_RUN_GUIDE), R.id.frame_main_content)

    }

    fun setupRecycler() {

        mViewDataBinding.recyclerVirtualRun.apply {
            adapter = VirtualRunAdapter(ArrayList(0), (requireActivity() as VirtualRunActivity).mViewModel)
            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(GridSpacesItemDecoration(2.dpToPx(context), false))
        }

        mViewDataBinding.recyclerMyVirtualRun.apply {
            adapter = VirtualRunAdapter(ArrayList(0), (requireActivity() as VirtualRunActivity).mViewModel)
            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(GridSpacesItemDecoration(2.dpToPx(context), false))
        }
    }

    override fun onItemClick(id: Int) {

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_history_virturun -> {
                (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(VirtualRunFragment
                        .newInstance(MY_VIRTURUN_TYPE), R.id.frame_main_content,
                        (VirtualRunFragment::class.java.simpleName) + MY_VIRTURUN_TYPE.toString())
                return true
            }
        }
        return false
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.your_virturun_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.snackbarMsg.observe(this, Observer {
            when (it) {
              TOKEN_EXPIRE -> {

                    mViewModel.repository.logout(null, null, true)

                    requireActivity().redirectToLogin()

                }
                else -> it?.showSnackBar(mViewDataBinding.root)
            }
        })

        (requireActivity() as AppCompatActivity).supportActionBar?.apply {
            show()
            if ((requireActivity() as VirtualRunActivity).mViewModel.virtuRunType == JOIN_VIRTURUN_TYPE) {
                title = getString(R.string.text_join_virtual_running)
                setHasOptionsMenu(true)
            } else {
                title = getString(R.string.text_your_virtual_running)
            }
        }

        (requireActivity() as VirtualRunActivity).mViewModel.eventItemClick.observe(this, Observer {
            val fragment = if ((requireActivity() as VirtualRunActivity).mViewModel.virtuRunType == JOIN_VIRTURUN_TYPE)
                DetailVirtualRunFragment.newInstance(it ?: 0)
            else
                StartVirtualRunFragment.newInstance(it ?: 0)

            (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(fragment, R.id.frame_main_content)

        })
    }

    companion object {
        const val JOIN_VIRTURUN_TYPE = 1
        const val MY_VIRTURUN_TYPE = 2
        const val VIRTURUN_TYPE = "virturun"

        fun newInstance(virturunType: Int) = VirtualRunFragment().withArgs {
            putInt(VIRTURUN_TYPE, virturunType)
        }

    }

}
