package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll


import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipDescription
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.DisplayMetrics
import android.view.*
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.RelativeLayout
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentCameraRollBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageFragment
import id.otsuka.gits.sweatgen.util.*
import kotlin.math.roundToInt

class CameraRollFragment : Fragment(), CameraRollUserActionListener {

    lateinit var mViewDataBinding: FragmentCameraRollBinding
    lateinit var mViewModel: CameraRollViewModel
    lateinit var mParentVm: ShareRunViewModel

    private var heightImage = 0
    private var scrollY: Int? = null
    private var isScrollToBottom: Boolean = false
    private var isCanDrag: Boolean = true
    private var state = STATE_OPEN

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentCameraRollBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            listImages.clear()
            listImages.addAll(requireActivity().getAllImagesFromGallery())
        }

        mViewDataBinding.mListener = this@CameraRollFragment

        setupImageView()
        setupRecycler()

        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        mParentVm = (requireActivity() as ShareRunActivity).obtainViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_share_run, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_next -> {
                mParentVm.bCapturedImage.set(mViewDataBinding.cropImgv.croppedBitmap.bitmap)
                (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(ShareEditImageFragment.newInstance(), R.id.frame_main_content)
            }
        }
        return true
    }


    private fun setupRecycler() {
        mViewDataBinding.recyclerGallery.apply {
            adapter = CameraRollAdapter(ArrayList(), this@CameraRollFragment, requireActivity(), mViewModel)

            layoutManager = GridLayoutManager(context, 3)

            addItemDecoration(GridSpacesItemDecoration(8.dpToPx(context), true))
        }
    }

    override fun onClickOpen(v: View, isOpen: Boolean) {
        isScrollToBottom = !isOpen
        mViewModel.isOpen.set(!isOpen)
        openOrCloseImage()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupImageView() {
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        heightImage = (displayMetrics.heightPixels).times(0.6).roundToInt()
        val width = displayMetrics.widthPixels

        val layoutParam = CollapsingToolbarLayout.LayoutParams(width, heightImage)

        mViewDataBinding.cropImgv.apply {
            layoutParams = layoutParam
        }

        mViewDataBinding.swipeUp.setOnLongClickListener {
            val item = ClipData.Item(it.tag as CharSequence)
            val mimeTypes = arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN)

            val dragData = ClipData(it.tag.toString(), mimeTypes, item)
            val myShadow = View.DragShadowBuilder(mViewDataBinding.swipeUp)

            it.startDrag(dragData, myShadow, null, 0)
            return@setOnLongClickListener true
        }

        mViewDataBinding.swipeUp.setOnTouchListener { view, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                val data = ClipData.newPlainText("", "")
                val shadowBuilder = View.DragShadowBuilder(mViewDataBinding.shadowView)
                mViewDataBinding.swipeUp.startDrag(data, shadowBuilder, mViewDataBinding.swipeUp, 0)
                return@setOnTouchListener true
            } else {
                return@setOnTouchListener false
            }
        }

        mViewDataBinding.swipeUp.setOnDragListener { view, event ->
            when (event.action) {

                DragEvent.ACTION_DRAG_EXITED -> {
                    openOrCloseImage()
                }

                DragEvent.ACTION_DRAG_LOCATION -> {
                    if (isCanDrag) {
                        scrollY = if (scrollY == null) {
                            event.y.toInt()
                        } else {
                            isScrollToBottom = event.y.toInt() > (scrollY ?: 0)
                            event.y.toInt()
                        }
                    }
                }

                DragEvent.ACTION_DRAG_ENDED -> {

                }

                DragEvent.ACTION_DROP -> {
                }
            }
            return@setOnDragListener true
        }

    }

    private fun openOrCloseImage() {
        if ((state == STATE_OPEN && isScrollToBottom) || (state == STATE_CLOSED && !isScrollToBottom)) {
            return
        }

        if (isCanDrag) {
            isCanDrag = false
            scrollY = null
            val range = heightImage - MINIMUM_TOOLBAR_SIZE
            val animation = object : Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                    super.applyTransformation(interpolatedTime, t)
                    val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            if (isScrollToBottom) {
                                state = STATE_OPEN
                                MINIMUM_TOOLBAR_SIZE + (range * interpolatedTime).toInt()
                            } else {
                                state = STATE_CLOSED
                                heightImage - (range * interpolatedTime).toInt()
                            }
                    )
                    mViewDataBinding.appbar.layoutParams = params
                }
            }.apply {
                setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationRepeat(p0: Animation?) {

                    }

                    override fun onAnimationEnd(p0: Animation?) {
                        isCanDrag = true
                    }

                    override fun onAnimationStart(p0: Animation?) {

                    }

                })
                duration = 500
            }
            mViewDataBinding.appbar.startAnimation(animation)
            mViewDataBinding.expandButton.animate().rotation(if (!isScrollToBottom) 180f else 0f)
        }
    }

    override fun onItemClick(path: String, pos: Int) {

        Handler().post {

            val lastSelected = mViewModel.bSelectedPos

            mViewModel.bSelectedPos = pos

            mViewDataBinding.recyclerGallery.adapter.notifyItemChanged(lastSelected)

            mViewDataBinding.recyclerGallery.adapter.notifyItemChanged(pos)

            mViewModel.bSelectedImage.set(BitmapFactory.decodeFile(path))
        }
    }

    fun obtainViewModel(): CameraRollViewModel = obtainViewModel(CameraRollViewModel::class.java)


    companion object {
        val MINIMUM_TOOLBAR_SIZE = 120
        val STATE_OPEN = 12
        val STATE_CLOSED = 13


        fun newInstance() = CameraRollFragment().apply {

        }

    }

}
