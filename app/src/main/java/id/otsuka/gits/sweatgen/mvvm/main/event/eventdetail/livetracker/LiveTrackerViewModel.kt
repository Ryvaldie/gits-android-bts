package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.livetracker;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField


class LiveTrackerViewModel(context: Application) : AndroidViewModel(context) {

    val bUrl = ObservableField("")
}