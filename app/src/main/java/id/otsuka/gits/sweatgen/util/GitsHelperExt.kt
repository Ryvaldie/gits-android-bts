package id.otsuka.gits.sweatgen.util

import android.Manifest
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Bitmap.createBitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.location.Location
import android.net.Uri
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.SpannableString
import android.text.Spanned
import android.text.format.DateUtils
import android.text.style.URLSpan
import android.text.util.Linkify
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.data.model.LocationDeserializer
import id.otsuka.gits.sweatgen.data.model.LocationSerializer
import id.otsuka.gits.sweatgen.databinding.CustomDialogScanQrBinding
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel
import id.otsuka.gits.sweatgen.mvvm.main.profile.ProfileViewModel
import id.otsuka.gits.sweatgen.mvvm.onboard.OnBoardActivity
import id.otsuka.gits.sweatgen.util.customView.OverlayLoading
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * @author radhikayusuf.
 */

/**
 *          ////////////////////////////////////////////
 *         ////                                    ////
 *        ////         all helper functions       ////
 *       ////                                    ////
 *      ////////////////////////////////////////////
 *
 */


val TODAY_YEAR_PICKER = -1
val DATA_NOT_AVAILABLE = "data-not-available"

/**
 * displays a snackbar message with default message(string) from string
 */
fun String?.showSnackBarWithEmptyState(v: View, defaultMessage: String, duration: Int = Snackbar.LENGTH_SHORT) {
    if (this.isNullOrEmpty()) {
        Snackbar.make(v, defaultMessage, duration).show()
    } else {
        if (this!!.equals(DATA_NOT_AVAILABLE, true)) {
            Snackbar.make(v, defaultMessage, duration).show()
        } else {
            Snackbar.make(v, this, duration).show()
        }
    }
}

fun Context.shareContent(title: String, url: String) {

    val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
    sharingIntent.type = "text/plain"
    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title)
    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, url)
    startActivity(Intent.createChooser(sharingIntent, "Share Event"))
}

fun Date.formatDate(format: String = "MMMM dd, yyyy 'at' h:mm a"): String {
    return try {
        val dateFormat = SimpleDateFormat(format, Locale.ENGLISH)
        dateFormat.format(this)
    } catch (e: Exception) {
        "-"
    }

}

fun Context.checkLocationPermission(): Boolean =
        (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)

fun Long.timeMillisFormat(format: String): String {

    return String.format(format, TimeUnit.MILLISECONDS.toHours(this),
            TimeUnit.MILLISECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(this)),
            TimeUnit.MILLISECONDS.toSeconds(this) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(this)))

}

fun Long.timeFromSecondFormat(format: String): String {

    return String.format(format, TimeUnit.SECONDS.toHours(this),
            TimeUnit.SECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(this)),
            TimeUnit.SECONDS.toSeconds(this) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(this))
    )

}

fun Long.timeFromSecondtoHourMinute(format: String): String {

    return String.format(format, TimeUnit.SECONDS.toHours(this),
            TimeUnit.SECONDS.toMinutes(this) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(this))
    )


}

fun Long.timeFromSecondtoMinute(format: String): String {

    return String.format(format, TimeUnit.SECONDS.toMinutes(this)
    )

}

fun ArrayList<Location?>.parseLatLong(): String {

    val locations = StringBuilder()

    this.mapIndexed { index, location ->

        if (location != null)
            locations.append(location.latitude)
        else
            locations.append("|")

        locations.append(",")

        if (location != null)
            locations.append(location.longitude)
        else
            locations.append("|")

        if (index != this.lastIndex)
            locations.append(",")
    }
    return locations.toString()
}

fun FragmentActivity.showLoading() {
    try {
        val fragment = OverlayLoading()

        val fragmentTransaction = supportFragmentManager?.beginTransaction()

        val prev = supportFragmentManager.findFragmentByTag(fragment::class.java.simpleName)

        if (prev != null)
            fragmentTransaction?.remove(prev)

        fragmentTransaction?.addToBackStack(null)

        fragment.show(fragmentTransaction, fragment::class.java.simpleName)
    } catch (e: Exception) {
        e.printStackTrace()
    }

}

fun FragmentActivity.dismissLoading() {
    try {
        val loading = supportFragmentManager.findFragmentByTag(OverlayLoading::class.java.simpleName)
        if (loading != null) {
            (loading as OverlayLoading).dismiss()
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

}

fun Int.bitmapDescriptorFromVector(context: Context): BitmapDescriptor {
    val vectorDrawable = ContextCompat.getDrawable(context, this)
    vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
    val bitmap = createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    vectorDrawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bitmap)
}

/**
 * displays a snackbar message with default message(string res) from string
 */
fun String?.showSnackBarWithEmptyState(v: View, defaultMessage: Int, duration: Int = Snackbar.LENGTH_SHORT) {
    if (this.isNullOrEmpty()) {
        Snackbar.make(v, defaultMessage, duration).show()
    } else {
        if (this!!.equals(DATA_NOT_AVAILABLE, true)) {
            Snackbar.make(v, defaultMessage, duration).show()
        } else {
            Snackbar.make(v, this, duration).show()
        }
    }
}

fun Fragment.findFragmentViewpager(vpId: Int, itemPos: Int): Fragment? {
    return childFragmentManager.findFragmentByTag("android:switcher:$vpId:$itemPos")
}

fun Context.createGsonBuilder(): Gson {
    val gsonBuilder = GsonBuilder();
    gsonBuilder.registerTypeAdapter(Location::class.java, LocationDeserializer())
    gsonBuilder.registerTypeAdapter(Location::class.java, LocationSerializer())
    return gsonBuilder.create()
}

fun FragmentActivity.showImageDialog(fragment: DialogFragment) {

    val fragmentTransaction = supportFragmentManager?.beginTransaction()

    val prev = supportFragmentManager.findFragmentByTag(fragment::class.java.simpleName)

    if (prev != null)
        fragmentTransaction?.remove(prev)

    fragmentTransaction?.addToBackStack(null)

    fragment.show(fragmentTransaction, fragment::class.java.simpleName)
}

fun Int.getOrdinal(): String {

    if (this in 11..13) {
        return "th"
    }

    return when (this % 10) {
        1 -> "st"
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }
}

fun ArrayList<Challenge>.searchChallengeFromList(keyword: String): ArrayList<Challenge> {
    val result = ArrayList<Challenge>()
    this.map {
        if (it.name != null) {
            if (it.name.toLowerCase().contains(keyword.toLowerCase())) {
                result.add(it)
            }
        }
    }
    return result
}

fun ArrayList<Event>.searchEventFromList(keyword: String): ArrayList<Event> {
    val result = ArrayList<Event>()
    this.map {
        if (it.name != null) {
            if (it.name.toLowerCase().contains(keyword.toLowerCase())) {
                result.add(it)
            }
        }
    }
    return result
}

fun ArrayList<NewsItemModel>.searchNewsFromList(keyword: String): ArrayList<NewsItemModel> {
    val result = ArrayList<NewsItemModel>()
    this.map {

        if (it.title.toLowerCase().contains(keyword.toLowerCase())) {
            result.add(it)
        }
    }
    return result
}

fun ArrayList<Location?>.convertToLatLng(): ArrayList<LatLng?> {
    val latLngList = ArrayList<LatLng?>()

    this.map { it ->
        if (it != null)
            latLngList.add(LatLng(it.latitude, it.longitude))
        else
            latLngList.add(null)
    }
    return latLngList
}

fun String?.parseStringToLocations(): ArrayList<Location?> {

    val stringNoSpaces = this ?: "".replace(" ", "")

    val stringList = stringNoSpaces.split(",")

    val locations = ArrayList<Location?>()

    var location = Location("")

    var isLat = true

    stringList.mapIndexed { index, it ->

        if (it == "|") {
            locations.add(null)
            isLat = true
        } else {
            if (isLat) {
                location = Location(index.toString())
                location.latitude = it.toDouble()
            } else {
                location.longitude = it.toDouble()
                locations.add(location)
            }
            isLat = !isLat
        }

    }

    return locations
}

fun FragmentActivity.shareToOthers(file: File?) {
    val intentShareList = ArrayList<Intent>()
    val shareIntent = Intent().apply {
        action = Intent.ACTION_SEND
        type = "image/*"
    }
    val resolveInfoList = packageManager.queryIntentActivities(shareIntent, 0)
    for (x in resolveInfoList) {
        val packageName = x.activityInfo.packageName
        val name = x.activityInfo.name

        if (packageName != "com.instagram.android") {
            val intent = Intent().apply {
                component = ComponentName(packageName, name)
                action = Intent.ACTION_SEND
                type = "image/*"
                putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
            }
            intentShareList.add(intent)
        }
    }
    if (intentShareList.isEmpty()) {

    } else {
        val chooserIntent = Intent.createChooser(intentShareList.removeAt(0), "Share image using").apply {
            putExtra(Intent.EXTRA_INITIAL_INTENTS, intentShareList.toArray<Parcelable>(arrayOf<Parcelable>()))
        }
        startActivity(chooserIntent)
    }

}

fun FragmentActivity.shareToInstagram(file: File?) {
    val shareIntent = Intent(Intent.ACTION_SEND)
    shareIntent.setType("image/*")
    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))
    shareIntent.setPackage("com.instagram.android");
    startActivity(shareIntent)
}

fun TextView.expandCollapse() {
    with(this) {
        val collapsedMaxLines = 100000
        val collapsedMinLines = 5
        val animation = ObjectAnimator.ofInt(this, "maxLines",
                if (this.maxLines == collapsedMaxLines) collapsedMinLines else collapsedMaxLines)
        animation.setDuration(200).start()
        Log.i("LINECOUNT", lineCount.toString())
    }

}


/**
 * displays a snackbar message from string
 */
fun String.showSnackBar(v: View, duration: Int = Snackbar.LENGTH_SHORT) {
    Snackbar.make(v, this, duration).show()
}

/**
 * displays a snackbar message from string
 */
fun String.showCustomSnackBar(v: View, duration: Int = Snackbar.LENGTH_SHORT, color: Int) {
    val snbar = Snackbar.make(v, this, duration)
    snbar.view.setBackgroundColor(color)
    snbar.show()
}

/**
 * displays a snackbar message from string
 */
fun Int.showSnackBar(v: View, duration: Int = Snackbar.LENGTH_SHORT) {
    Snackbar.make(v, this, duration).show()
}


/**
 * displays a toast message from string res
 */
fun Int.showToast(ctx: Context, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(ctx, this, duration).show()
}

/**
 * hide soft keyboard
 */
fun Activity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

/**
 * get current Year
 */
fun Any.getCurrentYear(): String {
    val date = Date(System.currentTimeMillis())
    val dateFormat = SimpleDateFormat("yyyy", Locale.ENGLISH)
    return dateFormat.format(date)
}


/**
 * displays a toast message from string
 */
fun String.showToast(ctx: Context, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(ctx, this, duration).show()
}


fun Any.inputStreamToString(inputStream: InputStream): String? {
    try {
        val bytes = kotlin.ByteArray(inputStream.available())
        inputStream.read(bytes, 0, bytes.size)
        return kotlin.text.String(bytes)
    } catch (e: IOException) {
        return null
    }
}

interface ChooseImageSourceListener {
    fun onItemSelected(itemId: Int)
}

fun Fragment.chooseImageSource(listener: ChooseImageSourceListener) {

    val options = arrayOf("Camera", "Gallery", "Cancel")
    val builder = AlertDialog.Builder(context!!)
    builder.setItems(options) { _, x ->
        when (options[x]) {
            "Camera" -> {
                listener.onItemSelected(0)
            }
            "Gallery" -> {
                listener.onItemSelected(1)
            }

            "Cancel" -> {

            }
        }
    }
    builder.show()
}

fun Fragment.generateQRCode(stringCode: String?): Bitmap? {
    val bitMatrix: BitMatrix
    try {
        bitMatrix = MultiFormatWriter().encode(
                stringCode,
                BarcodeFormat.QR_CODE,
                220, 220, null
        )

    } catch (Illegalargumentexception: IllegalArgumentException) {

        return null
    }

    val bitMatrixWidth = bitMatrix.width

    val bitMatrixHeight = bitMatrix.height

    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

    for (y in 0 until bitMatrixHeight) {
        val offset = y * bitMatrixWidth

        for (x in 0 until bitMatrixWidth) {

            pixels[offset + x] = if (bitMatrix.get(x, y))
                resources.getColor(R.color.colorBlackName)
            else
                resources.getColor(R.color.colorWhite)
        }
    }
    val bitmap = createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

    bitmap.setPixels(pixels, 0, 220, 0, 0, bitMatrixWidth, bitMatrixHeight)
    return bitmap
}

fun Fragment.showQRDialog(mViewModel: ProfileViewModel) {

    val dialog = Dialog(context)

    val dialogViewBinding = CustomDialogScanQrBinding.inflate(layoutInflater).apply {
        setMViewModel(mViewModel)
    }

    dialog.setContentView(dialogViewBinding.root)

    dialog.setCancelable(true)

    // val window = dialog.window

    //  window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)

    //  window.setGravity(Gravity.CENTER)

    dialog.show()

}

fun Fragment.showDatePicker(listener: DatePickerDialog.OnDateSetListener, dob: String = "") {
    var mYear = 0
    var mMonth = 0
    var mDay = 0
    // if (dob.isEmpty()) {
    val calendar = Calendar.getInstance()
    mYear = calendar.get(Calendar.YEAR) - 5
    mMonth = calendar.get(Calendar.MONTH)
    mDay = calendar.get(Calendar.DAY_OF_MONTH)
    calendar.set(mYear, mMonth, mDay)
    /* } else {
         val date = dob.toDate()
         mYear = date.yearv';
         mMonth = date.month
         mDay = date.day
     }*/


    val datePickerDialog = DatePickerDialog(context, R.style.MyAlertDialogStyle, listener, mYear, mMonth, mDay)
    datePickerDialog.datePicker.maxDate = calendar.time.time
    datePickerDialog.show()

}


/**
 * convert string to date
 * example format = dd-MM-yyyy
 * return Date
 */
fun String.toFormattedDate(format: String, locale: Locale = Locale.ENGLISH): Date {
    val formatter = SimpleDateFormat(format, locale)

    try {
        return formatter.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return Date()
}

fun String.changeHeaderHtml(): String {
    val head = "<head>" +
            "<style>img{width: 100%; }; body { font-family: Avenir !important; }</style>"
    "<meta name=\"viewport\" content=\"width=device-width,  initial-scale=1\" /></head><body>"

    val closedTag = "</body></html>"
    return head + this + closedTag
}

fun String.changeHeaderHtmlV2(): String {
    val head = "<head><style>img{max-width: 100%; height: auto;} " +
            "body { margin: 0; padding: 8px; font-size:12px;}" + "iframe {display: block; background: " +
            "#000; border-top: 4px solid #000; border-bottom: 4px solid #000;" +
            "top:0;left:0;width:100%;height:235;}</style></head>"

    return "<html>$head<body>$this</body></html>"
}

fun Date.agoFormat(context: Context): String {

    return try {

        if ((System.currentTimeMillis() - this.time) <= 60000) {
            ((System.currentTimeMillis() - this.time) / 1000).toString() + " second ago"
        } else {
            DateUtils.getRelativeTimeSpanString(this.time,
                    System.currentTimeMillis(),
                    DateUtils.MINUTE_IN_MILLIS
            ).toString()

/*  val now = System.currentTimeMillis()

  DateUtils.getRelativeDateTimeString(context, now, DateUtils.MINUTE_IN_MILLIS,0,0).toString()*/
        }
    } catch (e: Exception) {
        return "- ago"
    }
}


interface AlertBtnOkListener {
    fun onBtnOkClick()
}

fun FragmentActivity.redirectToLogin() {
    OnBoardActivity.startActivity(this)
    this.finish()
}

class LinkSpan(val context: Context, url: String) : URLSpan(url) {
    override fun onClick(view: View) {
        val url = url
        context.startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(url)))
    }
}

fun Bitmap.fixOrientation(rotation: Float): Bitmap {
    if (this.width > this.height) {
        val matrix = Matrix()
        matrix.postRotate(rotation)
        return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true);
    }
    return this
}

fun FragmentActivity.getAllImagesFromGallery(): ArrayList<String> {
    val imagesPath = ArrayList<String>()

    val uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
    val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
    val cursor = contentResolver.query(uri, projection, null, null, null)
    val columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
    val columnIndexFolderName = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
    while (cursor.moveToNext()) {
        val absolutePathImage = cursor.getString(columnIndexData)
        imagesPath.add(absolutePathImage)
    }
    return imagesPath.reversed() as ArrayList<String>
}

fun View.getViewBitmap(): Bitmap? {
    isDrawingCacheEnabled = true
    buildDrawingCache()
    return drawingCache
}

fun View.saveLayoutToImage(filename: String) {
    this.apply {
        isDrawingCacheEnabled = true
        buildDrawingCache()
        val cache = drawingCache
        try {
            val fileOutputStream = FileOutputStream(Environment.getExternalStoragePublicDirectory
            (Environment.DIRECTORY_PICTURES).absolutePath + "/sweatgen/$filename")
            cache.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
        } catch (e: Exception) {

        } finally {
            destroyDrawingCache()
        }
    }
}

fun String.getSpanString(context: Context): SpannableString {
    val spanString = SpannableString(this)
    Linkify.addLinks(spanString, Linkify.WEB_URLS)
    val urlSpans = spanString.getSpans(0, spanString.length, URLSpan::class.java)
    for (urlSpan in urlSpans) {
        val linkSpan = LinkSpan(context, urlSpan.url)
        val spanStart = spanString.getSpanStart(urlSpan)
        val spanEnd = spanString.getSpanEnd(urlSpan)
        spanString.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spanString.removeSpan(urlSpan)
    }

    return spanString
}

fun LinearLayout.setupViewPagerIndicator(dotsCount: Int, minSize: Int = 1) {
    if (dotsCount > minSize)
        with(this) {

            val dotsImages = kotlin.collections.mutableListOf<ImageView>()

            (0..dotsCount.minus(1)).mapTo(dotsImages) { ImageView(context) }

            dotsImages.forEachIndexed { x, it ->

                if (x == 0)
                    it.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.custom_active_vp_indicator_race))
                else
                    it.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.custom_inactive_vp_indicator_race))

                val params = android.widget.LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
                    setMargins(8, 0, 8, 0)
                }
                this.addView(it, params)
            }
        }
}


fun Activity.checkPermission(permission: String): Boolean {
    val permissionCheckResult = ActivityCompat.checkSelfPermission(
            this,
            permission
    )
    return permissionCheckResult == PackageManager.PERMISSION_GRANTED
}

fun Activity.requestPermission(permissions: Array<String>, requestCode: Int) {
    ActivityCompat.requestPermissions(
            this,
            permissions,
            requestCode
    )
}

/**
 * convert int month format
 * to string format
 * return string
 */
fun String.intMonthToString(intMonth: Int, format: String, locale: Locale = Locale.ENGLISH): String {
    val formatter = SimpleDateFormat(format, locale)
    val calendar = GregorianCalendar()
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    calendar.set(Calendar.MONTH, intMonth)
    try {
        return formatter.format(calendar.time)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return String()
}


/**
 * convert string to date
 * example format = dd-MM-yyyy
 * return TimeStamp
 */
fun String.toTimeStamp(format: String): Long {
    val date = this.toFormattedDate(format)
    return date.time
}


/**
 * convert double to Rupiah format
 */
fun Double?.convertToRupiah(showTwoNumberAfterPoint: Boolean = false): String {
    return if (this != null) {
        val kursIndonesia = DecimalFormat.getCurrencyInstance(Locale.ENGLISH) as DecimalFormat
        val formatRp = DecimalFormatSymbols(Locale.ENGLISH)

        formatRp.currencySymbol = "Rp. "
        formatRp.monetaryDecimalSeparator = ','
        formatRp.groupingSeparator = ','

        kursIndonesia.decimalFormatSymbols = formatRp
        val result = kursIndonesia.format(this)

        if (!showTwoNumberAfterPoint) {
            result.substring(0, result.length - 3)
        } else {
            result
        }

    } else {
        "Rp. 0"
    }
}




