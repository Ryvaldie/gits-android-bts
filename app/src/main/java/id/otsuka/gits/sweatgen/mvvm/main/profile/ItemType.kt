package id.otsuka.gits.sweatgen.mvvm.main.profile

public interface ItemType {
    companion object {
        val TYPE_EVENT = 0
        val TYPE_MAP = 1
    }

    fun getItemType(): Int
}