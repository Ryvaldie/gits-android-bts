package id.otsuka.gits.sweatgen.data.model

data class ChallengeMedal(
        val id: Int = 0,
        val user_id: Int = 0,
        val medal_id: Int = 0,
        val challange_id: Int = 0,
        val status: Int = 0,
        val medal: Medal? = null
)