package id.otsuka.gits.sweatgen.data.model

import java.io.Serializable


class EventRundown(
        var id: Int = 0,
        var eventId: Int = 0,
        var start_time: String? = null,
        var end_time: String? = null,
        var desc: String? = null
): Serializable