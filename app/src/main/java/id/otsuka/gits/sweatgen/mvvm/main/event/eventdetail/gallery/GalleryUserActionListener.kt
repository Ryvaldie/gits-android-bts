package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery;


interface GalleryUserActionListener {

    // Example
    // fun onClickItem()

    fun back()

    fun search()

}