package id.otsuka.gits.sweatgen.data.model

data class UpdatePersonalBioDao(val access_token: String? = null)