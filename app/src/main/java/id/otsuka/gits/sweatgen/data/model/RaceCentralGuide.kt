package id.otsuka.gits.sweatgen.data.model

import java.io.Serializable

data class RaceCentralGuide(val id: Int = 0,
                            val event_id: Int = 0,
                            val name: String? = null,
                            val image: String? = null
):Serializable