package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.rundata.RunDataFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.stickers.StickersFragment

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class StickerTabAdapter(val context: Context,
                        fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    val mListFragment: List<Fragment> = ArrayList<Fragment>().apply {
        add(RunDataFragment.newInstance())
        add(StickersFragment.newInstance())
    }

    val mListTitle: List<String> = ArrayList<String>().apply {
        add(context.getString(R.string.text_run_data))
        add(context.getString(R.string.text_sticker))
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mListTitle[position]
    }

    override fun getCount(): Int = mListFragment.size

    override fun getItem(position: Int): Fragment {
        return mListFragment[position]
    }
}