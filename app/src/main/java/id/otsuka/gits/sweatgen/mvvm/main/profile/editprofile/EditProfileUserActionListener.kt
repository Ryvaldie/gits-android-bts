package id.otsuka.gits.sweatgen.mvvm.main.profile.editprofile;

import android.view.View


interface EditProfileUserActionListener {

    // Example
    // fun onClickItem()
    fun triggerDatePicker(view: View)

    fun saveClicked()

    fun triggerPickImage()

    fun onNavigationBack()
}