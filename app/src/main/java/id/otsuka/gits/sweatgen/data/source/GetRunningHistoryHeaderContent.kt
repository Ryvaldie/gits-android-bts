package id.otsuka.gits.sweatgen.data.source

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class GetRunningHistoryHeaderContent(val distance_sum: Double? = null,
                                          val distance_total: Double? = null
)