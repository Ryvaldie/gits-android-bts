package id.otsuka.gits.sweatgen.mvvm.onboard.register;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.RegisterDao
import id.otsuka.gits.sweatgen.data.model.RegisterParams
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class RegisterViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bName = ObservableField("")
    val bEmail = ObservableField("")
    val bPassword = ObservableField("")
    val bErrorMsgSnack = ObservableField("")
    val bErrorMsg = ObservableField("")
    val bErrorMsg2 = ObservableField("")
    val bSuccess = ObservableBoolean(false)

    val eventLoading = SingleLiveEvent<Boolean>()

    fun validateFields(): Boolean = (!bName.get().isNullOrEmpty() && !bEmail.get().isNullOrEmpty() && !bPassword.get().isNullOrEmpty())

    fun register() {

        eventLoading.value = true

        val registerParams = RegisterParams(bName.get(), bEmail.get(), bPassword.get(), "")

        repository.register(registerParams, object : GitsDataSource.RegisterCallback {
            override fun onRegisterSuccess(registerDao: RegisterDao) {
                bSuccess.set(true)
                repository.saveUser(User(temporary_token = registerDao.users.token, id = registerDao.users.id))
                bErrorMsg.set("")
                bErrorMsg2.set("")
                eventLoading.value = false
            }

            override fun onRegisterFailed(errorMessage: String?, errorMsg: String?) {
                bSuccess.set(false)
                if (errorMessage!!.contains("email"))
                    bErrorMsg.set(errorMessage)
                else {
                    bErrorMsg.set("")
                    bErrorMsgSnack.set(errorMessage)
                }

                bErrorMsg2.set(errorMsg)
                eventLoading.value = false
            }
        })
    }

}