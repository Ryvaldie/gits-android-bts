package id.otsuka.gits.sweatgen.mvvm.main.event

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Event
import id.otsuka.gits.sweatgen.databinding.CustomItemVpEventBinding
import id.otsuka.gits.sweatgen.util.withArgs
import java.io.Serializable
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailActivity

class EventSliderItemFragment : Fragment() {

    lateinit var mViewDataBinding: CustomItemVpEventBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = CustomItemVpEventBinding.inflate(inflater, container, false).apply {

            event = this@EventSliderItemFragment.arguments?.getSerializable(EVENT_ITEM) as Event

            mListener = object : RecyclerAdapter.ItemClickListener {
                override fun onItemSelected(eventId: Int) {
                    EventDetailActivity.startActivity(context!!, eventId)
                }
            }
        }

        mViewDataBinding.rootContainer.setScaleBoth(arguments?.getFloat(SCALE)!!)

        return mViewDataBinding.root
    }

    companion object {
        const val EVENT_ITEM = "eventItem"
        const val SCALE = "scale"
        fun newInstance(eventItem: Serializable, scale: Float) = EventSliderItemFragment().withArgs {
            putSerializable(EVENT_ITEM, eventItem)
            putFloat(SCALE, scale)
        }

    }
}