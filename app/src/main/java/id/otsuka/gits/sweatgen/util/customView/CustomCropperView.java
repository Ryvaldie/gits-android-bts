package id.otsuka.gits.sweatgen.util.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.fenchtose.nocropper.CropperView;

public class CustomCropperView extends CropperView {

    public CustomCropperView(Context context) {
        super(context);
    }

    public CustomCropperView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CustomCropperView(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // requestDisallowInterceptTouchEvent();
        return false;
    }
}
