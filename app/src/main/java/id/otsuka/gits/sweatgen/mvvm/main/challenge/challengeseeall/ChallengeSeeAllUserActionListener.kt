package id.otsuka.gits.sweatgen.mvvm.main.challenge.challengeseeall;


interface ChallengeSeeAllUserActionListener {

    // Example
    // fun onClickItem()

    fun onItemClick(challengeId:Int)

}