package id.otsuka.gits.sweatgen.mvvm.main.search


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentSearchBinding
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail.NewsDetailActivity
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import id.otsuka.gits.sweatgen.util.obtainViewModel
import id.otsuka.gits.sweatgen.util.redirectToLogin
import id.otsuka.gits.sweatgen.util.searchNewsFromList
import id.otsuka.gits.sweatgen.util.showSnackBar

class SearchFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentSearchBinding
    lateinit var mViewModel: SearchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentSearchBinding.inflate(inflater, container, false)

        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            mViewModel.searchType.set((activity as MainActivity).obtainViewModel().saveStateEvent.value
                    ?: 0)
        }

        mViewDataBinding.mListener = object : SearchUserActionListener {

        }

        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (mViewModel.bIsNewsFeed.get() != true) {
            setupViewPager()
            setupTab()
            mViewModel.apply {
                searchValue.observe(this@SearchFragment, Observer {

                    val fragment = childFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpage_search + ":" + mViewDataBinding.viewpageSearch.currentItem)
                    if (fragment != null) {
                        /*if (searchType.get() == 0) {
                            (fragment as ResultFragment).mViewModel.start(search(eventVm.it ?: ""))
                        } else {
                            (fragment as ResultFragment).mViewModel.start(search(it ?: ""))
                        }*/
                    }

                })
            }
        }

        setupRecycler()
    }

    override fun onResume() {
        super.onResume()

        mViewDataBinding.viewpageSearch.currentItem = (requireActivity() as MainActivity).mViewModel.saveStateEvent.value ?: 0

        if (arguments != null) {

            mViewModel.bIsNewsFeed.set(arguments!!.getBoolean(IS_NEWSFEED, false))

            if (mViewModel.bIsNewsFeed.get() == true) {

                if (mViewModel.listAllNews.isEmpty()) {
                    mViewModel.getAllNews()
                }
            }

        }
    }

    private fun observeEvents() {
        mViewModel.snackbarMsg.observe(this, Observer {
            if (it != null) {
                when (it) {
                    TOKEN_EXPIRE -> {
                        mViewModel.repository.logout(null, null, true)

                        requireActivity().redirectToLogin()
                    }
                    else -> {
                        it.showSnackBar(mViewDataBinding.root)
                    }
                }
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeEvents()
        mViewModel.moveToNewsDetail.observe(this, Observer {
            if (it != null) {
                NewsDetailActivity.startActivity(it, requireContext())
            }

        })
        (requireActivity() as MainActivity).mViewModel.searchNewsfeed.observe(this, Observer {
            if (it != null) {
                search(it)
            }

        })
    }

    fun search(keyword: String) {
        if (keyword.isEmpty())
            mViewModel.filterdNews.clear()
        else
            if (mViewModel.listAllNews.isNotEmpty()) {
                mViewModel.filterdNews.apply {
                    clear()
                    addAll(mViewModel.listAllNews.searchNewsFromList(keyword))
                }
            }
    }


    private fun setupRecycler() {
        mViewDataBinding.recyclerNews.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = SearchNewsAdapter(ArrayList(0), mViewModel)
        }
    }


    private fun setupViewPager() {
        mViewDataBinding.viewpageSearch.adapter = TabAdapter(childFragmentManager)
        mViewDataBinding.viewpageSearch.currentItem = mViewModel.searchType.get()
        mViewDataBinding.viewpageSearch.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                (activity as MainActivity).showSearchBar(if (position == 0) {
                    0
                } else {
                    1
                })
                mViewModel.searchType.set(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        })

    }

    private fun setupTab() {
        mViewDataBinding.tablaySearch.setupWithViewPager(mViewDataBinding.viewpageSearch)
    }

    fun obtainViewModel(): SearchViewModel = obtainViewModel(SearchViewModel::class.java)

    companion object {

        val IS_NEWSFEED = "isnewsfeed"

        fun newInstance() = SearchFragment().apply {

        }

    }

}
