package id.otsuka.gits.sweatgen.util.customView

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import id.otsuka.gits.sweatgen.databinding.DialogInputCodeVirturunBinding
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun.DetailVirtualRunFragment
import id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun.DetailVirtualRunViewModel
import kotlinx.android.synthetic.main.dialog_input_code_virturun.*

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class InputAccessCodeDialog() : DialogFragment() {
    lateinit var mViewDataBinding: DialogInputCodeVirturunBinding

    lateinit var mVm: DetailVirtualRunViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = DialogInputCodeVirturunBinding.inflate(inflater, container, false).apply {
            val fragment = requireActivity().supportFragmentManager.findFragmentByTag(DetailVirtualRunFragment::class.java.simpleName)
            mListener = (fragment as DetailVirtualRunFragment)
            vm = (fragment).mViewModel
            mVm =(fragment).mViewModel
        }
        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_cancel.setOnClickListener {
            dismiss()
        }
        frame_submit.setOnClickListener {
            dismiss()
            mViewDataBinding.mListener?.onDialogSubmit()
        }

        mVm.afterRedeemSuccess.observe(this, Observer {
            dismiss()
        })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        // creating the fullscreen dialog
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window!!.setBackgroundDrawable(ContextCompat.getDrawable(context!!, android.R.color.transparent))
        dialog.window!!.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        return dialog
    }
}