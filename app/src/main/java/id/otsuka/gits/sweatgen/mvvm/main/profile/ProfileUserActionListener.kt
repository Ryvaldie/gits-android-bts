package id.otsuka.gits.sweatgen.mvvm.main.profile;

import android.view.View


interface ProfileUserActionListener {

    // Example
    // fun onClickItem()

    fun toReward()

    fun convertMapToImage(v: View)
}