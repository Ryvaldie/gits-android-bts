package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail

import android.databinding.BindingAdapter
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.MotionEvent
import android.webkit.WebView
import android.widget.TextView
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel
import id.otsuka.gits.sweatgen.util.HtmlHttpImageGetter
import id.otsuka.gits.sweatgen.util.dpToPx

/**
 * @author radhikayusuf.
 */

object NewsDetailBindings {

    @BindingAdapter("app:fromHtml")
    @JvmStatic
    fun contentFromHtml(textView: TextView, html: String) {
        textView.apply {

            text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY, HtmlHttpImageGetter(textView), null)
            } else {
                Html.fromHtml(html, HtmlHttpImageGetter(textView), null)

            }
            //movementMethod = LinkMovementMethod.getInstance()
        }
    }

    @BindingAdapter("app:webLoadHtml")
    @JvmStatic
    fun loadHtml(webView: WebView, html: String) {
        webView.apply {
            settings.javaScriptEnabled = true
        //    settings.defaultFontSize = 8.dpToPx(context)
            clipToPadding = false
            isVerticalScrollBarEnabled = false
            setOnTouchListener { v, event -> (event?.action == MotionEvent.ACTION_MOVE) }
            loadDataWithBaseURL(null, html, "text/html", "UTF-8", null)
        }
    }

    @BindingAdapter("app:similarNews")
    @JvmStatic
    fun setSimilarNews(recyclerView: RecyclerView, data: List<NewsItemModel>) {
        (recyclerView.adapter as SimilarNewsAdapter).apply {
            replaceData(data)
        }
    }
}