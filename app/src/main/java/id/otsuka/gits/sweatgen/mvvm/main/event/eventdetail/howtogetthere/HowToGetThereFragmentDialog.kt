package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.howtogetthere

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.data.model.Location
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.EventDetailFragment
import kotlinx.android.synthetic.main.custom_dialog_howtogetthere.*
import id.otsuka.gits.sweatgen.databinding.CustomDialogHowtogetthereBinding

class HowToGetThereFragmentDialog : DialogFragment(), RecyclerviewAdapter.onClickItemListener {

    lateinit var mViewBinding: CustomDialogHowtogetthereBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewBinding = CustomDialogHowtogetthereBinding.inflate(inflater, container, false)

        val mParentFragment = activity?.supportFragmentManager?.findFragmentByTag(EventDetailFragment::class.java.simpleName)

        mViewBinding.mViewModel = (mParentFragment as EventDetailFragment).mViewModel.apply {
            obsListener.set(this@HowToGetThereFragmentDialog)
        }


        return mViewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgv_close.setOnClickListener { dismiss() }
    }

    override fun onItemSelected(location: Location) {
        val directionBuilder = Uri.Builder()
                .scheme("https")
                .scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", location.laititude + "," +
                        location.longitude)
        startActivity(Intent(Intent.ACTION_VIEW, directionBuilder.build()))
    }
}