package id.otsuka.gits.sweatgen.mvvm.main.virtualrun.detailvirtualrun;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.os.Handler
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.DetailNavigasiMenu
import id.otsuka.gits.sweatgen.data.model.RaceCentralGuide
import id.otsuka.gits.sweatgen.data.model.VirtualRunDetail
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent

class DetailVirtualRunViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val bAccessCode = ObservableField("")

    val rulesImageList = ArrayList<RaceCentralGuide>()

    val hydratePointImageList = ArrayList<RaceCentralGuide>()

    val prizeImageList = ArrayList<RaceCentralGuide>()

    var virturunId = -1

    val listNav = ObservableArrayList<DetailNavigasiMenu>()

    var bDetailVirtuRun = VirtualRunDetail()

    val canAccessChallenge = ObservableBoolean(false)

    val snackbarMsg = SingleLiveEvent<String>()

    val isRequesting = ObservableBoolean(false)

    val submitNewDataEvent = SingleLiveEvent<Void>()

    val isBtnRequesting = ObservableBoolean(false)

    val afterRedeemSuccess = SingleLiveEvent<Void>()

    fun start() {
        listNav.apply {
            clear()
            add(DetailNavigasiMenu(R.drawable.ic_document, context.getString(R.string.text_rules)))
            add(DetailNavigasiMenu(R.drawable.ic_location2, context.getString(R.string.text_hydrate_point)))
            add(DetailNavigasiMenu(R.drawable.ic_gift, context.getString(R.string.text_prize)))
        }

        loadData()
    }

    fun loadData() {

        isRequesting.set(true)

        repository.getVirturunById("", virturunId, repository.getUser()?.id
                ?: 0, object : GitsDataSource.GetVirturunByIdCallback {
            override fun onLoaded(virturun: VirtualRunDetail) {
                rulesImageList.clear()
                hydratePointImageList.clear()
                prizeImageList.clear()

                Handler().post {
                    if (virturun.information != null) {
                        if (virturun.information.rules != null) {
                            virturun.information.rules.mapTo(rulesImageList) {
                                RaceCentralGuide(image = it)
                            }
                        }

                        if (virturun.information.hydration_point != null) {
                            virturun.information.hydration_point.mapTo(hydratePointImageList) {
                                RaceCentralGuide(image = it)
                            }
                        }

                        if (virturun.information.prize != null) {
                            virturun.information.prize.mapTo(prizeImageList) {
                                RaceCentralGuide(image = it)
                            }
                        }
                    }
                }
                bDetailVirtuRun = virturun
                canAccessChallenge.set((virturun.run_status ?: "").isNotEmpty())
                submitNewDataEvent.call()
            }

            override fun onFinish() {
                isRequesting.set(false)
            }

            override fun onError(errorMessage: String?) {
                snackbarMsg.value = errorMessage
            }
        })
    }

    fun redeemSuccess() {
        isBtnRequesting.set(true)

        repository.redeemValid("", virturunId, repository.getUser()?.id ?: 0, bAccessCode.get()
                ?: "-", object : GitsDataSource.GeneralPostCallback {
            override fun onFailed(errorMessage: String?) {
                snackbarMsg.value = errorMessage
            }

            override fun onFinish() {
                bAccessCode.set("")
                isBtnRequesting.set(false)
            }

            override fun onSuccess(message: String?) {
                afterRedeemSuccess.call()
            }
        })
    }
}