package id.otsuka.gits.sweatgen.mvvm.main.runtracker.challengeresult;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.Challenge


class ChallengeResultViewModel(context: Application) : AndroidViewModel(context) {
    val bChallenge = ObservableField<Challenge>()

    val bCompleted = ObservableBoolean(false)
}