package id.otsuka.gits.sweatgen.mvvm.main.runtracker.postingpage


import android.app.Activity.RESULT_OK
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.AddHistoryRunParams
import id.otsuka.gits.sweatgen.databinding.FragmentPostingPageBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.RunTrackerActivity
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Other.TOKEN_EXPIRE
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class PostingPageFragment : Fragment() {

    lateinit var mViewDataBinding: FragmentPostingPageBinding
    lateinit var mViewModel: PostingPageViewModel
    val TAG = PostingPageFragment::class.java.simpleName


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentPostingPageBinding.inflate(inflater, container, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            getDataFromActivity()
        }

        mViewDataBinding.mListener = object : PostingPageUserActionListener {
            override fun post() {

                if (mViewModel.validateField()) {

                    (activity as RunTrackerActivity).stopService()

                    mViewModel.postRunHistory()

                } else
                    getString(R.string.text_activity_cannot_be_empty).showSnackBar(mViewDataBinding.root)


                /*
      val param = Gson().toJson(mViewModel.obsAddRunHistory.get())
      ShareRunActivity.startActivityForChallenge(Gson().toJson((requireActivity() as RunTrackerActivity).obtainViewModel().obsLocations), param, requireContext())
*/
            }

            override fun onBack() {
                if (activity != null) {
                    activity!!.onBackPressed()
                }
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.redirectHome.observe(this, Observer {
            val message = Intent()
            message.putExtra("message", getString(R.string.text_new_run_posted))
            activity?.setResult(RESULT_OK, message)
            activity?.finish()
        })

        mViewModel.snackbarMessage.observe(this, Observer {
            if (it == TOKEN_EXPIRE) {
                mViewModel.repository.logout(null, null, true)
                activity!!.redirectToLogin()
            } else {
                (it ?: "-").showSnackBar(mViewDataBinding.root)
            }
        })

        mViewModel.loadingEvent.observe(this, Observer { it ->
            if (it!!) {
                activity?.showLoading()
            } else {
                activity?.dismissLoading()
            }
        })
    }

    private fun getDataFromActivity() {

        mViewModel.bCurrentDate.set(Calendar.getInstance().time.formatDate())

        val activityVm = (activity as RunTrackerActivity).obtainViewModel()

        mViewModel.bDistance.set(activityVm.bDistance.get())
        mViewModel.bStep.set(activityVm.bSteps.get())
        mViewModel.bCalories.set(activityVm.bCalories.get())
        val distance = activityVm.bDoubleDistance.get()?.times(1000)
        val step = activityVm.bSteps.get()?.toDouble()
        val calories = activityVm.bDoubleCalories.get().toDouble()
        mViewModel.bDoubleCal.set(calories)


        val timeMillis = activityVm.timeInMillis.get()
        val duration = TimeUnit.MILLISECONDS.toSeconds(timeMillis ?: 0)

        mViewModel.bDuration.set(timeMillis?.timeMillisFormat("%dh %dm %ds"))

        val locations = ArrayList(activityVm.obsLocations).parseLatLong()

        mViewModel.obsAddRunHistory.set(
                AddHistoryRunParams(
                        user_id = mViewModel.repository.getUser()?.id ?: 0,
                        distance = (distance ?: 0f).toDouble(),
                        step = step ?: 0.0,
                        calories = calories ?: 0.0,
                        duration = duration.toInt(),
                        long_lat = locations
                ))
    }


    fun obtainViewModel(): PostingPageViewModel = obtainViewModel(PostingPageViewModel::class.java)

    companion object {
        fun newInstance() = PostingPageFragment().apply {

        }

    }

}
