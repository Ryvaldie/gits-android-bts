package id.otsuka.gits.sweatgen.mvvm.main.search.result;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.util.searchChallengeFromList


class ResultViewModel(context: Application) : AndroidViewModel(context) {

    val resultChallengeList = ObservableArrayList<Challenge>()

    fun start(result: ArrayList<Challenge>) {
        resultChallengeList.apply {
            clear()
            addAll(result)
        }
    }

    fun search(temp: ArrayList<Challenge>, keyword: String) {
        if (keyword.isEmpty())
            resultChallengeList.clear()
        else
            if (temp.isNotEmpty()) {
                resultChallengeList.apply {
                    clear()
                    addAll(temp.searchChallengeFromList(keyword))
                }
            }
    }

}