package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery


import android.arch.lifecycle.Observer
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import id.otsuka.gits.sweatgen.data.model.ImageGallery
import id.otsuka.gits.sweatgen.databinding.FragmentGalleryBinding
import id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.gallery.GalleryViewModel
import kotlinx.android.synthetic.main.fragment_gallery.*
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.*
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.webkit.WebView


class GalleryFragment : Fragment(), RecyclerAdapter.onClickItemListener {

    lateinit var mViewDataBinding: FragmentGalleryBinding
    lateinit var mViewModel: GalleryViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentGalleryBinding.inflate(inflater!!, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            bUrl.set(arguments?.getString(GALLERY_URL, ""))
            /*  bEventId.set(arguments?.getInt(EVENT_ID, 0)!!)
              itemListener.set(this@GalleryFragment)
          */
        }

        mViewDataBinding.mListener = object : GalleryUserActionListener {
            override fun back() {
                activity!!.onBackPressed()
            }

            override fun search() {
                /*  if (mViewModel.validate())
                      mViewModel.start()
                  else
                      getString(R.string.text_please_input_bib_num).showSnackBar(mViewDataBinding.root)*/
            }
        }



        return mViewDataBinding.root

    }

    override fun onItemSelected(image: ImageGallery) {
        //  mViewModel.bPhotoDetail.set(image)
        //activity!!.showImageDialog(PhotoDetailFragmentDialog())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        /*  mViewModel.eventLoading.observe(this, Observer { it ->
              if (it!!) {
                  progressBar1.visibility = View.VISIBLE
              } else {
                  progressBar1.visibility = View.GONE

                  if (mViewModel.bMessage.get()!!.isNotEmpty()) {
                      if (mViewModel.bMessage.get() == "token_expired") {
                          mViewModel.repository.logout(null, null, true)
                          activity!!.redirectToLogin()
                      } else
                          mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
                  }
              }
          })*/
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webview_race_photo.webViewClient = object : android.webkit.WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)

                if (progressBar1 != null)
                    progressBar1.visibility = View.GONE
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                Log.d("INI ERRORNYA", error.toString())
            }
        }
        webview_race_photo.webChromeClient = object : WebChromeClient() {
            override fun onCreateWindow(view: WebView?, isDialog: Boolean, isUserGesture: Boolean, resultMsg: Message?): Boolean {

                val result = view?.hitTestResult
                val data = result?.extra
                val context = view?.context
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(data))
                context?.startActivity(browserIntent)

                return false

            }
        }

        toolbar_gallery.setNavigationOnClickListener { mViewDataBinding.mListener?.back() }
    }

    fun obtainViewModel(): GalleryViewModel = obtainViewModel(GalleryViewModel::class.java)

    companion object {
        val GALLERY_URL = "galleryUrl"
        fun newInstance(galleryUrl: String) = GalleryFragment().withArgs {
            putString(GALLERY_URL, galleryUrl)

        }

    }

}
