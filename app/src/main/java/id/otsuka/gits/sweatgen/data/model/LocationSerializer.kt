package id.otsuka.gits.sweatgen.data.model

import android.location.Location
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.lang.reflect.Type


/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class LocationSerializer() : JsonSerializer<Location> {
    override fun serialize(t: Location?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val jo = JsonObject()
        jo.addProperty("mProvider", t?.provider)
        jo.addProperty("mAccuracy", t?.accuracy)
        // etc for all the publicly available getters
        // for the information you're interested in
        // ...
        return jo
    }
}