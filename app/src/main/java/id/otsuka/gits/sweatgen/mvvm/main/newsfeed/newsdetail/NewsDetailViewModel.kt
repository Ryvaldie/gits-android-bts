package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.newsdetail;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.model.NewsFeed
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.newsfeed.NewsItemModel
import id.otsuka.gits.sweatgen.util.*


class NewsDetailViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    var newsId: Int = 0
    val isRequesting = ObservableBoolean(false)
    val bNewsDetail = ObservableField<NewsDetailModel>()
    val showMessage = SingleLiveEvent<String>()
    val bSimilarNews = ObservableArrayList<NewsItemModel>()

    fun start() {

        bNewsDetail.set(NewsDetailModel())

        isRequesting.set(true)

        repository.getNewsDetail("", newsId, object : GitsDataSource.GetNewsDetailCallback {
            override fun onDataNotAvailable() {
                isRequesting.set(false)
            }

            override fun onError(errorMessage: String?) {
                isRequesting.set(false)
                showMessage.value = errorMessage
            }

            override fun onLoaded(newsDetail: NewsFeed, similarNews: List<NewsFeed>?) {

                bNewsDetail.set(
                        NewsDetailModel(
                                newsDetail.banner ?: "-",
                                newsDetail.title ?: "-",
                                (newsDetail.date_publish
                                        ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").formatDate("MMM dd 'at' HH:mm"),
                                newsDetail.category ?: "-",
                                newsDetail.created_by?.first_name + " " + newsDetail.created_by?.last_name,
                                (newsDetail.content ?: "-").changeHeaderHtml(),
                                ((newsDetail.isShow ?: 0) == 1),
                                newsDetail.bio ?: "-",
                                newsDetail.category_id ?: "0"
                        ))

                bSimilarNews.clear()

                if (similarNews != null) {
                    bSimilarNews.addAll(similarNews.map {
                        NewsItemModel(
                                it.id,
                                it.title ?: "-",
                                (it.date_publish
                                        ?: "-").toFormattedDate("yyyy-MM-dd hh:mm:ss").agoFormat(context) + " - " + it.category,
                                it.content ?: "-",
                                it.banner ?: "")
                    })
                }

                isRequesting.set(false)

            }
        })
    }

}