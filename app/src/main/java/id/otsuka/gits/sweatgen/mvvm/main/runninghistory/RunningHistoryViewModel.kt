package id.otsuka.gits.sweatgen.mvvm.main.runninghistory;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.HistoryRun
import id.otsuka.gits.sweatgen.data.model.RunningHistory
import id.otsuka.gits.sweatgen.data.source.GetRunningHistoryHeaderContent
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent
import id.otsuka.gits.sweatgen.util.cutExactly2Num
import id.otsuka.gits.sweatgen.util.timeFromSecondtoMinute


class RunningHistoryViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val bCurrentDistance = ObservableField("")
    val bTotalDistance = ObservableField("")
    val bCountRun = ObservableField("")
    val bProgress = ObservableInt(0)
    val listRunHistory = ObservableArrayList<RunningHistory>()
    val userId = repository.getUser()?.id ?: 0
    var challengeId = 0
    val isRequesting = ObservableBoolean(false)
    val listHistoryRun = ObservableArrayList<HistoryRun>()
    val snackbarMessage = SingleLiveEvent<String>()

    fun start(histories: List<HistoryRun>) {
        listRunHistory.clear()

        histories.map {

            val distance = ((it.distance ?: 0.0) / 1000.0).cutExactly2Num()
            val duration = (it.duration ?: 0).timeFromSecondtoMinute("%d Min")
            val date = it.timestmaps
            listRunHistory.add(RunningHistory(date, distance, duration))

        }

        bCountRun.set(listRunHistory.size.toString())
    }

    fun setupHeaderContent(headerData: GetRunningHistoryHeaderContent) {
        bCurrentDistance.set(String.format("%.2f", (headerData.distance_sum ?: 0.0) / 1000.0))
        bTotalDistance.set(context.getString(R.string.text_distance_indicator)
                .replace("*", String.format("%.2f", (headerData.distance_total ?: 0.0) / 1000.0)))
        val mProgress = (headerData.distance_sum ?: 0.0) / (headerData.distance_total ?: 0.0) * 100
        bProgress.set(mProgress.toInt())
    }

    fun getHistories() {
        isRequesting.set(true)
        repository.getContinuesRunningHistory(userId, challengeId, "", object : GitsDataSource.GetContinuesRunningHistoryCallback {
            override fun onDataNotAvailable(headerData: GetRunningHistoryHeaderContent) {
                //setupHeaderContent(headerData)
                isRequesting.set(false)
            }

            override fun onError(errorMessage: String?) {
                isRequesting.set(false)
                snackbarMessage.value = errorMessage
            }

            override fun onLoaded(headerData: GetRunningHistoryHeaderContent, histories: List<HistoryRun>) {
                //setupHeaderContent(headerData)
                listHistoryRun.apply {
                    clear()
                    addAll(histories)
                }
                start(histories)
                isRequesting.set(false)
            }
        })
    }
}


/**
 * TODO add to bindings class
 * function for bindingsList
 *
 * object RunningHistoryBindings {
 *
 * 	@BindingAdapter("app:listDataRunningHistory")
 *     @JvmStatic
 *     fun setListDataRunningHistory(recyclerView: RecyclerView, data: List<RunningHistoryModel>) {
 *         with(recyclerView.adapter as RunningHistoryAdapter) {
 *             replaceData(data)
 *         }
 *     }
 *
 * }
 *
 **/