package id.otsuka.gits.sweatgen.mvvm.main.reward;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import id.otsuka.gits.sweatgen.data.model.ChallengeMedal
import id.otsuka.gits.sweatgen.data.model.CurrentMedal
import id.otsuka.gits.sweatgen.data.model.User
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.mvvm.main.reward.RecyclerAdapter.Companion.ITEM_PER_PAGE
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class RewardViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    var bMedals = ObservableArrayList<ChallengeMedal>()

    var bCurrentMedal = ObservableField<CurrentMedal>()

    val eventMessage = SingleLiveEvent<String?>()

    val loadingEvent = SingleLiveEvent<Boolean>()

    var isLastPage = ObservableBoolean(false)

    val isRequesting = ObservableBoolean(false)

    val showProgress = SingleLiveEvent<Boolean>()

    var firstTime = true

    var currentPage = 1

    fun start() {

        isLastPage.set(false)

        if (firstTime) {
            loadingEvent.value = true
            firstTime = false
        }

        repository.getMedalHistory("", (repository.getUser()?: User()).id, currentPage, object : GitsDataSource.GetMedalCallback {
            override fun onDataLoaded(challengesMedal: List<ChallengeMedal>, currentMedal: CurrentMedal?, page: Int) {
                if (page == 1) {
                    bMedals.clear()
                    bCurrentMedal.set(currentMedal)
                }

                bMedals.addAll(challengesMedal)

                isLastPage.set(bMedals.size < ITEM_PER_PAGE)

                showProgress.value = isLastPage.get()

                loadingEvent.value = false

                isRequesting.set(false)
            }

            override fun onDataNotAvailable() {
                showProgress.value = true
                loadingEvent.value = false
                isLastPage.set(true)
                isRequesting.set(false)
            }

            override fun onError(errorMessage: String?) {
                loadingEvent.value = false
                eventMessage.value = errorMessage
                isLastPage.set(false)
                isRequesting.set(false)
            }
        })
    }
}