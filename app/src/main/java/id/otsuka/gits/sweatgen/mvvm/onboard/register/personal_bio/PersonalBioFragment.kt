package id.otsuka.gits.sweatgen.mvvm.onboard.register.personal_bio


import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import id.otsuka.gits.sweatgen.base.BaseFragment
import id.otsuka.gits.sweatgen.databinding.FragmentPersonalBioBinding
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.onboard.register.verification.VerificationFragment
import id.otsuka.gits.sweatgen.mvvm.main.MainActivity
import id.otsuka.gits.sweatgen.util.*
import id.otsuka.gits.sweatgen.util.Preference.KEY_LOGIN
import java.util.*

class PersonalBioFragment : BaseFragment(), DatePickerDialog.OnDateSetListener {

    lateinit var mViewDataBinding: FragmentPersonalBioBinding
    lateinit var mViewModel: PersonalBioViewModel
    val pref by lazy { PreferenceManager.getDefaultSharedPreferences(context) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentPersonalBioBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            getTempUser()
        }

        mViewDataBinding.mListener = object : PersonalBioUserActionListener {
            override fun triggerDatePicker(view: View) {
                showDatePicker(this@PersonalBioFragment,(view as EditText).text.toString())
            }

            override fun letsGetSweatClick() {
                if (mViewModel.validateFields()) {
                    mViewModel.setGender()
                    mViewModel.updatePersonalBio()

                } else {
                    getString(R.string.text_please_fill_info).showSnackBar(mViewDataBinding.root)
                }
            }
        }



        return mViewDataBinding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.eventLoading.observe(this, Observer { it ->
            if (it!!) {
                showProgressDialog(getString(R.string.text_loading), getString(R.string.text_please_wait), false)
            } else {

                hideProgressDialog()

                if (mViewModel.isSuccess.get()) {
                    pref.edit().putBoolean(KEY_LOGIN, true).apply()

                    activity?.finish()

                    MainActivity.startActivity(context!!)
                } else
                    mViewModel.bMessage.get()?.showSnackBar(mViewDataBinding.root)
            }
        })
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        if (year > (Calendar.getInstance().get(Calendar.YEAR) - 5)) {
            showToast(getString(R.string.text_minimum_age_require))
        } else {
            mViewModel.bDateOfBirth.set(year.toString() + "-" + month.plus(1).toString().mToMm() + "-" + dayOfMonth.toString())
        }
    }

    fun obtainViewModel(): PersonalBioViewModel = obtainViewModel(PersonalBioViewModel::class.java)

    companion object {

        fun newInstance() = PersonalBioFragment().withArgs {

        }
    }

}
