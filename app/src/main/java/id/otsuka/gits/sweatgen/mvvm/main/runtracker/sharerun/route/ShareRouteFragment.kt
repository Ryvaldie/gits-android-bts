package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.route

import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.getColor
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.*
import android.widget.FrameLayout
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.databinding.FragmentShareRouteBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunActivity
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunActivity.Companion.LOCATION_INTENT
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.ShareRunViewModel
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageFragment
import id.otsuka.gits.sweatgen.util.*
import java.io.File
import kotlin.math.roundToInt

class ShareRouteFragment : Fragment(), ShareRouteUserActionListener, OnMapReadyCallback {

    lateinit var mViewDataBinding: FragmentShareRouteBinding
    lateinit var mViewModel: ShareRouteViewModel
    var mGoogleMap: GoogleMap? = null
    lateinit var mParentVm: ShareRunViewModel
    val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentShareRouteBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
            val locationJson = requireActivity().intent.getStringExtra(LOCATION_INTENT)
            if (!locationJson.isNullOrEmpty()) {
                start(locationJson)
            }
        }

        mViewDataBinding.mListener = this@ShareRouteFragment

        return mViewDataBinding.root

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_share_run, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_next -> {
                resizeMap()

                val filename = "route_map_${System.currentTimeMillis()}.png"

                mViewDataBinding.mapview.saveLayoutToImage(filename)

                mParentVm.bCapturedImage.set(File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_PICTURES).absolutePath + "/sweatgen/$filename"))

                (requireActivity() as AppCompatActivity).replaceFragmentAndAddToBackStack(ShareEditImageFragment.newInstance(), R.id.frame_main_content)
            }
        }
        return true
    }

    private fun resizeMap() {
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = (displayMetrics.heightPixels).times(0.6).roundToInt()
        val width = displayMetrics.widthPixels
        val layoutParam = FrameLayout.LayoutParams(width, height)

        mViewDataBinding.mapview.apply {
            layoutParams = layoutParam
        }

        if (mViewModel.listLocations.size > 1)
            fixZoom()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupMapview(savedInstanceState)
        setHasOptionsMenu(true)
        mParentVm = (requireActivity() as ShareRunActivity).obtainViewModel()
    }

    private fun setupMapview(savedInstanceState: Bundle?) {
        var mapViewBundle: Bundle? = null
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY)
        }
        mViewDataBinding.mapview.apply {
            onCreate(mapViewBundle)
            getMapAsync(this@ShareRouteFragment)
        }
    }


    private fun setupMap() {
        mGoogleMap?.apply {
            uiSettings.isMyLocationButtonEnabled = false

            if (requireContext().checkLocationPermission()) {
                isMyLocationEnabled = false
            }
        }
    }

    private fun putMarker(location: android.location.Location, resId: Int) {
        val userPositionMarkerBitmapDescriptor = resId
                .bitmapDescriptorFromVector(requireContext())

        val latLng = LatLng(location.latitude, location.longitude)

        mGoogleMap?.addMarker(MarkerOptions()
                .position(latLng)
                .flat(true)
                .anchor(0.3f, 0.3f)
                .icon(userPositionMarkerBitmapDescriptor))
    }

    private fun zoomMapTo(location: android.location.Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
    }

    private fun fixZoom() {

        val latLngBounds = LatLngBounds.Builder()

        mViewModel.listLatLng.forEach {
            if (it != null)
                latLngBounds.include(it)
        }
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 100))
    }

    private fun drawTrack() {
        val polyLineWidth = 6

        var polyline: Polyline? = null
        mViewModel.apply {
            for (index in 0..listLatLng.lastIndex) {
                if (index == 0) {
                    putMarker(listLocations[index]!!, R.drawable.ic_start_user_position)
                    zoomMapTo(listLocations[index]!!)
                    if (listLocations.size > 1)
                        fixZoom()
                }

                if (listLatLng.size > 1) {
                    if (index != listLatLng.lastIndex) {
                        if (polyline == null) {
                            if (listLatLng[index] != null && listLatLng[index.plus(1)] != null)
                                if (index != listLatLng.lastIndex)
                                    polyline = mGoogleMap?.addPolyline(PolylineOptions()
                                            .add(listLatLng[index], listLatLng[index.plus(1)])
                                            .width(polyLineWidth.toFloat()).color(getColor(requireContext(), R.color.colorBlueRadioCircle)).geodesic(true))
                        } else {
                            if (listLatLng[index] != null) {
                                val points = polyline?.points

                                points?.add(listLatLng[index])

                                polyline?.points = points
                            } else {
                                polyline = null
                            }

                        }
                    } else {
                        if (listLocations[index] != null)
                            putMarker(listLocations[index]!!, R.drawable.ic_finish)
                    }

                }
            }
        }

    }

    override fun onMapReady(map: GoogleMap?) {
        map?.clear()

        mGoogleMap = map

        setupMap()

        drawTrack()
    }

    override fun onStart() {
        super.onStart()
        mViewDataBinding.mapview.onStart()
    }

    override fun onStop() {
        super.onStop()
        mViewDataBinding.mapview.onStop()
    }

    override fun onResume() {
        super.onResume()
        mViewDataBinding.mapview.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewDataBinding.mapview.onDestroy()
    }

    override fun onPause() {
        super.onPause()
        mViewDataBinding.mapview.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mViewDataBinding.mapview.onLowMemory()
    }


    override fun onClickTest(text: String) {
        // TODO if you have a toast extension, you can replace this
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
    }


    // TODO import obtainViewModel & add ShareRouteViewModel to ViewModelFactory
    fun obtainViewModel(): ShareRouteViewModel = obtainViewModel(ShareRouteViewModel::class.java)

    companion object {
        fun newInstance() = ShareRouteFragment().apply {

        }

    }

}
