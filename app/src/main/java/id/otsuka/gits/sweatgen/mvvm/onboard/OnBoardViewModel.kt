package id.otsuka.gits.sweatgen.mvvm.onboard;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableArrayList
import id.otsuka.gits.sweatgen.BuildConfig
import id.otsuka.gits.sweatgen.data.model.OnBoardImage
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class OnBoardViewModel(val context: Application, val repository: GitsRepository) : AndroidViewModel(context) {

    val pagesContent = ObservableArrayList<OnBoardImage>()

    val eventLoading = SingleLiveEvent<Boolean>()

    val snackbarMsg = SingleLiveEvent<String>()

    fun start() {
        eventLoading.value = true

        repository.getOnBoardImage(object : GitsDataSource.GetOnBoardImageCallback {
            override fun onImageLoaded(images: List<OnBoardImage>) {
                pagesContent.apply {
                    clear()
                    addAll(images)
                }
                eventLoading.value = false
            }

            override fun onError(errorMessage: String?) {
                eventLoading.value = false
                snackbarMsg.value = errorMessage
            }
        })
    }


}