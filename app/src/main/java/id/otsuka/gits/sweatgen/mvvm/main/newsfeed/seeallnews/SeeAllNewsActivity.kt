package id.otsuka.gits.sweatgen.mvvm.main.newsfeed.seeallnews;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.util.replaceFragmentInActivity


class SeeAllNewsActivity : AppCompatActivity(), SeeAllNewsNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_see_all_news)
        setupFragment()
    }

    private fun setupFragment() {
        supportFragmentManager.findFragmentById(R.id.frame_main_content)
        SeeAllNewsFragment.newInstance().let {
            replaceFragmentInActivity(it, R.id.frame_main_content)
        }
    }

    companion object {
        val CATEGORY_ID = "categoryId"

        fun startActivity(categoryId: Int = -1, context: Context) {
            context.startActivity(Intent(context, SeeAllNewsActivity::class.java).putExtra(CATEGORY_ID, categoryId))
        }
    }
}
