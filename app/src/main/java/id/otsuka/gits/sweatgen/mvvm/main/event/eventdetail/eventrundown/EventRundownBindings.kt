package id.otsuka.gits.sweatgen.mvvm.main.event.eventdetail.eventrundown

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.data.model.EventRundown

/**
 * @author radhikayusuf.
 */

object EventRundownBindings {

    @BindingAdapter("app:listDataEventRundown")
    @JvmStatic
    fun setListDataEventRundown(recyclerView: RecyclerView, data: List<EventRundown>) {
        with(recyclerView.adapter as EventRundownAdapter) {
            replaceData(data)
        }
    }

}