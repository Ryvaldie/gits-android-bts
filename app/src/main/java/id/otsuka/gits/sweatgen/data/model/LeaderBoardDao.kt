package id.otsuka.gits.sweatgen.data.model

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

data class LeaderBoardDao(val name: String? = null,
                          val distance: Double? = null,
                          val duration: Long? = null,
                          val picture: String? = null,
                          val leaderboard:String?=null
)