package id.otsuka.gits.sweatgen.mvvm.main.search.resultevent

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import id.otsuka.gits.sweatgen.data.model.Event

/**
 * @author radhikayusuf.
 */

object ResultEventBindings {

    @BindingAdapter("app:listDataResultEvent")
    @JvmStatic
    fun setListDataResultEvent(recyclerView: RecyclerView, data: List<Event>) {
        with(recyclerView.adapter as ResultEventAdapter) {
            replaceData(data)
        }
    }

}