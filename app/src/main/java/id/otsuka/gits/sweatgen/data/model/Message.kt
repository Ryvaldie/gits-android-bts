package id.otsuka.gits.sweatgen.data.model

data class Message(val email: String? = null,
                   val password: String? = null,
                   val code: String? = null
)