package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll.CameraRollFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.customcam.CustomCameraFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.route.ShareRouteFragment

/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */

class ShareTabAdapter(val context: Context,
                      fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    val mListFragment: List<Fragment> = ArrayList<Fragment>().apply {
        add(ShareRouteFragment.newInstance())
        add(CustomCameraFragment.newInstance())
        add(CameraRollFragment.newInstance())
    }

    val mListTitle: List<String> = ArrayList<String>().apply {
        add(context.getString(R.string.text_route))
        add(context.getString(R.string.text_photo))
        add(context.getString(R.string.text_camera_roll))
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mListTitle[position]
    }

    override fun getCount(): Int = mListFragment.size

    override fun getItem(position: Int): Fragment {
        return mListFragment[position]
    }
}