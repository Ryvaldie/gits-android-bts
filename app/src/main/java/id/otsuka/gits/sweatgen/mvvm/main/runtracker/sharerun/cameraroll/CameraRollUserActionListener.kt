package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.cameraroll;

import android.view.View


interface CameraRollUserActionListener {

    fun onItemClick(path: String, pos: Int)

    fun onClickOpen(v: View, isOpen: Boolean)

}