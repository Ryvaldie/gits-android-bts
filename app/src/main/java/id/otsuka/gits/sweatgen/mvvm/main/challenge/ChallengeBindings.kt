package id.otsuka.gits.sweatgen.mvvm.main.challenge;

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.otsuka.gits.sweatgen.R
import id.otsuka.gits.sweatgen.data.model.Challenge
import id.otsuka.gits.sweatgen.util.*

/**
 * @author radhikayusuf.
 */

object ChallengeBindings {
    @BindingAdapter("app:dateUntil")
    @JvmStatic
    fun setupDateUntil(textView: TextView, date: String?) {
        textView.apply {
            text = if (!date.isNullOrEmpty()) {

                val dateObj = date?.toDate()

                if (java.util.Calendar.getInstance().time.before(dateObj)) {

                    val untilDate = context.getString(R.string.text_until_date)

                    val formatDate = dateObj?.toOrdinalFormat()

                    untilDate.replace("%", formatDate ?: "-")
                } else {
                    context.getString(R.string.text_expired)
                }
            } else {
                "-"
            }
        }
    }


    @BindingAdapter("app:startDate", "app:endDate")
    @JvmStatic
    fun startEndDate(textView: TextView, startDate: String?, endDate: String?) {
        with(textView) {
            val formatDate = context.getString(R.string.text_date_to_date)
            val stDate = startDate?.toDate()
            val enDate = endDate?.toDate()
            if (stDate != null && enDate != null) {
                text = if (stDate.getMm() != enDate.getMm()) {
                    formatDate.replace("%", stDate.getDdMmmm()).replace("*", enDate.toDdMMMMyyyy())
                } else {
                    if (stDate.getDd() != enDate.getDd()) {
                        formatDate
                                .replace("%", stDate.getDd())
                                .replace("*", enDate.toDdMMMMyyyy())
                    } else
                        enDate.toDdMMMMyyyy()
                }
            } else {
                text = "-"
            }
        }
    }

    @BindingAdapter("app:imageUrl")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: Any?) {

        val finalImage: Any = imageUrl ?: R.drawable.img_slider_event

        Glide.with(view.context)
                .load(finalImage)
                .into(view)
                .apply { RequestOptions().error(R.color.greyLineProfile).placeholder(R.color.greyLineProfile) }
    }

}