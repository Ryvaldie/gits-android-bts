package id.otsuka.gits.sweatgen.util.customView

import android.content.Context
import android.support.annotation.Nullable
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ViewParent
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView


/**
 * Dibuat oleh Azkiya Qolbi
 * @Copyright 2018
 */
class CustomMapView : MapView {

    private var mViewParent: ViewParent? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    fun setViewParent(@Nullable viewParent: ViewParent) { //any ViewGroup
        mViewParent = viewParent
    }

    constructor(context: Context, options: GoogleMapOptions) : super(context, options) {}

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {

                parent.parent.requestDisallowInterceptTouchEvent(true)
                Log.d(CustomMapView::class.java.simpleName, "Inside if of action down")
            }
            MotionEvent.ACTION_UP -> {

                parent.parent.requestDisallowInterceptTouchEvent(false)
                Log.d(CustomMapView::class.java.simpleName, "Inside if of action up")
            }
            else -> {
            }
        }

        return super.onInterceptTouchEvent(event)
    }
}