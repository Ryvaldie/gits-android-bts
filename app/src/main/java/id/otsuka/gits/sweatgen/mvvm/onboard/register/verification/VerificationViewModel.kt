package id.otsuka.gits.sweatgen.mvvm.onboard.register.verification;

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import id.otsuka.gits.sweatgen.data.source.jobs.GitsDataSource
import id.otsuka.gits.sweatgen.data.source.jobs.GitsRepository
import id.otsuka.gits.sweatgen.util.SingleLiveEvent


class VerificationViewModel(context: Application, val repository: GitsRepository) : AndroidViewModel(context) {
    val bTempToken = ObservableField("")
    val bUserId = ObservableField(0)
    val eventLoading = SingleLiveEvent<Boolean>()
    val bMessage = ObservableField("")
    val isValid = ObservableBoolean(false)

    fun resendEmail() {

        eventLoading.value = true

        repository.resendEmail(bTempToken.get()!!, object : GitsDataSource.ResendEmailCallback {
            override fun onResendSuccess(msg: String?) {
                bMessage.set(msg)
                eventLoading.value = false
            }

            override fun onResendFail(msg: String) {
                bMessage.set(msg)
                eventLoading.value = false
            }
        })
    }

    fun getTempUser() {
        bTempToken.set(repository.getUser()?.temporary_token)
        bUserId.set(repository.getUser()?.id)
    }

    fun userValidation() {

        eventLoading.value = true

        repository.userValidation(bUserId.get()!!, object : GitsDataSource.UserValidationCallback {
            override fun onValidationSuccess() {
                isValid.set(true)
                eventLoading.value = false
            }

            override fun onValidationFailed(msg: String) {
                bMessage.set(msg)
                eventLoading.value = false
            }
        })
    }

}