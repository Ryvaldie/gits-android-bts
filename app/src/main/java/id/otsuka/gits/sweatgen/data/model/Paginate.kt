package id.otsuka.gits.sweatgen.data.model

data class Paginate(val total: Int = 0,
                    val current_page: Int = 0,
                    val per_page: Int = 0

)