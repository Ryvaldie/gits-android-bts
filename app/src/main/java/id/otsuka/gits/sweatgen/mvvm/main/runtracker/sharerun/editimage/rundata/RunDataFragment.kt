package id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.rundata


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.otsuka.gits.sweatgen.databinding.FragmentRunDataBinding
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageFragment
import id.otsuka.gits.sweatgen.mvvm.main.runtracker.sharerun.editimage.ShareEditImageViewModel
import id.otsuka.gits.sweatgen.util.obtainViewModel

class RunDataFragment : Fragment(), RunDataUserActionListener {

    lateinit var mViewDataBinding: FragmentRunDataBinding
    lateinit var mViewModel: RunDataViewModel
    lateinit var mParentVm: ShareEditImageViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        mViewDataBinding = FragmentRunDataBinding.inflate(inflater, container!!, false)
        mViewModel = obtainViewModel()

        mViewDataBinding.mViewModel = mViewModel.apply {
        }

        mViewDataBinding.mListener = this@RunDataFragment

        return mViewDataBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getParentVm()
    }

    private fun getParentVm() {
        mParentVm = (parentFragment as ShareEditImageFragment).mViewModel
        mViewDataBinding.mParentVm = mParentVm
    }

    override fun onSelectedItem(styleCode: Int) {
        if (styleCode == mParentVm.bSelectedSticker.get())
            mParentVm.bSelectedSticker.set(-1)
        else
            mParentVm.bSelectedSticker.set(styleCode)
    }

    // TODO import obtainViewModel & add RunDataViewModel to ViewModelFactory
    fun obtainViewModel(): RunDataViewModel = obtainViewModel(RunDataViewModel::class.java)

    companion object {
        fun newInstance() = RunDataFragment().apply {

        }

    }

}
